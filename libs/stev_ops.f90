! __       __            __                                      ______             __    _
!|  \     /  \          |  \                                    /      \           |  \  |  \
!| $$\   /  $$  ______  | $$  _______  ______    _______       |  $$$$$$\ __    __  \$$ _| $$_     ______
!| $$$\ /  $$$ /      \ | $$ /       \|      \  /       \      | $$___\$$|  \  |  \|  \|   $$ \   /      \
!| $$$$\  $$$$|  $$$$$$\| $$|  $$$$$$$ \$$$$$$\|  $$$$$$$       \$$    \ | $$  | $$| $$ \$$$$$$  |  $$$$$$\
!| $$\$$ $$ $$| $$  | $$| $$| $$      /      $$ \$$    \        _\$$$$$$\| $$  | $$| $$  | $$ __ | $$    $$
!| $$ \$$$| $$| $$__/ $$| $$| $$_____|  $$$$$$$ _\$$$$$$\      |  \__| $$| $$__/ $$| $$  | $$|  \| $$$$$$$$
!| $$  \$ | $$ \$$    $$| $$ \$$     \\$$    $$|       $$       \$$    $$ \$$    $$| $$   \$$  $$ \$$     \
! \$$      \$$  \$$$$$$  \$$  \$$$$$$$ \$$$$$$$ \$$$$$$$         \$$$$$$   \$$$$$$  \$$    \$$$$   \$$$$$$$


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   stev_ops.f90 
!
!   This module is a part of molcas_suite
!
!
!   Calculates Stevens Operators at various precisions
!
!   
!      Authors:
!       Jon Kragskow
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

MODULE STEV_OPS 
    USE, INTRINSIC :: iso_fortran_env
    USE matrix_tools
    IMPLICIT NONE
    
    INTERFACE GET_STEVENS_OPERATORS
    !Creates Stevens operators and returns Stevens operators with
    ! with or without the angular momentum matrices
        MODULE PROCEDURE STEV_DP
        MODULE PROCEDURE STEV_QP
        MODULE PROCEDURE STEV_WITH_ANG_OPS_DP
        MODULE PROCEDURE STEV_WITH_ANG_OPS_QP
    END INTERFACE GET_STEVENS_OPERATORS

    INTERFACE STEVENS2
        !Creates 2nd rank Stevens operators
        MODULE PROCEDURE STEVENS2_DP
        MODULE PROCEDURE STEVENS2_QP
    END INTERFACE STEVENS2

    INTERFACE STEVENS4
        !Creates 4th rank Stevens operators
        MODULE PROCEDURE STEVENS4_DP
        MODULE PROCEDURE STEVENS4_QP
    END INTERFACE STEVENS4

    INTERFACE STEVENS6
        !Creates 6th rank Stevens operators
        MODULE PROCEDURE STEVENS6_DP
        MODULE PROCEDURE STEVENS6_QP
    END INTERFACE STEVENS6

    ! INTERFACE STEVENS8
    !     !Creates 6th rank Stevens operators
    !     MODULE PROCEDURE STEVENS8_DP
    !     MODULE PROCEDURE STEVENS8_QP
    ! END INTERFACE STEVENS8

    ! INTERFACE STEVENS10
    !     !Creates 6th rank Stevens operators
    !     MODULE PROCEDURE STEVENS10_DP
    !     MODULE PROCEDURE STEVENS10_QP
    ! END INTERFACE STEVENS10

    ! INTERFACE STEVENS12
    !     !Creates 6th rank Stevens operators
    !     MODULE PROCEDURE STEVENS12_DP
    !     MODULE PROCEDURE STEVENS12_QP
    ! END INTERFACE STEVENS12

    ! INTERFACE STEVENS14
    !     !Creates 6th rank Stevens operators
    !     MODULE PROCEDURE STEVENS14_DP
    !     MODULE PROCEDURE STEVENS14_QP
    ! END INTERFACE STEVENS14

    ! INTERFACE STEVENS16
    !     !Creates 6th rank Stevens operators
    !     MODULE PROCEDURE STEVENS16_DP
    !     MODULE PROCEDURE STEVENS16_QP
    ! END INTERFACE STEVENS16

CONTAINS

    SUBROUTINE STEV_QP(two_J, stevens_ops)
        !Calculate and return stevens operators at quadruple precision
        IMPLICIT NONE
        INTEGER, INTENT(IN)                            :: two_J
        INTEGER                                        :: i, k
        REAL(KIND = 16)                                :: mJ
        COMPLEX(KIND = 16), ALLOCATABLE, INTENT(OUT)   :: stevens_ops(:,:,:,:)
        COMPLEX(KIND = 16), ALLOCATABLE                :: JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        ALLOCATE(JP(two_J+1,two_J+1),JM(two_J+1,two_J+1),J2(two_J+1,two_J+1),JZ(two_J+1,two_J+1),stevens_ops(3, 13,two_J+1,two_J+1))
    
        JP               = (0.0_16,0.0_16)
        JM               = (0.0_16,0.0_16)
        J2               = (0.0_16,0.0_16)
        JZ               = (0.0_16,0.0_16)
        stevens_ops = (0.0_16,0.0_16)
    
        DO i = 1,two_J+1
            JZ(i,i) = -0.5_16*two_J + (i-1)*1.0_16
            J2(i,i) = 0.5_16*two_J*(0.5_16*two_J+1)
            DO k = 1,two_J+1
                mJ = -0.5_16*two_J + (k-1)*1.0_16
                IF (i == k+1) JP(i,k) = SQRT(0.5_16*two_J*(0.5_16*two_J+1)-mJ*(mJ+1.0_16))
                IF (i == k-1) JM(i,k) = SQRT(0.5_16*two_J*(0.5_16*two_J+1)-mJ*(mJ-1.0_16))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,stevens_ops(1, 1,:,:),stevens_ops(1, 2,:,:),stevens_ops(1, 3,:,:),stevens_ops(1, 4,:,:),stevens_ops(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,stevens_ops(2, 1,:,:),stevens_ops(2, 2,:,:),stevens_ops(2, 3,:,:),stevens_ops(2, 4,:,:),stevens_ops(2, 5,:,:),stevens_ops(2, 6,:,:),stevens_ops(2, 7,:,:),stevens_ops(2, 8,:,:),stevens_ops(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,stevens_ops(3, 1,:,:),stevens_ops(3, 2,:,:),stevens_ops(3, 3,:,:),stevens_ops(3, 4,:,:),stevens_ops(3, 5,:,:),stevens_ops(3, 6,:,:),stevens_ops(3, 7,:,:),stevens_ops(3, 8,:,:),stevens_ops(3, 9,:,:),stevens_ops(3, 10,:,:),stevens_ops(3, 11, :,:),stevens_ops(3, 12,:,:),stevens_ops(3, 13,:,:))
        
    END SUBROUTINE STEV_QP

    SUBROUTINE STEV_DP(two_J, stevens_ops)
        !Calculate and return stevens operators at double precision
        IMPLICIT NONE
        INTEGER, INTENT(IN)                           :: two_J
        INTEGER                                       :: i, k
        REAL(KIND = 8)                                :: mJ
        COMPLEX(KIND = 8), ALLOCATABLE, INTENT(OUT)   :: stevens_ops(:,:,:,:)
        COMPLEX(KIND = 8), ALLOCATABLE                :: JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        !Allocate arrays
        ALLOCATE(JP(two_J+1,two_J+1),JM(two_J+1,two_J+1),J2(two_J+1,two_J+1),JZ(two_J+1,two_J+1),stevens_ops(3, 13,two_J+1,two_J+1))
    
        JP               = (0.0_8,0.0_8)
        JM               = (0.0_8,0.0_8)
        J2               = (0.0_8,0.0_8)
        JZ               = (0.0_8,0.0_8)
        stevens_ops = (0.0_8,0.0_8)
    
        DO i = 1,two_J+1
            JZ(i,i) = -0.5_8*two_J + (i-1)*1.0_8
            J2(i,i) = 0.5_8*two_J*(0.5_8*two_J+1)
            DO k = 1,two_J+1
                mJ = -0.5_8*two_J + (k-1)*1.0_8
                IF (i == k+1) JP(i,k) = SQRT(0.5_8*two_J*(0.5_8*two_J+1)-mJ*(mJ+1.0_8))
                IF (i == k-1) JM(i,k) = SQRT(0.5_8*two_J*(0.5_8*two_J+1)-mJ*(mJ-1.0_8))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,stevens_ops(1, 1,:,:),stevens_ops(1, 2,:,:),stevens_ops(1, 3,:,:),stevens_ops(1, 4,:,:),stevens_ops(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,stevens_ops(2, 1,:,:),stevens_ops(2, 2,:,:),stevens_ops(2, 3,:,:),stevens_ops(2, 4,:,:),stevens_ops(2, 5,:,:),stevens_ops(2, 6,:,:),stevens_ops(2, 7,:,:),stevens_ops(2, 8,:,:),stevens_ops(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,stevens_ops(3, 1,:,:),stevens_ops(3, 2,:,:),stevens_ops(3, 3,:,:),stevens_ops(3, 4,:,:),stevens_ops(3, 5,:,:),stevens_ops(3, 6,:,:),stevens_ops(3, 7,:,:),stevens_ops(3, 8,:,:),stevens_ops(3, 9,:,:),stevens_ops(3, 10,:,:),stevens_ops(3, 11, :,:),stevens_ops(3, 12,:,:),stevens_ops(3, 13,:,:))
        
    END SUBROUTINE STEV_DP

    SUBROUTINE STEV_WITH_ANG_OPS_QP(two_J, stevens_ops, JP, JM, J2, JZ)
        !Calculate and return stevens operators at quadruple precision
        !Also returns angular momentum operators
        IMPLICIT NONE
        INTEGER, INTENT(IN)                            :: two_J
        INTEGER                                        :: i, k
        REAL(KIND = 16)                                :: mJ
        COMPLEX(KIND = 16), ALLOCATABLE, INTENT(OUT)   :: stevens_ops(:,:,:,:), JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        ALLOCATE(JP(two_J+1,two_J+1),JM(two_J+1,two_J+1),J2(two_J+1,two_J+1),JZ(two_J+1,two_J+1),stevens_ops(3, 13,two_J+1,two_J+1))
    
        JP               = (0.0_16,0.0_16)
        JM               = (0.0_16,0.0_16)
        J2               = (0.0_16,0.0_16)
        JZ               = (0.0_16,0.0_16)
        stevens_ops = (0.0_16,0.0_16)
    
        DO i = 1,two_J+1
            JZ(i,i) = -0.5_16*two_J + (i-1)*1.0_16
            J2(i,i) = 0.5_16*two_J*(0.5_16*two_J+1)
            DO k = 1,two_J+1
                mJ = -0.5_16*two_J + (k-1)*1.0_16
                IF (i == k+1) JP(i,k) = SQRT(0.5_16*two_J*(0.5_16*two_J+1)-mJ*(mJ+1.0_16))
                IF (i == k-1) JM(i,k) = SQRT(0.5_16*two_J*(0.5_16*two_J+1)-mJ*(mJ-1.0_16))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,stevens_ops(1, 1,:,:),stevens_ops(1, 2,:,:),stevens_ops(1, 3,:,:),stevens_ops(1, 4,:,:),stevens_ops(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,stevens_ops(2, 1,:,:),stevens_ops(2, 2,:,:),stevens_ops(2, 3,:,:),stevens_ops(2, 4,:,:),stevens_ops(2, 5,:,:),stevens_ops(2, 6,:,:),stevens_ops(2, 7,:,:),stevens_ops(2, 8,:,:),stevens_ops(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,stevens_ops(3, 1,:,:),stevens_ops(3, 2,:,:),stevens_ops(3, 3,:,:),stevens_ops(3, 4,:,:),stevens_ops(3, 5,:,:),stevens_ops(3, 6,:,:),stevens_ops(3, 7,:,:),stevens_ops(3, 8,:,:),stevens_ops(3, 9,:,:),stevens_ops(3, 10,:,:),stevens_ops(3, 11, :,:),stevens_ops(3, 12,:,:),stevens_ops(3, 13,:,:))
        
    END SUBROUTINE STEV_WITH_ANG_OPS_QP

    SUBROUTINE STEV_WITH_ANG_OPS_DP(two_J, stevens_ops, JP, JM, J2, JZ)
        !Calculate and return stevens operators at double precision
        !Also returns angular momentum operators
        IMPLICIT NONE
        INTEGER, INTENT(IN)                            :: two_J
        INTEGER                                        :: i, k
        REAL(KIND = 8)                                 :: mJ
        COMPLEX(KIND = 8), ALLOCATABLE, INTENT(OUT)    :: stevens_ops(:,:,:,:), JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        !Allocate arrays
        ALLOCATE(JP(two_J+1,two_J+1),JM(two_J+1,two_J+1),J2(two_J+1,two_J+1),JZ(two_J+1,two_J+1),stevens_ops(3, 13,two_J+1,two_J+1))
    
        JP               = (0.0_8,0.0_8)
        JM               = (0.0_8,0.0_8)
        J2               = (0.0_8,0.0_8)
        JZ               = (0.0_8,0.0_8)
        stevens_ops = (0.0_8,0.0_8)
    
        DO i = 1,two_J+1
            JZ(i,i) = -0.5_8*two_J + (i-1)*1.0_8
            J2(i,i) = 0.5_8*two_J*(0.5_8*two_J+1)
            DO k = 1,two_J+1
                mJ = -0.5_8*two_J + (k-1)*1.0_8
                IF (i == k+1) JP(i,k) = SQRT(0.5_8*two_J*(0.5_8*two_J+1)-mJ*(mJ+1.0_8))
                IF (i == k-1) JM(i,k) = SQRT(0.5_8*two_J*(0.5_8*two_J+1)-mJ*(mJ-1.0_8))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,stevens_ops(1, 1,:,:),stevens_ops(1, 2,:,:),stevens_ops(1, 3,:,:),stevens_ops(1, 4,:,:),stevens_ops(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,stevens_ops(2, 1,:,:),stevens_ops(2, 2,:,:),stevens_ops(2, 3,:,:),stevens_ops(2, 4,:,:),stevens_ops(2, 5,:,:),stevens_ops(2, 6,:,:),stevens_ops(2, 7,:,:),stevens_ops(2, 8,:,:),stevens_ops(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,stevens_ops(3, 1,:,:),stevens_ops(3, 2,:,:),stevens_ops(3, 3,:,:),stevens_ops(3, 4,:,:),stevens_ops(3, 5,:,:),stevens_ops(3, 6,:,:),stevens_ops(3, 7,:,:),stevens_ops(3, 8,:,:),stevens_ops(3, 9,:,:),stevens_ops(3, 10,:,:),stevens_ops(3, 11, :,:),stevens_ops(3, 12,:,:),stevens_ops(3, 13,:,:))
        
    END SUBROUTINE STEV_WITH_ANG_OPS_DP

    SUBROUTINE STEVENS2_DP(JP,JM,J2,JZ,O2M2,O2M1,O20,O21,O22)
        !Creates 2nd rank Stevens operators at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)   :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 8), INTENT(OUT)  :: O2M2(:,:),O2M1(:,:),O20(:,:),O21(:,:),O22(:,:)
        COMPLEX(KIND = 8)               :: ii
        
        ii = (0.0_8, 1.0_8)

        O2m2 = -0.5_8  * ii * (MAT_POWER(JP,2) - MAT_POWER(JM,2))
        O2m1 = -0.25_8 * ii * (ALT_MATMUL(JZ,(JP-JM)) + ALT_MATMUL((JP-JM),JZ))
        O20  =  3.0_8  *       MAT_POWER(JZ, 2) - J2
        O21  =  0.25_8 *      (ALT_MATMUL(JZ,(JP+JM)) + ALT_MATMUL((JP+JM),JZ))
        O22  =  0.5_8  *      (MAT_POWER(JP,2) + MAT_POWER(JM,2))

    END SUBROUTINE STEVENS2_DP
    

    SUBROUTINE STEVENS2_QP(JP,JM,J2,JZ,O2M2,O2M1,O20,O21,O22)
        !Creates 2nd rank Stevens operators at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)   :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 16), INTENT(OUT)  :: O2M2(:,:),O2M1(:,:),O20(:,:),O21(:,:),O22(:,:)
        COMPLEX(KIND = 16)               :: ii
        
        ii = (0.0_16, 1.0_16)

        O2m2 = -0.5_16  * ii * (MAT_POWER(JP,2) - MAT_POWER(JM,2))
        O2m1 = -0.25_16 * ii * (ALT_MATMUL(JZ,(JP-JM)) + ALT_MATMUL((JP-JM),JZ))
        O20  =  3.0_16  *       MAT_POWER(JZ, 2) - J2
        O21  =  0.25_16 *      (ALT_MATMUL(JZ,(JP+JM)) + ALT_MATMUL((JP+JM),JZ))
        O22  =  0.5_16  *      (MAT_POWER(JP,2) + MAT_POWER(JM,2))

    END SUBROUTINE STEVENS2_QP
        
    SUBROUTINE STEVENS4_DP(JP,JM,J2,JZ,O4M4,O4M3,O4M2,O4M1,O40,O41,O42,O43,O44)
        !Creates 4th rank Stevens operators at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)    :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 8), INTENT(INOUT) :: O4M4(:,:),O4M3(:,:),O4M2(:,:),O4M1(:,:),O40(:,:),O41(:,:),O42(:,:),O43(:,:),O44(:,:)
        COMPLEX(KIND = 8)                :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                          :: J
        COMPLEX(KIND = 8)                :: ii
        
        ii = (0.0_8, 1.0_8)

        IDENT = (0.0_8,0.0_8)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_8,0.0_8)
        END DO
    
        O4m4 = -0.5_8  * ii * (MAT_POWER(JP,4) - MAT_POWER(JM,4))
        O4m3 = -0.25_8 * ii * (ALT_MATMUL(JZ, (MAT_POWER(JP,3) - MAT_POWER(JM,3))) + ALT_MATMUL((MAT_POWER(JP,3) - MAT_POWER(JM,3)), JZ))
        O4m2 = -0.25_8 * ii * (ALT_MATMUL(7.0_8 * MAT_POWER(JZ,2) - J2 - 5.0_8 * IDENT, MAT_POWER(JP, 2) - MAT_POWER(JM, 2)) + ALT_MATMUL(MAT_POWER(JP, 2) - MAT_POWER(JM, 2), 7.0_8 * MAT_POWER(JZ,2) - J2 - 5.0_8 * IDENT))
        O4m1 = -0.25_8 * ii * (ALT_MATMUL(7.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2, JZ), JP - JM) + ALT_MATMUL(JP - JM, 7.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2, JZ)))
        O40  = 35.0_8  * MAT_POWER(JZ,4) - 30.0_8 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) + 25.0_8*MAT_POWER(JZ,2) + 3.0_8 * MAT_POWER(J2,2) - 6.0_8 * J2
        O41  =  0.25_8 *      (ALT_MATMUL(7.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2, JZ), JP + JM) + ALT_MATMUL(JP + JM, 7.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2, JZ)))
        O42  =  0.25_8 *      (ALT_MATMUL(7.0_8 * MAT_POWER(JZ,2) - J2 - 5.0_8 * IDENT, MAT_POWER(JP, 2) + MAT_POWER(JM, 2)) + ALT_MATMUL(MAT_POWER(JP, 2) + MAT_POWER(JM, 2), 7.0_8 * MAT_POWER(JZ,2) - J2 - 5.0_8 * IDENT))
        O43  =  0.25_8 *      (ALT_MATMUL(JZ, (MAT_POWER(JP,3) + MAT_POWER(JM,3))) + ALT_MATMUL((MAT_POWER(JP,3) + MAT_POWER(JM,3)), JZ))
        O44  =  0.5_8  *      (MAT_POWER(JP,4) + MAT_POWER(JM,4))

    END SUBROUTINE STEVENS4_DP

    SUBROUTINE STEVENS4_QP(JP,JM,J2,JZ,O4M4,O4M3,O4M2,O4M1,O40,O41,O42,O43,O44)
        !Creates 4th rank Stevens operators at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)    :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 16), INTENT(INOUT) :: O4M4(:,:),O4M3(:,:),O4M2(:,:),O4M1(:,:),O40(:,:),O41(:,:),O42(:,:),O43(:,:),O44(:,:)
        COMPLEX(KIND = 16)                :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                           :: J
        COMPLEX(KIND = 16)                :: ii
        
        ii = (0.0_16, 1.0_16)

        IDENT = (0.0_16,0.0_16)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_16,0.0_16)
        END DO
    
        O4m4 = -0.5_16  * ii * (MAT_POWER(JP,4) - MAT_POWER(JM,4))
        O4m3 = -0.25_16 * ii * (ALT_MATMUL(JZ, (MAT_POWER(JP,3) - MAT_POWER(JM,3))) + ALT_MATMUL((MAT_POWER(JP,3) - MAT_POWER(JM,3)), JZ))
        O4m2 = -0.25_16 * ii * (ALT_MATMUL(7.0_16 * MAT_POWER(JZ,2) - J2 - 5.0_16 * IDENT, MAT_POWER(JP, 2) - MAT_POWER(JM, 2)) + ALT_MATMUL(MAT_POWER(JP, 2) - MAT_POWER(JM, 2), 7.0_16 * MAT_POWER(JZ,2) - J2 - 5.0_16 * IDENT))
        O4m1 = -0.25_16 * ii * (ALT_MATMUL(7.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2, JZ), JP - JM) + ALT_MATMUL(JP - JM, 7.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2, JZ)))
        O40  = 35.0_16  * MAT_POWER(JZ,4) - 30.0_16 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) + 25.0_16*MAT_POWER(JZ,2) + 3.0_16 * MAT_POWER(J2,2) - 6.0_16 * J2
        O41  =  0.25_16 *      (ALT_MATMUL(7.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2, JZ), JP + JM) + ALT_MATMUL(JP + JM, 7.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2, JZ)))
        O42  =  0.25_16 *      (ALT_MATMUL(7.0_16 * MAT_POWER(JZ,2) - J2 - 5.0_16 * IDENT, MAT_POWER(JP, 2) + MAT_POWER(JM, 2)) + ALT_MATMUL(MAT_POWER(JP, 2) + MAT_POWER(JM, 2), 7.0_16 * MAT_POWER(JZ,2) - J2 - 5.0_16 * IDENT))
        O43  =  0.25_16 *      (ALT_MATMUL(JZ, (MAT_POWER(JP,3) + MAT_POWER(JM,3))) + ALT_MATMUL((MAT_POWER(JP,3) + MAT_POWER(JM,3)), JZ))
        O44  =  0.5_16  *      (MAT_POWER(JP,4) + MAT_POWER(JM,4))
    
    END SUBROUTINE STEVENS4_QP

    SUBROUTINE STEVENS6_DP(JP,JM,J2,JZ,O6M6,O6M5,O6M4,O6M3,O6M2,O6M1,O60,O61,O62,O63,O64,O65,O66)
        !Creates 6th rank Stevens operators at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)  :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 8), INTENT(OUT) :: O6M6(:,:),O6M5(:,:),O6M4(:,:),O6M3(:,:),O6M2(:,:),O6M1(:,:),O60(:,:),O61(:,:),O62(:,:),O63(:,:),O64(:,:),O65(:,:),O66(:,:)
        COMPLEX(KIND = 8)              :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                        :: J
        COMPLEX(KIND = 8)              :: ii
        
        ii = (0.0_8, 1.0_8)
    
        IDENT = (0.0_8,0.0_8)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_8,0.0_8)
        END DO
    
        O6M6 =  -0.5_8  * ii * (MAT_POWER(JP,6) - MAT_POWER(JM,6))
        O6M5 =  -0.25_8 * ii * (ALT_MATMUL((MAT_POWER(JP,5) - MAT_POWER(JM,5)),Jz) + ALT_MATMUL(JZ, (MAT_POWER(JP,5) - MAT_POWER(JM,5))))
        O6M4 =  -0.25_8 * ii * (ALT_MATMUL(MAT_POWER(JP,4) - MAT_POWER(JM,4),(11.0_8 * MAT_POWER(JZ,2) - J2 - 38.0_8 * IDENT)) + ALT_MATMUL((11.0_8 * MAT_POWER(JZ,2) - J2 - 38.0_8 * IDENT), MAT_POWER(JP,4) - MAT_POWER(JM,4)))
        O6M3 =  -0.25_8 * ii * (ALT_MATMUL(MAT_POWER(JP,3) - MAT_POWER(JM,3),(11.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2,JZ) - 59.0_8*JZ)) + ALT_MATMUL((11.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2,JZ) - 59.0_8 * JZ), MAT_POWER(JP,3) - MAT_POWER(JM,3)))
        O6M2 =  -0.25_8 * ii * (ALT_MATMUL(MAT_POWER(JP,2) - MAT_POWER(JM,2),(33.0_8 * MAT_POWER(JZ,4) - 18.0_8 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_8 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_8 * J2 + 102.0_8 * IDENT)) + ALT_MATMUL((33.0_8 * MAT_POWER(JZ,4) - 18.0_8 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_8 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_8 * J2 + 102.0_8 * IDENT), MAT_POWER(JP,2) - MAT_POWER(JM,2)))
        O6M1 =  -0.25_8 * ii * (ALT_MATMUL(JP - JM, (33.0_8 * MAT_POWER(JZ,5) - 30.0_8 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_8 * MAT_POWER(JZ, 3) + 5.0_8 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_8 * ALT_MATMUL(J2, JZ) + 12.0_8 * JZ)) + ALT_MATMUL((33.0_8 * MAT_POWER(JZ,5) - 30.0_8 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_8 * MAT_POWER(JZ, 3) + 5.0_8 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_8 * ALT_MATMUL(J2, JZ) + 12.0_8 * JZ), JP - JM))
        O60  =   231.0_8 * MAT_POWER(JZ, 6) - 315.0_8 * ALT_MATMUL(J2, MAT_POWER(Jz, 4)) + 735.0_8 * MAT_POWER(JZ, 4) + 105.0_8 * ALT_MATMUL(MAT_POWER(J2,2), MAT_POWER(JZ, 2)) - 525.0_8 * ALT_MATMUL(J2, MAT_POWER(JZ, 2)) + 294.0_8 * MAT_POWER(JZ, 2) - 5.0_8 * MAT_POWER(J2, 3) + 40.0_8 * MAT_POWER(J2, 2) - 60.0_8 * J2
        O61  =   0.25_8 *      (ALT_MATMUL(JP + JM, (33.0_8 * MAT_POWER(JZ,5) - 30.0_8 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_8 * MAT_POWER(JZ, 3) + 5.0_8 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_8 * ALT_MATMUL(J2, JZ) + 12.0_8 * JZ)) + ALT_MATMUL((33.0_8 * MAT_POWER(JZ,5) - 30.0_8 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_8 * MAT_POWER(JZ, 3) + 5.0_8 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_8 * ALT_MATMUL(J2, JZ) + 12.0_8 * JZ), JP + JM))
        O62  =   0.25_8 *      (ALT_MATMUL(MAT_POWER(JP,2) + MAT_POWER(JM,2),(33.0_8 * MAT_POWER(JZ,4) - 18.0_8 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_8 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_8 * J2 + 102.0_8 * IDENT)) + ALT_MATMUL((33.0_8 * MAT_POWER(JZ,4) - 18.0_8 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_8 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_8 * J2 + 102.0_8 * IDENT), MAT_POWER(JP,2) + MAT_POWER(JM,2)))
        O63  =   0.25_8 *      (ALT_MATMUL(MAT_POWER(JP,3) + MAT_POWER(JM,3),(11.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2,JZ) - 59.0_8*JZ)) + ALT_MATMUL((11.0_8 * MAT_POWER(JZ,3) - 3.0_8 * ALT_MATMUL(J2,JZ) - 59.0_8 * JZ), MAT_POWER(JP,3) + MAT_POWER(JM,3)))
        O64  =   0.25_8 *      (ALT_MATMUL(MAT_POWER(JP,4) + MAT_POWER(JM,4),(11.0_8 * MAT_POWER(JZ,2) - J2 - 38.0_8 * IDENT)) + ALT_MATMUL((11.0_8 * MAT_POWER(JZ,2) - J2 - 38.0_8 * IDENT), MAT_POWER(JP,4) + MAT_POWER(JM,4)))
        O65  =   0.25_8 *      (ALT_MATMUL((MAT_POWER(JP,5) + MAT_POWER(JM,5)),Jz) + ALT_MATMUL(Jz, (MAT_POWER(JP,5) + MAT_POWER(JM,5))))
        O66  =   0.5_8  *      (MAT_POWER(JP,6) + MAT_POWER(JM,6))

    END SUBROUTINE STEVENS6_DP

    SUBROUTINE STEVENS6_QP(JP,JM,J2,JZ,O6M6,O6M5,O6M4,O6M3,O6M2,O6M1,O60,O61,O62,O63,O64,O65,O66)
        !Creates 6th rank Stevens operators at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)  :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 16), INTENT(OUT) :: O6M6(:,:),O6M5(:,:),O6M4(:,:),O6M3(:,:),O6M2(:,:),O6M1(:,:),O60(:,:),O61(:,:),O62(:,:),O63(:,:),O64(:,:),O65(:,:),O66(:,:)
        COMPLEX(KIND = 16)              :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                       :: J
        COMPLEX(KIND = 16)               :: ii
        
        ii = (0.0_16, 1.0_16)
    
        IDENT = (0.0_16,0.0_16)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_16,0.0_16)
        END DO
    
    
        O6M6 =  -0.5_16  * ii * (MAT_POWER(JP,6) - MAT_POWER(JM,6))
        O6M5 =  -0.25_16 * ii * (ALT_MATMUL((MAT_POWER(JP,5) - MAT_POWER(JM,5)),Jz) + ALT_MATMUL(JZ, (MAT_POWER(JP,5) - MAT_POWER(JM,5))))
        O6M4 =  -0.25_16 * ii * (ALT_MATMUL(MAT_POWER(JP,4) - MAT_POWER(JM,4),(11.0_16 * MAT_POWER(JZ,2) - J2 - 38.0_16 * IDENT)) + ALT_MATMUL((11.0_16 * MAT_POWER(JZ,2) - J2 - 38.0_16 * IDENT), MAT_POWER(JP,4) - MAT_POWER(JM,4)))
        O6M3 =  -0.25_16 * ii * (ALT_MATMUL(MAT_POWER(JP,3) - MAT_POWER(JM,3),(11.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2,JZ) - 59.0_16*JZ)) + ALT_MATMUL((11.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2,JZ) - 59.0_16 * JZ), MAT_POWER(JP,3) - MAT_POWER(JM,3)))
        O6M2 =  -0.25_16 * ii * (ALT_MATMUL(MAT_POWER(JP,2) - MAT_POWER(JM,2),(33.0_16 * MAT_POWER(JZ,4) - 18.0_16 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_16 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_16 * J2 + 102.0_16 * IDENT)) + ALT_MATMUL((33.0_16 * MAT_POWER(JZ,4) - 18.0_16 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_16 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_16 * J2 + 102.0_16 * IDENT), MAT_POWER(JP,2) - MAT_POWER(JM,2)))
        O6M1 =  -0.25_16 * ii * (ALT_MATMUL(JP - JM, (33.0_16 * MAT_POWER(JZ,5) - 30.0_16 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_16 * MAT_POWER(JZ, 3) + 5.0_16 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_16 * ALT_MATMUL(J2, JZ) + 12.0_16 * JZ)) + ALT_MATMUL((33.0_16 * MAT_POWER(JZ,5) - 30.0_16 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_16 * MAT_POWER(JZ, 3) + 5.0_16 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_16 * ALT_MATMUL(J2, JZ) + 12.0_16 * JZ), JP - JM))
        O60  =   231.0_16 * MAT_POWER(JZ, 6) - 315.0_16 * ALT_MATMUL(J2, MAT_POWER(Jz, 4)) + 735.0_16 * MAT_POWER(JZ, 4) + 105.0_16 * ALT_MATMUL(MAT_POWER(J2,2), MAT_POWER(JZ, 2)) - 525.0_16 * ALT_MATMUL(J2, MAT_POWER(JZ, 2)) + 294.0_16 * MAT_POWER(JZ, 2) - 5.0_16 * MAT_POWER(J2, 3) + 40.0_16 * MAT_POWER(J2, 2) - 60.0_16 * J2
        O61  =   0.25_16 *      (ALT_MATMUL(JP + JM, (33.0_16 * MAT_POWER(JZ,5) - 30.0_16 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_16 * MAT_POWER(JZ, 3) + 5.0_16 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_16 * ALT_MATMUL(J2, JZ) + 12.0_16 * JZ)) + ALT_MATMUL((33.0_16 * MAT_POWER(JZ,5) - 30.0_16 * ALT_MATMUL(J2, MAT_POWER(JZ, 3)) + 15.0_16 * MAT_POWER(JZ, 3) + 5.0_16 * ALT_MATMUL(MAT_POWER(J2, 2), JZ) - 10.0_16 * ALT_MATMUL(J2, JZ) + 12.0_16 * JZ), JP + JM))
        O62  =   0.25_16 *      (ALT_MATMUL(MAT_POWER(JP,2) + MAT_POWER(JM,2),(33.0_16 * MAT_POWER(JZ,4) - 18.0_16 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_16 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_16 * J2 + 102.0_16 * IDENT)) + ALT_MATMUL((33.0_16 * MAT_POWER(JZ,4) - 18.0_16 * ALT_MATMUL(J2,MAT_POWER(JZ,2)) - 123.0_16 * MAT_POWER(JZ, 2) + MAT_POWER(J2, 2) + 10.0_16 * J2 + 102.0_16 * IDENT), MAT_POWER(JP,2) + MAT_POWER(JM,2)))
        O63  =   0.25_16 *      (ALT_MATMUL(MAT_POWER(JP,3) + MAT_POWER(JM,3),(11.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2,JZ) - 59.0_16*JZ)) + ALT_MATMUL((11.0_16 * MAT_POWER(JZ,3) - 3.0_16 * ALT_MATMUL(J2,JZ) - 59.0_16 * JZ), MAT_POWER(JP,3) + MAT_POWER(JM,3)))
        O64  =   0.25_16 *      (ALT_MATMUL(MAT_POWER(JP,4) + MAT_POWER(JM,4),(11.0_16 * MAT_POWER(JZ,2) - J2 - 38.0_16 * IDENT)) + ALT_MATMUL((11.0_16 * MAT_POWER(JZ,2) - J2 - 38.0_16 * IDENT), MAT_POWER(JP,4) + MAT_POWER(JM,4)))
        O65  =   0.25_16 *      (ALT_MATMUL((MAT_POWER(JP,5) + MAT_POWER(JM,5)),Jz) + ALT_MATMUL(Jz, (MAT_POWER(JP,5) + MAT_POWER(JM,5))))
        O66  =   0.5_16  *      (MAT_POWER(JP,6) + MAT_POWER(JM,6))
    
    END SUBROUTINE STEVENS6_QP

    ! SUBROUTINE STEVENS8_DP(JP,JM,J2,JZ,O6M6,O6M5,O6M4,O6M3,O6M2,O6M1,O60,O61,O62,O63,O64,O65,O66)
    !     !Creates 8th rank Stevens operators at double precision
    !     IMPLICIT NONE
    !     COMPLEX(KIND = 8), INTENT(IN)  :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
    !     COMPLEX(KIND = 8), INTENT(OUT) :: O6M6(:,:),O6M5(:,:),O6M4(:,:),O6M3(:,:),O6M2(:,:),O6M1(:,:),&
    !                                         O60(:,:),O61(:,:),O62(:,:),O63(:,:),O64(:,:),O65(:,:),O66(:,:)
    !     COMPLEX(KIND = 8)              :: IDENT(SIZE(JP,1),SIZE(JP,1))
    !     INTEGER                        :: J
    !     COMPLEX(KIND = 8)              :: ii
        
    !     ii = (0.0_8, 1.0_8)
    
    !     IDENT = (0.0_8,0.0_8)
    
    !     DO J=1,SIZE(JP,1)
    !         IDENT(J,J) = (1.0_8,0.0_8)
    !     END DO
    
    !     O8M8 = 0.5_8 * ii * (MAT_POWER(JP,8) - MAT_POWER(JM,8))
    !     O8M7 = 
    !     O8M6 =
    !     O8M5 =
    !     O8M4 =
    !     O8M3 =
    !     O8M2 =
    !     O8M1 =
    !     O80  =
    !     O81  =
    !     O82  =
    !     O83  =
    !     O84  =
    !     O85  =
    !     O86  =
    !     O87  = 0.25_8*(MAT_POWER(JM,7)*(-7.0_8*IDENT+2.0_8 *JZ) + MAT_POWER(JP,7)*(7.0_8*IDENT+2.0_8*Jz))
    !     O88  = 0.5_8 * (MAT_POWER(JP,8) + MAT_POWER(JM,8))
    
    ! END SUBROUTINE STEVENS8_DP


END MODULE STEV_OPS
