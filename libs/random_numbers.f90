!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   random_numbers.f90 
!
!   This module is a part of molcas_suite
!
!
!	Makes random numbers
!
!   
!      Authors:
!       Nicholas Chilton
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module random_numbers
use, intrinsic::iso_fortran_env
use prec
implicit none
integer::random_seed_vals(4)

contains

function random_kiss()
integer:: RANDO
real(kind=WP)::random_kiss
random_seed_vals(1) = 69069 * random_seed_vals(1) + 1327217885
random_seed_vals(2) = ieor(ieor(ieor(random_seed_vals(2),&
ishft(random_seed_vals(2),13)),ishft(ieor(random_seed_vals(2),&
ishft(random_seed_vals(2),13)),-17)),ishft(ieor(ieor(random_seed_vals(2),&
ishft(random_seed_vals(2),13)),ishft(ieor(random_seed_vals(2),&
ishft(random_seed_vals(2),13)),-17)),5))
random_seed_vals(3) = 18000 * iand (random_seed_vals(3), 65535) &
+ ishft (random_seed_vals(3), - 16)
random_seed_vals(4) = 30903 * iand (random_seed_vals(4), 65535) &
+ ishft (random_seed_vals(4), - 16)
RANDO = random_seed_vals(1) + random_seed_vals(2) + ishft (random_seed_vals(3), 16) &
+ random_seed_vals(4)
random_kiss = ((RANDO*4.654701870848E-10_WP)+1.0_WP)*0.5_WP
end function random_kiss

subroutine random_seed
integer::values(8),i
real(kind=WP)::burn
call date_and_time(VALUES=values)
random_seed_vals(1) = (values(7)*values(8) + values(8))*123
random_seed_vals(2) = (values(6)/(values(8)+1) + values(8))*362
random_seed_vals(3) = (values(6)+values(7)+values(8) + values(8))*521
random_seed_vals(4) = ((values(5)-values(6))*values(7)/(values(8)+1) + values(8))*916
do i = 1,4444
	burn = random_kiss()
end do
end subroutine random_seed

function random_gauss()
!http://www.design.caltech.edu/erik/Misc/Gaussian.html
!https://www.taygeta.com/random/gaussian.html
real(kind=WP)::x1,x2,w,random_gauss
w = 2.0_WP
do while(w >= 1.0_WP)
	x1 = 2.0_WP*random_kiss() - 1.0_WP
	x2 = 2.0_WP*random_kiss() - 1.0_WP
	w = x1*x1 + x2*x2
end do
w = sqrt(-2.0_WP*log(w)/w)
random_gauss = x1*w
!other = x2*w
end function random_gauss

end module random_numbers