!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   prec.f90 
!
!   This module is a part of molcas_suite
!
!
!	Defines precision of real and complex variables
!	Defines the constant pi
!
!   
!      Authors:
!       Nicholas Chilton
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


module prec
use, intrinsic::iso_fortran_env
implicit none
integer, parameter::WP = REAL64
real(kind=WP),parameter::pi = 3.141592653589793238462643383279502884197_WP

contains

subroutine lowercase(str)
implicit none
integer::i,gap
character*(*)::str
gap=ICHAR('a')-ICHAR('A')
if(len(str) > 0) then
	do i=1,len(str)
		if(str(i:i) <= 'Z') then
			if(str(i:i) >= 'A') str(i:i)=CHAR(ICHAR(str(i:i))+gap)
		end if
	end do
end if
end subroutine lowercase

subroutine removenumbers(str)
implicit none
integer::i
character*(*)::str
if(len(str) > 0) then
	do i=1,len(str)
		if(str(i:i) <= '9') then
			if(str(i:i) >= '0') str(i:i)=' '
		end if
	end do
end if
end subroutine removenumbers

end module prec
