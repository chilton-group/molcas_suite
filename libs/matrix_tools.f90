!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                                                  !
!                                                                                                  !
!    matrix_tools.f90                                                                              !
!                                                                                                  !
!    This module is a part of molcas_suite                                                         !
!                                                                                                  !
!                                                                                                  !
!    Subroutines for matrix operations on arrays of different types at various                     !
!    precisions                                                                                    !
!                                                                                                  !
!                                                                                                  !
!       Authors:                                                                                   !
!        Jon Kragskow                                                                              !
!                                                                                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE matrix_tools
    USE, INTRINSIC :: iso_fortran_env
    IMPLICIT NONE

    INTERFACE EXPECTATION
        ! Performs a forwards V^-1 * H * V or backwards V * H * V^-1 change of basis
        MODULE PROCEDURE EXPECTATION_R_DP
        MODULE PROCEDURE EXPECTATION_R_QP
        MODULE PROCEDURE EXPECTATION_C_DP
        MODULE PROCEDURE EXPECTATION_C_QP
    END INTERFACE EXPECTATION
    
    INTERFACE DIAGONALISE
        ! Diagonalisation
        MODULE PROCEDURE DIAGONALISE_R_DP_both
        MODULE PROCEDURE DIAGONALISE_R_DP_vals
        MODULE PROCEDURE DIAGONALISE_R_DP_vecs
        MODULE PROCEDURE DIAGONALISE_C_DP_both
        MODULE PROCEDURE DIAGONALISE_C_DP_vals
        MODULE PROCEDURE DIAGONALISE_C_DP_vecs
    END INTERFACE DIAGONALISE

    INTERFACE KRON_PROD
        ! Kronecker product
        MODULE PROCEDURE KRON_PROD_RR_DP
        MODULE PROCEDURE KRON_PROD_RR_QP
        MODULE PROCEDURE KRON_PROD_CC_DP
        MODULE PROCEDURE KRON_PROD_CC_QP
        MODULE PROCEDURE KRON_PROD_RC_DP
        MODULE PROCEDURE KRON_PROD_RC_QP
        MODULE PROCEDURE KRON_PROD_CR_DP
        MODULE PROCEDURE KRON_PROD_CR_QP
        MODULE PROCEDURE KRON_CHAR
    END INTERFACE KRON_PROD

    INTERFACE NUM_GREATER_THAN_EQ_TO
        ! Calculates how many elements in a vector are greater than or equal to a given value
        MODULE PROCEDURE GTEQ_R_DP
        MODULE PROCEDURE GTEQ_R_QP
        MODULE PROCEDURE GTEQ_I_SP
        MODULE PROCEDURE GTEQ_I_DP
    END INTERFACE NUM_GREATER_THAN_EQ_TO

    INTERFACE BUBBLE
        ! Sorts a vector of reals or integers high to low
        ! Returns sorted list and a companion describing the order of the new list in terms of the old one
        MODULE PROCEDURE BUBBLE_R_DP
        MODULE PROCEDURE BUBBLE_R_QP
        MODULE PROCEDURE BUBBLE_I_SP
        MODULE PROCEDURE BUBBLE_I_DP
    END INTERFACE BUBBLE

    INTERFACE ALT_MATMUL
        ! Matrix Multiplication at variable precision
        MODULE PROCEDURE MATMUL_R_DP
        MODULE PROCEDURE MATMUL_R_QP
        MODULE PROCEDURE MATVECMUL_R_DP
        MODULE PROCEDURE MATVECMUL_R_QP
        MODULE PROCEDURE MATMUL_C_DP
        MODULE PROCEDURE MATMUL_C_QP
        MODULE PROCEDURE MATVECMUL_C_DP
        MODULE PROCEDURE MATVECMUL_C_QP
    END INTERFACE ALT_MATMUL

    INTERFACE ARRAY_APPEND
        ! Appends scalar/vector to vector/matrix for complex/real/integer
        MODULE PROCEDURE ARRAY_APPEND_R_V_S_DP
        MODULE PROCEDURE ARRAY_APPEND_R_V_S_QP
        MODULE PROCEDURE ARRAY_APPEND_C_V_S_DP
        MODULE PROCEDURE ARRAY_APPEND_C_V_S_QP
        MODULE PROCEDURE ARRAY_APPEND_I_V_S_SP
        MODULE PROCEDURE ARRAY_APPEND_I_V_S_DP
        MODULE PROCEDURE ARRAY_APPEND_R_V_V_DP
        MODULE PROCEDURE ARRAY_APPEND_R_V_V_QP
        MODULE PROCEDURE ARRAY_APPEND_C_V_V_DP
        MODULE PROCEDURE ARRAY_APPEND_C_V_V_QP
        MODULE PROCEDURE ARRAY_APPEND_I_V_V_SP
        MODULE PROCEDURE ARRAY_APPEND_I_V_V_DP
        MODULE PROCEDURE ARRAY_APPEND_R_M_V_DP
        MODULE PROCEDURE ARRAY_APPEND_R_M_V_QP
        MODULE PROCEDURE ARRAY_APPEND_C_M_V_DP
        MODULE PROCEDURE ARRAY_APPEND_C_M_V_QP
        MODULE PROCEDURE ARRAY_APPEND_I_M_V_SP
        MODULE PROCEDURE ARRAY_APPEND_I_M_V_DP
    END INTERFACE ARRAY_APPEND

    INTERFACE FLIPUD
        ! Flip arrays
        MODULE PROCEDURE FLIPUD_R_V_DP
        MODULE PROCEDURE FLIPUD_R_V_QP
        MODULE PROCEDURE FLIPUD_C_V_DP
        MODULE PROCEDURE FLIPUD_C_V_QP
        MODULE PROCEDURE FLIPUD_I_V_SP
        MODULE PROCEDURE FLIPUD_I_V_DP
    END INTERFACE FLIPUD

    INTERFACE DIAG
        ! Create diagonal matrix from vector
        MODULE PROCEDURE DIAG_VM_R_DP
        MODULE PROCEDURE DIAG_VM_R_QP
        MODULE PROCEDURE DIAG_VM_C_DP
        MODULE PROCEDURE DIAG_VM_C_QP
        MODULE PROCEDURE DIAG_VM_I_SP
        MODULE PROCEDURE DIAG_VM_I_DP

        ! Create vector from diagonal matrix
        MODULE PROCEDURE DIAG_MV_R_DP
        MODULE PROCEDURE DIAG_MV_R_QP
        MODULE PROCEDURE DIAG_MV_C_DP
        MODULE PROCEDURE DIAG_MV_C_QP
        MODULE PROCEDURE DIAG_MV_I_SP
        MODULE PROCEDURE DIAG_MV_I_DP
    END INTERFACE DIAG

    INTERFACE RADIAL
    ! Calculates euclidian norm
        MODULE PROCEDURE EUC_NORM_R_DP
        MODULE PROCEDURE EUC_NORM_R_QP
    END INTERFACE RADIAL

    INTERFACE ANGLE
    ! Calculates angle between two vectors
        MODULE PROCEDURE ANGLE_R_DP
        MODULE PROCEDURE ANGLE_R_QP
    END INTERFACE ANGLE

    INTERFACE CHECK_HERMITIAN
    ! Returns SUM(matrix * matrix^+)
        MODULE PROCEDURE CHECK_HERMITIAN_DP
        MODULE PROCEDURE CHECK_HERMITIAN_QP
    END INTERFACE CHECK_HERMITIAN

    INTERFACE CHECK_SYMMETRIC
    ! Returns SUM(matrix * matrix^T)
        MODULE PROCEDURE CHECK_SYMMETRIC_DP
        MODULE PROCEDURE CHECK_SYMMETRIC_QP
    END INTERFACE CHECK_SYMMETRIC

    INTERFACE WRITE_ARRAY
    ! Writes array to file
        MODULE PROCEDURE WRITE_ARRAY_R_DP
        MODULE PROCEDURE WRITE_ARRAY_R_QP
        MODULE PROCEDURE WRITE_ARRAY_C_DP
        MODULE PROCEDURE WRITE_ARRAY_C_QP
        MODULE PROCEDURE WRITE_ARRAY_I_SP
        MODULE PROCEDURE WRITE_ARRAY_I_DP
    END INTERFACE WRITE_ARRAY

    INTERFACE MAT_POWER
    ! Calculates matrix raised to a power
        MODULE PROCEDURE MAT_POWER_R_DP
        MODULE PROCEDURE MAT_POWER_R_QP
        MODULE PROCEDURE MAT_POWER_C_DP
        MODULE PROCEDURE MAT_POWER_C_QP
    END INTERFACE

    CONTAINS

    SUBROUTINE EXPECTATION_R_DP(FB,vectors,pert)
        ! Performs change of basis for a double precision real matrix
        ! FB = 'F' Forwards (vectors^T).(pert.vectors)
        ! FB = 'B' Backwards (vectors.pert).(vectors^T)
        INTEGER                         :: dim
        CHARACTER(LEN = *), INTENT(IN)  :: FB
        REAL(KIND = 8), INTENT(IN)      :: vectors(:,:)
        REAL(KIND = 8), INTENT(INOUT)   :: pert(:,:)
        REAL(KIND = 8)                  :: temp(size(vectors,1), size(vectors,2))
      
        dim = size(vectors,1)
        
        ! Perform forwards or backwards change of basis
        IF(FB == 'F' .or. FB == 'f') then     ! (vectors^T).(pert.vectors)
              CALL DGEMM('N','N',dim,dim,dim,1.0_8,pert,dim,vectors,dim,0.0_8,temp,dim)
              CALL DGEMM('C','N',dim,dim,dim,1.0_8,vectors,dim,temp,dim,0.0_8,pert,dim)
        ELSE IF(FB == 'B' .or. FB == 'b') then  ! (vectors.pert).(vectors^T)
              CALL DGEMM('N','N',dim,dim,dim,1.0_8,vectors,dim,pert,dim,0.0_8,temp,dim)
              CALL DGEMM('N','C',dim,dim,dim,1.0_8,temp,dim,vectors,dim,0.0_8,pert,dim)
        END IF
    END SUBROUTINE EXPECTATION_R_DP
    
    SUBROUTINE EXPECTATION_R_QP(FB,vectors,pert)
        ! Performs change of basis for a quadruple precision real matrix
        ! FB = 'F' Forwards (vectors^T).(pert.vectors)
        ! FB = 'B' Backwards (vectors.pert).(vectors^T)
        INTEGER                         :: dim
        CHARACTER(LEN = *), INTENT(IN)  :: FB
        REAL(KIND = 16), INTENT(IN)     :: vectors(:,:)
        REAL(KIND = 16), INTENT(INOUT)  :: pert(:,:)
        REAL(KIND = 16)                 :: temp(size(vectors,1), size(vectors,2))
      
        dim = size(vectors,1)
        
        ! Perform forwards or backwards change of basis
        IF(FB == 'F' .or. FB == 'f') then     ! (vectors^T).(pert.vectors)
              CALL QGEMM('N','N',dim,dim,dim,1.0_16,pert,dim,vectors,dim,0.0_16,temp,dim)
              CALL QGEMM('C','N',dim,dim,dim,1.0_16,vectors,dim,temp,dim,0.0_16,pert,dim)
        ELSE IF(FB == 'B' .or. FB == 'b') then  ! (vectors.pert).(vectors^T)
              CALL QGEMM('N','N',dim,dim,dim,1.0_16,vectors,dim,pert,dim,0.0_16,temp,dim)
              CALL QGEMM('N','C',dim,dim,dim,1.0_16,temp,dim,vectors,dim,0.0_16,pert,dim)
        END IF
    END SUBROUTINE EXPECTATION_R_QP
    
    SUBROUTINE EXPECTATION_C_DP(FB,vectors,pert)
        ! Performs change of basis for a double precision complex matrix
        ! FB = 'F' Forwards (vectors^T).(pert.vectors)
        ! FB = 'B' Backwards (vectors.pert).(vectors^T)
        INTEGER                            :: dim
        CHARACTER(LEN = *), INTENT(IN)     :: FB
        COMPLEX(KIND = 8), INTENT(IN)      :: vectors(:,:)
        COMPLEX(KIND = 8), INTENT(INOUT)   :: pert(:,:)
        COMPLEX(KIND = 8)                  :: temp(size(vectors,1), size(vectors,2))
      
        dim = size(vectors,1)
        
        ! Perform forwards or backwards change of basis
        IF(FB == 'F' .or. FB == 'f') then     ! (vectors^T).(pert.vectors)
              CALL ZGEMM('N','N',dim,dim,dim,(1.0_8,0.0_8),pert,dim,vectors,dim,(0.0_8,0.0_8),temp,dim)
              CALL ZGEMM('C','N',dim,dim,dim,(1.0_8,0.0_8),vectors,dim,temp,dim,(0.0_8,0.0_8),pert,dim)
        ELSE IF(FB == 'B' .or. FB == 'b') then  ! (vectors.pert).(vectors^T)
              CALL ZGEMM('N','N',dim,dim,dim,(1.0_8,0.0_8),vectors,dim,pert,dim,(0.0_8,0.0_8),temp,dim)
              CALL ZGEMM('N','C',dim,dim,dim,(1.0_8,0.0_8),temp,dim,vectors,dim,(0.0_8,0.0_8),pert,dim)
        END IF
    END SUBROUTINE EXPECTATION_C_DP
    
    SUBROUTINE EXPECTATION_C_QP(FB,vectors,pert)
        ! Performs change of basis for a quadruple precision complex matrix
        ! FB = 'F' Forwards (vectors^T).(pert.vectors)
        ! FB = 'B' Backwards (vectors.pert).(vectors^T)
        INTEGER                            :: dim
        CHARACTER(LEN = *), INTENT(IN)     :: FB
        COMPLEX(KIND = 16), INTENT(IN)     :: vectors(:,:)
        COMPLEX(KIND = 16), INTENT(INOUT)  :: pert(:,:)
        COMPLEX(KIND = 16)                 :: temp(size(vectors,1), size(vectors,2))
      
        dim = size(vectors,1)
        
        ! Perform forwards or backwards change of basis
        IF(FB == 'F' .or. FB == 'f') THEN     ! (vectors^T).(pert.vectors)
              CALL ZQGEMM('N','N',dim,dim,dim,(1.0_16,0.0_16),pert,dim,vectors,dim,(0.0_16,0.0_16),temp,dim)
              CALL ZQGEMM('C','N',dim,dim,dim,(1.0_16,0.0_16),vectors,dim,temp,dim,(0.0_16,0.0_16),pert,dim)
        ELSE IF(FB == 'B' .or. FB == 'b') THEN  ! (vectors.pert).(vectors^T)
              CALL ZQGEMM('N','N',dim,dim,dim,(1.0_16,0.0_16),vectors,dim,pert,dim,(0.0_16,0.0_16),temp,dim)
              CALL ZQGEMM('N','C',dim,dim,dim,(1.0_16,0.0_16),temp,dim,vectors,dim,(0.0_16,0.0_16),pert,dim)
        END IF

    END SUBROUTINE EXPECTATION_C_QP

    FUNCTION KRON_PROD_RR_DP(A,B) RESULT(C)
        ! Calculates Kronecker product A(X)B = C
        ! For two real matrices at double precision
        IMPLICIT NONE
        REAL(KIND = 8)                 :: A(:,:),B(:,:) !A AND B MUST BE SQUARE
        REAL(KIND = 8), ALLOCATABLE    :: C(:,:)
        INTEGER                        :: J,K !INCREMENTERS
        INTEGER                        :: SA,SB !SIZE OF A AND B (SQUARE SO NCOLS=NROWS)

        SA = SIZE(A,1)
        SB = SIZE(B,1)

        IF (ALLOCATED(C) .EQV. .FALSE.) ALLOCATE(C(SA*SB,SA*SB))

        DO J=1,SA*SB
            DO K=1,SA*SB !from wikipedia page for kronecker product
                C(J,K) = A(INT(INT((J-1)/SB)+1),INT(INT((K-1)/SB)+1))*B(INT(J-INT((J-1)/SB)*SB),INT(K-INT((K-1)/SB)*SB))
            END DO
        END DO

    END FUNCTION KRON_PROD_RR_DP

    FUNCTION KRON_PROD_RR_QP(A,B) RESULT(C)
        !Calculates Kronecker product A(X)B = C
        ! For two real matrices at quadruple precision
        IMPLICIT NONE
        REAL(KIND = 16)                :: A(:,:),B(:,:) !A AND B MUST BE SQUARE
        REAL(KIND = 16), ALLOCATABLE   :: C(:,:)
        INTEGER                        :: J,K !INCREMENTERS
        INTEGER                        :: SA,SB !SIZE OF A AND B (SQUARE SO NCOLS=NROWS)

        SA = SIZE(A,1)
        SB = SIZE(B,1)

        IF (ALLOCATED(C) .EQV. .FALSE.) ALLOCATE(C(SA*SB,SA*SB))

        DO J=1,SA*SB
            DO K=1,SA*SB !from wikipedia page for kronecker product
                C(J,K) = A(INT(INT((J-1)/SB)+1),INT(INT((K-1)/SB)+1))*B(INT(J-INT((J-1)/SB)*SB),INT(K-INT((K-1)/SB)*SB))
            END DO
        END DO


    END FUNCTION KRON_PROD_RR_QP

    FUNCTION KRON_PROD_CC_DP(A,B) RESULT(C)
        !Calculates Kronecker product A(X)B = C
        ! For two complex matrices at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8)              :: A(:,:),B(:,:) !A AND B MUST BE SQUARE
        COMPLEX(KIND = 8), ALLOCATABLE :: C(:,:)
        INTEGER                        :: J,K !INCREMENTERS
        INTEGER                        :: SA,SB !SIZE OF A AND B (SQUARE SO NCOLS=NROWS)

        SA = SIZE(A,1)
        SB = SIZE(B,1)

        IF (ALLOCATED(C) .EQV. .FALSE.) ALLOCATE(C(SA*SB,SA*SB))

        DO J=1,SA*SB
            DO K=1,SA*SB !from wikipedia page for kronecker product
                C(J,K) = A(INT(INT((J-1)/SB)+1),INT(INT((K-1)/SB)+1))*B(INT(J-INT((J-1)/SB)*SB),INT(K-INT((K-1)/SB)*SB))
            END DO
        END DO

    END FUNCTION KRON_PROD_CC_DP

    FUNCTION KRON_PROD_CC_QP(A,B) RESULT(C)
        !Calculates Kronecker product A(X)B = C
        ! For two complex matrices at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16)              :: A(:,:),B(:,:) !A AND B MUST BE SQUARE
        COMPLEX(KIND = 16), ALLOCATABLE :: C(:,:)
        INTEGER                         :: J,K !INCREMENTERS
        INTEGER                         :: SA,SB !SIZE OF A AND B (SQUARE SO NCOLS=NROWS)

        SA = SIZE(A,1)
        SB = SIZE(B,1)

        IF (ALLOCATED(C) .EQV. .FALSE.) ALLOCATE(C(SA*SB,SA*SB))

        DO J=1,SA*SB
            DO K=1,SA*SB !from wikipedia page for kronecker product
                C(J,K) = A(INT(INT((J-1)/SB)+1),INT(INT((K-1)/SB)+1))*B(INT(J-INT((J-1)/SB)*SB),INT(K-INT((K-1)/SB)*SB))
            END DO
        END DO

    END FUNCTION KRON_PROD_CC_QP

    FUNCTION KRON_PROD_RC_DP(A,B) RESULT(C)
        !Calculates Kronecker product A(X)B = C
        ! For real(X)complex at double precision
        IMPLICIT NONE
        REAL(KIND = 8)                  :: A(:,:) !A AND B MUST BE SQUARE
        COMPLEX(KIND = 8)               :: B(:,:) !A AND B MUST BE SQUARE
        COMPLEX(KIND = 8), ALLOCATABLE  :: C(:,:), A_COMPLEX(:,:)
        INTEGER                         :: J,K !INCREMENTERS
        INTEGER                         :: SA,SB !SIZE OF A AND B (SQUARE SO NCOLS=NROWS)

        SA = SIZE(A,1)
        SB = SIZE(B,1)

        ALLOCATE(A_COMPLEX(SIZE(A,1),SIZE(A,1)))
        A_COMPLEX = (0.0_8, 0.0_8)
        A_COMPLEX = A_COMPLEX + A

        IF (ALLOCATED(C) .EQV. .FALSE.) ALLOCATE(C(SA*SB,SA*SB))

        DO J=1,SA*SB
            DO K=1,SA*SB !from wikipedia page for kronecker product
                C(J,K) = A_COMPLEX(INT(INT((J-1)/SB)+1),INT(INT((K-1)/SB)+1))*B(INT(J-INT((J-1)/SB)*SB),INT(K-INT((K-1)/SB)*SB))
            END DO
        END DO

    END FUNCTION KRON_PROD_RC_DP

    FUNCTION KRON_PROD_RC_QP(A,B) RESULT(C)
        !Calculates Kronecker product A(X)B = C
        ! For real(X)complex at quadruple precision
        IMPLICIT NONE
        REAL(KIND = 16)                  :: A(:,:) !A AND B MUST BE SQUARE
        COMPLEX(KIND = 16)               :: B(:,:) !A AND B MUST BE SQUARE
        COMPLEX(KIND = 16), ALLOCATABLE  :: C(:,:), A_COMPLEX(:,:)
        INTEGER                          :: J,K !INCREMENTERS
        INTEGER                          :: SA,SB !SIZE OF A AND B (SQUARE SO NCOLS=NROWS)

        SA = SIZE(A,1)
        SB = SIZE(B,1)

        ALLOCATE(A_COMPLEX(SIZE(A,1),SIZE(A,1)))
        A_COMPLEX = (0.0_16, 0.0_16)
        A_COMPLEX = A_COMPLEX + A

        IF (ALLOCATED(C) .EQV. .FALSE.) ALLOCATE(C(SA*SB,SA*SB))

        DO J=1,SA*SB
            DO K=1,SA*SB !from wikipedia page for kronecker product
                C(J,K) = A_COMPLEX(INT(INT((J-1)/SB)+1),INT(INT((K-1)/SB)+1))*B(INT(J-INT((J-1)/SB)*SB),INT(K-INT((K-1)/SB)*SB))
            END DO
        END DO

    END FUNCTION KRON_PROD_RC_QP

    FUNCTION KRON_PROD_CR_DP(A,B) RESULT(C)
        !Calculates Kronecker product A(X)B = C
        ! For complex(X)real at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8)               :: A(:,:) !A AND B MUST BE SQUARE
        REAL(KIND = 8)                  :: B(:,:) !A AND B MUST BE SQUARE
        COMPLEX(KIND = 8), ALLOCATABLE  :: C(:,:), B_COMPLEX(:,:)
        INTEGER                         :: J,K !INCREMENTERS
        INTEGER                         :: SA,SB !SIZE OF A AND B (SQUARE SO NCOLS=NROWS)

        SA = SIZE(A,1)
        SB = SIZE(B,1)

        ALLOCATE(B_COMPLEX(SIZE(B,1),SIZE(B,1)))
        B_COMPLEX = (0.0_8, 0.0_8)
        B_COMPLEX = B_COMPLEX + B

        IF (ALLOCATED(C) .EQV. .FALSE.) ALLOCATE(C(SA*SB,SA*SB))

        DO J=1,SA*SB
            DO K=1,SA*SB !from wikipedia page for kronecker product
                C(J,K) = A(INT(INT((J-1)/SB)+1),INT(INT((K-1)/SB)+1))*B_COMPLEX(INT(J-INT((J-1)/SB)*SB),INT(K-INT((K-1)/SB)*SB))
            END DO
        END DO

    END FUNCTION KRON_PROD_CR_DP

    FUNCTION KRON_PROD_CR_QP(A,B) RESULT(C)
        !Calculates Kronecker product A(X)B = C
        ! For complex(X)real at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16)               :: A(:,:) !A AND B MUST BE SQUARE
        REAL(KIND = 16)                  :: B(:,:) !A AND B MUST BE SQUARE
        COMPLEX(KIND = 16), ALLOCATABLE  :: C(:,:), B_COMPLEX(:,:)
        INTEGER                          :: J,K !INCREMENTERS
        INTEGER                          :: SA,SB !SIZE OF A AND B (SQUARE SO NCOLS=NROWS)

        SA = SIZE(A,1)
        SB = SIZE(B,1)

        ALLOCATE(B_COMPLEX(SIZE(B,1),SIZE(B,1)))
        B_COMPLEX = (0.0_16, 0.0_16)
        B_COMPLEX = B_COMPLEX + B

        IF (ALLOCATED(C) .EQV. .FALSE.) ALLOCATE(C(SA*SB,SA*SB))

        DO J=1,SA*SB
            DO K=1,SA*SB !from wikipedia page for kronecker product
                C(J,K) = A(INT(INT((J-1)/SB)+1),INT(INT((K-1)/SB)+1))*B_COMPLEX(INT(J-INT((J-1)/SB)*SB),INT(K-INT((K-1)/SB)*SB))
            END DO
        END DO

    END FUNCTION KRON_PROD_CR_QP

    FUNCTION KRON_CHAR(A, B) RESULT(C)
        ! Forms Kronecker product of character arrays
        IMPLICIT NONE
        CHARACTER(LEN = 1000), ALLOCATABLE   :: C(:,:)
        CHARACTER(LEN = 500), INTENT(IN)     :: A(:,:), B(:,:)
        INTEGER                              :: J

        ALLOCATE(C(SIZE(A,1)*SIZE(B,1), SIZE(A,1)*SIZE(B,1)))

        DO J=1, SIZE(C, 1) 
                !WRITE(6,'(2A)') TRIM(A(INT(INT((J-1)/SIZE(B,1))+1),INT(INT((K-1)/SIZE(B,1))+1))), TRIM(B(INT(J-INT((J-1)/SIZE(B,1))*SIZE(B,1)),INT(K-INT((K-1)/SIZE(B,1))*SIZE(B,1))))
                WRITE(C(j, j),'(5A)') '|',TRIM(A(INT(INT((J-1)/SIZE(B,1))+1),INT(INT((J-1)/SIZE(B,1))+1))), ',' , TRIM(B(INT(J-INT((J-1)/SIZE(B,1))*SIZE(B,1)),INT(J-INT((J-1)/SIZE(B,1))*SIZE(B,1)))),'>'
        END DO

    END FUNCTION KRON_CHAR

    FUNCTION GTEQ_R_DP(vector, value) RESULT(GTEQ)
        !Returns the number of numbers greater than or equal to a given value
        !Both vector and value are double precision reals
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)  :: vector(:), value
        INTEGER                     :: row, GTEQ
      
        GTEQ = 0
      
        DO row = 1, size(vector)
              IF(vector(row) >= value) GTEQ = GTEQ + 1
        END DO
      
    END FUNCTION GTEQ_R_DP

    FUNCTION GTEQ_R_QP(vector, value) RESULT(GTEQ)
        !Returns the number of numbers greater than or equal to a given value
        !Both vector and value are quadruple precision reals
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)  :: vector(:), value
        INTEGER                      :: row, GTEQ
      
        GTEQ = 0
      
        DO row = 1, size(vector)
              IF(vector(row) >= value) GTEQ = GTEQ + 1
        END DO
      
    END FUNCTION GTEQ_R_QP

    FUNCTION GTEQ_I_SP(vector, value)  RESULT(GTEQ)
        !Returns the number of numbers greater than or equal to a given value
        !Both vector and value are single precision integers
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: vector(:), value
        INTEGER             :: row, GTEQ
      
        GTEQ = 0
      
        DO row = 1, size(vector)
              IF(vector(row) >= value) GTEQ = GTEQ + 1
        END DO
      
    END FUNCTION GTEQ_I_SP

    FUNCTION GTEQ_I_DP(vector, value)  RESULT(GTEQ)
        !Returns the number of numbers greater than or equal to a given value
        !Both vector and value are double precision integers
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(IN) :: vector(:), value
        INTEGER(KIND = 8)             :: row, GTEQ
      
        GTEQ = 0
      
        DO row = 1, size(vector)
              IF(vector(row) >= value) GTEQ = GTEQ + 1
        END DO
      
    END FUNCTION GTEQ_I_DP

    SUBROUTINE DIAGONALISE_R_DP_both(UL,matrix,eigenvectors,eigenvalues)
        ! Real Hermitian matrix eigensolver using LAPACK's ZHEEV SUBROUTINE
        ! Returns eigenvalues and eigenvectors
        ! UL = 'U' (Upper triangle of matrix is stored) OR 'L' (Lower triangle of matrix is stored)
        IMPLICIT NONE
            CHARACTER(LEN = *),INTENT(IN)    :: UL
            REAL(KIND = 8),INTENT(IN)        :: matrix(:,:)
            REAL(KIND = 8),INTENT(OUT)       :: eigenvectors(size(matrix,1),size(matrix,2))
            REAL(KIND = 8),INTENT(OUT)       :: eigenvalues(size(matrix,1))
            REAL(KIND = 8),ALLOCATABLE       :: rwork(:)
            INTEGER                          :: dim,lwork,diaginfo
    
            !Perform workspace query to calculate optimum workspace size
            dim = SIZE(matrix,1)
            eigenvectors = matrix
            ALLOCATE(rwork(4))
            CALL DSYEV('V',UL,dim,eigenvectors,dim,eigenvalues,rwork,-1,diaginfo)
    
            !Reallocate work based on lwork returned by query, then perform diagonalisation
            lwork = NINT(rwork(1))
            DEALLOCATE(rwork)
            ALLOCATE(rwork(lwork))
            CALL DSYEV('V',UL,dim,eigenvectors,dim,eigenvalues,rwork,lwork,diaginfo)
            IF(diaginfo /= 0) WRITE(6,'(A21,I10)') "Diagonalisation Error",diaginfo
            DEALLOCATE(rwork)
    
    END SUBROUTINE DIAGONALISE_R_DP_both

    SUBROUTINE DIAGONALISE_R_DP_vals(UL,matrix,eigenvalues)
        ! Real Hermitian matrix eigensolver using LAPACK's ZHEEV SUBROUTINE
        ! Only returns eigenvalues
        ! UL = 'U' (Upper triangle of matrix is stored) OR 'L' (Lower triangle of matrix is stored)
        IMPLICIT NONE
            CHARACTER(LEN = *),INTENT(IN)    :: UL
            REAL(KIND = 8),INTENT(IN)        :: matrix(:,:)
            REAL(KIND = 8)                   :: eigenvectors(size(matrix,1),size(matrix,2))
            REAL(KIND = 8),INTENT(OUT)       :: eigenvalues(size(matrix,1))
            REAL(KIND = 8),ALLOCATABLE       :: rwork(:)
            INTEGER                          :: dim,lwork,diaginfo
    
            !Perform workspace query to calculate optimum workspace size
            dim = SIZE(matrix,1)
            eigenvectors = matrix
            ALLOCATE(rwork(4))
            CALL DSYEV('N',UL,dim,eigenvectors,dim,eigenvalues,rwork,-1,diaginfo)
    
            !Reallocate work based on lwork returned by query, then perform diagonalisation
            lwork = NINT(rwork(1))
            DEALLOCATE(rwork)
            ALLOCATE(rwork(lwork))
            CALL DSYEV('N',UL,dim,eigenvectors,dim,eigenvalues,rwork,lwork,diaginfo)
            IF(diaginfo /= 0) WRITE(6,'(A21,I10)') "Diagonalisation Error",diaginfo
            DEALLOCATE(rwork)
    
    END SUBROUTINE DIAGONALISE_R_DP_vals
    
    SUBROUTINE DIAGONALISE_R_DP_vecs(UL,matrix,eigenvectors)
        ! Real Hermitian matrix eigensolver using LAPACK's ZHEEV SUBROUTINE
        ! Only returns eigenvectors
        ! UL = 'U' (Upper triangle of matrix is stored) OR 'L' (Lower triangle of matrix is stored)
        IMPLICIT NONE
            CHARACTER(LEN = *),INTENT(IN)    :: UL
            REAL(KIND = 8),INTENT(IN)        :: matrix(:,:)
            REAL(KIND = 8),INTENT(OUT)       :: eigenvectors(size(matrix,1),size(matrix,2))
            REAL(KIND = 8)                   :: eigenvalues(size(matrix,1))
            REAL(KIND = 8),ALLOCATABLE       :: rwork(:)
            INTEGER                          :: dim,lwork,diaginfo
    
            write(6,*) 'vecs'

            !Perform workspace query to calculate optimum workspace size
            dim = SIZE(matrix,1)
            eigenvectors = matrix
            ALLOCATE(rwork(4))
            CALL DSYEV('V',UL,dim,eigenvectors,dim,eigenvalues,rwork,-1,diaginfo)
    
            !Reallocate work based on lwork returned by query, then perform diagonalisation
            lwork = NINT(rwork(1))
            DEALLOCATE(rwork)
            ALLOCATE(rwork(lwork))
            CALL DSYEV('V',UL,dim,eigenvectors,dim,eigenvalues,rwork,lwork,diaginfo)
            IF(diaginfo /= 0) WRITE(6,'(A21,I10)') "Diagonalisation Error",diaginfo
            DEALLOCATE(rwork)
    
    END SUBROUTINE DIAGONALISE_R_DP_vecs

    SUBROUTINE DIAGONALISE_C_DP_both(UL,matrix,eigenvectors,eigenvalues)
        ! Complex Hermitian matrix eigensolver using LAPACK's ZHEEV SUBROUTINE
        ! Returns eigenvectors and eigenvalues
        ! UL = 'U' (Upper triangle of matrix is stored) OR 'L' (Lower triangle of matrix is stored)
        IMPLICIT NONE
        CHARACTER(LEN = *),INTENT(IN)       :: UL
        COMPLEX(KIND = 8),INTENT(IN)        :: matrix(:,:)
        COMPLEX(KIND = 8),INTENT(OUT)       :: eigenvectors(size(matrix,1),size(matrix,2))
        REAL(KIND = 8),INTENT(OUT)          :: eigenvalues(size(matrix,1))
        REAL(KIND = 8),ALLOCATABLE          :: rwork(:)
        COMPLEX(KIND = 8),ALLOCATABLE       :: work(:)
        INTEGER                             :: dim,lwork,diaginfo
    
        !Perform workspace query to calculate optimum workspace size
        dim = SIZE(matrix,1)
        eigenvectors = matrix
        ALLOCATE(work(4),rwork(3*dim-2))
        CALL ZHEEV('V',UL,dim,eigenvectors,dim,eigenvalues,work,-1,rwork,diaginfo)
        
        !Reallocate work based on lwork returned by query, then perform diagonalisation
        lwork = NINT(REAL(work(1),8))
        DEALLOCATE(work)
        ALLOCATE(work(lwork))
        CALL ZHEEV('V',UL,dim,eigenvectors,dim,eigenvalues,work,lwork,rwork,diaginfo)
        IF(diaginfo /= 0) WRITE(6,'(A21,I10)') "Diagonalisation Error",diaginfo
        DEALLOCATE(work,rwork)
    
    END SUBROUTINE DIAGONALISE_C_DP_both

    SUBROUTINE DIAGONALISE_C_DP_vals(UL,matrix,eigenvalues)
        ! Complex Hermitian matrix eigensolver using LAPACK's ZHEEV SUBROUTINE
        ! Only returns eigenvalues
        ! UL = 'U' (Upper triangle of matrix is stored) OR 'L' (Lower triangle of matrix is stored)
        IMPLICIT NONE
        CHARACTER(LEN = *),INTENT(IN)       :: UL
        COMPLEX(KIND = 8),INTENT(IN)        :: matrix(:,:)
        COMPLEX(KIND = 8)                   :: eigenvectors(size(matrix,1),size(matrix,2))
        REAL(KIND = 8),INTENT(OUT)          :: eigenvalues(size(matrix,1))
        REAL(KIND = 8),ALLOCATABLE          :: rwork(:)
        COMPLEX(KIND = 8),ALLOCATABLE       :: work(:)
        INTEGER                             :: dim,lwork,diaginfo
    
    
        !Perform workspace query to calculate optimum workspace size
        dim = SIZE(matrix,1)
        eigenvectors = matrix
        ALLOCATE(work(4),rwork(3*dim-2))
        CALL ZHEEV('N',UL,dim,eigenvectors,dim,eigenvalues,work,-1,rwork,diaginfo)
        
        !Reallocate work based on lwork returned by query, then perform diagonalisation
        lwork = NINT(REAL(work(1),8))
        DEALLOCATE(work)
        ALLOCATE(work(lwork))
        CALL ZHEEV('N',UL,dim,eigenvectors,dim,eigenvalues,work,lwork,rwork,diaginfo)
        IF(diaginfo /= 0) WRITE(6,'(A21,I10)') "Diagonalisation Error",diaginfo
        DEALLOCATE(work, rwork)
    
    END SUBROUTINE DIAGONALISE_C_DP_vals

    SUBROUTINE DIAGONALISE_C_DP_vecs(UL,matrix,eigenvectors)
        ! Complex Hermitian matrix eigensolver using LAPACK's ZHEEV SUBROUTINE
        ! Only returns eigenvectors
        ! UL = 'U' (Upper triangle of matrix is stored) OR 'L' (Lower triangle of matrix is stored)
        IMPLICIT NONE
        CHARACTER(LEN = *),INTENT(IN)       :: UL
        COMPLEX(KIND = 8),INTENT(IN)        :: matrix(:,:)
        COMPLEX(KIND = 8),INTENT(OUT)       :: eigenvectors(size(matrix,1),size(matrix,2))
        REAL(KIND = 8)                      :: eigenvalues(size(matrix,1))
        REAL(KIND = 8),ALLOCATABLE          :: rwork(:)
        COMPLEX(KIND = 8),ALLOCATABLE       :: work(:)
        INTEGER                             :: dim,lwork,diaginfo
    
        !Perform workspace query to calculate optimum workspace size
        dim = SIZE(matrix,1)
        eigenvectors = matrix
        ALLOCATE(work(4),rwork(3*dim-2))
        CALL ZHEEV('V',UL,dim,eigenvectors,dim,eigenvalues,work,-1,rwork,diaginfo)
        
        !Reallocate work based on lwork returned by query, then perform diagonalisation
        lwork = NINT(REAL(work(1),8))
        DEALLOCATE(work)
        ALLOCATE(work(lwork))
        CALL ZHEEV('V',UL,dim,eigenvectors,dim,eigenvalues,work,lwork,rwork,diaginfo)
        IF(diaginfo /= 0) WRITE(6,'(A21,I10)') "Diagonalisation Error",diaginfo
        DEALLOCATE(work,rwork)
    
    END SUBROUTINE DIAGONALISE_C_DP_vecs

    SUBROUTINE BUBBLE_R_DP(list, companion, top)
    !Bubble sort for a list of double precision reals
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(INOUT)  :: list(:) 
        REAL(KIND = 8)                 :: RTEMP
        INTEGER, INTENT(INOUT)         :: companion(:,:)
        INTEGER                        :: J, ITEMP
        LOGICAL                        :: swapped
        CHARACTER(LEN = 3)             :: top
    
        companion = 0
        companion(:,1) = [(J, J = 1, SIZE(list))]
        companion(:,2) = companion(:,1)
    
        swapped = .TRUE.
    

    IF (top == 'max') THEN
        DO WHILE (swapped .EQV. .TRUE.)
        !Loop until no swaps occur
        swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) < list(J + 1)) THEN
                    !Swap list elements
                    RTEMP        = list(J) 
                    list(J)    = list(J + 1) 
                    list(J + 1)  = RTEMP
                    !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP
                    swapped = .TRUE.
                END IF
            END DO
        END DO
    ELSE IF (top == 'min') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) > list(J + 1)) THEN
                    !Swap list elements
                    RTEMP        = list(J) 
                    list(J)    = list(J + 1) 
                    list(J + 1)  = RTEMP
                    !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP
                    swapped = .TRUE.
                END IF
            END DO
        END DO
    END IF
    
    END SUBROUTINE BUBBLE_R_DP
    
    SUBROUTINE BUBBLE_R_QP(list, companion, top)
    !Bubble sort for a list of quadruple precision reals
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(INOUT) :: list(:) 
        REAL(KIND = 16)                :: RTEMP
        INTEGER, INTENT(INOUT)         :: companion(:,:)
        INTEGER                        :: J, ITEMP
        LOGICAL                        :: swapped
        CHARACTER(LEN = 3)             :: top
    
        companion = 0
        companion(:,1) = [(J, J = 1, SIZE(list))]
        companion(:,2) = companion(:,1)
    
        swapped = .TRUE.
    

    IF (top == 'max') THEN
        DO WHILE (swapped .EQV. .TRUE.)
        !Loop until no swaps occur
        swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) < list(J + 1)) THEN
                    !Swap list elements
                    RTEMP        = list(J) 
                    list(J)    = list(J + 1) 
                    list(J + 1)  = RTEMP
                    !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP
                    swapped = .TRUE.
                END IF
            END DO
        END DO
    ELSE IF (top == 'min') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) > list(J + 1)) THEN
                    !Swap list elements
                    RTEMP        = list(J) 
                    list(J)    = list(J + 1) 
                    list(J + 1)  = RTEMP
                    !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP
                    swapped = .TRUE.
                END IF
            END DO
        END DO
    END IF
    
    END SUBROUTINE BUBBLE_R_QP

    SUBROUTINE BUBBLE_I_SP(list,companion, top)
    !Bubble sort for a list of single precision integers
        IMPLICIT NONE
        INTEGER,   INTENT(INOUT) :: list(:), companion(:,:)
        INTEGER                  :: J, TEMP, ITEMP
        LOGICAL                  :: swapped
        CHARACTER(LEN = 3)       :: top

        companion = 0
        companion(:,1) = [(J, J = 1, SIZE(list))]
        companion(:,2) = companion(:,1)
    
        swapped = .TRUE.
    
    IF (top == 'max') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) < list(J + 1)) THEN
              !Swap list elements
                    TEMP        = list(J) 
                    list(J)     = list(J + 1) 
                    list(J + 1) = TEMP
              !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP

                    swapped = .TRUE.
                END IF
            END DO
        END DO
    ELSE IF (top == 'min') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) > list(J + 1)) THEN
              !Swap list elements
                    TEMP        = list(J) 
                    list(J)     = list(J + 1) 
                    list(J + 1) = TEMP
              !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP

                    swapped = .TRUE.
                END IF
            END DO
        END DO
    END IF
    END SUBROUTINE BUBBLE_I_SP

    SUBROUTINE BUBBLE_I_DP(list,companion, top)
    !Bubble sort for a list of double precision integers
        IMPLICIT NONE
        INTEGER,   INTENT(INOUT)           :: companion(:,:)
        INTEGER(KIND = 8),   INTENT(INOUT) :: list(:)
        INTEGER                            :: J, ITEMP
        INTEGER(KIND = 8)                  :: TEMP
        LOGICAL                            :: swapped
        CHARACTER(LEN = 3)                 :: top

        companion = 0
        companion(:,1) = [(J, J = 1, SIZE(list))]
        companion(:,2) = companion(:,1)
    
        swapped = .TRUE.
    
    IF (top == 'max') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) < list(J + 1)) THEN
              !Swap list elements
                    TEMP        = list(J) 
                    list(J)     = list(J + 1) 
                    list(J + 1) = TEMP
              !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP

                    swapped = .TRUE.
                END IF
            END DO
        END DO
    ELSE IF (top == 'min') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) > list(J + 1)) THEN
              !Swap list elements
                    TEMP        = list(J) 
                    list(J)     = list(J + 1) 
                    list(J + 1) = TEMP
              !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP

                    swapped = .TRUE.
                END IF
            END DO
        END DO
    END IF
    END SUBROUTINE BUBBLE_I_DP

    FUNCTION MATMUL_R_QP(A,B) RESULT(C)
        !Multiplication of real matrix and real matrix
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)  :: A(:,:), B(:,:)
        REAL(KIND = 16)              :: C(size(A,1),size(B,2))
        INTEGER                     :: K,L,P

        IF (SIZE(A,2) /= SIZE(B,1)) THEN
          WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
          STOP
        END IF

        C = 0.0_16
    
        DO K = 1, SIZE(A,1)
            DO L = 1, SIZE(B,2)
                DO P = 1, SIZE(A,2)
                    C(K,L) = C(K,L) + A(K,P)*B(P,L)
                END DO
            END DO
        END DO

    END FUNCTION MATMUL_R_QP

    FUNCTION MATMUL_R_DP(A,B) RESULT(C)
        !Multiplication of real matrix and real matrix
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)  :: A(:,:), B(:,:)
        REAL(KIND = 8)              :: C(size(A,1),size(B,2))
        INTEGER                     :: K,L,P

        IF (SIZE(A,2) /= SIZE(B,1)) THEN
          WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
          STOP
        END IF

        C = 0.0_8
    
        DO K = 1, SIZE(A,1)
            DO L = 1, SIZE(B,2)
                DO P = 1, SIZE(A,2)
                    C(K,L) = C(K,L) + A(K,P)*B(P,L)
                END DO
            END DO
        END DO

    END FUNCTION MATMUL_R_DP

    FUNCTION MATVECMUL_R_QP(A,B) RESULT(C)
        !Multiplication of real vector and real matrix
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)  :: A(:,:), B(:)
        REAL(KIND = 16)              :: C(SIZE(B))
        INTEGER                      :: K,P

        IF (SIZE(A,2) /= SIZE(B)) THEN
          WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
          STOP
        END IF

        C = 0.0_16   

        DO K = 1, SIZE(A,1)
            DO P = 1, SIZE(A,2)
                C(K) = C(K) + A(K,P)*B(P)
            END DO
        END DO

    END FUNCTION MATVECMUL_R_QP

    FUNCTION MATVECMUL_R_DP(A,B) RESULT(C)
        !Multiplication of real vector and real matrix
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)   :: A(:,:), B(:)
        REAL(KIND = 8)               :: C(SIZE(B))
        INTEGER                      :: K,P

        IF (SIZE(A,2) /= SIZE(B)) THEN
          WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
          STOP
        END IF

        C = 0.0_8   

        DO K = 1, SIZE(A,1)
            DO P = 1, SIZE(A,2)
                C(K) = C(K) + A(K,P)*B(P)
            END DO
        END DO

    END FUNCTION MATVECMUL_R_DP

    FUNCTION MATMUL_C_DP(A,B) RESULT(C)
        !Multiplication of complex matrix and complex matrix
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN) :: A(:,:), B(:,:)
        COMPLEX(KIND = 8)             :: C(SIZE(A,1),SIZE(B,2))
        INTEGER                        :: K,L,P

        IF (SIZE(A,2) /= SIZE(B,1)) THEN
            WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
            STOP
        END IF

        C = (0.0_8, 0.0_8)    

        DO K = 1, SIZE(A,1)
            DO L = 1, SIZE(B,2)
                DO P = 1, SIZE(A,2)
                    C(K,L) = C(K,L) + A(K,P)*B(P,L)
                END DO
            END DO
        END DO

    END FUNCTION MATMUL_C_DP

    FUNCTION MATMUL_C_QP(A,B) RESULT(C)
        !Multiplication of complex matrix and complex matrix
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN) :: A(:,:), B(:,:)
        COMPLEX(KIND = 16)             :: C(SIZE(A,1),SIZE(B,2))
        INTEGER                        :: K,L,P

        IF (SIZE(A,2) /= SIZE(B,1)) THEN
            WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
            STOP
        END IF

        C = (0.0_16, 0.0_16)  

        DO K = 1, SIZE(A,1)
            DO L = 1, SIZE(B,2)
                DO P = 1, SIZE(A,2)
                    C(K,L) = C(K,L) + A(K,P)*B(P,L)
                END DO
            END DO
        END DO

    END FUNCTION MATMUL_C_QP

    FUNCTION MATVECMUL_C_DP(A,B) RESULT(C)
        !Multiplication of complex vector and complex matrix
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)   :: A(:,:), B(:)
        COMPLEX(KIND = 8)               :: C(SIZE(B))
        INTEGER                         :: K,P

        IF (SIZE(A,2) /= SIZE(B)) THEN
            WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
            STOP
        END IF

        C = 0.0_8   

        DO K = 1, SIZE(A,1)
            DO P = 1, SIZE(A,2)
                C(K) = C(K) + A(K,P)*B(P)
            END DO
        END DO

    END FUNCTION MATVECMUL_C_DP

    FUNCTION MATVECMUL_C_QP(A,B) RESULT(C)
        !Multiplication of complex vector and complex matrix
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)  :: A(:,:), B(:)
        COMPLEX(KIND = 16)              :: C(SIZE(B))
        INTEGER                         :: K,P

        IF (SIZE(A,2) /= SIZE(B)) THEN
            WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
            STOP
        END IF

        C = 0.0_16   

        DO K = 1, SIZE(A,1)
            DO P = 1, SIZE(A,2)
                C(K) = C(K) + A(K,P)*B(P)
            END DO
        END DO

    END FUNCTION MATVECMUL_C_QP
    
    SUBROUTINE ARRAY_APPEND_I_V_S_SP(array, element)
        !Appends an integer scalar to a vector of integers
        IMPLICIT NONE
        INTEGER, INTENT(INOUT), ALLOCATABLE :: array(:)
        INTEGER, INTENT(IN)                 :: element
        INTEGER, ALLOCATABLE                :: tarray(:)
        INTEGER                             :: array_length, i

        IF (ALLOCATED(array) .EQV. .TRUE.) THEN

            array_length = SIZE(array)

            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length
                tarray(i) = array(i)
            END DO

            tarray(array_length+1) = element

        ELSE
            ALLOCATE(tarray(1))
            tarray(1) = element
        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_I_V_S_SP
    
    SUBROUTINE ARRAY_APPEND_I_V_S_DP(array, element)
        !Appends an integer scalar to a vector of integers
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array(:)
        INTEGER(KIND = 8), INTENT(IN)                 :: element
        INTEGER(KIND = 8), ALLOCATABLE                :: tarray(:)
        INTEGER(KIND = 8)                             :: array_length, i


        IF (ALLOCATED(array) .EQV. .TRUE.) THEN

            array_length = SIZE(array)

            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length
                tarray(i) = array(i)
            END DO

            tarray(array_length+1) = element

        ELSE
            ALLOCATE(tarray(1))
            tarray(1) = element
        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_I_V_S_DP

    SUBROUTINE ARRAY_APPEND_R_V_S_DP(array, element)
        !Appends a real scalar to a vector of reals
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(INOUT), ALLOCATABLE    :: array(:)
        REAL(KIND = 8), INTENT(IN)                    :: element
        REAL(KIND = 8), ALLOCATABLE                   :: tarray(:)
        INTEGER                             :: array_length, i

        array_length = SIZE(array)

        IF (ALLOCATED(array) .EQV. .TRUE.) THEN
            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length  
                tarray(i) = array(i)
            END DO

            tarray(array_length + 1) = element
            
        ELSE
            
            ALLOCATE(tarray(1))
            tarray(1) = element

        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_R_V_S_DP

    SUBROUTINE ARRAY_APPEND_R_V_S_QP(array, element)
        !Appends a real scalar to a vector of reals
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(INOUT), ALLOCATABLE    :: array(:)
        REAL(KIND = 16), INTENT(IN)                    :: element
        REAL(KIND = 16), ALLOCATABLE                   :: tarray(:)
        INTEGER                             :: array_length, i

        array_length = SIZE(array)

        IF (ALLOCATED(array) .EQV. .TRUE.) THEN
            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length  
                tarray(i) = array(i)
            END DO

            tarray(array_length + 1) = element
            
        ELSE
            
            ALLOCATE(tarray(1))
            tarray(1) = element

        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_R_V_S_QP


    SUBROUTINE ARRAY_APPEND_C_V_S_DP(array, element)
        !Appends a complex scalar to a vector of complex values
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array(:)
        COMPLEX(KIND = 8), INTENT(IN)                 :: element
        COMPLEX(KIND = 8), ALLOCATABLE                :: tarray(:)
        INTEGER                             :: array_length, i

        array_length = SIZE(array) + 1

        IF (ALLOCATED(array) .EQV. .TRUE.) THEN
            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length 
                tarray(i) = array(i)
            END DO
            tarray(array_length + 1) = element
        ELSE
            ALLOCATE(tarray(1))
            tarray(1) = element
        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_C_V_S_DP

    SUBROUTINE ARRAY_APPEND_C_V_S_QP(array, element)
        !Appends a complex scalar to a vector of complex values
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(INOUT), ALLOCATABLE :: array(:)
        COMPLEX(KIND = 16), INTENT(IN)                 :: element
        COMPLEX(KIND = 16), ALLOCATABLE                :: tarray(:)
        INTEGER                             :: array_length, i

        array_length = SIZE(array) + 1

        IF (ALLOCATED(array) .EQV. .TRUE.) THEN
            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length 
                tarray(i) = array(i)
            END DO
            tarray(array_length + 1) = element
        ELSE
            ALLOCATE(tarray(1))
            tarray(1) = element
        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_C_V_S_QP

    SUBROUTINE ARRAY_APPEND_I_V_V_SP(array_1, array_2)
        !Appends a vector of integers to a vector of integers
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        INTEGER, INTENT(INOUT), ALLOCATABLE :: array_1(:)
        INTEGER, INTENT(IN)                 :: array_2(:)
        INTEGER, ALLOCATABLE                :: tarray(:)
        INTEGER                             :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_I_V_V_SP

    SUBROUTINE ARRAY_APPEND_I_V_V_DP(array_1, array_2)
        !Appends a vector of integers to a vector of integers
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array_1(:)
        INTEGER(KIND = 8), INTENT(IN)                 :: array_2(:)
        INTEGER(KIND = 8), ALLOCATABLE                :: tarray(:)
        INTEGER(KIND = 8)                             :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_I_V_V_DP

    SUBROUTINE ARRAY_APPEND_R_V_V_DP(array_1, array_2)
        !Appends a vector of reals to a vector of reals
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(INOUT), ALLOCATABLE  :: array_1(:)
        REAL(KIND = 8), INTENT(IN)                  :: array_2(:)
        REAL(KIND = 8), ALLOCATABLE                 :: tarray(:)
        INTEGER                                     :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO
        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_R_V_V_DP

SUBROUTINE ARRAY_APPEND_R_V_V_QP(array_1, array_2)
        !Appends a vector of reals to a vector of reals
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(INOUT), ALLOCATABLE :: array_1(:)
        REAL(KIND = 16), INTENT(IN)                 :: array_2(:)
        REAL(KIND = 16), ALLOCATABLE                :: tarray(:)
        INTEGER                                     :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO
        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_R_V_V_QP

    SUBROUTINE ARRAY_APPEND_C_V_V_DP(array_1, array_2)
        !Appends a vector of reals to a vector of reals
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array_1(:)
        COMPLEX(KIND = 8), INTENT(IN)                 :: array_2(:)
        COMPLEX(KIND = 8), ALLOCATABLE                :: tarray(:)
        INTEGER                                     :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO
        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_C_V_V_DP

    SUBROUTINE ARRAY_APPEND_C_V_V_QP(array_1, array_2)
        !Appends a vector of reals to a vector of reals
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(INOUT), ALLOCATABLE :: array_1(:)
        COMPLEX(KIND = 16), INTENT(IN)                 :: array_2(:)
        COMPLEX(KIND = 16), ALLOCATABLE                :: tarray(:)
        INTEGER                                     :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO
        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_C_V_V_QP

    SUBROUTINE ARRAY_APPEND_R_M_V_DP(array_1, array_2)
        !Appends a vector of reals to a matrix of reals
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        REAL(KIND = 8), INTENT(IN)                 :: array_2(:)
        REAL(KIND = 8), ALLOCATABLE                :: tarray(:,:)
        INTEGER                                    :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = 0.0_8

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = 0.0_8

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_R_M_V_DP

    SUBROUTINE ARRAY_APPEND_R_M_V_QP(array_1, array_2)
        !Appends a vector of reals to a matrix of reals
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        REAL(KIND = 16), INTENT(IN)                 :: array_2(:)
        REAL(KIND = 16), ALLOCATABLE                :: tarray(:,:)
        INTEGER                                    :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = 0.0_16

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = 0.0_16

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_R_M_V_QP

    SUBROUTINE ARRAY_APPEND_C_M_V_DP(array_1, array_2)
        !Appends a vector of complex numbers to a matrix of complex numbers
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        COMPLEX(KIND = 8), INTENT(IN)                 :: array_2(:)
        COMPLEX(KIND = 8), ALLOCATABLE                :: tarray(:,:)
        INTEGER                                       :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = (0.0_8, 0.0_8)

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = (0.0_8, 0.0_8)

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_C_M_V_DP

    SUBROUTINE ARRAY_APPEND_C_M_V_QP(array_1, array_2)
        !Appends a vector of complex numbers to a matrix of complex numbers
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        COMPLEX(KIND = 16), INTENT(IN)                 :: array_2(:)
        COMPLEX(KIND = 16), ALLOCATABLE                :: tarray(:,:)
        INTEGER                                       :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = (0.0_16, 0.0_16)

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = (0.0_16, 0.0_16)

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_C_M_V_QP

    SUBROUTINE ARRAY_APPEND_I_M_V_SP(array_1, array_2)
        !Appends a vector of integers to a matrix of integers
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        INTEGER, INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        INTEGER, INTENT(IN)                 :: array_2(:)
        INTEGER, ALLOCATABLE                :: tarray(:,:)
        INTEGER                             :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = 0

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = 0

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_I_M_V_SP

    SUBROUTINE ARRAY_APPEND_I_M_V_DP(array_1, array_2)
        !Appends a vector of integers to a matrix of integers
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        INTEGER(KIND = 8), INTENT(IN)                 :: array_2(:)
        INTEGER(KIND = 8), ALLOCATABLE                :: tarray(:,:)
        INTEGER(KIND = 8)                             :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = 0

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = 0

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_I_M_V_DP


    FUNCTION FLIPUD_R_V_QP(input) RESULT(output)
        !Flips a vector of quadruple precision reals 
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)       :: input(:)
        REAL(KIND = 16)                   :: output(SIZE(input))
        INTEGER                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_R_V_QP

    FUNCTION FLIPUD_R_V_DP(input) RESULT(output)
        !Flips a vector of double precision reals 
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)        :: input(:)
        REAL(KIND = 8)                    :: output(SIZE(input))
        INTEGER                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_R_V_DP

    FUNCTION FLIPUD_C_V_QP(input) RESULT(output)
        !Flips a vector of quadruple precision complexes
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)    :: input(:)
        COMPLEX(KIND = 16)                :: output(SIZE(input))
        INTEGER                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_C_V_QP

    FUNCTION FLIPUD_C_V_DP(input) RESULT(output)
        !Flips a vector of double precision complexes
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)     :: input(:)
        COMPLEX(KIND = 8)                 :: output(SIZE(input))
        INTEGER                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_C_V_DP

    FUNCTION FLIPUD_I_V_SP(input) RESULT(output)
        !Flips a vector of single precision integers
        IMPLICIT NONE
        INTEGER, INTENT(IN)               :: input(:)
        INTEGER                           :: output(SIZE(input))
        INTEGER                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_I_V_SP

    FUNCTION FLIPUD_I_V_DP(input) RESULT(output)
        !Flips a vector of double precision integers
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(IN)               :: input(:)
        INTEGER(KIND = 8)                           :: output(SIZE(input))
        INTEGER(KIND = 8)                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_I_V_DP

    FUNCTION DIAG_MV_R_DP(input) RESULT(output)
        ! Returns diagonal entries of a double precision real array
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN) :: input(:,:)
        REAL(KIND = 8)             :: output(SIZE(input,1))
        INTEGER                    :: row

        DO row = 1, SIZE(input,1)
            output(row) = input(row, row)
        END DO

    END FUNCTION DIAG_MV_R_DP

    FUNCTION DIAG_MV_R_QP(input) RESULT(output)
        ! Returns diagonal entries of a quadruple precision real array
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN) :: input(:,:)
        REAL(KIND = 16)             :: output(SIZE(input,1))
        INTEGER                     :: row

        DO row = 1, SIZE(input,1)
            output(row) = input(row, row)
        END DO

    END FUNCTION DIAG_MV_R_QP

    FUNCTION DIAG_MV_C_DP(input) RESULT(output)
        ! Returns diagonal entries of a double precision complex array
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN) :: input(:,:)
        COMPLEX(KIND = 8)             :: output(SIZE(input,1))
        INTEGER                       :: row

        DO row = 1, SIZE(input,1)
            output(row) = input(row, row)
        END DO

    END FUNCTION DIAG_MV_C_DP

    FUNCTION DIAG_MV_C_QP(input) RESULT(output)
        ! Returns diagonal entries of a quadruple precision complex array
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN) :: input(:,:)
        COMPLEX(KIND = 16)             :: output(SIZE(input,1))
        INTEGER                        :: row

        DO row = 1, SIZE(input,1)
            output(row) = input(row, row)
        END DO

    END FUNCTION DIAG_MV_C_QP

    FUNCTION DIAG_MV_I_SP(input) RESULT(output)
        ! Returns diagonal entries of a single precision integer array
        IMPLICIT NONE
        INTEGER(KIND = 4), INTENT(IN) :: input(:,:)
        INTEGER(KIND = 4)             :: output(SIZE(input,1))
        INTEGER                       :: row

        DO row = 1, SIZE(input,1)
            output(row) = input(row, row)
        END DO

    END FUNCTION DIAG_MV_I_SP

    FUNCTION DIAG_MV_I_DP(input) RESULT(output)
        ! Returns diagonal entries of a double precision integer array
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(IN) :: input(:,:)
        INTEGER(KIND = 8)             :: output(SIZE(input,1))
        INTEGER                       :: row

        DO row = 1, SIZE(input,1)
            output(row) = input(row, row)
        END DO

    END FUNCTION DIAG_MV_I_DP

    FUNCTION DIAG_VM_R_DP(input) RESULT(output)
        ! Creates diagonal matrix from input 1D real double precision array
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN) :: input(:)
        REAL(KIND = 8)             :: output(SIZE(input,1), SIZE(input,1))
        INTEGER                    :: row

        output = 0.0_8

        DO row = 1, SIZE(input,1)
            output(row,row) = input(row)
        END DO

    END FUNCTION DIAG_VM_R_DP

    FUNCTION DIAG_VM_R_QP(input) RESULT(output)
        ! Creates diagonal matrix from input 1D real quadruple precision array
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN) :: input(:)
        REAL(KIND = 16)             :: output(SIZE(input,1), SIZE(input,1))
        INTEGER                     :: row

        output = 0.0_16

        DO row = 1, SIZE(input,1)
            output(row,row) = input(row)
        END DO

    END FUNCTION DIAG_VM_R_QP

    FUNCTION DIAG_VM_C_DP(input) RESULT(output)
        ! Creates diagonal matrix from input 1D complex double precision array
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN) :: input(:)
        COMPLEX(KIND = 8)             :: output(SIZE(input,1), SIZE(input,1))
        INTEGER                       :: row

        output = (0.0_8, 0.0_8)

        DO row = 1, SIZE(input,1)
            output(row,row) = input(row)
        END DO

    END FUNCTION DIAG_VM_C_DP

    FUNCTION DIAG_VM_C_QP(input) RESULT(output)
        ! Creates diagonal matrix from input 1D complex quadruple precision array
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN) :: input(:)
        COMPLEX(KIND = 16)             :: output(SIZE(input,1), SIZE(input,1))
        INTEGER                        :: row

        output = (0.0_16, 0.0_16)

        DO row = 1, SIZE(input,1)
            output(row,row) = input(row)
        END DO

    END FUNCTION DIAG_VM_C_QP

    FUNCTION DIAG_VM_I_SP(input) RESULT(output)
        ! Creates diagonal matrix from input 1D integer single precision array
        IMPLICIT NONE
        INTEGER(KIND = 4), INTENT(IN) :: input(:)
        INTEGER(KIND = 4)             :: output(SIZE(input,1), SIZE(input,1))
        INTEGER                       :: row

        output = 0

        DO row = 1, SIZE(input,1)
            output(row,row) = input(row)
        END DO

    END FUNCTION DIAG_VM_I_SP


    FUNCTION DIAG_VM_I_DP(input) RESULT(output)
        ! Creates diagonal matrix from input 1D integer DOBULE precision array
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(IN) :: input(:)
        INTEGER(KIND = 8)             :: output(SIZE(input,1), SIZE(input,1))
        INTEGER                       :: row

        output = 0

        DO row = 1, SIZE(input,1)
            output(row,row) = input(row)
        END DO

    END FUNCTION DIAG_VM_I_DP

    FUNCTION EUC_NORM_R_DP(vector) RESULT(out)
        !Calculates the euclidian norm of a double precision real vector
        REAL(KIND = 8)  :: out, vector(:)
        INTEGER         :: i

        out = 0.0_8
        DO i = 1,size(vector)
              out = out + vector(i)*vector(i)
        END DO

        out = sqrt(out)
    END FUNCTION EUC_NORM_R_DP
    
    FUNCTION EUC_NORM_R_QP(vector) RESULT(out)
        !Calculates the euclidian norm of a quadruple precision real vector
        REAL(KIND = 16) :: out, vector(:)
        INTEGER         :: i

        out = 0.0_16
        DO i = 1,size(vector)
              out = out + vector(i)*vector(i)
        END DO

        out = sqrt(out)
    END FUNCTION EUC_NORM_R_QP

    FUNCTION ANGLE_R_DP(v,u) RESULT(out)
        !Calculates the angle between two double precision real vectors
        REAL(KIND = 8), INTENT(IN)  :: v(:), u(:)
        REAL(KIND = 8)              :: out
        INTEGER                     :: i

        out = 0.0_8
        IF(size(v) /= size(u)) THEN
              write(6,'(A)') "Vectors of unequal length in call to angle"
              STOP
        ELSE IF(all(u == 0.0_8) .OR. all(v == 0.0_8)) THEN
              !write(6,'(A)') "One vector is null in call to angle"
              RETURN
        END IF

        DO i = 1, size(v)
              out = out + v(i)*u(i)
        END DO

        out = out/(radial(v)*radial(u))
        out = acos(out)
    END FUNCTION ANGLE_R_DP
    
    FUNCTION ANGLE_R_QP(v,u) RESULT(out)
        !Calculates the angle between two quadruple precision real vectors
        REAL(KIND = 16), INTENT(IN)  :: v(:), u(:)
        REAL(KIND = 16)              :: out
        INTEGER                     :: i

        out = 0.0_16
        IF(size(v) /= size(u)) THEN
              write(6,'(A)') "Vectors of unequal length in call to angle"
              STOP
        ELSE IF(all(u == 0.0_16) .OR. all(v == 0.0_16)) THEN
              !write(6,'(A)') "One vector is null in call to angle"
              RETURN
        END IF

        DO i = 1, size(v)
              out = out + v(i)*u(i)
        END DO

        out = out/(radial(v)*radial(u))
        out = acos(out)
    END FUNCTION ANGLE_R_QP

    FUNCTION CHECK_HERMITIAN_DP(matrix) RESULT(out)
        !Returns SUM(matrix * matrix^+) for a double precision matrix of complexes
        IMPLICIT NONE
        COMPLEX(KIND = 8) :: matrix(:,:)
        REAL(KIND = 8)    :: temp(size(matrix,1), size(matrix,2))
        REAL(KIND = 8)    :: out

        temp = abs(conjg(transpose(matrix)) - matrix)
        out = sum(temp)

    END FUNCTION CHECK_HERMITIAN_DP
    
    FUNCTION CHECK_HERMITIAN_QP(matrix) RESULT(out)
        !Returns SUM(matrix * matrix^+) for a quadruple precision matrix of complexes
        IMPLICIT NONE
        COMPLEX(KIND = 16) :: matrix(:,:)
        REAL(KIND = 16)    :: temp(size(matrix,1), size(matrix,2))
        REAL(KIND = 16)    :: out

        temp = abs(conjg(transpose(matrix)) - matrix)
        out = sum(temp)

    END FUNCTION CHECK_HERMITIAN_QP

    FUNCTION CHECK_SYMMETRIC_DP(matrix) RESULT(out)
        !Returns SUM(matrix * matrix^T) for a double precision matrix of reals
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN) :: matrix(:,:)
        REAL(KIND = 8)             :: temp(SIZE(matrix,1), SIZE(matrix,2))
        REAL(KIND = 8)             :: out

        temp = ABS(TRANSPOSE(matrix) - matrix)
        out  = SUM(temp)

    END FUNCTION CHECK_SYMMETRIC_DP
    
    FUNCTION CHECK_SYMMETRIC_QP(matrix) RESULT(out)
        !Returns SUM(matrix * matrix^T) for a quadruple precision matrix of reals
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN) :: matrix(:,:)
        REAL(KIND = 16)             :: temp(SIZE(matrix,1), SIZE(matrix,2))
        REAL(KIND = 16)             :: out

        temp = ABS(TRANSPOSE(matrix) - matrix)
        out  = SUM(temp)

    END FUNCTION CHECK_SYMMETRIC_QP

    SUBROUTINE WRITE_ARRAY_R_DP(matrix, file, fmt, unit)
        ! Writes double precision array of reals to file
        ! INPUT:
        !           matrix - 2D array of double precision reals
        !           file_name - optional - name of output file for matrix
        !           fmt - optional - format of output numbers e.g F15.7 or E32.16E4
        !           unit - optional - unit number to write to - will be opened if closed

        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)                :: matrix(:,:)
        INTEGER, INTENT(IN), OPTIONAL             :: unit
        INTEGER                                   :: row, col, unit_num
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: file
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: fmt
        CHARACTER(LEN = 500)                      :: fmat
        LOGICAL                                   :: opened

        ! Abort if no file name and no unit number
        IF (.NOT. PRESENT(file) .AND. .NOT. PRESENT(unit)) THEN
            WRITE(6,*) 'No file name or unit provided to WRITE_ARRAY, aborting'
            RETURN
        END IF

        ! Check formatting for output
        IF (.NOT. PRESENT(fmt)) THEN
            WRITE(fmat,'(A,I0,A)') '(',SIZE(matrix,2),'F15.7)' 
        ELSE
            WRITE(fmat,'(A,I0,2A)') '(',SIZE(matrix,2),TRIM(ADJUSTL(fmt)),')' 
        END IF

        IF (PRESENT(FILE)) THEN
            OPEN(NEWUNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
        ELSE IF (PRESENT(unit)) THEN
            unit_num = unit
            INQUIRE(unit=unit_num,opened=opened)
            IF (.NOT. opened) THEN
                OPEN(UNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
            ELSE
                CONTINUE
            END IF
        END IF

        DO row = 1, SIZE(MATRIX, 1)
            WRITE(unit_num, fmat) (matrix(row, col), col = 1, SIZE(matrix,2))
        END DO

        CLOSE(unit_num)

    END SUBROUTINE WRITE_ARRAY_R_DP

    SUBROUTINE WRITE_ARRAY_R_QP(matrix, file, fmt, unit)
        ! Writes quadruple precision array of reals to file
        ! INPUT:
        !           matrix - 2D array of double precision reals
        !           file_name - optional - name of output file for matrix
        !           fmt - optional - format of output numbers e.g F15.7 or E32.16E4
        !           unit - optional - unit number to write to - will be opened if closed
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)               :: matrix(:,:)
        INTEGER, INTENT(IN), OPTIONAL             :: unit
        INTEGER                                   :: row, col, unit_num
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: file
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: fmt
        CHARACTER(LEN = 500)                      :: fmat
        LOGICAL                                   :: opened

        ! Abort if no file name and no unit number
        IF (.NOT. PRESENT(file) .AND. .NOT. PRESENT(unit)) THEN
            WRITE(6,*) 'No file name or unit provided to WRITE_ARRAY, aborting'
            RETURN
        END IF

        ! Check formatting for output
        IF (.NOT. PRESENT(fmt)) THEN
            WRITE(fmat,'(A,I0,A)') '(',SIZE(matrix,2),'F15.7)' 
        ELSE
            WRITE(fmat,'(A,I0,2A)') '(',SIZE(matrix,2),TRIM(ADJUSTL(fmt)),')' 
        END IF

        IF (PRESENT(FILE)) THEN
            OPEN(NEWUNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
        ELSE IF (PRESENT(unit)) THEN
            unit_num = unit
            INQUIRE(unit=unit_num,opened=opened)
            IF (.NOT. opened) THEN
                OPEN(UNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
            ELSE
                CONTINUE
            END IF
        END IF

        DO row = 1, SIZE(MATRIX, 1)
            WRITE(unit_num, fmat) (matrix(row, col), col = 1, SIZE(matrix,2))
        END DO

        CLOSE(unit_num)

    END SUBROUTINE WRITE_ARRAY_R_QP

    SUBROUTINE WRITE_ARRAY_C_DP(matrix, file, fmt, unit)
        ! Writes quadruple precision array of complex numbers to file
        ! Two blocks are written, real (top) and imaginary (bottom)
        ! INPUT:
        !           matrix - 2D array of double precision reals
        !           file_name - optional - name of output file for matrix
        !           fmt - optional - format of output numbers e.g F15.7 or E32.16E4
        !           unit - optional - unit number to write to - will be opened if closed
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)             :: matrix(:,:)
        INTEGER, INTENT(IN), OPTIONAL             :: unit
        INTEGER                                   :: row, col, unit_num
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: file
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: fmt
        CHARACTER(LEN = 500)                      :: fmat
        LOGICAL                                   :: opened

        ! Abort if no file name and no unit number
        IF (.NOT. PRESENT(file) .AND. .NOT. PRESENT(unit)) THEN
            WRITE(6,*) 'No file name or unit provided to WRITE_ARRAY, aborting'
            RETURN
        END IF

        ! Check formatting for output
        IF (.NOT. PRESENT(fmt)) THEN
            WRITE(fmat,'(A,I0,A)') '(',SIZE(matrix,2),'F15.7)' 
        ELSE
            WRITE(fmat,'(A,I0,2A)') '(',SIZE(matrix,2),TRIM(ADJUSTL(fmt)),')' 
        END IF

        IF (PRESENT(FILE)) THEN
            OPEN(NEWUNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
        ELSE IF (PRESENT(unit)) THEN
            unit_num = unit
            INQUIRE(unit=unit_num,opened=opened)
            IF (.NOT. opened) THEN
                OPEN(UNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
            ELSE
                CONTINUE
            END IF
        END IF

        DO row = 1, SIZE(MATRIX, 1)
            WRITE(unit_num, fmat) (REAL(matrix(row, col), 8), col = 1, SIZE(matrix,2))
        END DO
        WRITE(unit_num, *)
        DO row = 1, SIZE(MATRIX, 1)
            WRITE(unit_num, fmat) (AIMAG(matrix(row, col)), col = 1, SIZE(matrix,2))
        END DO

        CLOSE(unit_num)

    END SUBROUTINE WRITE_ARRAY_C_DP

    SUBROUTINE WRITE_ARRAY_C_QP(matrix, file, fmt, unit)
        ! Writes quadruple precision array of complex numbers to file
        ! Two blocks are written, real (top) and imaginary (bottom)
        ! INPUT:
        !           matrix - 2D array of double precision reals
        !           file_name - optional - name of output file for matrix
        !           fmt - optional - format of output numbers e.g F15.7 or E32.16E4
        !           unit - optional - unit number to write to - will be opened if closed
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)            :: matrix(:,:)
        INTEGER, INTENT(IN), OPTIONAL             :: unit
        INTEGER                                   :: row, col, unit_num
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: file
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: fmt
        CHARACTER(LEN = 500)                      :: fmat
        LOGICAL                                   :: opened

        ! Abort if no file name and no unit number
        IF (.NOT. PRESENT(file) .AND. .NOT. PRESENT(unit)) THEN
            WRITE(6,*) 'No file name or unit provided to WRITE_ARRAY, aborting'
            RETURN
        END IF

        ! Check formatting for output
        IF (.NOT. PRESENT(fmt)) THEN
            WRITE(fmat,'(A,I0,A)') '(',SIZE(matrix,2),'F15.7)' 
        ELSE
            WRITE(fmat,'(A,I0,2A)') '(',SIZE(matrix,2),TRIM(ADJUSTL(fmt)),')' 
        END IF

        IF (PRESENT(FILE)) THEN
            OPEN(NEWUNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
        ELSE IF (PRESENT(unit)) THEN
            unit_num = unit
            INQUIRE(unit=unit_num,opened=opened)
            IF (.NOT. opened) THEN
                OPEN(UNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
            ELSE
                CONTINUE
            END IF
        END IF

        DO row = 1, SIZE(MATRIX, 1)
            WRITE(unit_num, fmat) (REAL(matrix(row, col), 16), col = 1, SIZE(matrix,2))
        END DO
        WRITE(unit_num, *)
        DO row = 1, SIZE(MATRIX, 1)
            WRITE(unit_num, fmat) (AIMAG(matrix(row, col)), col = 1, SIZE(matrix,2))
        END DO

        CLOSE(unit_num)

    END SUBROUTINE WRITE_ARRAY_C_QP

    SUBROUTINE WRITE_ARRAY_I_SP(matrix, file, fmt, unit)
        ! Writes single precision array of integers to file
        ! INPUT:
        !           matrix - 2D array of double precision reals
        !           file_name - optional - name of output file for matrix
        !           fmt - optional - format of output numbers e.g F15.7 or E32.16E4
        !           unit - optional - unit number to write to - will be opened if closed
        IMPLICIT NONE
        INTEGER(KIND = 4), INTENT(IN)             :: matrix(:,:)
        INTEGER, INTENT(IN), OPTIONAL             :: unit
        INTEGER                                   :: row, col, unit_num
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: file
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: fmt
        CHARACTER(LEN = 500)                      :: fmat
        LOGICAL                                   :: opened

        ! Abort if no file name and no unit number
        IF (.NOT. PRESENT(file) .AND. .NOT. PRESENT(unit)) THEN
            WRITE(6,*) 'No file name or unit provided to WRITE_ARRAY, aborting'
            RETURN
        END IF

        ! Check formatting for output
        IF (.NOT. PRESENT(fmt)) THEN
            WRITE(fmat,'(A,I0,A)') '(',SIZE(matrix,2),'I6)' 
        ELSE
            WRITE(fmat,'(A,I0,2A)') '(',SIZE(matrix,2),TRIM(ADJUSTL(fmt)),')' 
        END IF

        IF (PRESENT(FILE)) THEN
            OPEN(NEWUNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
        ELSE IF (PRESENT(unit)) THEN
            unit_num = unit
            INQUIRE(unit=unit_num,opened=opened)
            IF (.NOT. opened) THEN
                OPEN(UNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
            ELSE
                CONTINUE
            END IF
        END IF

        DO row = 1, SIZE(MATRIX, 1)
            WRITE(unit_num, fmat) (matrix(row, col), col = 1, SIZE(matrix,2))
        END DO

        CLOSE(unit_num)

    END SUBROUTINE WRITE_ARRAY_I_SP

    SUBROUTINE WRITE_ARRAY_I_DP(matrix, file, fmt, unit)
        ! Writes single precision array of integers to file
        ! INPUT:
        !           matrix - 2D array of double precision reals
        !           file_name - optional - name of output file for matrix
        !           fmt - optional - format of output numbers e.g F15.7 or E32.16E4
        !           unit - optional - unit number to write to - will be opened if closed
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(IN)             :: matrix(:,:)
        INTEGER, INTENT(IN), OPTIONAL             :: unit
        INTEGER                                   :: row, col, unit_num
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: file
        CHARACTER(LEN = *), INTENT(IN) , OPTIONAL :: fmt
        CHARACTER(LEN = 500)                      :: fmat
        LOGICAL                                   :: opened

        ! Abort if no file name and no unit number
        IF (.NOT. PRESENT(file) .AND. .NOT. PRESENT(unit)) THEN
            WRITE(6,*) 'No file name or unit provided to WRITE_ARRAY, aborting'
            RETURN
        END IF

        ! Check formatting for output
        IF (.NOT. PRESENT(fmt)) THEN
            WRITE(fmat,'(A,I0,A)') '(',SIZE(matrix,2),'I6)' 
        ELSE
            WRITE(fmat,'(A,I0,2A)') '(',SIZE(matrix,2),TRIM(ADJUSTL(fmt)),')' 
        END IF

        IF (PRESENT(FILE)) THEN
            OPEN(NEWUNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
        ELSE IF (PRESENT(unit)) THEN
            unit_num = unit
            INQUIRE(unit=unit_num,opened=opened)
            IF (.NOT. opened) THEN
                OPEN(UNIT = unit_num, FILE = TRIM(ADJUSTL(file)), STATUS='UNKNOWN')
            ELSE
                CONTINUE
            END IF
        END IF

        DO row = 1, SIZE(MATRIX, 1)
            WRITE(unit_num, fmat) (matrix(row, col), col = 1, SIZE(matrix,2))
        END DO

        CLOSE(unit_num)

    END SUBROUTINE WRITE_ARRAY_I_DP

    FUNCTION MAT_POWER_R_DP(matrix, power) RESULT(out)
        ! Calculates matrix power for double precision array of reals
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN) :: matrix(:,:)
        REAL(KIND = 8)             :: out(SIZE(matrix,1), SIZE(matrix,2))
        INTEGER, INTENT(IN)        :: power
        INTEGER                    :: i

        out = ALT_MATMUL(matrix,matrix)

        IF (power == 2) RETURN

        DO i = 1, power - 2

            out = ALT_MATMUL(out,matrix)

        END DO

    END FUNCTION MAT_POWER_R_DP

    FUNCTION MAT_POWER_R_QP(matrix, power) RESULT(out)
        ! Calculates matrix power for double precision array of reals
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN) :: matrix(:,:)
        REAL(KIND = 16)             :: out(SIZE(matrix,1), SIZE(matrix,2))
        INTEGER, INTENT(IN)         :: power
        INTEGER                     :: i

        out = ALT_MATMUL(matrix,matrix)

        IF (power == 2) RETURN

        DO i = 1, power - 2

            out = ALT_MATMUL(out,matrix)

        END DO

    END FUNCTION MAT_POWER_R_QP

    FUNCTION MAT_POWER_C_DP(matrix, power) RESULT(out)
        ! Calculates matrix power for double precision array of reals
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN) :: matrix(:,:)
        COMPLEX(KIND = 8)             :: out(SIZE(matrix,1), SIZE(matrix,2))
        INTEGER, INTENT(IN)           :: power
        INTEGER                       :: i

        out = ALT_MATMUL(matrix,matrix)

        IF (power == 2) RETURN

        DO i = 1, power - 2

            out = ALT_MATMUL(out,matrix)

        END DO

    END FUNCTION MAT_POWER_C_DP

    FUNCTION MAT_POWER_C_QP(matrix, power) RESULT(out)
        ! Calculates matrix power for double precision array of reals
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN) :: matrix(:,:)
        COMPLEX(KIND = 16)             :: out(SIZE(matrix,1), SIZE(matrix,2))
        INTEGER, INTENT(IN)            :: power
        INTEGER                        :: i

        out = ALT_MATMUL(matrix,matrix)

        IF (power == 2) RETURN

        DO i = 1, power - 2

            out = ALT_MATMUL(out,matrix)

        END DO

    END FUNCTION MAT_POWER_C_QP

    FUNCTION FACTORIAL(in) RESULT(out)
        !Calculates factorial of 
        !$n!$
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(IN) :: in
        INTEGER(KIND = 8)             :: out, n

        IF (in == 0) THEN
            out = 0
            RETURN
        END IF

        out = 1
        n = in

        DO WHILE (n /= 0)
            out = out * n
            n   = n - 1
        END DO

    END FUNCTION FACTORIAL

    FUNCTION NCR(n, r) RESULT(c)
        !Calulates the number of combinations of objects (n) in a sample (r)
        !$C(n,r) = \frac{n!}{r!(n-r)!}$
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: n, r
        INTEGER(KIND = 8)   :: top, bot, answer, c

        top = FACTORIAL(INT(n,8))
        bot = FACTORIAL(INT(r,8)) * FACTORIAL(INT(n - r,8))
        answer = top/bot

        c = answer


    END FUNCTION NCR

    FUNCTION NPR(N, R) RESULT(P)
        !Calulates the number of permutations of objects (n) in a sample (r)
        !$P(n,r) = \frac{n!}{(n-r)!}$
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: N, R
        INTEGER(KIND = 8)   :: TOP, BOT, ANSWER, P

        TOP = FACTORIAL(INT(N,8))
        WRITE(6,*) TOP
        BOT = FACTORIAL(INT(N - R,8))
        ANSWER = TOP/BOT
        P = ANSWER

    END FUNCTION NPR

FUNCTION LSAME(CA,CB)
      IMPLICIT NONE
      CHARACTER(LEN = *) :: CA,CB
      LOGICAL            :: LSAME
      INTEGER            :: INTA, INTB, ZCODE
      
      LSAME = CA .EQ. CB
      IF (LSAME) RETURN
      ZCODE = ICHAR('Z')
      INTA = ICHAR(CA)
      INTB = ICHAR(CB)

      IF (ZCODE.EQ.90 .OR. ZCODE.EQ.122) THEN
        IF (INTA.GE.97 .AND. INTA.LE.122) INTA = INTA - 32
        IF (INTB.GE.97 .AND. INTB.LE.122) INTB = INTB - 32
      ELSE IF (ZCODE.EQ.233 .OR. ZCODE.EQ.169) THEN
        IF (INTA.GE.129 .AND. INTA.LE.137 .OR. INTA.GE.145 .AND. INTA.LE.153 .OR. INTA.GE.162 .AND. INTA.LE.169) INTA = INTA + 64
        IF (INTB.GE.129 .AND. INTB.LE.137 .OR. INTB.GE.145 .AND. INTB.LE.153 .OR. INTB.GE.162 .AND. INTB.LE.169) INTB = INTB + 64
      ELSE IF (ZCODE.EQ.218 .OR. ZCODE.EQ.250) THEN
        IF (INTA.GE.225 .AND. INTA.LE.250) INTA = INTA - 32
        IF (INTB.GE.225 .AND. INTB.LE.250) INTB = INTB - 32
      END IF

      LSAME = INTA .EQ. INTB
      
    END FUNCTION LSAME
    
    SUBROUTINE XERBLA( SRNAME, INFO )
      IMPLICIT NONE
      CHARACTER(LEN = *) ::  SRNAME
      INTEGER            ::  INFO

      WRITE(6,'(A,A,A,I0,A)') ' ** On entry to ', SRNAME(1:LEN_TRIM( SRNAME )), ' parameter number ', INFO, ' had an illegal value'
      STOP
    END SUBROUTINE XERBLA
    
    SUBROUTINE QGEMM(TRANSA,TRANSB,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)
      IMPLICIT NONE
      REAL(KIND = 16)             :: ALPHA ,BETA
      INTEGER                     :: K, LDA, LDB, LDC, M, N
      CHARACTER(LEN = 1)          :: TRANSA, TRANSB
      REAL(KIND = 16)             :: A(LDA,*), B(LDB,*), C(LDC,*)
      REAL(KIND = 16)             :: TEMP
      INTEGER                     :: I, INFO, J, L, NCOLA, NROWA, NROWB
      LOGICAL                     :: NOTA, NOTB
      REAL(KIND = 16),PARAMETER   :: ONE=1.0_16, ZERO=0.0_16
    
      NOTA = LSAME(TRANSA,'N')
      NOTB = LSAME(TRANSB,'N')
      IF (NOTA) THEN
        NROWA = M
        NCOLA = K
      ELSE
        NROWA = K
        NCOLA = M
      END IF
      IF (NOTB) THEN
        NROWB = K
      ELSE
        NROWB = N
      END IF
      
      INFO = 0
      IF ((.NOT.NOTA) .AND. (.NOT.LSAME(TRANSA,'C')) .AND. (.NOT.LSAME(TRANSA,'T'))) THEN
        INFO = 1
      ELSE IF ((.NOT.NOTB) .AND. (.NOT.LSAME(TRANSB,'C')) .AND. (.NOT.LSAME(TRANSB,'T'))) THEN
        INFO = 2
      ELSE IF (M.LT.0) THEN
        INFO = 3
      ELSE IF (N.LT.0) THEN
        INFO = 4
      ELSE IF (K.LT.0) THEN
        INFO = 5
      ELSE IF (LDA.LT.MAX(1,NROWA)) THEN
        INFO = 8
      ELSE IF (LDB.LT.MAX(1,NROWB)) THEN
        INFO = 10
      ELSE IF (LDC.LT.MAX(1,M)) THEN
        INFO = 13
      END IF
      IF (INFO.NE.0) THEN
        CALL XERBLA('QGEMM ',INFO)
        RETURN
      END IF
      IF ((M.EQ.0) .OR. (N.EQ.0) .OR. (((ALPHA.EQ.ZERO).OR. (K.EQ.0)).AND. (BETA.EQ.ONE))) RETURN
      IF (ALPHA.EQ.ZERO) THEN
        IF (BETA.EQ.ZERO) THEN
            DO J = 1,N
                DO I = 1,M
                    C(I,J) = ZERO
                END DO
            END DO
        ELSE
            DO J = 1,N
                DO I = 1,M
                    C(I,J) = BETA*C(I,J)
                END DO
            END DO
        END IF
        RETURN
      END IF
      IF (NOTB) THEN
        IF (NOTA) THEN
            DO J = 1,N
                IF (BETA.EQ.ZERO) THEN
                    DO I = 1,M
                        C(I,J) = ZERO
                    END DO
                ELSE IF (BETA.NE.ONE) THEN
                    DO I = 1,M
                        C(I,J) = BETA*C(I,J)
                    END DO
                END IF
                DO L = 1,K
                    TEMP = ALPHA*B(L,J)
                    DO I = 1,M
                        C(I,J) = C(I,J) + TEMP*A(I,L)
                    END DO
                END DO
            END DO
        ELSE
            DO J = 1,N
                DO I = 1,M
                    TEMP = ZERO
                    DO L = 1,K
                        TEMP = TEMP + A(L,I)*B(L,J)
                    END DO
                    IF (BETA.EQ.ZERO) THEN
                        C(I,J) = ALPHA*TEMP
                    ELSE
                        C(I,J) = ALPHA*TEMP + BETA*C(I,J)
                    END IF
                END DO
            END DO
        END IF
      ELSE
        IF (NOTA) THEN
            DO J = 1,N
                IF (BETA.EQ.ZERO) THEN
                    DO I = 1,M
                        C(I,J) = ZERO
                    END DO
                ELSE IF (BETA.NE.ONE) THEN
                    DO I = 1,M
                        C(I,J) = BETA*C(I,J)
                    END DO
                END IF
                DO L = 1,K
                    TEMP = ALPHA*B(J,L)
                    DO I = 1,M
                        C(I,J) = C(I,J) + TEMP*A(I,L)
                    END DO
                END DO
            END DO
        ELSE
            DO J = 1,N
                DO I = 1,M
                    TEMP = ZERO
                    DO L = 1,K
                        TEMP = TEMP + A(L,I)*B(J,L)
                    END DO
                    IF (BETA.EQ.ZERO) THEN
                        C(I,J) = ALPHA*TEMP
                    ELSE
                        C(I,J) = ALPHA*TEMP + BETA*C(I,J)
                    END IF
                END DO
            END DO
        END IF
      END IF
    END SUBROUTINE QGEMM
    
    SUBROUTINE ZQGEMM(TRANSA,TRANSB,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)
      IMPLICIT NONE
      COMPLEX(KIND = 16)            :: ALPHA, BETA
      INTEGER                       :: K, LDA, LDB, LDC, M, N
      CHARACTER(LEN = 1)            :: TRANSA, TRANSB
      COMPLEX(KIND = 16)            :: A(LDA,*), B(LDB,*), C(LDC,*)
      COMPLEX(KIND = 16)            :: TEMP
      INTEGER                       :: I, INFO, J, L, NCOLA, NROWA, NROWB
      LOGICAL                       :: CONJA, CONJB, NOTA, NOTB
      COMPLEX(KIND = 16), PARAMETER :: ONE = (1.0_16,0.0_16), ZERO = (0.0_16,0.0_16)
      
      NOTA = LSAME(TRANSA,'N')
      NOTB = LSAME(TRANSB,'N')
      CONJA = LSAME(TRANSA,'C')
      CONJB = LSAME(TRANSB,'C')
      IF (NOTA) THEN
        NROWA = M
        NCOLA = K
      ELSE
        NROWA = K
        NCOLA = M
      END IF
      IF (NOTB) THEN
        NROWB = K
      ELSE
        NROWB = N
      END IF
      
      INFO = 0
      IF ((.NOT.NOTA) .AND. (.NOT.CONJA) .AND.     (.NOT.LSAME(TRANSA,'T'))) THEN
        INFO = 1
      ELSE IF ((.NOT.NOTB) .AND. (.NOT.CONJB) .AND.          (.NOT.LSAME(TRANSB,'T'))) THEN
        INFO = 2
      ELSE IF (M.LT.0) THEN
        INFO = 3
      ELSE IF (N.LT.0) THEN
        INFO = 4
      ELSE IF (K.LT.0) THEN
        INFO = 5
      ELSE IF (LDA.LT.MAX(1,NROWA)) THEN
        INFO = 8
      ELSE IF (LDB.LT.MAX(1,NROWB)) THEN
        INFO = 10
      ELSE IF (LDC.LT.MAX(1,M)) THEN
        INFO = 13
      END IF
      IF (INFO.NE.0) THEN
        CALL XERBLA('QGEMM ',INFO)
        RETURN
      END IF
      
      IF ((M.EQ.0) .OR. (N.EQ.0) .OR. (((ALPHA.EQ.ZERO).OR. (K.EQ.0)).AND. (BETA.EQ.ONE))) RETURN
      
      IF (ALPHA.EQ.ZERO) THEN
        IF (BETA.EQ.ZERO) THEN
            DO J = 1,N
                DO I = 1,M
                    C(I,J) = ZERO
                END DO
            END DO
        ELSE
            DO J = 1,N
                DO I = 1,M
                    C(I,J) = BETA*C(I,J)
                END DO
            END DO
        END IF
        RETURN
      END IF
      IF (NOTB) THEN
        IF (NOTA) THEN
            DO J = 1,N
                IF (BETA.EQ.ZERO) THEN
                    DO I = 1,M
                        C(I,J) = ZERO
                    END DO
                ELSE IF (BETA.NE.ONE) THEN
                    DO I = 1,M
                        C(I,J) = BETA*C(I,J)
                    END DO
                END IF
                DO L = 1,K
                    TEMP = ALPHA*B(L,J)
                    DO I = 1,M
                        C(I,J) = C(I,J) + TEMP*A(I,L)
                    END DO
                END DO
            END DO
        ELSE IF (CONJA) THEN
            DO J = 1,N
                DO I = 1,M
                    TEMP = ZERO
                    DO L = 1,K
                        TEMP = TEMP + CONJG(A(L,I))*B(L,J)
                    END DO
                    IF (BETA.EQ.ZERO) THEN
                        C(I,J) = ALPHA*TEMP
                    ELSE
                        C(I,J) = ALPHA*TEMP + BETA*C(I,J)
                    END IF
                END DO
            END DO
        ELSE
            DO J = 1,N
                DO I = 1,M
                    TEMP = ZERO
                    DO L = 1,K
                        TEMP = TEMP + A(L,I)*B(L,J)
                    END DO
                    IF (BETA.EQ.ZERO) THEN
                        C(I,J) = ALPHA*TEMP
                    ELSE
                        C(I,J) = ALPHA*TEMP + BETA*C(I,J)
                    END IF
                END DO
             END DO
        END IF
      ELSE IF (NOTA) THEN
        IF (CONJB) THEN
            DO J = 1,N
                IF (BETA.EQ.ZERO) THEN
                    DO I = 1,M
                        C(I,J) = ZERO
                    END DO
                ELSE IF (BETA.NE.ONE) THEN
                    DO I = 1,M
                        C(I,J) = BETA*C(I,J)
                    END DO
                END IF
                DO L = 1,K
                    TEMP = ALPHA*CONJG(B(J,L))
                    DO I = 1,M
                        C(I,J) = C(I,J) + TEMP*A(I,L)
                    END DO
                END DO
            END DO
        ELSE
            DO J = 1,N
                IF (BETA.EQ.ZERO) THEN
                    DO I = 1,M
                        C(I,J) = ZERO
                    END DO
                ELSE IF (BETA.NE.ONE) THEN
                    DO I = 1,M
                        C(I,J) = BETA*C(I,J)
                    END DO
                END IF
                DO L = 1,K
                    TEMP = ALPHA*B(J,L)
                    DO I = 1,M
                        C(I,J) = C(I,J) + TEMP*A(I,L)
                    END DO
                END DO
            END DO
        END IF
      ELSE IF (CONJA) THEN
        IF (CONJB) THEN
            DO J = 1,N
                DO I = 1,M
                    TEMP = ZERO
                    DO L = 1,K
                        TEMP = TEMP + CONJG(A(L,I))*CONJG(B(J,L))
                    END DO
                    IF (BETA.EQ.ZERO) THEN
                        C(I,J) = ALPHA*TEMP
                    ELSE
                        C(I,J) = ALPHA*TEMP + BETA*C(I,J)
                    END IF
                END DO
            END DO
        ELSE
            DO J = 1,N
                DO I = 1,M
                    TEMP = ZERO
                    DO L = 1,K
                        TEMP = TEMP + CONJG(A(L,I))*B(J,L)
                    END DO
                    IF (BETA.EQ.ZERO) THEN
                        C(I,J) = ALPHA*TEMP
                    ELSE
                        C(I,J) = ALPHA*TEMP + BETA*C(I,J)
                    END IF
                END DO
            END DO
        END IF
      ELSE
        IF (CONJB) THEN
            DO J = 1,N
                DO I = 1,M
                    TEMP = ZERO
                    DO L = 1,K
                        TEMP = TEMP + A(L,I)*CONJG(B(J,L))
                    END DO
                    IF (BETA.EQ.ZERO) THEN
                        C(I,J) = ALPHA*TEMP
                    ELSE
                        C(I,J) = ALPHA*TEMP + BETA*C(I,J)
                    END IF
                END DO
            END DO
        ELSE
            DO J = 1,N
                DO I = 1,M
                    TEMP = ZERO
                    DO L = 1,K
                        TEMP = TEMP + A(L,I)*B(J,L)
                    END DO
                    IF (BETA.EQ.ZERO) THEN
                        C(I,J) = ALPHA*TEMP
                    ELSE
                        C(I,J) = ALPHA*TEMP + BETA*C(I,J)
                    END IF
                END DO
            END DO
        END IF
      END IF
    END SUBROUTINE ZQGEMM

END MODULE matrix_tools