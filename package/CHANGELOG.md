# CHANGELOG


## v1.33.1 (2025-02-21)

### Build System

- Add pandas module to setup.py
  ([`1b3b2b2`](https://gitlab.com/chilton-group/molcas_suite/-/commit/1b3b2b268fa4f9f3f7a51f9a54db0e48db2596b0))


## v1.33.0 (2025-02-21)

### Continuous Integration

- Update build variables
  ([`a1fe0a3`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a1fe0a327ee9d094fbc176d962294a874416dc80))

- Update changelog location [skip-ci]
  ([`037d5c8`](https://gitlab.com/chilton-group/molcas_suite/-/commit/037d5c836b489de295a8c12a5d4fb2c01d3c528b))

### Features

- Evaluate optical properties from OpenMolcas rassi HDF5 output
  ([`037e9ac`](https://gitlab.com/chilton-group/molcas_suite/-/commit/037e9ac7c8d3f6ff501019d3de31930a24c1e475))


## v1.32.4 (2024-11-29)

### Bug Fixes

- Raised MOLCAS_DISK to 200GB on HPCs
  ([`bc8171f`](https://gitlab.com/chilton-group/molcas_suite/-/commit/bc8171f1378eb9d394bbe62ab4a2b9926d360070))


## v1.32.3 (2024-09-04)

### Bug Fixes

- Orbs --help, escape % in string
  ([`ddca309`](https://gitlab.com/chilton-group/molcas_suite/-/commit/ddca309ca47f099192168a8105c3daaafe849d61))


## v1.32.2 (2024-08-13)

### Bug Fixes

- Update default openmolcas module on gadi
  ([`60abd72`](https://gitlab.com/chilton-group/molcas_suite/-/commit/60abd72da3fdad02178db4e3a3d820d6c24a1480))


## v1.32.1 (2024-08-09)

### Bug Fixes

- Update default module
  ([`ca1c6bf`](https://gitlab.com/chilton-group/molcas_suite/-/commit/ca1c6bf7f38af547428e6b46ab884831a209b987))

Updated the default molcas module loaded in the submission script on the csf3 - new version 24.06


## v1.32.0 (2024-05-03)

### Bug Fixes

- Molcas_mem is correctly set when requesting multiple cores
  ([`abcfea1`](https://gitlab.com/chilton-group/molcas_suite/-/commit/abcfea1698dd490f923f5d5da1608d31f2cf93f0))

### Build System

- Bump xyz_py requirement
  ([`5d86f6f`](https://gitlab.com/chilton-group/molcas_suite/-/commit/5d86f6f7833dd328ade8cae6adefe014f78ee3a8))

Completes previous commit ...

### Features

- Gadi support
  ([`cd351c4`](https://gitlab.com/chilton-group/molcas_suite/-/commit/cd351c4405a5d1e8ccdcd9af9f86e2feb35d87d3))

- Gadi/pbs support
  ([`bee8900`](https://gitlab.com/chilton-group/molcas_suite/-/commit/bee89000c7a7d25fcca65748660a16c92f1341bb))


## v1.31.1 (2024-04-30)

### Bug Fixes

- Skip xyz checks
  ([`54b3044`](https://gitlab.com/chilton-group/molcas_suite/-/commit/54b3044fedd509f9a1e65bec24a5258e424cec90))

Allows for predefined basis set labels present in the xyz coordinate file.


## v1.31.0 (2024-04-10)

### Features

- Allow printing total components of orbtials in orbs via --total_component flag
  ([`e22db62`](https://gitlab.com/chilton-group/molcas_suite/-/commit/e22db62ae38a567a83f41683cfd1890eeb84db96))


## v1.30.0 (2024-02-06)

### Continuous Integration

- Add "Docs" badge
  ([`55ba7c7`](https://gitlab.com/chilton-group/molcas_suite/-/commit/55ba7c7afb34c2eb2b849f1db757591919b51c0d))

### Features

- Added filtering by energy, occupation and orbital index to
  ([`a288c61`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a288c616678e7cc4fad22d95da47552aa8d8188e))

### Refactoring

- Cosmetic changes
  ([`fb14936`](https://gitlab.com/chilton-group/molcas_suite/-/commit/fb1493694300d17fd4b678113698702d60539be1))

Rename e_range to ener_range for clarity and make linter happy.


## v1.28.0 (2023-11-01)

### Build System

- Bump angmom_suite version
  ([`1ea4d34`](https://gitlab.com/chilton-group/molcas_suite/-/commit/1ea4d343bdc1c386a88f5f434f94f9f2b994bdab))

Halfway to removing all angmom_suite dependecies ...

### Refactoring

- Move proj middle-end to angmom_suite
  ([`0fd47e6`](https://gitlab.com/chilton-group/molcas_suite/-/commit/0fd47e630e2b99fe167294c5ba68828e365a40c3))

Move ProjectModelHamiltonianMolcas class body to angmom_suite. Next step: move front-end.


## v1.27.0 (2023-10-24)

### Features

- Root specific orbs, generation of rasorb file from h5 file
  ([`137b260`](https://gitlab.com/chilton-group/molcas_suite/-/commit/137b26036ce2d52e2d6eeac6b9ff1c09889d6e88))

Add optional --root keyword to molcas_suite orbs that prints root specific orbitals. This must be
  followed by an integer specifying the root, based on the order they appear in the h5 file
  (ascending energy). If this is 0 or not specified, average orbitals will be printed. Add
  molcas_suite generate_rasorb, which generates a RasOrb file from an h5 file. This requires names
  of the input h5 file and output RasOrb file to be specified, in that order. It has the optional
  keyword --root, behaving the same as for molcas_suite orbs. It has the optional keywords --alpha
  and --beta, specifying that these orbitals should be written instead of natural orbitals for UHF
  data.


## v1.26.0 (2023-10-02)

### Features

- Update default openmolcas module on csf3 + 4 to 23.02
  ([`4bc2ba8`](https://gitlab.com/chilton-group/molcas_suite/-/commit/4bc2ba8cb8120b16aa1abad79d6f92358af8c5a7))

update default openmolcas module on csf3 + 4 to: chiltongroup/openmolcas/23.02


## v1.25.0 (2023-08-01)

### Bug Fixes

- Remove EJob keyword in default rassi input, remove RasOrb.n and SpdOrb.n saving, set MAXORB=1 in
  default rasscf input
  ([`6e058f0`](https://gitlab.com/chilton-group/molcas_suite/-/commit/6e058f08f051b66f3ead51c19299331389b122e1))

### Features

- Orbital extractor
  ([`b2e7b31`](https://gitlab.com/chilton-group/molcas_suite/-/commit/b2e7b31aab550c078d1a15b3115366fb8397e39e))

Extractor for orbital energies, occupation numbers and coefficients


## v1.24.1 (2023-07-06)

### Build System

- Bump angmom_suite version
  ([`6ba6830`](https://gitlab.com/chilton-group/molcas_suite/-/commit/6ba6830566cfb8827004a84e3e4a2b2d21757b65))

Depends on angmom_suite with new group code


## v1.24.0 (2023-07-05)

### Build System

- Bump angmom_suite dependence
  ([`4adefce`](https://gitlab.com/chilton-group/molcas_suite/-/commit/4adefce8ba16684012644df9130aa5c9edf385b3))

Update backend

### Features

- Bring back SOC term print-out
  ([`43ce5d3`](https://gitlab.com/chilton-group/molcas_suite/-/commit/43ce5d31a1fcf4b6ca5cffdca7e48b3e9337b9d2))

Also comes with state energies now!


## v1.23.0 (2023-06-01)

### Build System

- Update xyz_py version
  ([`9664dde`](https://gitlab.com/chilton-group/molcas_suite/-/commit/9664dde850eb3c166635ef6dead0a86d88283630))

Updated xyz_py library raises an error for malformed xyz files.

### Features

- Kirkwood keyword
  ([`cd58afb`](https://gitlab.com/chilton-group/molcas_suite/-/commit/cd58afb1ed7711987629efd32b109248f91f42b2))

Implements kirkwood keyword for front and back end.


## v1.22.0 (2023-05-28)

### Features

- By default turn of symmetry
  ([`fe8e883`](https://gitlab.com/chilton-group/molcas_suite/-/commit/fe8e883525dd66f39ee1b301bd53a3a8de97fb00))

Symmetry is turned off by default. Can be turned on in special cases by editing the input file
  manually.


## v1.21.0 (2023-03-07)

### Build System

- Bump angmom_suite version
  ([`848b32b`](https://gitlab.com/chilton-group/molcas_suite/-/commit/848b32b3830574c287a34332335375ecd28983db))

### Features

- Epot/efld/fldg extractor
  ([`a2b855c`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a2b855c31099b4ae8df349f5a8f42d00a78d0a2b))

Implement EPOT/EFLD/FLDG extractor for the RASSCF section


## v1.20.0 (2023-02-20)

### Build System

- Bump suite versions
  ([`bc08dda`](https://gitlab.com/chilton-group/molcas_suite/-/commit/bc08dda4001c5f77571e2118dac17badd3d3b940))

update angmom_suite and hpc_suite

### Refactoring

- Modernise cli
  ([`44b3a1c`](https://gitlab.com/chilton-group/molcas_suite/-/commit/44b3a1cb11aca127bdbd75d5d9becfe46c8ca104))

argments to use more modern version of ordered-selection action

- Unify backend
  ([`c8af440`](https://gitlab.com/chilton-group/molcas_suite/-/commit/c8af440316fb0617911e1225cd199ccabdcae9c0))

implement ops argument for evaluate() function


## v1.19.0 (2023-01-27)

### Features

- Use FileOrb and Typeindex
  ([`d239537`](https://gitlab.com/chilton-group/molcas_suite/-/commit/d239537fe6d7bfcc4d05830d66883e7a32ec306e))

FileOrb and Typeindex keywords are used appropriately to restart from orbitals and subspace of
  previous calculation.


## v1.18.1 (2023-01-19)

### Bug Fixes

- Include day(s) into pattern
  ([`9a7ee93`](https://gitlab.com/chilton-group/molcas_suite/-/commit/9a7ee9305096e34591738d714a74522272dba7e7))

Including day(s) into timings pattern matches modules with runtime > 1 day correctly.


## v1.18.0 (2022-12-15)

### Features

- Enable caspt2 section writing
  ([`15dbf4b`](https://gitlab.com/chilton-group/molcas_suite/-/commit/15dbf4b59d434b30a8668db887193f3b752519e5))

Caspt2 section is written when requested.

### Refactoring

- Avoid default overwrite of coordinates
  ([`05c222a`](https://gitlab.com/chilton-group/molcas_suite/-/commit/05c222a2ce470fe6857d0880b35b53263430c9f7))

coordinates with inserted basis labels saved at a new location.

- Modernise boolean args and bump python version to 3.9
  ([`ab87662`](https://gitlab.com/chilton-group/molcas_suite/-/commit/ab876627f45cee3b2561acd4e3a274bcef561203))

Python version requirement is increased to 3.9 (most of the other suites already are) to allow for
  boolean argparse action.


## v1.17.1 (2022-10-07)

### Bug Fixes

- Correct file pointers
  ([`7ed4f65`](https://gitlab.com/chilton-group/molcas_suite/-/commit/7ed4f65aafa66040a9340b327ef449340cdd0cfc))

coordinate keyword and xfield keyword were missing ${CurrDir}


## v1.17.0 (2022-10-06)

### Features

- Add additional quax options
  ([`0649993`](https://gitlab.com/chilton-group/molcas_suite/-/commit/0649993076e635a685b4bf003858907295200d63))

quax can now also be defined through text files or x, y, z.

### Refactoring

- Clean up cli arguments
  ([`62cc6ac`](https://gitlab.com/chilton-group/molcas_suite/-/commit/62cc6ac07f7453559a98480631b7bdfc17038134))

improve help and delete unnecessary arguments.


## v1.16.3 (2022-10-04)

### Bug Fixes

- Move ricd back to gateway
  ([`a7dd92a`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a7dd92a4151683cbfee097fa8bb6b7ba7adee22c))

RICD keyword is forbidden in seward if gateway is used.


## v1.16.2 (2022-10-03)

### Bug Fixes

- Decomp keyword
  ([`e95c3fe`](https://gitlab.com/chilton-group/molcas_suite/-/commit/e95c3fe1bc576c567c7431be0b836c039d45afd3))

correct indention

- Move decomp to seward section
  ([`4b96f85`](https://gitlab.com/chilton-group/molcas_suite/-/commit/4b96f8595f404184a7650409f515bd08e33f8d90))

resolves bug with "High Cholesky" being placed into the Gateway section.

### Refactoring

- Enable list/tuple arguments for __str__ method
  ([`32cddf9`](https://gitlab.com/chilton-group/molcas_suite/-/commit/32cddf9ce76e27465585d07ff4ca07ffacf60ab2))

Multiline arguments can be now passed to the str method


## v1.16.1 (2022-09-29)

### Bug Fixes

- Bug in rassi section
  ([`4d78110`](https://gitlab.com/chilton-group/molcas_suite/-/commit/4d78110e8cb13a7ded1d9dbfbc8f08c5ce3a365f))

IPHN keyword has to appear after NR OF JOBIPH.


## v1.16.0 (2022-09-29)

### Features

- Refactor basis label generation and enable predifined labels, wip
  ([`6f76642`](https://gitlab.com/chilton-group/molcas_suite/-/commit/6f76642eab261e1de803d71253d086527a7a4586))

The basis generation functionality is moved to its own function and predefined molcas basis set
  labels are allowed in the xyz coordinates.

### Refactoring

- Bug fixes, wip
  ([`97d7035`](https://gitlab.com/chilton-group/molcas_suite/-/commit/97d703526d30d0880bfc022b417163d660e722a2))

Fix major bugs introduced during initial implementation.

- Fix bugs
  ([`ada7997`](https://gitlab.com/chilton-group/molcas_suite/-/commit/ada79979d2a4f133c1aab49040c1d682bd0f5d00))

Fix bugs introduced in previous commits.

- Move to molcas input classes, wip
  ([`9449fba`](https://gitlab.com/chilton-group/molcas_suite/-/commit/9449fbae95de475c3cc8c261c4ea6fb7fae48a6d))

Move all input sections to Molcas input classes.


## v1.15.1 (2022-09-22)

### Bug Fixes

- Add missing newline
  ([`b19eb5e`](https://gitlab.com/chilton-group/molcas_suite/-/commit/b19eb5e4d2ec484b9d413685a192320a3580cb59))


## v1.15.0 (2022-09-22)

### Bug Fixes

- Correct atom labels in input file
  ([`36cb362`](https://gitlab.com/chilton-group/molcas_suite/-/commit/36cb362907146214e8209944f1d0c97226296f75))

### Features

- Add option to skip reordering
  ([`64077ca`](https://gitlab.com/chilton-group/molcas_suite/-/commit/64077caec14efbb9d1dcc4389114644c24b67f14))


## v1.14.1 (2022-09-15)

### Bug Fixes

- Updated modules for csf3 and 4
  ([`e1d06a5`](https://gitlab.com/chilton-group/molcas_suite/-/commit/e1d06a547a1941e73925f092b943dbef207787a5))


## v1.14.0 (2022-08-31)

### Build System

- Fix incorrect version number
  ([`74c9bc5`](https://gitlab.com/chilton-group/molcas_suite/-/commit/74c9bc549902095cd9af87e772f5a6608935c5c7))

### Features

- Add option for extra basis xyz file in extended molcas format, and remove xyz_external option
  ([`00d75f2`](https://gitlab.com/chilton-group/molcas_suite/-/commit/00d75f2e5c4d587887aa656a06de0865cb6ed939))


## v1.13.7 (2022-08-22)

### Bug Fixes

- Bugs in generate_input
  ([`60d0413`](https://gitlab.com/chilton-group/molcas_suite/-/commit/60d041306bbcdc574b434b4c5db6ffacc62f8f2f))

job name was not set properly and cosmetic changes.


## v1.13.6 (2022-08-08)

### Bug Fixes

- Update call to xyz_py
  ([`2198f26`](https://gitlab.com/chilton-group/molcas_suite/-/commit/2198f26a146654ae2619ab008f8474bb218a23f0))


## v1.13.5 (2022-08-08)

### Bug Fixes

- Change calls to xyz_py
  ([`8612269`](https://gitlab.com/chilton-group/molcas_suite/-/commit/86122694e2fdc80dcb4c05fa0ddf6117aa9fd322))

- Print state/term composition separately per multiplicity
  ([`6d0720b`](https://gitlab.com/chilton-group/molcas_suite/-/commit/6d0720bfbd8694cf6b4428025a937ca791bfc728))

### Build System

- Bump xyz_py vers
  ([`de06524`](https://gitlab.com/chilton-group/molcas_suite/-/commit/de06524d760a65136348e003d1a615e34d54a339))

### Features

- Add proj program, wip
  ([`1d79804`](https://gitlab.com/chilton-group/molcas_suite/-/commit/1d79804c234a064faef49133bed0c1531506c396))

add completely general spinHam proj functionality.

- Angm irrep content
  ([`55acde6`](https://gitlab.com/chilton-group/molcas_suite/-/commit/55acde641713768533dc4ee0f5f83f503b33883f))

the proj project provides info about angm content of angm operators provided.

- Clean up projection frontend
  ([`c6df0ee`](https://gitlab.com/chilton-group/molcas_suite/-/commit/c6df0ee955a3634e65bb6336c0ad681cf3a6c15b))

Now in usable shape.

- State/term composition
  ([`7dde5e1`](https://gitlab.com/chilton-group/molcas_suite/-/commit/7dde5e1ecadc706ac5a2c6b2c8cb76e56e58d57b))

In addition to number of angmom terms, provide composition of terms/states.

### Refactoring

- Change of default for plotting
  ([`31765aa`](https://gitlab.com/chilton-group/molcas_suite/-/commit/31765aa1cfa207a5724bd5cd364a188c7ad8c9d7))

Default argument was changed in the backend.

- Clean up
  ([`14dfc08`](https://gitlab.com/chilton-group/molcas_suite/-/commit/14dfc089d7bf68c98df99b369cf1bf2f1ebca2a9))

clean up of the backend.

- Revert quaxAction for proj and cli programmes
  ([`dca50e5`](https://gitlab.com/chilton-group/molcas_suite/-/commit/dca50e59b20e0d5c69acd42497d3e60842205546))

Disables use of molcas output for quax argument.

- Spin Hamiltonian Projection
  ([`594343b`](https://gitlab.com/chilton-group/molcas_suite/-/commit/594343b41fe31769d20837d66c21cebd9cde0ef7))

Pushing projection code to the angmom_suite backend.


## v1.13.4 (2022-06-13)

### Build System

- Bump angmom_suite version number
  ([`60dc397`](https://gitlab.com/chilton-group/molcas_suite/-/commit/60dc397025472beb195fcd33f47c310289d27e4e))

### Features

- Migrate quax input from molcas output
  ([`c1fb5f7`](https://gitlab.com/chilton-group/molcas_suite/-/commit/c1fb5f7853ee1c60df7a91abc7d7a07a11493a80))

Resolves circular import issue in angmom_suite.


## v1.13.3 (2022-06-08)

### Build System

- Activate docs pipeline only when version number updated
  ([`27b0cad`](https://gitlab.com/chilton-group/molcas_suite/-/commit/27b0cad57ae0b8678bfa4088d0c2f655730e9209))

- Remove gitlab package registry upload
  ([`ef39ae1`](https://gitlab.com/chilton-group/molcas_suite/-/commit/ef39ae1f0d615c33b71938781a3af44db141686c))

- Remove old pypi file
  ([`5975fbc`](https://gitlab.com/chilton-group/molcas_suite/-/commit/5975fbc66404ec7fbb88aad9395a29b6bcaf11c2))

- Switch to pypi
  ([`d4d11fe`](https://gitlab.com/chilton-group/molcas_suite/-/commit/d4d11fe027db6803aa85362d4a68e3f5ab9b3544))

### Documentation

- Update readme
  ([`8c2e7c5`](https://gitlab.com/chilton-group/molcas_suite/-/commit/8c2e7c513fbf8f1bad68409d88f975094b5f2e92))


## v1.13.2 (2022-05-17)

### Bug Fixes

- Add support for bugged single aniso timing output
  ([`dc4203b`](https://gitlab.com/chilton-group/molcas_suite/-/commit/dc4203bfa8830cebe91fc98cdd1a9fad25c9efc9))


## v1.13.1 (2022-05-09)

### Bug Fixes

- Fix bug in printing index section to file
  ([`deee5a1`](https://gitlab.com/chilton-group/molcas_suite/-/commit/deee5a119294bba478ed8389dee765c933aebc28))


## v1.13.0 (2022-05-06)

### Bug Fixes

- Correct terminal colour bleed over
  ([`bb8920d`](https://gitlab.com/chilton-group/molcas_suite/-/commit/bb8920d0f94cb72beba74a96e07377cb38c837db))

- Correct terminal text colour bleed over
  ([`50adeb8`](https://gitlab.com/chilton-group/molcas_suite/-/commit/50adeb891c60aa582c73d38183fadfa979c11323))

### Code Style

- Improve pep8 compliance
  ([`28480de`](https://gitlab.com/chilton-group/molcas_suite/-/commit/28480ded5c8393232a4b65f63cca1c479964992e))

- Improve pep8 compliance
  ([`71b1811`](https://gitlab.com/chilton-group/molcas_suite/-/commit/71b18115af6596ca1d006f852d9edbbff78ed738))

- Move class parameters docstring to __init__
  ([`dbca816`](https://gitlab.com/chilton-group/molcas_suite/-/commit/dbca816da40acf93207317d617e6e8ef5ce7fb0f))

### Documentation

- Add comments for RasOrb layout
  ([`4fad02a`](https://gitlab.com/chilton-group/molcas_suite/-/commit/4fad02a4497d12f1358301f650f45e2c28dbd278))

- Update ref to group wiki page
  ([`6fceb96`](https://gitlab.com/chilton-group/molcas_suite/-/commit/6fceb96bcd015a037c8546e5621175ac1ed5539e))

### Features

- Add orbital reordering code, and rasorb creation function. update and simplify rotation code
  ([`fa19259`](https://gitlab.com/chilton-group/molcas_suite/-/commit/fa19259fc466569f0ffec82dd7da266cb32a9035))


## v1.12.2 (2022-04-29)

### Bug Fixes

- Remove profile and improve pep8 compliance
  ([`d1c4213`](https://gitlab.com/chilton-group/molcas_suite/-/commit/d1c42130cb38b90e9e909eb6c136a9d3ddd2c588))

### Build System

- Change hpc_suite version dependency
  ([`92b5d8a`](https://gitlab.com/chilton-group/molcas_suite/-/commit/92b5d8aff73ec24f874508fd784fa2949d9cb57a))

### Code Style

- Improve pep8 compliance
  ([`e727f96`](https://gitlab.com/chilton-group/molcas_suite/-/commit/e727f96859ed7f49df210d4223553bf9312b6b9e))

- Remove blank lines
  ([`1cc1f81`](https://gitlab.com/chilton-group/molcas_suite/-/commit/1cc1f818967aef52ec1ebbbdf2b608147aa3e5b3))

### Documentation

- Add hdf5 module load to csf custom install guide
  ([`161c37f`](https://gitlab.com/chilton-group/molcas_suite/-/commit/161c37f73dbf295bbe54e4dafc7e3a3e6172d5aa))


## v1.12.1 (2022-04-11)

### Build System

- Update angmom_suite dependency
  ([`0a20f06`](https://gitlab.com/chilton-group/molcas_suite/-/commit/0a20f0668964dd9dfaee3cf72341507139ac7992))

angmom_suite 1.7.3 -> 1.7.5


## v1.12.0 (2022-04-11)

### Features

- Add cli arg and option to limit number of rasorb files saved to disk
  ([`89939ba`](https://gitlab.com/chilton-group/molcas_suite/-/commit/89939ba91f6dd0a7e3123c1f4af793dbf77bc0ef))

- Modify max_orb to support 0 and add warning for >100 rasorb files
  ([`039326c`](https://gitlab.com/chilton-group/molcas_suite/-/commit/039326c394a4f954b60497f5933e9809493273b5))

### Refactoring

- Minor cosmetic changes
  ([`ce24a3a`](https://gitlab.com/chilton-group/molcas_suite/-/commit/ce24a3a9866ed477839bfa99ec7009cfe86485e4))


## v1.11.1 (2022-04-06)

### Bug Fixes

- Add cutoff for k_max of 12
  ([`6b9759f`](https://gitlab.com/chilton-group/molcas_suite/-/commit/6b9759fd0fd4dced5b25943dff04cedcf9e8cd27))


## v1.11.0 (2022-04-05)

### Build System

- Bump angmom_suite version
  ([`07ef981`](https://gitlab.com/chilton-group/molcas_suite/-/commit/07ef981eb6d566617dc66c97d6e80b56c4fe2915))

angmom_suite version at 1.7.3 now

### Features

- Add support for actinides and 1st row transition metals to crys keyword, and tidy up docstrings
  ([`199d7a2`](https://gitlab.com/chilton-group/molcas_suite/-/commit/199d7a2d95440d1c3f1c371fe2ed6a927952ed33))

### Refactoring

- Clean up warning message print style
  ([`2726b63`](https://gitlab.com/chilton-group/molcas_suite/-/commit/2726b6382e32c440ad644cb6617241dd95c3d591))


## v1.10.1 (2022-04-04)

### Refactoring

- Move barrier dependencies to angmom_suite
  ([`a742643`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a7426434bd607567d1772bf13fda083084f335eb))


## v1.10.0 (2022-04-04)

### Build System

- Bump version number for angmom_suite
  ([`a118a8a`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a118a8a91af6826bd3dc3777799872f929f63fc6))

### Features

- Add option to adjust bz field and add output file to allow comparison of in and out of field CF
  energies
  ([`b897f8a`](https://gitlab.com/chilton-group/molcas_suite/-/commit/b897f8a5ab6898485ae6628a64f175cbae33d4da))


## v1.9.0 (2022-04-04)

### Build System

- Bump angmom_suite version
  ([`d8fe792`](https://gitlab.com/chilton-group/molcas_suite/-/commit/d8fe79208f45f8a0b67c8dc489874573a418a3e3))

Update version of angmom_suite package to 1.7.0 containing the latest Kramers doublet features.

### Features

- Add preconditioning of Kramers doublets
  ([`d01346b`](https://gitlab.com/chilton-group/molcas_suite/-/commit/d01346ba948593ccfc0d7c0b2f22ec29f53d411d))

Kramers doublets can be either split by explicit magnetic field or rotated into eigenstates of Jz at
  zero field.


## v1.8.3 (2022-03-30)

### Build System

- Add angmom_suite version number
  ([`9a7dbfb`](https://gitlab.com/chilton-group/molcas_suite/-/commit/9a7dbfb1d95f54cd3b48cc30306f3244483ac8bb))

- Update dependency versions
  ([`d49cfdf`](https://gitlab.com/chilton-group/molcas_suite/-/commit/d49cfdf746e175d1097d0dd2a8ea2cbe8eb8a6d0))

hpc_suite and angmom_suite version

### Continuous Integration

- Add `only` to avoid pipelines triggering on all branches
  ([`6685a8b`](https://gitlab.com/chilton-group/molcas_suite/-/commit/6685a8b4779fc7d2f83ad5e33b4595e364c39b2c))

- Add more checks to ensure only master branch ci jobs run
  ([`64acd39`](https://gitlab.com/chilton-group/molcas_suite/-/commit/64acd39bd452250966c06ce39ffe4d030bd752b2))

- Remove `only` and add `if` to avoid pipelines triggering on all branches
  ([`5eae264`](https://gitlab.com/chilton-group/molcas_suite/-/commit/5eae2645ee6bceed4fed34d79a5b3600fe2c2f53))

### Features

- Improvements for cfp extractor and fix dependencies
  ([`f32e9f1`](https://gitlab.com/chilton-group/molcas_suite/-/commit/f32e9f1c26d60ba46277b66fbef3b6a0483caeaf))

Deprecate usage of --blk_count and replace solely by --space. Update version of angmom_suite
  requirement.

- Improvements in cfp program
  ([`61f977f`](https://gitlab.com/chilton-group/molcas_suite/-/commit/61f977f9ac396958f0e02b63327e3e53648594b5))

Improved output description. blk_count can be now deduced from input space.


## v1.8.2 (2022-03-28)

### Continuous Integration

- Add more checks to ensure only master branch ci jobs run
  ([`c7944af`](https://gitlab.com/chilton-group/molcas_suite/-/commit/c7944af8eb8874003eb0e1ee3a925a4757d16bb8))


## v1.8.1 (2022-03-28)

### Build System

- Add angmom_suite version number
  ([`ff9c0e0`](https://gitlab.com/chilton-group/molcas_suite/-/commit/ff9c0e04e6144dfa62a6f0b1b8a5ff46debcceba))

### Continuous Integration

- Add `only` to avoid pipelines triggering on all branches
  ([`0af8873`](https://gitlab.com/chilton-group/molcas_suite/-/commit/0af8873b894c71d79f8e45aec240feebfbba898d))

- Remove `only` and add `if` to avoid pipelines triggering on all branches
  ([`851f73f`](https://gitlab.com/chilton-group/molcas_suite/-/commit/851f73f0a89a9d77fe5c8551fd7c288b39d41b6e))


## v1.8.0 (2022-03-21)

### Bug Fixes

- Call parser only once in case of unknown_args parser
  ([`d309378`](https://gitlab.com/chilton-group/molcas_suite/-/commit/d3093789a9a6417fc9e6a37ecd73aabe1c40e528))

Avoid second call to parse method in case of unknown_args subparser.

- Fix cli parsing
  ([`cb508ff`](https://gitlab.com/chilton-group/molcas_suite/-/commit/cb508ff351b25f628ad262e9ae8099726e52b56b))

Add external CFP option parser and quax parsing to use custom action.

### Build System

- Amend setup.py
  ([`db91571`](https://gitlab.com/chilton-group/molcas_suite/-/commit/db915716e65156f3fbe24f44e9db8d3062731da4))

add angmom_suite to requirements.

### Features

- Add cfp evaluator
  ([`1d42158`](https://gitlab.com/chilton-group/molcas_suite/-/commit/1d42158b18149b593634309c28b8772edf3b8511))

Add new functionality to molcas_suite which facilitates CFP evaluation based on rassi.h5 data.


## v1.7.1 (2022-03-14)

### Bug Fixes

- Add MAXORB for nroots>100
  ([`f6664f6`](https://gitlab.com/chilton-group/molcas_suite/-/commit/f6664f6f5826ad072d4ebe7adf716c16796f0d63))

Add the MAXORB keyword to each RASSCF section and set to nroots. This ensures that all RASORB.XX
  files get generated which usually stop at 100.


## v1.7.0 (2022-03-14)

### Bug Fixes

- Revert changes to generate_input
  ([`aed1197`](https://gitlab.com/chilton-group/molcas_suite/-/commit/aed1197c3f729d6aa57ae4109efeb9788418781f))

dissect previous commit to keep integrity of input generation function.

### Continuous Integration

- Ci will only trigger if package directory is changed
  ([`377d096`](https://gitlab.com/chilton-group/molcas_suite/-/commit/377d09659d68be973ee69c52229e1b80720f10df))

### Features

- Add mclr class
  ([`1cb1f55`](https://gitlab.com/chilton-group/molcas_suite/-/commit/1cb1f55b0baf63d1288ac14b4fbe07665e2efba4))

Add Mclr input class

### Refactoring

- Adapt generate_input to new molcas input classes (wip)
  ([`2a49209`](https://gitlab.com/chilton-group/molcas_suite/-/commit/2a49209202d42922f5f359875b550135c10e9150))

WIP

- Clean up input class definitions
  ([`b9bb566`](https://gitlab.com/chilton-group/molcas_suite/-/commit/b9bb566729422005d07537887439dbb4a2416d8c))

Clean up.


## v1.6.1 (2022-03-03)

### Bug Fixes

- Amend pymolcas call
  ([`a0eeea0`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a0eeea03eeb53ce7057101f30b1c5cebd520fff3))

Add two more pymolcas options for older versions of OpenMolcas. Add warning message if system-wide
  pymolcas installation is used.


## v1.6.0 (2022-02-22)

### Continuous Integration

- Switch to chiltron user
  ([`609d863`](https://gitlab.com/chilton-group/molcas_suite/-/commit/609d86310fbd0f667b0953c39a1cd143f4f081c6))

### Features

- Add edipmom extractor
  ([`3a0af55`](https://gitlab.com/chilton-group/molcas_suite/-/commit/3a0af558a5c8299491737fa3cf42377d1e902945))

Add extractor backend and cli options.


## v1.5.0 (2022-02-17)

### Features

- Add coordinate extractor from rassi.h5
  ([`51c4d0d`](https://gitlab.com/chilton-group/molcas_suite/-/commit/51c4d0d2675fc919be9c0b67b345abc3fd91570d))

extracts "center_coordinates" dataset from rassi.h5.


## v1.4.0 (2022-02-14)

### Features

- Add extra parsing for rotate if user includes quotation marks in swap string
  ([`c5bafc9`](https://gitlab.com/chilton-group/molcas_suite/-/commit/c5bafc93a501cdb0a591b4cc560561773729fbf3))


## v1.3.0 (2022-02-04)

### Features

- Fix default filtering
  ([`1558dd1`](https://gitlab.com/chilton-group/molcas_suite/-/commit/1558dd11726e4da8aa18d9c6a9f0c8686962a012))

Default filtered extracted data is now written to the root level of HDF5 files instead of the
  repetition index sub dir.


## v1.2.0 (2022-02-01)

### Chores

- Remove commented out code
  ([`a4b52da`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a4b52daeccae7615e83fd748ebfb0061ad0905c0))

### Documentation

- Update help text for rotate command
  ([`630129a`](https://gitlab.com/chilton-group/molcas_suite/-/commit/630129a768ae0e93eba7e951b69c157cb66a6954))

### Features

- Add rotate command
  ([`522f035`](https://gitlab.com/chilton-group/molcas_suite/-/commit/522f035d0769dc82ea0f3beb95556b468078e373))

add rotate command to move orbitals between active spaces in ASCII orbital files


## v1.1.3 (2022-01-28)


## v1.1.2 (2022-01-28)

### Performance Improvements

- Update xyz_py minimum version to include better label indexing removal
  ([`9b51f09`](https://gitlab.com/chilton-group/molcas_suite/-/commit/9b51f094ecb80caf5d472d06b6901c85757c8213))


## v1.1.1 (2022-01-13)

### Bug Fixes

- Delete extra line
  ([`9108ed6`](https://gitlab.com/chilton-group/molcas_suite/-/commit/9108ed6ab139bc951657f9a57733869f075d0e9b))

Delete extra line which causes errors in cmd line option parsing.


## v1.1.0 (2022-01-12)

### Bug Fixes

- Bug fix in single_aniso extractor
  ([`a061a3f`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a061a3f08f0b24e97c2a1baa20a72db19ebec614))

If QUAX are specified in the molcas input, the program header of the single_aniso section is called
  SINGLE_ANISO_OPEN. Also, the label has been adjusted to "occurrence" to follow the new convention.

- Bugs introduced by recent changes
  ([`fadc441`](https://gitlab.com/chilton-group/molcas_suite/-/commit/fadc4412fbaa605db9e98353d7b46748d23f89c7))

fix bugs that were introduced during implementation of new extractor interface

- Preprocess rassi h5 output
  ([`7fb98c6`](https://gitlab.com/chilton-group/molcas_suite/-/commit/7fb98c68e36a047ce6ce02adf85703bb0b84a6bc))

Output to the *.rassi.h5 file requires processing such as transpose, multiplication with the
  imaginary unit or conjugation. Now, the user does not have to take care of these steps anymore.

### Build System

- Define version number for hpc_suite
  ([`4b26bb2`](https://gitlab.com/chilton-group/molcas_suite/-/commit/4b26bb2cdb15e54b353cb3307a86092d86fcdf09))

Define version of hpc_suite since it incremented to 1.0.0 with implementations required by the new
  extractor.

### Documentation

- Documentation of extractors
  ([`6beb8e6`](https://gitlab.com/chilton-group/molcas_suite/-/commit/6beb8e691971d60f6780d7b915b234983afa7704))

add documentation of extractor interface and classes

### Features

- Wave function spec extractor
  ([`2059d12`](https://gitlab.com/chilton-group/molcas_suite/-/commit/2059d1239e4c3e09dedd3d98ffe49f52998c18b4))

Implementation of wave function specification extractor.

### Refactoring

- Add hdf5 extractors
  ([`489f407`](https://gitlab.com/chilton-group/molcas_suite/-/commit/489f40707792d474c92ae4b4f3d4bb254d36409f))

add RASSCF and RASSI HDF5 extractors

- Add MolcasExtractor base class
  ([`25e86ce`](https://gitlab.com/chilton-group/molcas_suite/-/commit/25e86ce54cd72a9c4a1b5996d4197db35131943f))

The MolcasExtractor base class is provided for convenience. It implements a common header and footer
  structure and extra safety that patterns only match within the same section.

- Clean up extractor data types
  ([`96cf5ba`](https://gitlab.com/chilton-group/molcas_suite/-/commit/96cf5ba5afb3c4031967a091473dde2d67294f2a))

Simplify code for handling of complex valued data.

- Clean up of extractor class definitions
  ([`d111996`](https://gitlab.com/chilton-group/molcas_suite/-/commit/d111996cc163c60bcf8d337b3b423a4172ab3d64))

minor adjustments in extractor class definitions. Now uses data type mixins.

- Clean up redundant code
  ([`a844266`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a84426654eca766f3da61d623d00e89ccc941862))

remove redundant extractor code and clean definitions

- Clean up regex extractors
  ([`393ceb8`](https://gitlab.com/chilton-group/molcas_suite/-/commit/393ceb8e9f53ed14f1537c54d526d9a1cd5ef27a))

Remove lab_pattern labels and attribute. Rename raw_data to array.

- Convert HDF5 attribute extraction
  ([`d6e2947`](https://gitlab.com/chilton-group/molcas_suite/-/commit/d6e2947815412478ec2c929ce7908432483bb763))

add attribute extractor for spin multiplicity

- Improve regex for alaska extractor
  ([`a2e12d2`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a2e12d2efc0a4c7709dd6b66fa8e144c065052fd))

alaska extractor now slightly safer against matching wrong MCLR wave function specification

- Initial refactoring of extractor
  ([`a6ec922`](https://gitlab.com/chilton-group/molcas_suite/-/commit/a6ec9224a9b4809e87c6d04f1dbced0a507a4ada))

introducing filter, grouping per input file and fmt for pretty printing

- Introduce --occ to deal with order-only arguments
  ([`5985a7f`](https://gitlab.com/chilton-group/molcas_suite/-/commit/5985a7f06d288a6e628cfacb774ef2f259d18e93))

For extractor items which are labelled by repetition index, the --occ keyword can be supplied
  similar to the filter keyword for labelled items.

- Introduce regex parser
  ([`f7fa7b1`](https://gitlab.com/chilton-group/molcas_suite/-/commit/f7fa7b1ed51174e3feb8f96fcfee39904c9fe0c7))

convert CFP, QUAX, RASSCF and ALASKA extractor to regex extractor

- Modify docs and adapt class arguments
  ([`d88a2b7`](https://gitlab.com/chilton-group/molcas_suite/-/commit/d88a2b745c056b5c2e8944f6897890e15ea084d5))

In order to make passing arguments between constructors of extractor classes and their documentation
  more clear, constructor parameters are modified and doc strings are adapted.

- Pyparsing tests
  ([`2e2107c`](https://gitlab.com/chilton-group/molcas_suite/-/commit/2e2107c324fc331d7929650ea2bddb3864800d23))

experiments with pyparsing to extract data from plain text, very poor performance

- Use new extractor interface for barrier figure
  ([`8ddc870`](https://gitlab.com/chilton-group/molcas_suite/-/commit/8ddc870952fff4c134e2f3d9fe6a29c3220c47c0))

Replace old CFP extractor wrapper by new interface. This also resolves a bug introduced through the
  new interface.


## v1.0.1 (2021-12-07)

### Bug Fixes

- Use env_var for submit.sh files rather than hard code
  ([`7653c0a`](https://gitlab.com/chilton-group/molcas_suite/-/commit/7653c0a09e4fcf7fbe4d491d7063deabed7a9c14))

Instead of hard-coding the file names, it is more useful to re-use the existing $MOLCAS_PROJECT
  env_var so that the script can be copied and edited for similar runs and only one line needs to be
  edited (not three lines with seven instances)

### Chores

- Fix ci yaml bug
  ([`02e91b6`](https://gitlab.com/chilton-group/molcas_suite/-/commit/02e91b67a83f6df2cd65eb532bc18f0aa76650d7))

Fix bug in ci yaml file syntax

- Separate venvs for jobs and disable pipeline self-triggering
  ([`ddfc141`](https://gitlab.com/chilton-group/molcas_suite/-/commit/ddfc14135b264887cd086ad79921290d45a51b3a))

The package/build and docs steps now have separate virtual environments to avoid incompatible
  dependencies

The commit to the master with new version number caused by a successful package/build step no longer
  triggers another ci/cd pipeline

### Continuous Integration

- Fix bash scripting
  ([`036a1c2`](https://gitlab.com/chilton-group/molcas_suite/-/commit/036a1c2b6c8bd7f3c1f11366cfd9859ab5433520))

### Documentation

- Update docs to point to group wiki and emphasise new install process
  ([`e7d5108`](https://gitlab.com/chilton-group/molcas_suite/-/commit/e7d510857cb01b9c2ab8beb50da9f34f12c17d57))

Remove editable install as default and move to advanced section Add links to group wiki for package
  registry setup and contributing guidelines Add bug reporting section


## v1.0.0 (2021-11-04)

### Chores

- Add new version numbering code
  ([`8947c9a`](https://gitlab.com/chilton-group/molcas_suite/-/commit/8947c9aca8361e58c90b23bb454a79205b338b24))

- Fix mistake in ci/cd configuration file
  ([`61dd819`](https://gitlab.com/chilton-group/molcas_suite/-/commit/61dd819172388ec59e9d8f6a6a7363a075247ed7))

There was a bash error in the ci/cd config yaml file which breaks the manual iteration functionality

- Further updates to forced version iteration
  ([`b31c165`](https://gitlab.com/chilton-group/molcas_suite/-/commit/b31c16518d0771d91c0f46a19ad90cfddf3c2d07))

Finishes implementation of forced major/minor/patch version number iteration in gitlab ci/cd
  configuration file

- Modify gitlab ci/cd config to allow manual version number iteration
  ([`ae899b3`](https://gitlab.com/chilton-group/molcas_suite/-/commit/ae899b349db1c807603f80104f8ea7ced050ac36))
