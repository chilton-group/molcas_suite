#!/bin/bash

homedir=$HOME

rm -rf $HOME/OpenMolcas-src
git clone https://gitlab.com/Molcas/OpenMolcas.git $HOME/OpenMolcas-src
cp patchmolcas.patch $HOME/OpenMolcas-src/patchmolcas.patch

cd $HOME/OpenMolcas-src
git submodule update --init External/grid_it/
git submodule update --init External/NECI
patch -p0 -i patchmolcas.patch

sudo su
source $homedir/.bashrc
mkdir /opt/OpenMolcas-latest
cd /opt/OpenMolcas-latest
cmake -DQCMaquis_UPDATE=ON -DQCMaquis_NAME="Nicholas Chilton" -DQCMaquis_EMAIL="nicholas.chilton@manchester.ac.uk" -DDMRG=ON -DNEVPT2=ON -DLINALG=MKL -DGRID_IT=ON $homedir/OpenMolcas-src
nohup make > make.log &
