#!/bin/bash

homedir=$HOME

git clone https://gitlab.com/Molcas/OpenMolcas.git $HOME/OpenMolcas-src-1911

cd $HOME/OpenMolcas-src-1911
git checkout tags/1601-g4377c3d -b v19.11-1601
git submodule update --init External/grid_it/

sudo su
source $homedir/.bashrc
mkdir /opt/OpenMolcas-1911-1601
cd /opt/OpenMolcas-1911-1601
cmake -DQCMaquis_UPDATE=ON -DQCMaquis_NAME="Nicholas Chilton" -DQCMaquis_EMAIL="nicholas.chilton@manchester.ac.uk" -DDMRG=ON -DNEVPT2=ON -DLINALG=MKL -DGRID_IT=ON $homedir/OpenMolcas-src-1911
nohup make > make.log &
