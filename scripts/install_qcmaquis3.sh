#!/bin/bash

homedir=$HOME

git clone https://gitlab.com/Molcas/OpenMolcas.git $HOME/OpenMolcas-src-2010
cp patchmolcas.patch $HOME/OpenMolcas-src-2010/patchmolcas.patch

cd $HOME/OpenMolcas-src-2010
git checkout qcmaquis-release
patch -p0 -i patchmolcas.patch

sudo su
source $homedir/.bashrc
mkdir /opt/OpenMolcas-qcmaquis3
cd /opt/OpenMolcas-qcmaquis3
cmake -DDMRG=ON -DNEVPT2=ON -DLINALG=MKL $homedir/OpenMolcas-src-2010
nohup make > make.log &
