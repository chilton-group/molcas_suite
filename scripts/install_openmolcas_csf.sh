#!/bin/bash --login
#$ -S /bin/bash
#$ -N mol_build
#$ -cwd
#$ -l short

# Set this variable to either 'tags/TAGNAME' or 'COMMIT-HASH'
# e.g.    to_checkout=tags/v18.09

to_checkout=

# Load modules
module load tools/env/proxy
module load apps/git/2.19.0
module load compilers/intel/18.0.3
module load tools/gcc/cmake/3.16.4
module load libs/intel-17.0/hdf5/1.8.21
module load apps/binapps/anaconda3/2019.07

# Clone repo
git clone https://gitlab.com/Molcas/OpenMolcas.git OpenMolcas-src

# Switch to specified branch
cd OpenMolcas-src
git checkout $to_checkout

# Make build directory andmove there
mkdir build
cd build

# Run cmake to generate Makefile
cmake -DLINALG=MKL -D CMAKE_INSTALL_PREFIX=$HOME/bin/molcas ..

# Compile programs and pipe output to make.log
make > make.log
