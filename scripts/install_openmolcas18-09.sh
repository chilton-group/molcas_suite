#!/bin/bash

homedir=$HOME

git clone https://gitlab.com/Molcas/OpenMolcas.git $HOME/OpenMolcas-src-1809

cd $HOME/OpenMolcas-src-1809
git checkout tags/790-g23d48b3b -b v18.09-790

sudo su
source $homedir/.bashrc
mkdir /opt/OpenMolcas-18-09-790
cd /opt/OpenMolcas-18-09-790
cmake -DLINALG=MKL $homedir/OpenMolcas-src-1809
nohup make > make.log &
