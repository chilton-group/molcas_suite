!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! 
! 
! 
!   molcas_gen.f90 
! 
!   This program is a part of molcas_suite
! 
! 
!   Creates a molcas input file from an xyz file, central_label metal ion, charge, number of orbitals
!   in the active space, number of electrons in the active space, number of donors, and optionally
!   can be run in spin-phonon mode - calculates lowest energy multiplet only and enables RICD ACcd
! 
!   
!      Authors:
!       Jon Kragskow
!       Nicholas Chilton
! 
! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! 

PROGRAM molcas_input_gen
    USE, INTRINSIC :: iso_fortran_env
    IMPLICIT NONE
    CHARACTER(LEN = 300)           :: xyz_file
    CHARACTER(LEN = 6),ALLOCATABLE :: atoms_atomic_label(:)
    CHARACTER(LEN = 3)             :: central_label, molcas_file_header
    REAL(KIND = 8),ALLOCATABLE     :: coords(:,:)
    INTEGER                        :: num_atoms, num_central_choices, central_index, electrons, num_spin_states
    INTEGER, ALLOCATABLE           :: list(:), atoms_atomic_number(:), ordering(:,:), spin_states(:), roots(:), trunc_roots(:)
    ! User configurable variables 
    INTEGER                        :: charge, active_electrons, active_orbitals, num_donors
    LOGICAL                        :: phonon

    ! Read user input
    CALL READ_USER_INPUT(xyz_file, central_label, charge, active_electrons, active_orbitals, num_donors, phonon)

    ! Write deprecation warning
    CALL WARNING('Warning: molcas_gen is deprecated - use openmolcas_gen', 6)

    ! Read in xyz file
    CALL READ_XYZ_FILE(xyz_file, num_atoms, coords, atoms_atomic_label)

    ! Make name for molcas input file - different on each system
    CALL MAKE_MOLCAS_FILE_HEADER(molcas_file_header, central_label, xyz_file)

    ! Relabel atoms, find central atom and all distances to it, and give list which orders atom list by distance to central (small --> big)
    CALL ORGANISE_ATOMS(central_label, central_index, num_central_choices, num_atoms, coords, atoms_atomic_label, atoms_atomic_number, ordering)

    ! Calculate number of spin states, what those spin states are, and how many roots are in each spin state
    CALL ORGANISE_ELECTRONIC(phonon, active_electrons, active_orbitals, roots, trunc_roots, spin_states, num_spin_states)

    ! Write SEWARD section
    CALL WRITE_SEWARD_SECTION(molcas_file_header, central_label, central_index, num_central_choices, num_atoms, coords, atoms_atomic_label, atoms_atomic_number, ordering, num_donors)

    ! Write CASSCF Section
    CALL WRITE_CASSCF_SECTION(molcas_file_header, central_index, atoms_atomic_number, num_spin_states, spin_states, roots, active_electrons, active_orbitals, charge, phonon)

    ! Optionally write CASPT2 section
    !IF (TRIM(CASPT2) /= 'N') THEN
    !    CALL WRITE_CASPT2_SECTION()
    !END IF

    ! Write RASSI section
    CALL WRITE_RASSI_SECTION(molcas_file_header, central_index, num_spin_states, spin_states, trunc_roots, active_electrons, active_orbitals)

    ! Write SINLE_ANISO section
    CALL WRITE_SINGLE_ANISO_SECTION(molcas_file_header, central_label, central_index, atoms_atomic_number, spin_states(1), active_orbitals, phonon)

    ! Write job script for either the CSF or local Chilton group machines depending on preprocessor variables
#ifdef csf
    CALL WRITE_JOB_SCRIPT_CSF(molcas_file_header, num_spin_states)
#endif

#ifdef local
    CALL WRITE_JOB_SCRIPT_CHILTON(molcas_file_header, num_spin_states)
#endif

#ifdef medusa
    CALL WRITE_JOB_SCRIPT_CHILTON(molcas_file_header, num_spin_states)
#endif

    ! Deallocate arrays
    DEALLOCATE(atoms_atomic_label, coords, atoms_atomic_number, ordering)
    
    CONTAINS

    SUBROUTINE READ_USER_INPUT(xyz_file, central_label, charge, active_electrons, active_orbitals, num_donors, phonon)
        USE version, ONLY : PRINT_VERSION
        IMPLICIT NONE
        INTEGER, INTENT(OUT)            :: charge, active_electrons, active_orbitals, num_donors
        LOGICAL, INTENT(OUT)            :: phonon
        CHARACTER(LEN = *), INTENT(OUT) :: xyz_file, central_label
        CHARACTER(LEN = 500)            :: cdummy

        ! Read xyz file name
        CALL GET_COMMAND_ARGUMENT(1,xyz_file)

        ! Print help information
        IF (TRIM(xyz_file) == '-h' .or. TRIM(xyz_file) == '-H' .or. TRIM(xyz_file) == '') THEN
            ! Write deprecation warning
            CALL WARNING('Warning: molcas_gen is deprecated - use openmolcas_gen', 6)
            WRITE(6,'(A)') 'molcas_gen <xyz_file> <element> <charge> <active_electrons> <active_orbitals> <num_donors> [<spin-phonon y/n>] '![<CASPT2 y/n>]
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Creates a molcas input file from an xyz file, central_label metal ion, charge, number of orbitals'
            WRITE(6,'(A)')   'in the active space, number of electrons in the active space, number of donors, and optionally'
            WRITE(6,'(A)')   'can be run in spin-phonon mode - calculates lowest energy multiplet only and enables RICD ACcd'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'xyz_file          : CHARACTER          xyz file of complex                               e.g  dy.xyz'
            WRITE(6,'(A)') 'element           : CHARACTER          metal at centre of complex                        e.g. dy'
            WRITE(6,'(A)') 'charge            : INTEGER            charge on complex                                 e.g  0 for neutral, 1 for +1, -1 for -1'
            WRITE(6,'(A)') 'active_electrons  : INTEGER            number of electrons in active space               e.g  9'
            WRITE(6,'(A)') 'active_orbitals   : INTEGER            number of orbitals in active space                e.g  7'
            WRITE(6,'(A)') 'num_donors        : INTEGER            number of donor atoms connected to central metal  e.g  5'
            WRITE(6,'(A)') 'spin-phonon       * OPTIONAL CHARACTER yes or no to spin-phonon mode                     e.g  y or n'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Example ------ molcas_gen dy.xyz dy 0 9 7 5 n'
            WRITE(6,'(A)') ''
            STOP
        END IF

        ! Print version number
        IF (TRIM(xyz_file) == '-v' .OR. TRIM(xyz_file) == '-version' .OR. TRIM(xyz_file) == 'version') THEN
            CALL PRINT_VERSION('molcas_gen')
            STOP
        END IF

        ! Read in element
        CALL GET_COMMAND_ARGUMENT(2,central_label)

        ! Set first letter of element as uppercase and second as lowercase
        central_label(1:1) = UPPERCASE(central_label(1:1))
        IF (LEN(TRIM(central_label)) > 1) central_label(2:2) = LOWERCASE(central_label(2:2))
        
        ! Read charge
        CALL GET_COMMAND_ARGUMENT(3,cdummy)
        READ(cdummy,*) charge

        ! Read number of active electrons
        CALL GET_COMMAND_ARGUMENT(4,cdummy)
        READ(cdummy,*) active_electrons

        ! Read number of active orbitals
        CALL GET_COMMAND_ARGUMENT(5,cdummy)
        READ(cdummy,*) active_orbitals

        ! Read number of donors to central atom
        CALL GET_COMMAND_ARGUMENT(6,cdummy)
        READ(cdummy,*) num_donors

        ! Spin phonon mode
        CALL GET_COMMAND_ARGUMENT(7,cdummy)
        IF (TRIM(ADJUSTL(cdummy)) == 'y' .OR. TRIM(ADJUSTL(cdummy)) == 'Y') THEN
            phonon = .TRUE.
        ELSE 
            phonon = .FALSE.
        END IF

    END SUBROUTINE READ_USER_INPUT
    
    SUBROUTINE WARNING(title, unit)
        ! Writes a warning header to unit
        ! Three lines
        ! *********
        ! * title *
        ! *********
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN) :: title
        INTEGER, INTENT(IN)            :: unit

        INTEGER                        :: num_l_spaces, num_r_spaces

        num_l_spaces = (70 - LEN_TRIM(title)) / 2
        num_r_spaces = (70 - LEN_TRIM(title)) / 2

        IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
            num_l_spaces = num_l_spaces + 1

        END IF

        WRITE(unit,'(A)')   REPEAT('*',80)
        WRITE(unit, '(5A)') REPEAT('*',5), REPEAT(' ',num_l_spaces), TRIM(title), &
                            REPEAT(' ',num_r_spaces), REPEAT('*',5)
        WRITE(unit,'(A)')   REPEAT('*',80)

    END SUBROUTINE WARNING

    SUBROUTINE READ_XYZ_FILE(xyz_file, num_atoms, coords, atoms_atomic_label)
        ! READS XYZ FILE
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN)               :: xyz_file
        INTEGER, INTENT(OUT)                         :: num_atoms
        INTEGER                                      :: J, reason
        REAL(KIND = 8), ALLOCATABLE, INTENT(OUT)     :: coords(:,:)
        CHARACTER(LEN = *), ALLOCATABLE, INTENT(OUT) :: atoms_atomic_label(:)
        LOGICAL                                      :: file_exists
        

        ! Check xyz file exists
        INQUIRE(FILE = xyz_file, EXIST = file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(3A)') 'Specified file ',TRIM(xyz_file),' Does not exist'
            STOP
        END IF
        
        ! Read input xyz file
        OPEN(30, FILE = TRIM(ADJUSTL(xyz_file)), STATUS = 'UNKNOWN')
        
            ! Read number of atoms_atomic_label
            READ(30, *, IOSTAT = reason) num_atoms
            CALL CHECK_IO(reason)

            ! Read blank space
            READ(30, *, IOSTAT = reason)
            CALL CHECK_IO(reason)

            ! ALLOCATE arrays for xyz coordinates and labels
            ALLOCATE(coords(num_atoms,3),atoms_atomic_label(num_atoms))
            
            ! Read xyz coordinates and labels
            DO J = 1, num_atoms
                READ(30, *, IOSTAT = reason) atoms_atomic_label(J), coords(J,1), coords(J,2), coords(J,3)
                CALL CHECK_IO(reason)
            END DO
        
        ! Close input file
        CLOSE(30)

        
    END SUBROUTINE READ_XYZ_FILE

    SUBROUTINE MAKE_MOLCAS_FILE_HEADER(molcas_file_header, central_label, xyz_file)
        ! Creates header of molcas input file
        ! For local machines this is the name of the central metal ion
        ! For the CSF this is the same as the xyz file header 
        ! this is done because the CSF uses one big scratch directory
        ! whereas our local machines use individual scratch directories for
        ! each job
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(OUT) :: molcas_file_header
        CHARACTER(LEN = *), INTENT(IN)  :: central_label, xyz_file

#ifdef csf
        molcas_file_header = TRIM(xyz_file(1:LEN(TRIM(xyz_file)) - 4))
#endif

#ifdef local
        molcas_file_header = LOWERCASE(central_label)
#endif

#ifdef medusa
        molcas_file_header = LOWERCASE(central_label)
#endif


    END SUBROUTINE MAKE_MOLCAS_FILE_HEADER

    SUBROUTINE CHECK_IO(IOREPORT)
        ! Checks IOSTAT return variable for errors
        IMPLICIT NONE
        INTEGER, INTENT(IN)   :: IOREPORT

        ! Check file isnt corrupted or ends too soon
            IF (IOREPORT > 0)  THEN
                WRITE(6,'(A)') '! ! ! !                         Error reading xyz file                              ! ! ! ! '
                STOP
            ELSE IF (IOREPORT < 0) THEN
                WRITE(6,'(A)') '! ! ! !                Error: Reached end of xyz file too soon                      ! ! ! ! '
                STOP
            END IF

    END SUBROUTINE CHECK_IO


    SUBROUTINE ORGANISE_ATOMS(central_label, central_index, num_central_choices, num_atoms, coords, atoms_atomic_label, atoms_atomic_number, ordering)
        use matrix_tools
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(INOUT)          :: coords(:,:)
        REAL(KIND = 8), ALLOCATABLE            :: radii(:)
        REAL(KIND = 8)                         :: temp_coords(3)
        CHARACTER(LEN = *), INTENT(INOUT)      :: atoms_atomic_label(:), central_label
        INTEGER, INTENT(IN)                    :: num_atoms
        INTEGER, INTENT(OUT)                   :: central_index, num_central_choices
        INTEGER, INTENT(OUT), ALLOCATABLE      :: atoms_atomic_number(:), ordering(:,:)
        INTEGER                                :: i, j, col, counter(118), central_list(1000)

        ! Allocate arrays
        ALLOCATE(radii(num_atoms),ordering(num_atoms,2),atoms_atomic_number(num_atoms))

        ! Zero arrays and counter
        atoms_atomic_number = 0
        radii = 0.0_8
        counter = 0

        ! Loop over atoms and replace each label with a label plus a number
        DO i = 1, num_atoms
            ! Convert atom labels to atomic number and append index number
            CALL NAME_PARSE(atoms_atomic_label(i), atoms_atomic_number, i, counter)
        END DO

        ! Add up the total number of electrons
        electrons = 0
        DO i = 1,118
            electrons = electrons + counter(i)*i
        END DO

        ! Check how many times the central atom appears
        !  and record its location(s)
        num_central_choices = 0
        DO i = 1,num_atoms
            IF (TRIM(UP_TO_NUM(atoms_atomic_label(i))) == TRIM(central_label)) THEN
                num_central_choices = num_central_choices + 1
                central_list(num_central_choices) = i
            END IF
        END DO

        ! Check the central atom exists
        IF (num_central_choices < 1) THEN
            write(6,'(3A)') 'No atoms of ',central_label,' found! '
            STOP
        END IF

        ! If > 1 instances of the central atom are found then ask user
        IF (num_central_choices > 1) THEN
            WRITE(6,'(A)') 'Which atom is the central atom?'
            DO j = 1, num_central_choices
                WRITE(6,'(I4, 3A, 3F14.8)') j, '  ', atoms_atomic_label(central_list(j)), ' ', (coords(central_list(j),col), col = 1, 3)
            END DO
            READ(5,*) central_index
        ELSE
            central_index = central_list(1)
        END IF

        ! Calculate distance of each atom to the central atom
        DO i = 1,num_atoms
            IF (i /= central_index) THEN
                coords(i,:) = coords(i,:) - coords(central_index,:)
                temp_coords = coords(i,:)
                radii(i) =  RADIAL(temp_coords)
            END IF
        END DO

        ! Set coordinates of central atom to (0,0,0)
        coords(central_index,:) = 0.0_8

        ! Set central atom's self-distance to a very big number
        radii(central_index) = 99D9

        ! Sort atoms in so that smallest distance is at the top of the list
        CALL BUBBLE(radii, ordering, 'min')

    END SUBROUTINE ORGANISE_ATOMS

    SUBROUTINE NAME_PARSE(str, atoms_atomic_number, ii, counter)
        ! For a given atom expressed as its atomic symbol:
        !   Converts atomic symbol to atomic number
        !   Keeps a counter of how many of each element there are
        !   Appends the atomic symbol to an index counting how many of this element there are
        !   and which one this is.
        !   e.g. 'Dy' --> 'dy' --> 66 --> counter(66) += 1 --> counter(66) == 2 --> 'Dy'+'2' -- > 'Dy2'
        IMPLICIT NONE
        CHARACTER(LEN = 3)                     :: strN
        CHARACTER(LEN = *), INTENT(INOUT)      :: str
        INTEGER, INTENT(IN)                    :: ii
        INTEGER, INTENT(INOUT)                 :: counter(:), atoms_atomic_number(:)
        INTEGER                                :: ele

        ! Set element as all lowercase and remove any numbering
        str = LOWERCASE(str)

        ! Convert element label to atomic number
        ele = ATOMIC_LABEL_TO_NO(str)

        ! Set atomic number for this atom
        atoms_atomic_number(ii) = ele

        ! Add one to the counter for this element
        counter(ele) = counter(ele) + 1
        
        ! Write the current counter for this element to a string
        ! e.g. 2 --> '2'
        WRITE(strN,'(I0)') counter(ele)

        ! Replace the element label with itself + the current counter for that element
        ! e.g. 'C' --> 'C2'
        str = TRIM(str)//TRIM(strN)

        !Set first letter as uppercase
        str(1:1) = UPPERCASE(str(1:1))

    END SUBROUTINE NAME_PARSE

    SUBROUTINE ORGANISE_ELECTRONIC(phonon, active_electrons, active_orbitals, roots, trunc_roots, spin_states, num_spin_states)
        IMPLICIT NONE
        INTEGER, INTENT(OUT)              :: num_spin_states
        INTEGER, INTENT(OUT), ALLOCATABLE :: roots(:), trunc_roots(:), spin_states(:)
        INTEGER                           :: i, k, lim, high_spin, low_spin
        INTEGER, INTENT(IN)               :: active_electrons, active_orbitals
        LOGICAL, INTENT(IN)               :: phonon


        ! Calculate high and low spin states
        IF (active_electrons <= active_orbitals) THEN
            high_spin = active_electrons + 1
        ELSE
            high_spin = 2*active_orbitals - active_electrons + 1
        END IF
        low_spin = MOD(active_electrons, 2) + 1

        ! Calculate total number of spin states
        IF (phonon .EQV. .TRUE.)  lim = high_spin
        IF (phonon .EQV. .FALSE.) lim = low_spin

        num_spin_states = 0

        DO i = high_spin, lim, -2
            num_spin_states = num_spin_states + 1
        END DO

        ! Create array containing spin values for each spin states
        ALLOCATE(spin_states(num_spin_states))

        DO i = 0, num_spin_states - 1
            spin_states(i + 1) = high_spin - (2 * i)
        END DO

        ! Now calculate the roots of each spin

        ! Allocate and zero the array for the number of roots in each spin state, and 
        !  a truncated number of roots used in RASSI
        ALLOCATE(roots(num_spin_states), trunc_roots(num_spin_states))
        roots = 0
        trunc_roots = 0

        !Loop over each spin state and calculate how many roots there are
        DO i = 1, num_spin_states
            ! for lanthanides just use this list of roots
            IF (atoms_atomic_number(central_index) >= 57 .AND. atoms_atomic_number(central_index) <= 71 .AND. active_orbitals == 7) THEN
                IF (active_electrons == 1 .or. active_electrons == 13) THEN
                    IF (spin_states(i) == 2) THEN
                        roots(i) = 7
                        trunc_roots(i) = 7
                    END IF
                ELSE IF (active_electrons == 2 .or. active_electrons == 12) THEN
                    IF (spin_states(i) == 3) THEN
                        roots(i) = 21
                        trunc_roots(i) = 21                        
                    ELSE IF (spin_states(i) == 1) THEN
                        roots(i) = 28
                        trunc_roots(i) = 28
                    END IF
                ELSE IF (active_electrons == 3 .or. active_electrons == 11) THEN
                    IF (spin_states(i) == 4) THEN
                        roots(i) = 35
                        trunc_roots(i) = 35
                    ELSE IF (spin_states(i) == 2) THEN
                        roots(i) = 112
                        trunc_roots(i) = 112
                    END IF
                ELSE IF (active_electrons == 4 .or. active_electrons == 10) THEN
                    IF (spin_states(i) == 5) THEN
                        roots(i) = 35
                        trunc_roots(i) = 35
                    ELSE IF (spin_states(i) == 3) THEN
                        roots(i) = 210
                        trunc_roots(i) = 159
                    ELSE IF (spin_states(i) == 1) THEN
                        roots(i) = 196
                        trunc_roots(i) = 156
                    END IF
                ELSE IF (active_electrons == 5 .or. active_electrons == 9) THEN
                    IF (spin_states(i) == 6) THEN
                        roots(i) = 21
                        trunc_roots(i) = 21
                    ELSE IF (spin_states(i) == 4) THEN
                        roots(i) = 224
                        trunc_roots(i) = 128
                    ELSE IF (spin_states(i) == 2) THEN
                        roots(i) = 490
                        trunc_roots(i) = 130
                    END IF
                ELSE IF (active_electrons == 6 .or. active_electrons == 8) THEN
                    IF (spin_states(i) == 7) THEN
                        roots(i) = 7
                        trunc_roots(i) = 7
                    ELSE IF (spin_states(i) == 5) THEN
                        roots(i) = 140
                        trunc_roots(i) = 140
                    ELSE IF (spin_states(i) == 3) THEN
                        roots(i) = 472
                        trunc_roots(i) = 195
                    ELSE IF (spin_states(i) == 1) THEN
                        roots(i) = 490
                        trunc_roots(i) = 197
                    END IF
                ELSE IF (active_electrons == 7) THEN
                    IF (spin_states(i) == 8) THEN
                        roots(i) = 1
                        trunc_roots(i) = 1
                    ELSE IF (spin_states(i) == 6) THEN
                        roots(i) = 48
                        trunc_roots(i) = 48
                    ELSE IF (spin_states(i) == 4) THEN
                        roots(i) = 392
                        trunc_roots(i) = 119
                    ELSE IF (spin_states(i) == 2) THEN
                        roots(i) = 784
                        trunc_roots(i) = 113
                    END IF
                END IF
            ELSE
                ! All other elements calculate the number of roots with Weyl's formula
                ! In this case trunc_roots is equal to roots
                k = NINT(DBLE((DBLE(spin_states(i))/DBLE(active_orbitals + 1))*binom(active_orbitals + 1,NINT(0.5_8*active_electrons - (spin_states(i) - 1)*0.5_8))*binom(active_orbitals + 1,NINT(0.5_8*active_electrons + (spin_states(i) - 1)*0.5_8+1))))
                IF (k <= 500) THEN
                    roots(i) = k
                    trunc_roots(i) = k
                ELSE
                    roots(i) = -1
                    trunc_roots(i) = -1
                END IF
            END IF
        END DO

    END SUBROUTINE ORGANISE_ELECTRONIC



    SUBROUTINE WRITE_SEWARD_SECTION(molcas_file_header, central_label, central_index, num_central_choices, num_atoms, coords, atoms_atomic_label, atoms_atomic_number, ordering, num_donors)
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(INOUT)  :: atoms_atomic_label(:)
        CHARACTER(LEN = *), INTENT(IN)     :: molcas_file_header, central_label
        REAL(KIND = 8), INTENT(IN)         :: coords(:,:)
        INTEGER, INTENT(IN)                :: num_atoms, num_central_choices, central_index, atoms_atomic_number(:), ordering(:,:), num_donors
        INTEGER, ALLOCATABLE               :: done(:), donor_list(:)
        INTEGER                            :: switch, l, i, col, atomic_number

        ! Allocate array to keep track of atoms which have been written to file
        ALLOCATE(done(num_atoms), donor_list(num_atoms))
        done = 0
        donor_list = 0

        ! Open molcas input file
        OPEN(30, FILE = TRIM(molcas_file_header)//'.in', STATUS = 'UNKNOWN')

        ! Write SEWARD header
        WRITE(30,'(A)') '&SEWARD'
        WRITE(30,'(A)') 'AMFI'
        WRITE(30,'(A)') 'ANGMOM=0.0 0.0 0.0 Angstrom'

        ! Different 2-electron decompositions for phonon
        !  and normal calculation types
        IF (phonon .EQV. .TRUE.) THEN
            WRITE(30,'(A)') 'RICD'
            WRITE(30,'(A)') 'acCD'
        ELSE
            WRITE(30,'(A)') 'HIGH CHOLESKY'
        END IF

        ! Blank line
        WRITE(30,*)

        ! Write basis set information for central atom 

        ! Start basis set section
        WRITE(30,'(A)') 'Basis Set'
        WRITE(30,'(A)') TRIM(central_label)//'.ANO-RCC-VTZP'
        WRITE(30,'(A7, A, 3F21.15, A10)') atoms_atomic_label(central_index), ' ', (coords(central_index,col), col = 1, 3),'  Angstrom'
        WRITE(30,'(A)') 'End of Basis Set'

        ! Blank line
        WRITE(30,*)
        
        ! Mark central atom as done
        done(central_index) = 1
        
        ! Write basis set information for atoms which are the same element as the central atom
        IF (num_central_choices > 1) THEN

            ! Start basis set section
            WRITE(30,'(A)') 'Basis Set'

            WRITE(30,'(A)') TRIM(central_label)//'.ANO-RCC-VDZ'            

            DO i = 1, num_atoms
                IF (i == central_index) CYCLE 
                IF (TRIM(UP_TO_NUM(atoms_atomic_label(i))) /= central_label) CYCLE
                    
                WRITE(30,'(A7, A, 3F21.15, A)') atoms_atomic_label(i), ' ', (coords(i,col), col = 1, 3), '  Angstrom'
                done(i) = 1

            END DO

            ! Terminate basis set section
            WRITE(30,'(A)') 'End of Basis Set'
            ! Blank line
            WRITE(30,*)

        END IF

        ! Close molcas input file
        CLOSE(30)
        
        ! Write basis set info for donor atoms 
        switch = 0
        ! Go down the list of atoms and mark which are donors
        ! The list is ordered by distance to central atom
        ! This will skip hydrogens
        DO i = 1,num_atoms
        IF (done(ordering(i,2)) == 1) CYCLE
        IF (atoms_atomic_label(ordering(i,2))(1:1) == 'H') CYCLE
            switch = switch + 1
            donor_list(ordering(i,2)) = 1
            IF (switch == num_donors) EXIT
        END DO

        ! Loop over all elements and if they are present and a donor
        ! write the header for a basis set block for VDZP quality
        DO atomic_number = 118,1,-1
            DO l = 1,num_atoms
                IF (donor_list(l) == 1 .AND. atoms_atomic_number(l) == atomic_number .AND. done(l) == 0) THEN
                    CALL WRITE_DONOR_BASIS_SECTION(molcas_file_header, num_atoms, atomic_number, atoms_atomic_number, done, donor_list, atoms_atomic_label, coords)
                END IF
            END DO
        END DO
        

        ! Write basis set info for donor atoms  

        ! Loop over all elements and if they are present and a donor
        ! write the header for a basis set block for VDZP quality

        DO atomic_number = 118,1,-1
            DO l = 1, num_atoms
                IF (atoms_atomic_number(l) == atomic_number .AND. done(l) == 0) THEN
                    CALL WRITE_OTHER_BASIS_SECTION(molcas_file_header, num_atoms, atomic_number, atoms_atomic_number, done, atoms_atomic_label, coords)
                END IF
            END DO
        END DO


    END SUBROUTINE WRITE_SEWARD_SECTION


    SUBROUTINE WRITE_DONOR_BASIS_SECTION(molcas_file_header, num_atoms, atomic_number, atoms_atomic_number, done, donor_list, atoms_atomic_label, coords)
        IMPLICIT NONE
        INTEGER, INTENT(IN)             :: num_atoms, atomic_number, atoms_atomic_number(:), donor_list(:)
        INTEGER, INTENT(INOUT)          :: done(:)
        INTEGER                         :: l, col
        CHARACTER(LEN = *), INTENT(IN)  :: atoms_atomic_label(:), molcas_file_header
        REAL(KIND = 8), INTENT(IN)      :: coords(:, :)

        ! Open molcas input file
        OPEN(30, FILE = TRIM(molcas_file_header)//'.in', STATUS = 'UNKNOWN', POSITION = 'APPEND')

            ! Write header for basis set section
            WRITE(30,'(A)') 'Basis Set'
            ! Write element label and quality
            WRITE(30,'(2A)') TRIM(ATOMIC_NO_TO_LABEL(atomic_number)),'.ANO-RCC-VDZP'

            ! Write the coordinates of the donors of this element
            DO l = 1, num_atoms
                IF (atoms_atomic_number(l) == atomic_number .AND. done(l) == 0 .AND. donor_list(l) == 1) THEN
                    WRITE(30,'(A8,3F21.15,A10)') atoms_atomic_label(l)//' ',(coords(l,col), col = 1, 3),'  Angstrom'
                    done(l) = 1
                END IF
            END DO

            ! Write end marker for basis set section
            WRITE(30,'(A)') 'End of Basis Set'
            ! Blank line
            WRITE(30,*)

        ! Close molcas input file
        CLOSE(30)

    END SUBROUTINE WRITE_DONOR_BASIS_SECTION

    SUBROUTINE WRITE_OTHER_BASIS_SECTION(molcas_file_header, num_atoms, atomic_number, atoms_atomic_number, done, atoms_atomic_label, coords)
        IMPLICIT NONE
        INTEGER, INTENT(IN)             :: num_atoms, atomic_number, atoms_atomic_number(:)
        INTEGER, INTENT(INOUT)          :: done(:)
        INTEGER                         :: l, col
        CHARACTER(LEN = *), INTENT(IN)  :: atoms_atomic_label(:), molcas_file_header
        REAL(KIND = 8), INTENT(IN)      :: coords(:, :)

        ! Open molcas input file
        OPEN(30, FILE = TRIM(molcas_file_header)//'.in', STATUS = 'UNKNOWN', POSITION = 'APPEND')

            ! Write header for basis set
            WRITE(30,'(A)') 'Basis Set'
            ! Write element label and quality
            WRITE(30,'(2A)') TRIM(ATOMIC_NO_TO_LABEL(atomic_number)),'.ANO-RCC-VDZ'

            ! Write the coordinates of the atoms of this element
            DO l = 1,num_atoms
                IF (atoms_atomic_number(l) == atomic_number .AND. done(l) == 0 ) THEN
                    WRITE(30,'(A8,3F21.15,A10)') atoms_atomic_label(l)//' ',(coords(l,col), col = 1, 3),'  Angstrom'
                    done(l) = 1
                END IF
            END DO

            ! Write end marker for basis set section
            WRITE(30,'(A)') 'End of Basis Set'
            ! Blank line
            WRITE(30,*)

        ! Close molcas input file
        CLOSE(30)

    END SUBROUTINE WRITE_OTHER_BASIS_SECTION

    SUBROUTINE WRITE_CASSCF_SECTION(molcas_file_header, central_index, atoms_atomic_number, num_spin_states, spin_states, roots, active_electrons, active_orbitals, charge, phonon)
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN)  :: molcas_file_header
        LOGICAL, INTENT(IN)             :: phonon
        INTEGER, INTENT(IN)             :: active_electrons, active_orbitals, charge, atoms_atomic_number(:), central_index, spin_states(:), num_spin_states, roots(:)
        INTEGER                         :: k, i

        ! Open molcas input file
        OPEN(30,FILE = TRIM(molcas_file_header) // '.in', STATUS = 'UNKNOWN', POSITION = 'APPEND')

            ! Loop over each spin state and write CASSCF section
            DO i = 1, num_spin_states

                ! Write blank line to separate sections
                IF (i /= 1) THEN
                    WRITE(30,*)
                END IF

                ! Write section header
                WRITE(30,'(A)') '&RASSCF'

                ! Write name of previous orbital file to use as initial guess
                ! If not looking at first spin-state
                IF (i /= 1) THEN
                    WRITE(30,'(A,I0,A)') 'FILEORB=',i - 1,'.RasOrb'
                END IF

                ! Write spin for current state
                WRITE(30,'(A,I2)') 'Spin= ', spin_states(i)

                ! Write number of active electrons in current state
                WRITE(30,'(A,I2,A)') 'Nactel= ',active_electrons,' 0 0'

                ! Write number of active orbitals in current state
                WRITE(30,'(A,I2)') 'Ras2= ',active_orbitals

                ! Write number of inactive electrons in current state
                WRITE(30,'(A,I3)') 'Inactive= ',NINT((electrons-charge-active_electrons)*0.5_8)

                ! Write number of CI roots for this spin state
                ! Input is (number of roots, number of roots to use in averaging, number 1 which indicates all roots weighted equally in averaging)
                ! See the MOLCAS manual for this bonkers input

                !Specific inputs for lanthanides
                IF (atoms_atomic_number(central_index) >= 57 .AND. atoms_atomic_number(central_index) <= 71 .AND. active_orbitals == 7) THEN
                    !
                    IF (active_electrons == 7 .AND. spin_states(i) == 2) THEN
                        WRITE(30,'(A)') 'CiRoot= x x 1'
                        WRITE(6,'(A)') '    !Large number of roots for Spin=2!'     
                        WRITE(6,'(A)') '!Check input file and modify CiRoot= x x 1!'
                        WRITE(6,'(A)') ' !x is the number of roots you would like!'
                    ELSE
                        WRITE(30, '(A, I0, A, I0, A)') 'CiRoot= ', roots(i), ' ', roots(i), ' 1'
                    END IF

                ELSE
                    ! If not a lanthanide then truncate at 500 roots
                    IF (roots(i) <= 500) THEN
                        WRITE(30,'(A,2I4,A)') 'CiRoot= ',roots(i) ,roots(i) ,' 1'
                    ELSE
                        WRITE(30,'(A)')       'CiRoot= x x 1'
                        WRITE(6,'(A)') '          !Large number of roots!'                
                        WRITE(6,'(A)') '!Check input file and modify CiRoot= x x 1!'
                        WRITE(6,'(A)') ' !x is the number of roots you would like!'
                    END IF
                END IF

                ! Write additional commands
                ! Set orbital output to include all contributions no matter how small
                WRITE(30,'(A)') 'ORBA=FULL'
                ! Set max number of orbital files as 1
                WRITE(30,'(A)') 'MAXO=1'

                ! Write molcas commands to copy current IPH and RasOrb files to new  
                ! locations as molcas overwrites these by default
                WRITE(30,'(A,I0,A)') '>> COPY $Project.JobIph ',i,'_IPH'
                WRITE(30,'(A,I0,A)') '>> COPY $Project.RasOrb ',i,'.RasOrb'

                ! Write canonical orbitals section for first spin state
                IF ((i == 1) .AND. (phonon .EQV. .FALSE.)) THEN
                    
                    ! Blank line
                    WRITE(30,*)
                    ! Write header
                    WRITE(30,'(A)') '&RASSCF'

                    ! Write name of orbital file to use as initial guess
                    WRITE(30,'(A)') 'FILEORB=1.RasOrb'

                    !Write number of active electrons
                    WRITE(30,'(A,I2,A)') 'Nactel= ', active_electrons,' 0 0'

                    !Write number of roots for lanthanides in high spin state
                    IF (atoms_atomic_number(central_index) >= 57 .AND. atoms_atomic_number(central_index) <= 71 .AND. active_orbitals == 7) THEN
                        WRITE(30, '(A, I0, A, I0, A)') 'CiRoot= ', roots(i), ' ', roots(i), ' 1'
                    !Or use the Weyl formula for other elements
                    ELSE
                        ! If not a lanthanide then truncate at 500 roots
                        IF (roots(i) <= 500) THEN
                            WRITE(30,'(A,2I4,A)') 'CiRoot= ',roots(i) ,roots(i) ,' 1'
                        ELSE
                            WRITE(30,'(A)')       'CiRoot= x x 1'
                        END IF
                    END IF

                    ! Write additional commands
                    ! Set output orbitals as canonical
                    WRITE(30,'(A)') 'OUTO=CANO'
                    ! Set max number of orbital files as 1
                    WRITE(30,'(A)') 'MAXO=1'
                    ! Write molcas commands to copy current RasOrb file to new 
                    ! location as molcas overwrites it by default
                    WRITE(30,'(A,I2,A)') '>> COPY $Project.RasOrb ', i,'.CanOrb'
                END IF

            END DO

        ! Close file
        CLOSE(30)

    END SUBROUTINE WRITE_CASSCF_SECTION

    SUBROUTINE WRITE_CASPT2_SECTION()
        IMPLICIT NONE

    END SUBROUTINE WRITE_CASPT2_SECTION

    SUBROUTINE WRITE_RASSI_SECTION(molcas_file_header, central_index, num_spin_states, spin_states, roots, active_electrons, active_orbitals)
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN)   :: molcas_file_header
        INTEGER, INTENT(IN)              :: central_index, num_spin_states, spin_states(:), roots(:), active_electrons, active_orbitals
        INTEGER                          :: l, i, k
            
        ! Open molcas input file
        OPEN(30,FILE = TRIM(molcas_file_header) // '.in', STATUS = 'UNKNOWN', POSITION = 'APPEND')

            ! Write blank line
            WRITE(30,*)

            ! Write section header
            WRITE(30,'(A)') '&RASSI'

            ! Write roots list
            WRITE(30,'(A14,I2,A1)', ADVANCE = 'NO') 'Nr of JobIph= ',num_spin_states,' '

            ! Loop over each spin state and add each total number of roots to the list
            DO i = 1, num_spin_states

                IF (roots(i) < 500) THEN
                    ! Write total number of roots for this spin state
                    WRITE(30,'(I4)', ADVANCE = 'NO') roots(i)
                ELSE
                    WRITE(30,'(A2)', ADVANCE = 'NO') 'x '
                END IF

                ! Write a semi-colon to separate the total number of roots from the list of each root number
                IF (i == num_spin_states) WRITE(30,'(A2)', ADVANCE = 'NO') '; '

            END DO


            ! Loop over each spin state and add each individual root number to the list
            DO i = 1, num_spin_states
                IF (roots(i) < 500 .AND. roots(i) > 0) THEN

                    ! Write each root number for each spin state
                    DO k = 1, roots(i)
                        WRITE(30,'(I4)', ADVANCE = 'NO') k
                    END DO
                    
                    ! If the end is reached, break the line off
                    IF (i == num_spin_states) THEN
                        WRITE(30, *) 
                    ! If not write a semi-colon to separate the roots
                    ELSE
                        WRITE(30,'(A2)', ADVANCE = 'NO') '; '
                    END IF

                END IF
            END DO


            WRITE(30,'(A)') 'Spin'
            WRITE(30,'(A)') 'IPHN'

            DO i = 1,  num_spin_states
                WRITE(30,'(I0,A)') i,'_IPH'
            END DO

            ! Set optional parameters

            WRITE(30,'(A)') 'MEES'
            WRITE(30,'(A)') 'EJob'
            WRITE(30,'(A)') 'PROP'
            WRITE(30,'(A)') '3'
            WRITE(30,'(A)') "'ANGMOM' 1"
            WRITE(30,'(A)') "'ANGMOM' 2"
            WRITE(30,'(A)') "'ANGMOM' 3"
            WRITE(30,'(A)') 'EPRG'
            WRITE(30,'(A)') '7.0D-1'

        ! Close file
        CLOSE(30)

    END SUBROUTINE WRITE_RASSI_SECTION

    SUBROUTINE WRITE_SINGLE_ANISO_SECTION(molcas_file_header, central_label, central_index, atoms_atomic_number, high_spin, active_orbitals, phonon)
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN)   :: molcas_file_header, central_label
        INTEGER, INTENT(IN)              :: central_index, atoms_atomic_number(:), high_spin, active_orbitals
        LOGICAL, INTENT(IN)              :: phonon

        ! Open molcas input file
        OPEN(30,FILE = TRIM(molcas_file_header) // '.in', STATUS = 'UNKNOWN', POSITION = 'APPEND')

            ! Write blank line
            WRITE(30,*)

            ! Write SINGLE_ANISO header
            WRITE(30,'(A)') '&SINGLE_ANISO'

            ! Write variable for operation mode
            WRITE(30,'(A)') 'TYPE'

            ! Phonon calculation disable magnetic data printing
            IF (phonon .EQV. .TRUE.) WRITE(30,'(A)') '1'


            IF (phonon .EQV. .FALSE.) THEN
                ! If lanthanide 
                IF (atoms_atomic_number(central_index) >= 57 .AND. atoms_atomic_number(central_index) <= 71 .AND. active_orbitals == 7) THEN
                    WRITE(30,'(A)') '6'
                ! If anything else
                ELSE
                    WRITE(30,'(A)') '7'
                END IF

                ! Settings for magnetic properties
                WRITE(30,'(A)') 'TINT'
                WRITE(30,'(A)') '0.0  330.0  300  0.0001'
                WRITE(30,'(A)') 'HINT'
                WRITE(30,'(A)') '0.0  10.0  201'
                WRITE(30,'(A)') 'TMAG'
                WRITE(30,'(A)') '6 1.8 2 4 5 10 20'
            END IF
            IF (atoms_atomic_number(central_index) >= 57 .AND. atoms_atomic_number(central_index) <= 71 .AND. active_orbitals == 7) THEN
                WRITE(30,'(A)') 'CRYS'
                WRITE(30,'(A)') TRIM(LOWERCASE(central_label))
            ELSE
                WRITE(30,'(A)') 'MLTP'
                WRITE(30,'(A)') '1'
                WRITE(30,'(I0)') high_spin
            END IF   

        CLOSE(30)

    END SUBROUTINE WRITE_SINGLE_ANISO_SECTION

    SUBROUTINE WRITE_JOB_SCRIPT_CSF(molcas_file_header, num_spin_states)
        IMPLICIT NONE
        CHARACTER(LEN = *) , INTENT(IN) :: molcas_file_header
        CHARACTER(LEN = 500)            :: cwd
        INTEGER, INTENT(IN)             :: num_spin_states
        INTEGER                         :: i

        ! Get current directory
        CALL GETCWD(cwd)

        ! Open and write job script
        OPEN(30,FILE =  TRIM(molcas_file_header)//'.sh', STATUS = 'UNKNOWN')

            WRITE(30,'(A)')  '#!/bin/bash --login'
            WRITE(30,'(2A)') '#$ -N ',  TRIM(molcas_file_header)
            WRITE(30,'(A)')  '#$ -cwd'
            WRITE(30,'(A)')  '#$ -l mem256'
            WRITE(30,'(A)') '#------------------------------------------------------------------------------#'
            WRITE(30,'(A)')  'module load apps/binapps/molcasuu/8.0sp1'
            WRITE(30,'(A)')  '#------------------------------------------------------------------------------#'
            WRITE(30,'(2A)') 'export Project=', TRIM(molcas_file_header)
            WRITE(30,'(A)')  '#------------------------------------------------------------------------------#'
            WRITE(30,'(A)')  'export MOLCAS_WORKDIR=/scratch/$USER/$Project'
            WRITE(30,'(A)')  'mkdir -p $MOLCAS_WORKDIR'
            WRITE(30,'(A)') '#------------------------------------------------------------------------------#'
            WRITE(30,'(2A)') 'export CurrDir=', TRIM(cwd)
            WRITE(30,'(A)')  'export MOLCAS_MEM=32000'
            WRITE(30,'(A)')  'export MOLCAS_PRINT=2'
            WRITE(30,'(A)')  'export MOLCAS_DISK=20000'
            WRITE(30,'(A)')  'export MOLCAS_MOLDEN=ON'
            WRITE(30,'(A)') '#------------------------------------------------------------------------------#'
            WRITE(30,'(A)')  'molcas $Project.in >> $CurrDir/$Project.out'
            WRITE(30,'(A)')  'molcas_basis 8 $CurrDir/$Project.out L'

            ! Write commands to run molcas_orbs on RasOrb files
            DO i = 1, num_spin_states
                WRITE(30,'(A,I0,A,I0,A)') 'molcas_orbs $MOLCAS_WORKDIR/$Project/',i,'.RasOrb > $CurrDir/',i,'_rasorb'
                IF (i == 1) WRITE(30,'(A,I0,A,I0,A)') 'molcas_orbs $MOLCAS_WORKDIR/$Project/',i,'.CanOrb > $CurrDir/',i,'_canorb'
            END DO

            WRITE(30,'(A)') 'exit'

        ! Close job script
        CLOSE(30)

        ! Make job script executable
        CALL SYSTEM('chmod +x '//TRIM(molcas_file_header)//'.sh')

    END SUBROUTINE WRITE_JOB_SCRIPT_CSF

    SUBROUTINE WRITE_JOB_SCRIPT_CHILTON(molcas_file_header, num_spin_states)
        IMPLICIT NONE
        CHARACTER(LEN = *) , INTENT(IN) :: molcas_file_header
        CHARACTER(LEN = 500)            :: cwd
        INTEGER, INTENT(IN)             :: num_spin_states
        INTEGER                         :: i

        ! Get current directory
        CALL GETCWD(cwd)

        ! Open and write job script
        OPEN(30,FILE = 'job.sh', STATUS = 'UNKNOWN')

            WRITE(30,'(A)') '#! /bin/bash'
            WRITE(30,'(A)') '#------------------------------------------------------------------------------#'
            WRITE(30,'(2A)') 'input=', TRIM(molcas_file_header)
            WRITE(30,'(2A)') 'export CurrDir=', TRIM(cwd)
            WRITE(30,'(A)') 'export WorkDir=$CurrDir/scratch'
            WRITE(30,'(A)') 'export Project=$input'
            WRITE(30,'(A)') '#------------------------------------------------------------------------------#'
            WRITE(30,'(A)') 'export MOLCAS=/usr/local/src/molcas-UU-8.0-15.06.18-serial-12'
            WRITE(30,'(A)') 'export MOLCASMEM=16000'
            WRITE(30,'(A)') 'export MOLCAS_PRINT=2'
            WRITE(30,'(A)') 'export MOLCASDISK=20000'
            WRITE(30,'(A)') 'export MOLCAS_MOLDEN=ON'
            WRITE(30,'(A)') '#------------------------------------------------------------------------------#'
            WRITE(30,'(A)') 'mkdir  $WorkDir'
            WRITE(30,'(A)') 'cd $WorkDir'
            WRITE(30,'(A)') 'molcas $CurrDir/$input.in >>  $CurrDir/$input.out'
            WRITE(30,'(A)') 'molcas_basis 8 $CurrDir/$input.out L'
            
            ! Write commands to run molcas_orbs on RasOrb files
            DO i = 1, num_spin_states
                WRITE(30,'(A,I0,A,I0,A)') 'molcas_orbs ',i,'.RasOrb > ',i,'_rasorb'
                IF (i == 1) WRITE(30,'(A,I0,A,I0,A)') 'molcas_orbs ',i,'.CanOrb > ',i,'_canorb'
            END DO

            WRITE(30,'(A)') 'exit'

        ! Close job script
        CLOSE(30)

        ! Make job script executable
        CALL SYSTEM('chmod +x job.sh')

    END SUBROUTINE WRITE_JOB_SCRIPT_CHILTON

    FUNCTION LOWERCASE(str) RESULT(str_out)
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN)     :: str
        CHARACTER(LEN = LEN(str))          :: str_out
        INTEGER                            :: gap, i

        str_out = str

        gap = ICHAR('a')-ICHAR('A')

        IF (LEN(str_out) > 0) THEN
            DO i = 1, LEN(str_out)
                IF (str_out(i:i) <= 'Z' .AND. str_out(i:i) >= 'A') THEN
                    str_out(i:i) = CHAR(ICHAR(str_out(i:i)) + gap)
                ELSE IF (str_out(i:i) <= 'z' .AND. str_out(i:i) >= 'a') THEN
                    CYCLE
                ELSE 
                    str_out(i:i) = ' '
                END IF
            END DO
        END IF

    END FUNCTION LOWERCASE

    FUNCTION UPPERCASE(str) RESULT(str_out)
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN)     :: str
        CHARACTER(LEN = LEN(str))          :: str_out
        INTEGER                            :: gap, i

        str_out = str

        gap = ICHAR('a')-ICHAR('A')
        IF (LEN(str_out) > 0) THEN
            DO i = 1, LEN(str_out)
                IF (str_out(i:i) <= 'z' .AND. str_out(i:i) >= 'a') THEN
                    str_out(i:i) = CHAR(ICHAR(str_out(i:i)) - gap)
                ELSE IF (str_out(i:i) <= 'Z' .AND. str_out(i:i) >= 'A') THEN
                    CYCLE
                ELSE 
                    str_out(i:i) = ' '
                END IF
            END DO
        END IF

    END FUNCTION UPPERCASE

    FUNCTION UP_TO_NUM(str) RESULT(str_out)
        CHARACTER(LEN = *), INTENT(IN)  :: str
        CHARACTER(LEN = LEN(str))       :: str_out
        INTEGER                         :: i, end_stop

        DO i = 1, LEN(str)
            IF ((str(i:i) <= 'z' .AND. str(i:i) >= 'a') .OR. (str(i:i) <= 'Z' .AND. str(i:i) >= 'A')) THEN
                CYCLE
            ELSE
                end_stop = i - 1
                EXIT
            END IF
        END DO

        IF (end_stop == 0) THEN 
            WRITE(6,'(A)') ' Error in UP_TO_NUM'
            STOP
        END IF

        str_out = str(1:end_stop)

    END FUNCTION UP_TO_NUM

    FUNCTION ATOMIC_NO_TO_LABEL(number) RESULT(label)
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: number
        CHARACTER(LEN = 3)  :: label

        IF (number == 1) label = 'H' 
        IF (number == 2) label = 'He'
        IF (number == 3) label = 'Li'
        IF (number == 4) label = 'Be'
        IF (number == 5) label = 'B' 
        IF (number == 6) label = 'C' 
        IF (number == 7) label = 'N' 
        IF (number == 8) label = 'O' 
        IF (number == 9) label = 'F' 
        IF (number == 10) label = 'Ne'
        IF (number == 11) label = 'Na'
        IF (number == 12) label = 'Mg'
        IF (number == 13) label = 'Al'
        IF (number == 14) label = 'Si'
        IF (number == 15) label = 'P' 
        IF (number == 16) label = 'S' 
        IF (number == 17) label = 'Cl'
        IF (number == 18) label = 'Ar'
        IF (number == 19) label = 'K' 
        IF (number == 20) label = 'Ca'
        IF (number == 21) label = 'Sc'
        IF (number == 22) label = 'Ti'
        IF (number == 23) label = 'V' 
        IF (number == 24) label = 'Cr'
        IF (number == 25) label = 'Mn'
        IF (number == 26) label = 'Fe'
        IF (number == 27) label = 'Co'
        IF (number == 28) label = 'Ni'
        IF (number == 29) label = 'Cu'
        IF (number == 30) label = 'Zn'
        IF (number == 31) label = 'Ga'
        IF (number == 32) label = 'Ge'
        IF (number == 33) label = 'As'
        IF (number == 34) label = 'Se'
        IF (number == 35) label = 'Br'
        IF (number == 36) label = 'Kr'
        IF (number == 37) label = 'Rb'
        IF (number == 38) label = 'Sr'
        IF (number == 39) label = 'Y' 
        IF (number == 40) label = 'Zr'
        IF (number == 41) label = 'Nb'
        IF (number == 42) label = 'Mo'
        IF (number == 43) label = 'Tc'
        IF (number == 44) label = 'Ru'
        IF (number == 45) label = 'Rh'
        IF (number == 46) label = 'Pd'
        IF (number == 47) label = 'Ag'
        IF (number == 48) label = 'Cd'
        IF (number == 49) label = 'In'
        IF (number == 50) label = 'Sn'
        IF (number == 51) label = 'Sb'
        IF (number == 52) label = 'Te'
        IF (number == 53) label = 'I'
        IF (number == 54) label = 'Xe'
        IF (number == 55) label = 'Cs'
        IF (number == 56) label = 'Ba'
        IF (number == 57) label = 'La'
        IF (number == 58) label = 'Ce'
        IF (number == 59) label = 'Pr'
        IF (number == 60) label = 'Nd'
        IF (number == 61) label = 'Pm'
        IF (number == 62) label = 'Sm'
        IF (number == 63) label = 'Eu'
        IF (number == 64) label = 'Gd'
        IF (number == 65) label = 'Tb'
        IF (number == 66) label = 'Dy'
        IF (number == 67) label = 'Ho'
        IF (number == 68) label = 'Er'
        IF (number == 69) label = 'Tm'
        IF (number == 70) label = 'Yb'
        IF (number == 71) label = 'Lu'
        IF (number == 72) label = 'Hf'
        IF (number == 73) label = 'Ta'
        IF (number == 74) label = 'W' 
        IF (number == 75) label = 'Re'
        IF (number == 76) label = 'Os'
        IF (number == 77) label = 'Ir'
        IF (number == 78) label = 'Pt'
        IF (number == 79) label = 'Au'
        IF (number == 80) label = 'Hg'
        IF (number == 81) label = 'tl'
        IF (number == 82) label = 'Pb'
        IF (number == 83) label = 'Bi'
        IF (number == 84) label = 'Po'
        IF (number == 85) label = 'At'
        IF (number == 86) label = 'Rn'
        IF (number == 87) label = 'Fr'
        IF (number == 88) label = 'Ra'
        IF (number == 89) label = 'Ac'
        IF (number == 90) label = 'Th'
        IF (number == 91) label = 'Pa'
        IF (number == 92) label = 'U' 
        IF (number == 93) label = 'Np'
        IF (number == 94) label = 'Pu'
        IF (number == 95) label = 'Am'
        IF (number == 96) label = 'Cm'
        IF (number == 97) label = 'Bk'
        IF (number == 98) label = 'Cf'
        IF (number == 99) label = 'Es'
        IF (number == 100) label = 'Fm'
        IF (number == 101) label = 'Md'
        IF (number == 102) label = 'No'
        IF (number == 103) label = 'Lr'
        IF (number == 104) label = 'Rf'
        IF (number == 105) label = 'Db'
        IF (number == 106) label = 'Sg'
        IF (number == 107) label = 'Bh'
        IF (number == 108) label = 'Hs'
        IF (number == 109) label = 'Mt'
        IF (number == 110) label = 'Ds'
        IF (number == 111) label = 'Rg'
        IF (number == 112) label = 'Cn'
        IF (number == 113) label = 'Uut'
        IF (number == 114) label = 'Fl'
        IF (number == 115) label = 'Uup'
        IF (number == 116) label = 'Lv'
        IF (number == 117) label = 'Uus'
        IF (number == 118) label = 'Uuo'

    END FUNCTION ATOMIC_NO_TO_LABEL
    

    FUNCTION ATOMIC_LABEL_TO_NO(label) RESULT(number)
        IMPLICIT NONE
        INTEGER                         :: number
        CHARACTER(LEN = 3), INTENT(IN)  :: label

        IF (TRIM(label) == 'h') number = 1
        IF (TRIM(label) == 'he') number = 2
        IF (TRIM(label) == 'li') number = 3
        IF (TRIM(label) == 'be') number = 4
        IF (TRIM(label) == 'b') number = 5
        IF (TRIM(label) == 'c') number = 6
        IF (TRIM(label) == 'n') number = 7
        IF (TRIM(label) == 'o') number = 8
        IF (TRIM(label) == 'f') number = 9
        IF (TRIM(label) == 'ne') number = 10
        IF (TRIM(label) == 'na') number = 11
        IF (TRIM(label) == 'mg') number = 12
        IF (TRIM(label) == 'al') number = 13
        IF (TRIM(label) == 'si') number = 14
        IF (TRIM(label) == 'p') number = 15
        IF (TRIM(label) == 's') number = 16
        IF (TRIM(label) == 'cl') number = 17
        IF (TRIM(label) == 'ar') number = 18
        IF (TRIM(label) == 'k') number = 19
        IF (TRIM(label) == 'ca') number = 20
        IF (TRIM(label) == 'sc') number = 21
        IF (TRIM(label) == 'ti') number = 22
        IF (TRIM(label) == 'v') number = 23
        IF (TRIM(label) == 'cr') number = 24
        IF (TRIM(label) == 'mn') number = 25
        IF (TRIM(label) == 'fe') number = 26
        IF (TRIM(label) == 'co') number = 27
        IF (TRIM(label) == 'ni') number = 28
        IF (TRIM(label) == 'cu') number = 29
        IF (TRIM(label) == 'zn') number = 30
        IF (TRIM(label) == 'ga') number = 31
        IF (TRIM(label) == 'ge') number = 32
        IF (TRIM(label) == 'as') number = 33
        IF (TRIM(label) == 'se') number = 34
        IF (TRIM(label) == 'br') number = 35
        IF (TRIM(label) == 'kr') number = 36
        IF (TRIM(label) == 'rb') number = 37
        IF (TRIM(label) == 'sr') number = 38
        IF (TRIM(label) == 'y') number = 39
        IF (TRIM(label) == 'zr') number = 40
        IF (TRIM(label) == 'nb') number = 41
        IF (TRIM(label) == 'mo') number = 42
        IF (TRIM(label) == 'tc') number = 43
        IF (TRIM(label) == 'ru') number = 44
        IF (TRIM(label) == 'rh') number = 45
        IF (TRIM(label) == 'pd') number = 46
        IF (TRIM(label) == 'ag') number = 47
        IF (TRIM(label) == 'cd') number = 48
        IF (TRIM(label) == 'in') number = 49
        IF (TRIM(label) == 'sn') number = 50
        IF (TRIM(label) == 'sb') number = 51
        IF (TRIM(label) == 'te') number = 52
        IF (TRIM(label) == 'i ') number = 53
        IF (TRIM(label) == 'xe') number = 54
        IF (TRIM(label) == 'cs') number = 55
        IF (TRIM(label) == 'ba') number = 56
        IF (TRIM(label) == 'la') number = 57
        IF (TRIM(label) == 'ce') number = 58
        IF (TRIM(label) == 'pr') number = 59
        IF (TRIM(label) == 'nd') number = 60
        IF (TRIM(label) == 'pm') number = 61
        IF (TRIM(label) == 'sm') number = 62
        IF (TRIM(label) == 'eu') number = 63
        IF (TRIM(label) == 'gd') number = 64
        IF (TRIM(label) == 'tb') number = 65
        IF (TRIM(label) == 'dy') number = 66
        IF (TRIM(label) == 'ho') number = 67
        IF (TRIM(label) == 'er') number = 68
        IF (TRIM(label) == 'tm') number = 69
        IF (TRIM(label) == 'yb') number = 70
        IF (TRIM(label) == 'lu') number = 71
        IF (TRIM(label) == 'hf') number = 72
        IF (TRIM(label) == 'ta') number = 73
        IF (TRIM(label) == 'w') number = 74
        IF (TRIM(label) == 're') number = 75
        IF (TRIM(label) == 'os') number = 76
        IF (TRIM(label) == 'ir') number = 77
        IF (TRIM(label) == 'pt') number = 78
        IF (TRIM(label) == 'au') number = 79
        IF (TRIM(label) == 'hg') number = 80
        IF (TRIM(label) == 'tl') number = 81
        IF (TRIM(label) == 'pb') number = 82
        IF (TRIM(label) == 'bi') number = 83
        IF (TRIM(label) == 'po') number = 84
        IF (TRIM(label) == 'at') number = 85
        IF (TRIM(label) == 'rn') number = 86
        IF (TRIM(label) == 'fr') number = 87
        IF (TRIM(label) == 'ra') number = 88
        IF (TRIM(label) == 'ac') number = 89
        IF (TRIM(label) == 'th') number = 90
        IF (TRIM(label) == 'pa') number = 91
        IF (TRIM(label) == 'u') number = 92
        IF (TRIM(label) == 'np') number = 93
        IF (TRIM(label) == 'pu') number = 94
        IF (TRIM(label) == 'am') number = 95
        IF (TRIM(label) == 'cm') number = 96
        IF (TRIM(label) == 'bk') number = 97
        IF (TRIM(label) == 'cf') number = 98
        IF (TRIM(label) == 'es') number = 99
        IF (TRIM(label) == 'fm') number = 100
        IF (TRIM(label) == 'md') number = 101
        IF (TRIM(label) == 'no') number = 102
        IF (TRIM(label) == 'lr') number = 103
        IF (TRIM(label) == 'rf') number = 104
        IF (TRIM(label) == 'db') number = 105
        IF (TRIM(label) == 'sg') number = 106
        IF (TRIM(label) == 'bh') number = 107
        IF (TRIM(label) == 'hs') number = 108
        IF (TRIM(label) == 'mt') number = 109
        IF (TRIM(label) == 'ds') number = 110
        IF (TRIM(label) == 'rg') number = 111
        IF (TRIM(label) == 'cn') number = 112
        IF (TRIM(label) == 'uut') number = 113
        IF (TRIM(label) == 'fl') number = 114
        IF (TRIM(label) == 'uup') number = 115
        IF (TRIM(label) == 'lv') number = 116
        IF (TRIM(label) == 'uus') number = 117
        IF (TRIM(label) == 'uuo') number = 118

        ! If element wasnt assigned report error and stop
        IF (number == 0) THEN
            WRITE(6,'(3A)') 'Element ',TRIM(label),' is UNKNOWN'
            STOP
        END IF

    END FUNCTION ATOMIC_LABEL_TO_NO

    recursive function binom(n,r) result(res)
    implicit none
    integer,intent(in)::n,r
    real(kind=8)::res
    IF (n < 0 .or. r < 0 .or. r > n) THEN
        res = 0.0_8
    ELSE IF (n == 0) THEN
        IF (r == 0) THEN
            res = 1.0_8
        ELSE
            res = 0.0_8
        END IF
    ELSE IF (n==r .or. r==0) THEN
       res = 1.0_8
    ELSE IF (r==1) THEN
       res = dble(n)
    ELSE
       res = dble(n)/dble(n-r)*binom(n-1,r)
    END IF
    end function binom
end program molcas_input_gen
