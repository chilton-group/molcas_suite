!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   cfe.f90 
!
!   This module is a part of molcas_suite
!
!
!   Calculates CF energies from single aniso output 
!   Uses parameters up to and including k = 6
!
!   
!      Authors:
!       Jon Kragskow
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

PROGRAM cfe
    USE, INTRINSIC::iso_fortran_env
    USE matrix_tools
    USE stev_ops
    IMPLICIT NONE

    CHARACTER(LEN = 5000)             :: input_file
    REAL(KIND = 8)                    :: CFP(3,27)
    REAL(KIND = 8), ALLOCATABLE       :: cf_percents(:,:), cf_energies(:)
    INTEGER                           :: num_states, k_max_index

    !Read user input from command line
    CALL READ_USER_INPUT(num_states, input_file)

    !Extract info from input_file (molcas output file)
    CALL EXTRACT_INFO(input_file, CFP, k_max_index)

    !Calculate crystal field energies using SINGLE_ANISO parameters with k <= 2J
    ! Returns unique energies only, i.e. DOes not repeat degenerate levels
    CALL CALCULATE_CF_PROPS(CFP, cf_energies, k_max_index, num_states, cf_percents)

    CALL WRITE_OUTPUT_FILE(cf_energies)
    
    CONTAINS

    SUBROUTINE READ_USER_INPUT(num_states, file)
        USE version, ONLY : PRINT_VERSION
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(OUT) :: file
        CHARACTER(LEN = 100)            :: dummyC
        INTEGER, INTENT(OUT)            :: num_states
        LOGICAL                         :: file_exists

        !Read number of states
        CALL GET_COMMAND_ARGUMENT(1,dummyC)
        
        !Print help
        IF(TRIM(dummyC) == '-h' .OR. TRIM(dummyC) == '') THEN
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'cfe <num_states> <file>'
            WRITE(6,'(A)') ''
            STOP
        END IF

        ! Print version number
        IF (TRIM(dummyC) == '-v' .OR. TRIM(dummyC) == '-version' .OR. TRIM(dummyC) == 'version') THEN
            CALL PRINT_VERSION('cfe')
            STOP
        END IF

        READ(dummyC,*) num_states

        !Read molcas .out file name
        CALL GET_COMMAND_ARGUMENT(2,file)

        !Check file exists
        INQUIRE(FILE=TRIM(file), EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(A)') 'Specified file does not exist'
            STOP
        END IF

    END SUBROUTINE

    SUBROUTINE EXTRACT_INFO(input_file, CFP, k_max_index)
        IMPLICIT NONE
        CHARACTER(LEN = 5000)                       :: line
        CHARACTER(LEN = *), INTENT(IN)              :: input_file
        REAL(KIND = 8), INTENT(OUT)                 :: CFP(3,27)
        INTEGER, INTENT(OUT)                        :: k_max_index
        INTEGER                                     :: k, q

        OPEN(30, file = TRIM(input_file), STATUS = 'OLD')

        DO WHILE (.TRUE.)
            READ(30,'(A)') line
            IF(TRIM(line) == '  k |  q  |    (Knm)^2  |         B(k,q)        |') THEN
            READ(30,*)
            k_max_index = 3
            !Read in Crystal field parameters
                DO k = 1,3
                    DO q = 1,4*k+1
                        READ(30,'(A)') line
                        READ(line(26:),*) CFP(k,q)
                    END DO
                    IF (k /= 3) READ(30,*)
                END DO
                EXIT
            END IF
        END DO


    END SUBROUTINE EXTRACT_INFO



    SUBROUTINE CHECK_IO(IOREPORT)
        IMPLICIT NONE
        INTEGER, INTENT(IN)   :: IOREPORT

        !Check file isnt corrupted or ends too soon
            IF (IOREPORT > 0)  THEN
                WRITE(6,'(A)') '!!!!                         Error reading molcas output file                              !!!!'
                STOP
            ELSE IF (IOREPORT < 0) THEN
                WRITE(6,'(A)') '!!!!                Error: Reached end of molcas output file too soon                      !!!!'
                STOP
            END IF

    END SUBROUTINE CHECK_IO


    SUBROUTINE CALCULATE_CF_PROPS(CFP, trunc_cf_energies, k_max_index, num_states, trunc_cf_percents)
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)              :: CFP(:,:)
        REAL(KIND = 8),ALLOCATABLE              :: cf_energies(:), cf_percents(:,:)
        REAL(KIND = 8),ALLOCATABLE, INTENT(OUT) :: trunc_cf_energies(:), trunc_cf_percents(:,:)
        INTEGER, INTENT(IN)                     :: k_max_index, num_states
        INTEGER                                 :: k, q
        COMPLEX(KIND = 8),ALLOCATABLE           :: HCF(:,:), HCF_vecs(:,:), StevensOperators(:,:,:,:)

        !Allocate arrays for hamiltonian, eigenvectors, and eigenvalues
        ALLOCATE(HCF(num_states, num_states),HCF_vecs(num_states, num_states), cf_energies(num_states), cf_percents(num_states, num_states))

        !Calculate stevens operators
        CALL GET_STEVENS_OPERATORS(num_states - 1, StevensOperators)

        !Form HCF
        DO k = 1, k_max_index
            DO q = 1, 4*k+1
                HCF = HCF + CFP(k,q) * StevensOperators(k,q,:,:)
            END DO
        END DO

        !Diagonalise
        CALL DIAGONALISE('U', HCF, HCF_vecs, cf_energies)

        !Set first CF Energy as zero
        !cf_energies = cf_energies - cf_energies(1) 
        IF (MODULO(num_states,2) == 0) THEN
            !For Kramers systems print every 2nd energy as degenerate

            ALLOCATE(trunc_cf_energies(num_states/2))
    
            q = 1
            DO k = 1, num_states
                IF (MODULO(k,2) > 0) THEN
                    trunc_cf_energies(q) = cf_energies(k)
                    q = q + 1
                END IF
            END DO
        ELSE
            !For non-Kramers systems print all energies
            ALLOCATE(trunc_cf_energies(num_states))
            trunc_cf_energies = cf_energies
        END IF

        !Calculate % contributions of each state to each eigenvector

        DO k = 1, num_states
            cf_percents(k,:) = DABS(REAL(HCF_vecs(:,k),8))**2 + DABS(DIMAG(HCF_vecs(:,k)))**2
        END DO

        IF (MODULO(num_states,2) == 0) THEN
            !For Kramers systems print every 2nd percentage vector as degenerate

            ALLOCATE(trunc_cf_percents(num_states/2, num_states))
    
            q = 0
            DO k = 1, num_states
                IF (MODULO(k,2) > 0) THEN
                    q = q + 1
                    trunc_cf_percents(q,:) = cf_percents(k,:)
                END IF
            END DO
        ELSE
            !For non-Kramers systems print all energies
            ALLOCATE(trunc_cf_percents(num_states, num_states))
            trunc_cf_percents = cf_percents
        END IF

    END SUBROUTINE CALCULATE_CF_PROPS

    SUBROUTINE WRITE_OUTPUT_FILE(cf_energies)
        IMPLICIT NONE
        INTEGER                         :: i
        REAL(KIND = 8), INTENT(IN)      :: cf_energies(:)
    

        !Open output file
        OPEN(66,FILE = TRIM(input_file) //"_cfe", STATUS = 'UNKNOWN')

            WRITE(66,'(A)') ' Crystal Field'
            WRITE(66,'(A)') ' Energy (cm-1)'
            DO i = 1,num_states/2
                WRITE(66,'(F15.7)') cf_energies(i)
            END DO
                    
        CLOSE(66)

    END SUBROUTINE WRITE_OUTPUT_FILE

END program cfe
