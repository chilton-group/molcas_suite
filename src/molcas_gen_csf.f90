!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   molcas_gen_csf.f90 
!
!   This program is a part of molcas_suite
!
!
!   THIS CODE IS EXCLUSIVELY FOR USE ON THE UNIVERSITY OF
!   MANCHESTER'S COMPUTATIONAL SHARED FACILITY (CSF)
!   PLEASE SEE molcas_gen.f90 FOR THE VERSION USED ON OUR
!   LOCAL GROUP MACHINES
!
!
!   Creates a molcas input file from an xyz file, central metal ion, charge, number of orbitals
!   in the active space, number of electrons in the active space, number of donors, and optionally
!   can be run in spin-phonon mode - calculates lowest energy multiplet only and enables RICD ACcd
!
!   
!      Authors:
!       Nicholas Chilton
!       Commented by Jon Kragskow
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


PROGRAM MOLCAS_INPUT_GEN
    IMPLICIT NONE
    CHARACTER(LEN = 300)                      :: file,dummy,cwd,rawfile
    CHARACTER(LEN = 6),ALLOCATABLE            :: atoms(:)
    CHARACTER(LEN = 3)                        :: fmt,element,to_do
    REAL(KIND = 8), ALLOCATABLE               :: coords(:,:),radii(:)
    REAL(KIND = 8)                            :: thresh, temp_coords(3)
    INTEGER                                   :: i,j,k,l,num,gap,counter(118),length,atoms_to_do,switch,atoms_list(1000),prev,charge,active,electrons,IOstatus,donors,std,orbitals,high_spin,low_spin,phonon,lim, col
    INTEGER, ALLOCATABLE                      :: list(:),atom_index(:),done(:),donor_list(:),root_list(:)
    LOGICAL                                   :: super_high_mem
   
    counter = 0
    atoms_to_do = 0
    std = 0
    phonon = 0

    CALL READ_USER_INPUT(file, element, charge, active, orbitals, donors, phonon, super_high_mem)

    rawfile = trim(file(1:len_trim(file)-4))!Rawfile is file with .xyz part removed - allows csf job names to be more descriptive

    !if(trim(adjustl(dummy)) == 'y') std = 1
    std = 1
    gap=ICHAR('a')-ICHAR('A')
    do i=1,len(element)
        if(element(i:i) <= 'Z' .and. element(i:i) >= 'A') element(i:i)=CHAR(ICHAR(element(i:i))+gap)
        if(.not.(element(i:i) <= 'z' .and. element(i:i) >= 'a')) element(i:i) = ' '
    end do
    open(30,file=trim(file),status='unknown')
    read(30,*) num
    allocate(atoms(num),coords(num,3),radii(num),list(num),atom_index(num),done(num),donor_list(num))
    atom_index = 0
    atoms_list = 0
    read(30,*)
    do i = 1,num
        read(30,*) atoms(i),(coords(i,col), col = 1, 3)
        prev = atoms_to_do
        call name_parse(atoms(i),i)
        if(atoms_to_do /= prev) atoms_list(atoms_to_do) = i
    end do
    close(30)
    if(atoms_to_do < 1) then
        write(6,*) "No atoms of that type found!"
        stop
    end if
    electrons = 0
    do i = 1,118
        electrons = electrons + counter(i)*i
    end do
    j = 0
    do i = 1,num
        if(trim(atoms(i)) == trim(element)) then
            j = j + 1
            atoms_list(j) = i
        end if
    end do
    if(atoms_to_do > 1) then
        write(6,'(A)') 'Which atom is active?'
        do j = 1,atoms_to_do
            write(6,'(I2,A,3F14.8)') j,atoms(atoms_list(j))//' ',coords(atoms_list(j),:)
        end do
        read(*,*) j
    else
        j = 1
    end if
    done = 0
    radii = 0.0_8
    do i = 1,num
        if(i /= atoms_list(j)) then
            coords(i,:) = coords(i,:) - coords(atoms_list(j),:)
            temp_coords = coords(i,:)
            radii(i) = radial(temp_coords)
        end if
    end do
    coords(atoms_list(j),:) = 0.0_8
    radii(atoms_list(j)) = 99D9
    call re_order(num,radii,list,'min')

    if(std == 0) then
        open(30,file=trim(file)//'.molcas',status='unknown')
        write(30,'(A11,F5.1)') 'Inactive = ',(electrons-charge-active)*0.5_8
        write(30,'(A63)') 'Do not forget to add/remove electrons if substituting elements!'
    end if
    if(std == 1) then
        open(30,file=trim(rawfile)//'.in',status='unknown')
        write(30,'(A)') '&SEWARD'
        write(30,'(A)') 'AMFI'
        write(30,'(A)') 'ANGMOM=0.0 0.0 0.0 Angstrom'
        if(phonon == 1) then
            write(30,'(A)') 'RICD'
            write(30,'(A)') 'acCD'
        else
            write(30,'(A)') 'HIGH CHOLESKY'
        end if
        write(30,*)
        write(30,'(A)') 'Basis Set'
        if(element(1:1) <= 'z' .and. element(1:1) >= 'a') element(1:1)=CHAR(ICHAR(element(1:1))-gap)
        write(30,'(A)') trim(element)//'.ANO-RCC-VTZP'
        if(element(1:1) <= 'Z' .and. element(1:1) >= 'A') element(1:1)=CHAR(ICHAR(element(1:1))+gap)
    end if
    write(30,'(A8,3F21.15,A10)') atoms(atoms_list(j))//' ',(coords(atoms_list(j),3), col = 1, 3),'  Angstrom'
    if(std == 1) write(30,'(A)') 'End of Basis Set'
    write(30,*)
    
    done(atoms_list(j)) = 1
    if(atoms_to_do > 1) then
        if(std == 1) then
            write(30,'(A)') 'Basis Set'
            if(element(1:1) <= 'z' .and. element(1:1) >= 'a') element(1:1)=CHAR(ICHAR(element(1:1))-gap)
            write(30,'(A)') trim(element)//'.ANO-RCC-VDZ'
            if(element(1:1) <= 'Z' .and. element(1:1) >= 'A') element(1:1)=CHAR(ICHAR(element(1:1))+gap)
        end if
        do i = 1,atoms_to_do
            if(i /= j) then
                write(30,'(A8,3F21.15,A10)') atoms(atoms_list(i))//' ',coords(atoms_list(i),:),'  Angstrom'
                done(atoms_list(i)) = 1
            end if
        end do
        if(std == 1) write(30,'(A)') 'End of Basis Set'
        write(30,*)
    end if

    
    
    switch = 0
    donor_list = 0
    if(std == 0) write(30,'(I2,A16)') donors," closest atom(s)"
    do i = 1,num
        if(done(list(i)) == 1) cycle
        if(atoms(list(i))(1:1) == 'H') cycle
        switch = switch + 1
        if(std == 0) write(30,'(A8,3F21.15,A10)') atoms(list(i))//' ',coords(list(i),:),'  Angstrom'
        if(std == 0) done(list(i)) = 1
        donor_list(list(i)) = 1
        if(switch == donors) exit
    end do
    if(std == 0) write(30,*)
    if(std == 1) then
        do i = 118,1,-1
            do l = 1,num
                if(donor_list(l) == 1 .and. atom_index(l) == i) goto 61
            end do
            cycle
            61 continue
            write(30,'(A)') 'Basis Set'
            if(i == 1) write(30,'(A)') 'H.ANO-RCC-VDZP'
            if(i == 2) write(30,'(A)') 'He.ANO-RCC-VDZP'
            if(i == 3) write(30,'(A)') 'Li.ANO-RCC-VDZP'
            if(i == 4) write(30,'(A)') 'Be.ANO-RCC-VDZP'
            if(i == 5) write(30,'(A)') 'B.ANO-RCC-VDZP'
            if(i == 6) write(30,'(A)') 'C.ANO-RCC-VDZP'
            if(i == 7) write(30,'(A)') 'N.ANO-RCC-VDZP'
            if(i == 8) write(30,'(A)') 'O.ANO-RCC-VDZP'
            if(i == 9) write(30,'(A)') 'F.ANO-RCC-VDZP'
            if(i == 10) write(30,'(A)') 'Ne.ANO-RCC-VDZP'
            if(i == 11) write(30,'(A)') 'Na.ANO-RCC-VDZP'
            if(i == 12) write(30,'(A)') 'Mg.ANO-RCC-VDZP'
            if(i == 13) write(30,'(A)') 'Al.ANO-RCC-VDZP'
            if(i == 14) write(30,'(A)') 'Si.ANO-RCC-VDZP'
            if(i == 15) write(30,'(A)') 'P.ANO-RCC-VDZP'
            if(i == 16) write(30,'(A)') 'S.ANO-RCC-VDZP'
            if(i == 17) write(30,'(A)') 'Cl.ANO-RCC-VDZP'
            if(i == 18) write(30,'(A)') 'Ar.ANO-RCC-VDZP'
            if(i == 19) write(30,'(A)') 'K.ANO-RCC-VDZP'
            if(i == 20) write(30,'(A)') 'Ca.ANO-RCC-VDZP'
            if(i == 21) write(30,'(A)') 'Sc.ANO-RCC-VDZP'
            if(i == 22) write(30,'(A)') 'Ti.ANO-RCC-VDZP'
            if(i == 23) write(30,'(A)') 'V.ANO-RCC-VDZP'
            if(i == 24) write(30,'(A)') 'Cr.ANO-RCC-VDZP'
            if(i == 25) write(30,'(A)') 'Mn.ANO-RCC-VDZP'
            if(i == 26) write(30,'(A)') 'Fe.ANO-RCC-VDZP'
            if(i == 27) write(30,'(A)') 'Co.ANO-RCC-VDZP'
            if(i == 28) write(30,'(A)') 'Ni.ANO-RCC-VDZP'
            if(i == 29) write(30,'(A)') 'Cu.ANO-RCC-VDZP'
            if(i == 30) write(30,'(A)') 'Zn.ANO-RCC-VDZP'
            if(i == 31) write(30,'(A)') 'Ga.ANO-RCC-VDZP'
            if(i == 32) write(30,'(A)') 'Ge.ANO-RCC-VDZP'
            if(i == 33) write(30,'(A)') 'As.ANO-RCC-VDZP'
            if(i == 34) write(30,'(A)') 'Se.ANO-RCC-VDZP'
            if(i == 35) write(30,'(A)') 'Br.ANO-RCC-VDZP'
            if(i == 36) write(30,'(A)') 'Kr.ANO-RCC-VDZP'
            if(i == 37) write(30,'(A)') 'Rb.ANO-RCC-VDZP'
            if(i == 38) write(30,'(A)') 'Sr.ANO-RCC-VDZP'
            if(i == 39) write(30,'(A)') 'Y.ANO-RCC-VDZP'
            if(i == 40) write(30,'(A)') 'Zr.ANO-RCC-VDZP'
            if(i == 41) write(30,'(A)') 'Nb.ANO-RCC-VDZP'
            if(i == 42) write(30,'(A)') 'Mo.ANO-RCC-VDZP'
            if(i == 43) write(30,'(A)') 'Tc.ANO-RCC-VDZP'
            if(i == 44) write(30,'(A)') 'Ru.ANO-RCC-VDZP'
            if(i == 45) write(30,'(A)') 'Rh.ANO-RCC-VDZP'
            if(i == 46) write(30,'(A)') 'Pd.ANO-RCC-VDZP'
            if(i == 47) write(30,'(A)') 'Ag.ANO-RCC-VDZP'
            if(i == 48) write(30,'(A)') 'Cd.ANO-RCC-VDZP'
            if(i == 49) write(30,'(A)') 'In.ANO-RCC-VDZP'
            if(i == 50) write(30,'(A)') 'Sn.ANO-RCC-VDZP'
            if(i == 51) write(30,'(A)') 'Sb.ANO-RCC-VDZP'
            if(i == 52) write(30,'(A)') 'Te.ANO-RCC-VDZP'
            if(i == 53) write(30,'(A)') 'I.ANO-RCC-VDZP'
            if(i == 54) write(30,'(A)') 'Xe.ANO-RCC-VDZP'
            if(i == 55) write(30,'(A)') 'Cs.ANO-RCC-VDZP'
            if(i == 56) write(30,'(A)') 'Ba.ANO-RCC-VDZP'
            if(i == 57) write(30,'(A)') 'La.ANO-RCC-VDZP'
            if(i == 58) write(30,'(A)') 'Ce.ANO-RCC-VDZP'
            if(i == 59) write(30,'(A)') 'Pr.ANO-RCC-VDZP'
            if(i == 60) write(30,'(A)') 'Nd.ANO-RCC-VDZP'
            if(i == 61) write(30,'(A)') 'Pm.ANO-RCC-VDZP'
            if(i == 62) write(30,'(A)') 'Sm.ANO-RCC-VDZP'
            if(i == 63) write(30,'(A)') 'Eu.ANO-RCC-VDZP'
            if(i == 64) write(30,'(A)') 'Gd.ANO-RCC-VDZP'
            if(i == 65) write(30,'(A)') 'Tb.ANO-RCC-VDZP'
            if(i == 66) write(30,'(A)') 'Dy.ANO-RCC-VDZP'
            if(i == 67) write(30,'(A)') 'Ho.ANO-RCC-VDZP'
            if(i == 68) write(30,'(A)') 'Er.ANO-RCC-VDZP'
            if(i == 69) write(30,'(A)') 'Tm.ANO-RCC-VDZP'
            if(i == 70) write(30,'(A)') 'Yb.ANO-RCC-VDZP'
            if(i == 71) write(30,'(A)') 'Lu.ANO-RCC-VDZP'
            if(i == 72) write(30,'(A)') 'Hf.ANO-RCC-VDZP'
            if(i == 73) write(30,'(A)') 'Ta.ANO-RCC-VDZP'
            if(i == 74) write(30,'(A)') 'W.ANO-RCC-VDZP'
            if(i == 75) write(30,'(A)') 'Re.ANO-RCC-VDZP'
            if(i == 76) write(30,'(A)') 'Os.ANO-RCC-VDZP'
            if(i == 77) write(30,'(A)') 'Ir.ANO-RCC-VDZP'
            if(i == 78) write(30,'(A)') 'Pt.ANO-RCC-VDZP'
            if(i == 79) write(30,'(A)') 'Au.ANO-RCC-VDZP'
            if(i == 80) write(30,'(A)') 'Hg.ANO-RCC-VDZP'
            if(i == 81) write(30,'(A)') 'Tl.ANO-RCC-VDZP'
            if(i == 82) write(30,'(A)') 'Pb.ANO-RCC-VDZP'
            if(i == 83) write(30,'(A)') 'Bi.ANO-RCC-VDZP'
            if(i == 84) write(30,'(A)') 'Po.ANO-RCC-VDZP'
            if(i == 85) write(30,'(A)') 'At.ANO-RCC-VDZP'
            if(i == 86) write(30,'(A)') 'Rn.ANO-RCC-VDZP'
            if(i == 87) write(30,'(A)') 'Fr.ANO-RCC-VDZP'
            if(i == 88) write(30,'(A)') 'Ra.ANO-RCC-VDZP'
            if(i == 89) write(30,'(A)') 'Ac.ANO-RCC-VDZP'
            if(i == 90) write(30,'(A)') 'Th.ANO-RCC-VDZP'
            if(i == 91) write(30,'(A)') 'Pa.ANO-RCC-VDZP'
            if(i == 92) write(30,'(A)') 'U.ANO-RCC-VDZP'
            if(i == 93) write(30,'(A)') 'Np.ANO-RCC-VDZP'
            if(i == 94) write(30,'(A)') 'Pu.ANO-RCC-VDZP'
            if(i == 95) write(30,'(A)') 'Am.ANO-RCC-VDZP'
            if(i == 96) write(30,'(A)') 'Cm.ANO-RCC-VDZP'
            if(i == 97) write(30,'(A)') 'Bk.ANO-RCC-VDZP'
            if(i == 98) write(30,'(A)') 'Cf.ANO-RCC-VDZP'
            if(i == 99) write(30,'(A)') 'Es.ANO-RCC-VDZP'
            if(i == 100) write(30,'(A)') 'Fm.ANO-RCC-VDZP'
            if(i == 101) write(30,'(A)') 'Md.ANO-RCC-VDZP'
            if(i == 102) write(30,'(A)') 'No.ANO-RCC-VDZP'
            if(i == 103) write(30,'(A)') 'Lr.ANO-RCC-VDZP'
            if(i == 104) write(30,'(A)') 'Rf.ANO-RCC-VDZP'
            if(i == 105) write(30,'(A)') 'Db.ANO-RCC-VDZP'
            if(i == 106) write(30,'(A)') 'Sg.ANO-RCC-VDZP'
            if(i == 107) write(30,'(A)') 'Bh.ANO-RCC-VDZP'
            if(i == 108) write(30,'(A)') 'Hs.ANO-RCC-VDZP'
            if(i == 109) write(30,'(A)') 'Mt.ANO-RCC-VDZP'
            if(i == 110) write(30,'(A)') 'Ds.ANO-RCC-VDZP'
            if(i == 111) write(30,'(A)') 'Rg.ANO-RCC-VDZP'
            if(i == 112) write(30,'(A)') 'Cn.ANO-RCC-VDZP'
            if(i == 113) write(30,'(A)') 'Uut.ANO-RCC-VDZP'
            if(i == 114) write(30,'(A)') 'Fl.ANO-RCC-VDZP'
            if(i == 115) write(30,'(A)') 'Uup.ANO-RCC-VDZP'
            if(i == 116) write(30,'(A)') 'Lv.ANO-RCC-VDZP'
            if(i == 117) write(30,'(A)') 'Uus.ANO-RCC-VDZP'
            if(i == 118) write(30,'(A)') 'Uuo.ANO-RCC-VDZP'
            do l = 1,num
                if(atom_index(l) == i .and. done(l) == 0 .and. donor_list(l) == 1) then
                    write(30,'(A8,3F21.15,A10)') atoms(l)//' ',(coords(l,col), col = 1, 3),'  Angstrom'
                    done(l) = 1
                end if
            end do
            write(30,'(A)') 'End of Basis Set'
            write(30,*)
        end do
    end if
    
    if(std == 0) write(30,'(A15)') "Remaining atoms"
    do i = 118,1,-1
        do l = 1,num
            if(atom_index(l) == i .and. done(l) == 0) goto 62
        end do
        cycle
        62 continue
        if(std == 1) then
            write(30,'(A)') 'Basis Set'
            if(i == 1) write(30,'(A)') 'H.ANO-RCC-VDZ'
            if(i == 2) write(30,'(A)') 'He.ANO-RCC-VDZ'
            if(i == 3) write(30,'(A)') 'Li.ANO-RCC-VDZ'
            if(i == 4) write(30,'(A)') 'Be.ANO-RCC-VDZ'
            if(i == 5) write(30,'(A)') 'B.ANO-RCC-VDZ'
            if(i == 6) write(30,'(A)') 'C.ANO-RCC-VDZ'
            if(i == 7) write(30,'(A)') 'N.ANO-RCC-VDZ'
            if(i == 8) write(30,'(A)') 'O.ANO-RCC-VDZ'
            if(i == 9) write(30,'(A)') 'F.ANO-RCC-VDZ'
            if(i == 10) write(30,'(A)') 'Ne.ANO-RCC-VDZ'
            if(i == 11) write(30,'(A)') 'Na.ANO-RCC-VDZ'
            if(i == 12) write(30,'(A)') 'Mg.ANO-RCC-VDZ'
            if(i == 13) write(30,'(A)') 'Al.ANO-RCC-VDZ'
            if(i == 14) write(30,'(A)') 'Si.ANO-RCC-VDZ'
            if(i == 15) write(30,'(A)') 'P.ANO-RCC-VDZ'
            if(i == 16) write(30,'(A)') 'S.ANO-RCC-VDZ'
            if(i == 17) write(30,'(A)') 'Cl.ANO-RCC-VDZ'
            if(i == 18) write(30,'(A)') 'Ar.ANO-RCC-VDZ'
            if(i == 19) write(30,'(A)') 'K.ANO-RCC-VDZ'
            if(i == 20) write(30,'(A)') 'Ca.ANO-RCC-VDZ'
            if(i == 21) write(30,'(A)') 'Sc.ANO-RCC-VDZ'
            if(i == 22) write(30,'(A)') 'Ti.ANO-RCC-VDZ'
            if(i == 23) write(30,'(A)') 'V.ANO-RCC-VDZ'
            if(i == 24) write(30,'(A)') 'Cr.ANO-RCC-VDZ'
            if(i == 25) write(30,'(A)') 'Mn.ANO-RCC-VDZ'
            if(i == 26) write(30,'(A)') 'Fe.ANO-RCC-VDZ'
            if(i == 27) write(30,'(A)') 'Co.ANO-RCC-VDZ'
            if(i == 28) write(30,'(A)') 'Ni.ANO-RCC-VDZ'
            if(i == 29) write(30,'(A)') 'Cu.ANO-RCC-VDZ'
            if(i == 30) write(30,'(A)') 'Zn.ANO-RCC-VDZ'
            if(i == 31) write(30,'(A)') 'Ga.ANO-RCC-VDZ'
            if(i == 32) write(30,'(A)') 'Ge.ANO-RCC-VDZ'
            if(i == 33) write(30,'(A)') 'As.ANO-RCC-VDZ'
            if(i == 34) write(30,'(A)') 'Se.ANO-RCC-VDZ'
            if(i == 35) write(30,'(A)') 'Br.ANO-RCC-VDZ'
            if(i == 36) write(30,'(A)') 'Kr.ANO-RCC-VDZ'
            if(i == 37) write(30,'(A)') 'Rb.ANO-RCC-VDZ'
            if(i == 38) write(30,'(A)') 'Sr.ANO-RCC-VDZ'
            if(i == 39) write(30,'(A)') 'Y.ANO-RCC-VDZ'
            if(i == 40) write(30,'(A)') 'Zr.ANO-RCC-VDZ'
            if(i == 41) write(30,'(A)') 'Nb.ANO-RCC-VDZ'
            if(i == 42) write(30,'(A)') 'Mo.ANO-RCC-VDZ'
            if(i == 43) write(30,'(A)') 'Tc.ANO-RCC-VDZ'
            if(i == 44) write(30,'(A)') 'Ru.ANO-RCC-VDZ'
            if(i == 45) write(30,'(A)') 'Rh.ANO-RCC-VDZ'
            if(i == 46) write(30,'(A)') 'Pd.ANO-RCC-VDZ'
            if(i == 47) write(30,'(A)') 'Ag.ANO-RCC-VDZ'
            if(i == 48) write(30,'(A)') 'Cd.ANO-RCC-VDZ'
            if(i == 49) write(30,'(A)') 'In.ANO-RCC-VDZ'
            if(i == 50) write(30,'(A)') 'Sn.ANO-RCC-VDZ'
            if(i == 51) write(30,'(A)') 'Sb.ANO-RCC-VDZ'
            if(i == 52) write(30,'(A)') 'Te.ANO-RCC-VDZ'
            if(i == 53) write(30,'(A)') 'I.ANO-RCC-VDZ'
            if(i == 54) write(30,'(A)') 'Xe.ANO-RCC-VDZ'
            if(i == 55) write(30,'(A)') 'Cs.ANO-RCC-VDZ'
            if(i == 56) write(30,'(A)') 'Ba.ANO-RCC-VDZ'
            if(i == 57) write(30,'(A)') 'La.ANO-RCC-VDZ'
            if(i == 58) write(30,'(A)') 'Ce.ANO-RCC-VDZ'
            if(i == 59) write(30,'(A)') 'Pr.ANO-RCC-VDZ'
            if(i == 60) write(30,'(A)') 'Nd.ANO-RCC-VDZ'
            if(i == 61) write(30,'(A)') 'Pm.ANO-RCC-VDZ'
            if(i == 62) write(30,'(A)') 'Sm.ANO-RCC-VDZ'
            if(i == 63) write(30,'(A)') 'Eu.ANO-RCC-VDZ'
            if(i == 64) write(30,'(A)') 'Gd.ANO-RCC-VDZ'
            if(i == 65) write(30,'(A)') 'Tb.ANO-RCC-VDZ'
            if(i == 66) write(30,'(A)') 'Dy.ANO-RCC-VDZ'
            if(i == 67) write(30,'(A)') 'Ho.ANO-RCC-VDZ'
            if(i == 68) write(30,'(A)') 'Er.ANO-RCC-VDZ'
            if(i == 69) write(30,'(A)') 'Tm.ANO-RCC-VDZ'
            if(i == 70) write(30,'(A)') 'Yb.ANO-RCC-VDZ'
            if(i == 71) write(30,'(A)') 'Lu.ANO-RCC-VDZ'
            if(i == 72) write(30,'(A)') 'Hf.ANO-RCC-VDZ'
            if(i == 73) write(30,'(A)') 'Ta.ANO-RCC-VDZ'
            if(i == 74) write(30,'(A)') 'W.ANO-RCC-VDZ'
            if(i == 75) write(30,'(A)') 'Re.ANO-RCC-VDZ'
            if(i == 76) write(30,'(A)') 'Os.ANO-RCC-VDZ'
            if(i == 77) write(30,'(A)') 'Ir.ANO-RCC-VDZ'
            if(i == 78) write(30,'(A)') 'Pt.ANO-RCC-VDZ'
            if(i == 79) write(30,'(A)') 'Au.ANO-RCC-VDZ'
            if(i == 80) write(30,'(A)') 'Hg.ANO-RCC-VDZ'
            if(i == 81) write(30,'(A)') 'Tl.ANO-RCC-VDZ'
            if(i == 82) write(30,'(A)') 'Pb.ANO-RCC-VDZ'
            if(i == 83) write(30,'(A)') 'Bi.ANO-RCC-VDZ'
            if(i == 84) write(30,'(A)') 'Po.ANO-RCC-VDZ'
            if(i == 85) write(30,'(A)') 'At.ANO-RCC-VDZ'
            if(i == 86) write(30,'(A)') 'Rn.ANO-RCC-VDZ'
            if(i == 87) write(30,'(A)') 'Fr.ANO-RCC-VDZ'
            if(i == 88) write(30,'(A)') 'Ra.ANO-RCC-VDZ'
            if(i == 89) write(30,'(A)') 'Ac.ANO-RCC-VDZ'
            if(i == 90) write(30,'(A)') 'Th.ANO-RCC-VDZ'
            if(i == 91) write(30,'(A)') 'Pa.ANO-RCC-VDZ'
            if(i == 92) write(30,'(A)') 'U.ANO-RCC-VDZ'
            if(i == 93) write(30,'(A)') 'Np.ANO-RCC-VDZ'
            if(i == 94) write(30,'(A)') 'Pu.ANO-RCC-VDZ'
            if(i == 95) write(30,'(A)') 'Am.ANO-RCC-VDZ'
            if(i == 96) write(30,'(A)') 'Cm.ANO-RCC-VDZ'
            if(i == 97) write(30,'(A)') 'Bk.ANO-RCC-VDZ'
            if(i == 98) write(30,'(A)') 'Cf.ANO-RCC-VDZ'
            if(i == 99) write(30,'(A)') 'Es.ANO-RCC-VDZ'
            if(i == 100) write(30,'(A)') 'Fm.ANO-RCC-VDZ'
            if(i == 101) write(30,'(A)') 'Md.ANO-RCC-VDZ'
            if(i == 102) write(30,'(A)') 'No.ANO-RCC-VDZ'
            if(i == 103) write(30,'(A)') 'Lr.ANO-RCC-VDZ'
            if(i == 104) write(30,'(A)') 'Rf.ANO-RCC-VDZ'
            if(i == 105) write(30,'(A)') 'Db.ANO-RCC-VDZ'
            if(i == 106) write(30,'(A)') 'Sg.ANO-RCC-VDZ'
            if(i == 107) write(30,'(A)') 'Bh.ANO-RCC-VDZ'
            if(i == 108) write(30,'(A)') 'Hs.ANO-RCC-VDZ'
            if(i == 109) write(30,'(A)') 'Mt.ANO-RCC-VDZ'
            if(i == 110) write(30,'(A)') 'Ds.ANO-RCC-VDZ'
            if(i == 111) write(30,'(A)') 'Rg.ANO-RCC-VDZ'
            if(i == 112) write(30,'(A)') 'Cn.ANO-RCC-VDZ'
            if(i == 113) write(30,'(A)') 'Uut.ANO-RCC-VDZ'
            if(i == 114) write(30,'(A)') 'Fl.ANO-RCC-VDZ'
            if(i == 115) write(30,'(A)') 'Uup.ANO-RCC-VDZ'
            if(i == 116) write(30,'(A)') 'Lv.ANO-RCC-VDZ'
            if(i == 117) write(30,'(A)') 'Uus.ANO-RCC-VDZ'
            if(i == 118) write(30,'(A)') 'Uuo.ANO-RCC-VDZ'
        end if
        do l = 1,num
            if(atom_index(l) == i .and. done(l) == 0) then
                write(30,'(A8,3F21.15,A10)') atoms(l)//' ',(coords(l,col),col = 1,3),'  Angstrom'
                done(l) = 1
            end if
        end do
        if(std == 1) then
            write(30,'(A)') 'End of Basis Set'
            write(30,*)
        end if
    end do
    
    if(std == 1) then
        call getcwd(cwd)
        
        if(active <= orbitals) then
            high_spin = active + 1
        else
            high_spin = 2*orbitals - active + 1
        end if
        low_spin = mod(active,2) + 1

        l = 0
        if(phonon == 1) lim = high_spin
        if(phonon /= 1) lim = low_spin
        do i = high_spin,lim,-2
            l = l + 1
            if(l /= 1) then
                write(30,*)
            end if
            write(30,'(A)') '&RASSCF'
            if(l /= 1) then
                write(30,'(A8,I2,A7)') 'FILEORB=',l-1,'.RasOrb'
            end if
            write(30,'(A,I2)') 'Spin= ',i
            write(30,'(A,I2,A)') 'Nactel= ',active,' 0 0'
            write(30,'(A,I2)') 'Ras2= ',orbitals
            write(30,'(A,I3)') 'Inactive= ',nint((electrons-charge-active)*0.5_8)
            if(atom_index(atoms_list(j)) >= 57 .and. atom_index(atoms_list(j)) <= 71 .and. orbitals == 7) then
                if(active == 1 .or. active == 13) then
                    if(i == 2) write(30,'(A)') 'CiRoot= 7 7 1'
                else if(active == 2 .or. active == 12) then
                    if(i == 3) write(30,'(A)') 'CiRoot= 21 21 1'
                    if(i == 1) write(30,'(A)') 'CiRoot= 28 28 1'
                else if(active == 3 .or. active == 11) then
                    if(i == 4) write(30,'(A)') 'CiRoot= 35 35 1'
                    if(i == 2) write(30,'(A)') 'CiRoot= 112 112 1'
                else if(active == 4 .or. active == 10) then
                    if(i == 5) write(30,'(A)') 'CiRoot= 35 35 1'
                    if(i == 3) write(30,'(A)') 'CiRoot= 210 210 1'
                    if(i == 1) write(30,'(A)') 'CiRoot= 196 196 1'
                else if(active == 5 .or. active == 9) then
                    if(i == 6) write(30,'(A)') 'CiRoot= 21 21 1'
                    if(i == 4) write(30,'(A)') 'CiRoot= 224 224 1'
                    if(i == 2) write(30,'(A)') 'CiRoot= 490 490 1'
                else if(active == 6 .or. active == 8) then
                    if(i == 7) write(30,'(A)') 'CiRoot= 7 7 1'
                    if(i == 5) write(30,'(A)') 'CiRoot= 140 140 1'
                    if(i == 3) write(30,'(A)') 'CiRoot= 472 472 1'
                    if(i == 1) write(30,'(A)') 'CiRoot= 490 490 1'
                else if(active == 7) then
                    if(i == 8) write(30,'(A)') 'CiRoot= 1 1 1'
                    if(i == 6) write(30,'(A)') 'CiRoot= 48 48 1'
                    if(i == 4) write(30,'(A)') 'CiRoot= 392 392 1'
                    if(i == 2) write(30,'(A)') 'CiRoot= x x 1'
                end if
            else
                k = nint(dble((dble(i)/dble(orbitals+1))*binom(orbitals+1,nint(0.5_8*active-(i-1)*0.5_8))*binom(orbitals+1,nint(0.5_8*active+(i-1)*0.5_8+1))))
                if(k <= 500) then
                    write(30,'(A,2I4,A)') 'CiRoot= ',k,k,' 1'
                else
                    write(30,'(A)') 'CiRoot= x x 1'
                end if
            end if
            write(30,'(A)') 'ORBA=FULL'
            write(30,'(A)') 'MAXO=1'
            write(30,'(A,I2,A)') '>> COPY $Project.JobIph ',l,'_IPH'
            write(30,'(A,I2,A)') '>> COPY $Project.RasOrb ',l,'.RasOrb'
            if(l == 1 .and. phonon == 0) then
                write(30,*)
                write(30,'(A)') '&RASSCF'
                write(30,'(A)') 'FILEORB=1.RasOrb'
                write(30,'(A,I2,A)') 'Nactel= ',active,' 0 0'
                if(atom_index(atoms_list(j)) >= 57 .and. atom_index(atoms_list(j)) <= 71 .and. orbitals == 7) then
                    if(active == 1 .or. active == 13) write(30,'(A)') 'CiRoot= 7 7 1'
                    if(active == 2 .or. active == 12) write(30,'(A)') 'CiRoot= 21 21 1'
                    if(active == 3 .or. active == 11) write(30,'(A)') 'CiRoot= 35 35 1'
                    if(active == 4 .or. active == 10) write(30,'(A)') 'CiRoot= 35 35 1'
                    if(active == 5 .or. active == 9) write(30,'(A)') 'CiRoot= 21 21 1'
                    if(active == 6 .or. active == 8) write(30,'(A)') 'CiRoot= 7 7 1'
                    if(active == 7) write(30,'(A)') 'CiRoot= 1 1 1'
                else
                    k = nint(dble((dble(i)/dble(orbitals+1))*binom(orbitals+1,nint(0.5_8*active-(i-1)*0.5_8))*binom(orbitals+1,nint(0.5_8*active+(i-1)*0.5_8+1))))
                    if(k <= 500) then
                        write(30,'(A,2I4,A)') 'CiRoot= ',k,k,' 1'
                    else
                        write(30,'(A)') 'CiRoot= x x 1'
                    end if
                end if
                write(30,'(A)') 'OUTO=CANO'
                write(30,'(A)') 'MAXO=1'
                write(30,'(A,I2,A)') '>> COPY $Project.RasOrb ',l,'.CanOrb'
            end if
        end do

        write(30,*)
        write(30,'(A)') '&RASSI'
        !Spin phonon mode
        if(phonon == 1) then 
            allocate(root_list(l))
            root_list = 0
            write(30,'(A14,I2,A1)',advance='no') 'Nr of JobIph= ',l,' '
            i = high_spin
            l = l + 1
            !for lanthanides just use this list of roots
            if(atom_index(atoms_list(j)) >= 57 .and. atom_index(atoms_list(j)) <= 71 .and. orbitals == 7) then
                if(active == 1 .or. active == 13) then
                    if(i == 2) then
                        write(30,'(A2)',advance='no') '7 '
                        root_list(l) = 7
                    end if
                else if(active == 2 .or. active == 12) then
                    if(i == 3) then
                        write(30,'(A3)',advance='no') '21 '
                        root_list(l) = 21
                    else if(i == 1) then
                        write(30,'(A3)',advance='no') '28 '
                        root_list(l) = 28
                    end if
                else if(active == 3 .or. active == 11) then
                    if(i == 4) then
                        write(30,'(A3)',advance='no') '35 '
                        root_list(l) = 35
                    else if(i == 2) then
                        write(30,'(A4)',advance='no') '112 '
                        root_list(l) = 112
                    end if
                else if(active == 4 .or. active == 10) then
                    if(i == 5) then
                        write(30,'(A3)',advance='no') '35 '
                        root_list(l) = 35
                    else if(i == 3) then
                        write(30,'(A4)',advance='no') '159 '
                        root_list(l) = 159
                    else if(i == 1) then
                        write(30,'(A4)',advance='no') '156 '
                        root_list(l) = 156
                    end if
                else if(active == 5 .or. active == 9) then
                    if(i == 6) then
                        write(30,'(A3)',advance='no') '21 '
                        root_list(l) = 21
                    else if(i == 4) then
                        write(30,'(A4)',advance='no') '128 '
                        root_list(l) = 128
                    else if(i == 2) then
                        write(30,'(A4)',advance='no') '130 '
                        root_list(l) = 130
                    end if
                else if(active == 6 .or. active == 8) then
                    if(i == 7) then
                        write(30,'(A2)',advance='no') '7 '
                        root_list(l) = 7
                    else if(i == 5) then
                        write(30,'(A4)',advance='no') '140 '
                        root_list(l) = 140
                    else if(i == 3) then
                        write(30,'(A4)',advance='no') '195 '
                        root_list(l) = 195
                    else if(i == 1) then
                        write(30,'(A4)',advance='no') '197 '
                        root_list(l) = 197
                    end if
                else if(active == 7) then
                    if(i == 8) then
                        write(30,'(A24)',advance='no') '1 '
                        root_list(l) = 1
                    else if(i == 6) then
                        write(30,'(A3)',advance='no') '48 '
                        root_list(l) = 48
                    else if(i == 4) then
                        write(30,'(A4)',advance='no') '119 '
                        root_list(l) = 119
                    else if(i == 2) then
                        write(30,'(A4)',advance='no') '113 '
                        root_list(l) = 113
                    end if
                end if
            else
                !all other elements calculate the binomial formula
                k = nint(dble((dble(i)/dble(orbitals+1))*binom(orbitals+1,nint(0.5_8*active-(i-1)*0.5_8))*binom(orbitals+1,nint(0.5_8*active+(i-1)*0.5_8+1))))
                if(k <= 500) then
                    write(30,'(I3,A1)',advance='no') k,' '
                    root_list(l) = k
                else
                    write(30,'(A2)',advance='no') 'x '
                    root_list(l) = -1
                end if
            end if

            write(30,'(A2)',advance='no') '; '
            do i = 1,l
                if(root_list(i) > 0) then
                    do k = 1,root_list(i)
                        write(30,'(I4)',advance='no') k
                    end do
                    if(i /= l) then
                        write(30,'(A2)',advance='no') '; '
                    else
                        write(30,*)
                    end if
                end if
            end do
        end if


        !Normal mode
        if(phonon == 0) then

        allocate(root_list(l))
        root_list = 0
            write(30,'(A14,I2,A1)',advance='no') 'Nr of JobIph= ',l,' '
            l = 0
            do i = high_spin,low_spin,-2
                l = l + 1
                !for lanthanides just use this list of roots
                if(atom_index(atoms_list(j)) >= 57 .and. atom_index(atoms_list(j)) <= 71 .and. orbitals == 7) then
                    if(active == 1 .or. active == 13) then
                        if(i == 2) then
                            write(30,'(A2)',advance='no') '7 '
                            root_list(l) = 7
                        end if
                    else if(active == 2 .or. active == 12) then
                        if(i == 3) then
                            write(30,'(A3)',advance='no') '21 '
                            root_list(l) = 21
                        else if(i == 1) then
                            write(30,'(A3)',advance='no') '28 '
                            root_list(l) = 28
                        end if
                    else if(active == 3 .or. active == 11) then
                        if(i == 4) then
                            write(30,'(A3)',advance='no') '35 '
                            root_list(l) = 35
                        else if(i == 2) then
                            write(30,'(A4)',advance='no') '112 '
                            root_list(l) = 112
                        end if
                    else if(active == 4 .or. active == 10) then
                        if(i == 5) then
                            write(30,'(A3)',advance='no') '35 '
                            root_list(l) = 35
                        else if(i == 3) then
                            write(30,'(A4)',advance='no') '159 '
                            root_list(l) = 159
                        else if(i == 1) then
                            write(30,'(A4)',advance='no') '156 '
                            root_list(l) = 156
                        end if
                    else if(active == 5 .or. active == 9) then
                        if(i == 6) then
                            write(30,'(A3)',advance='no') '21 '
                            root_list(l) = 21
                        else if(i == 4) then
                            write(30,'(A4)',advance='no') '128 '
                            root_list(l) = 128
                        else if(i == 2) then
                            write(30,'(A4)',advance='no') '130 '
                            root_list(l) = 130
                        end if
                    else if(active == 6 .or. active == 8) then
                        if(i == 7) then
                            write(30,'(A2)',advance='no') '7 '
                            root_list(l) = 7
                        else if(i == 5) then
                            write(30,'(A4)',advance='no') '140 '
                            root_list(l) = 140
                        else if(i == 3) then
                            write(30,'(A4)',advance='no') '195 '
                            root_list(l) = 195
                        else if(i == 1) then
                            write(30,'(A4)',advance='no') '197 '
                            root_list(l) = 197
                        end if
                    else if(active == 7) then
                        if(i == 8) then
                            write(30,'(A24)',advance='no') '1 '
                            root_list(l) = 1
                        else if(i == 6) then
                            write(30,'(A3)',advance='no') '48 '
                            root_list(l) = 48
                        else if(i == 4) then
                            write(30,'(A4)',advance='no') '119 '
                            root_list(l) = 119
                        else if(i == 2) then
                            write(30,'(A4)',advance='no') '113 '
                            root_list(l) = 113
                        end if
                    end if
                else
                    !all other elements calculate the binomial formula
                    k = nint(dble((dble(i)/dble(orbitals+1))*binom(orbitals+1,nint(0.5_8*active-(i-1)*0.5_8))*binom(orbitals+1,nint(0.5_8*active+(i-1)*0.5_8+1))))
                    if(k <= 500) then
                        write(30,'(I3,A1)',advance='no') k,' '
                        root_list(l) = k
                    else
                        write(30,'(A2)',advance='no') 'x '
                        root_list(l) = -1
                    end if
                end if
            end do
            write(30,'(A2)',advance='no') '; '
            do i = 1,l
                if(root_list(i) > 0) then
                    do k = 1,root_list(i)
                        write(30,'(I4)',advance='no') k
                    end do
                    if(i /= l) then
                        write(30,'(A2)',advance='no') '; '
                    else
                        write(30,*)
                    end if
                end if
            end do
        end if
        write(30,'(A)') 'Spin'
        write(30,'(A)') 'IPHN'
        write(30,'(A)') '1_IPH'
        if(phonon == 0) then
            do i = 2,l
                write(30,'(I2,A)') i,'_IPH'
            end do
        end if
        write(30,'(A)') 'MEES'
        write(30,'(A)') 'EJob'
        write(30,'(A)') 'PROP'
        write(30,'(A)') '3'
        write(30,'(A)') "'ANGMOM' 1"
        write(30,'(A)') "'ANGMOM' 2"
        write(30,'(A)') "'ANGMOM' 3"
        write(30,'(A)') 'EPRG'
        write(30,'(A)') '7.0D-1'
        write(30,*)
        write(30,'(A)') '&SINGLE_ANISO'
        write(30,'(A)') 'TYPE'
        if(phonon == 1) write(30,'(A)') '1'
        if(phonon == 0) then
            if(atom_index(atoms_list(j)) >= 57 .and. atom_index(atoms_list(j)) <= 71 .and. orbitals == 7) then
                write(30,'(A)') '6'
            else
                write(30,'(A)') '7'
            end if
            write(30,'(A)') 'TINT'
            write(30,'(A)') '0.0  330.0  300  0.0001'
            write(30,'(A)') 'HINT'
            write(30,'(A)') '0.0  10.0  201'
            write(30,'(A)') 'TMAG'
            write(30,'(A)') '6 1.8 2 4 5 10 20'
        end if
        if(atom_index(atoms_list(j)) >= 57 .and. atom_index(atoms_list(j)) <= 71 .and. orbitals == 7) then
            write(30,'(A)') 'CRYS'
            write(30,'(A)') trim(element)
        else
            write(30,'(A)') 'MLTP'
            write(30,'(A)') '1'
            write(30,'(I2)') high_spin
        end if
        open(31,file= trim(rawfile)//'.sh',status='unknown')
        write(31,'(A)') '#!/bin/bash --login'
        write(31,'(A)') '#$ -N '//trim(rawfile)
        write(31,'(A)') '#$ -cwd'
        IF (super_high_mem .EQV. .TRUE.) THEN
            WRITE(31,'(A)') '#$ -l mem512'
        ELSE
            WRITE(31,'(A)') '#$ -l mem256'
        END IF
        write(31,'(A)') 'module load apps/binapps/molcasuu/8.0sp1'
        write(31,'(A)') '#------------------------------------------------------------------------------#'
        write(31,'(A)') 'Project='//trim(rawfile)
        write(31,'(A)') '#------------------------------------------------------------------------------#'
        write(31,'(A)') 'export MOLCAS_WORKDIR=/scratch/$USER/$Project'
        IF (super_high_mem .EQV. .TRUE.) THEN
            WRITE(31,'(A)') 'export MOLCAS_MEM=32000'
        ELSE
            WRITE(31,'(A)') 'export MOLCAS_MEM=16000'
        END IF
        write(31,'(A)') 'export MOLCAS_PRINT=2'
        write(31,'(A)') 'export MOLCASDISK=20000'
        write(31,'(A)') 'export MOLCAS_MOLDEN=ON'
        write(31,'(A)') 'mkdir -p $MOLCAS_WORKDIR'
        write(31,'(A)') 'molcas $Project.in >> $Project.out'
        close(31)
        call system('chmod +x '//trim(rawfile)//'.sh')
    end if
    close(30)
    deallocate(atoms,coords,radii,list,atom_index,done,donor_list,root_list)
    stop
    
    contains

    SUBROUTINE READ_USER_INPUT(file, element, charge, active, orbitals, donors, phonon, super_high_mem)
        IMPLICIT NONE
        INTEGER, INTENT(OUT)            :: charge, active, orbitals, donors, phonon
        CHARACTER(LEN = *), INTENT(OUT) :: file, element
        CHARACTER(LEN = 500)            :: cdummy
        LOGICAL, INTENT(OUT)            :: super_high_mem
        LOGICAL                         :: file_exists

        CALL GET_COMMAND_ARGUMENT(1,file)

        IF(TRIM(file) == "-h" .or. TRIM(file) == '-H' .or. TRIM(file) == '') then
            WRITE(6,'(A)') "molcas_gen <file> <element> <charge> <active_electrons> <orbitals> <donors> [<spin-phonon y/n>] [<high-mem y/n>]"
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Creates a molcas input file for the CSF from an xyz file, central metal ion, charge, number of orbitals'
            WRITE(6,'(A)')   'in the active space, number of electrons in the active space, number of donors, and optionally'
            WRITE(6,'(A)')   'can be run in spin-phonon mode - calculates lowest energy multiplet only and enables RICD ACcd'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'file             : CHARACTER          xyz file of complex                           e.g  dy.xyz'
            WRITE(6,'(A)') 'element          : CHARACTER          metal at centre of complex                    e.g. dy'
            WRITE(6,'(A)') 'charge           : INTEGER            charge on complex                             e.g  0 for neutral, 1 for +1, -1 for -1'
            WRITE(6,'(A)') 'active_electrons : INTEGER            number of electrons in active space           e.g  9'
            WRITE(6,'(A)') 'orbitals         : INTEGER            number of orbitals in active space            e.g  7'
            WRITE(6,'(A)') 'donors           : INTEGER            number of donor atoms bound to central metal  e.g  5'
            WRITE(6,'(A)') 'spin-phonon      * OPTIONAL CHARACTER yes or no to spin-phonon mode                 e.g  y or n'
            WRITE(6,'(A)') 'high-mem         * OPTIONAL CHARACTER requests 32GB node on CSF rather than 16GB    e.g  y or n'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Example ------ molcas_gen dy.xyz dy 0 9 7 5 n y'
            WRITE(6,'(A)') ''
            STOP
        END IF

        !Check file exists
        INQUIRE(FILE=file, EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(A)') 'Specified file does not exist'
            STOP
        END IF

        CALL GET_COMMAND_ARGUMENT(2,element)

        CALL GET_COMMAND_ARGUMENT(3,cdummy)
        READ(cdummy,*) charge

        CALL GET_COMMAND_ARGUMENT(4,cdummy)
        READ(cdummy,*) active

        CALL GET_COMMAND_ARGUMENT(5,cdummy)
        READ(cdummy,*) orbitals

        CALL GET_COMMAND_ARGUMENT(6,cdummy)
        READ(cdummy,*) donors

        CALL GET_COMMAND_ARGUMENT(7,cdummy)
        IF(TRIM(ADJUSTL(cdummy)) == 'y' .OR. TRIM(ADJUSTL(cdummy)) == 'Y') phonon = 1

        CALL GET_COMMAND_ARGUMENT(8,cdummy)
        IF(TRIM(ADJUSTL(cdummy)) == 'y' .OR. TRIM(ADJUSTL(cdummy)) == 'Y') THEN
            super_high_mem = .TRUE.
        ELSE
            super_high_mem = .FALSE.
        END IF

    END SUBROUTINE READ_USER_INPUT
    
    recursive subroutine re_order(num,values,list,way)
        implicit none
        integer,intent(in)::num
        integer,intent(out),dimension(:)::list(:)
        integer::j,k
        real(kind=8),intent(in),dimension(:)::values(:)
        real(kind=8)::low
        integer,allocatable::done(:)
        character(len=3),intent(in)::way
        allocate(done(num))
        list(:) = 0
        done(:) = 0
        list(1) = minloc(values,1)
        done(list(1)) = 1
        do j=2,num
            low = 1D99
            do k=1,num
                if((values(k) - values(list(1))) >= (values(list(j-1)) - values(list(1))) .and. (values(k) - values(list(1))) < low .and. done(k) == 0) then
                    low = (values(k) - values(list(1)))
                    list(j) = k
                end if
            end do
            done(list(j)) = 1
        end do
        if(way == 'max') then
            do j = 1,num
                done(j) = list(num-j+1)
            end do
            list = done
        end if
        deallocate(done)
    end subroutine re_order
    
    subroutine name_parse(str,ii)
    implicit none
        character(len=3)::strN
        character(len=6)::str
        integer::gap,i,ele,length,ii
        gap=ICHAR('a')-ICHAR('A')
        if(len(str) > 0) then
            do i=1,len(str)
                if(str(i:i) <= 'Z' .and. str(i:i) >= 'A') str(i:i)=CHAR(ICHAR(str(i:i))+gap)
                if(.not.(str(i:i) <= 'z' .and. str(i:i) >= 'a')) str(i:i) = ' '
            end do
        end if
        ele = 0
        if(trim(str) == 'h') ele = 1
        if(trim(str) == 'he') ele = 2
        if(trim(str) == 'li') ele = 3
        if(trim(str) == 'be') ele = 4
        if(trim(str) == 'b') ele = 5
        if(trim(str) == 'c') ele = 6
        if(trim(str) == 'n') ele = 7
        if(trim(str) == 'o') ele = 8
        if(trim(str) == 'f') ele = 9
        if(trim(str) == 'ne') ele = 10
        if(trim(str) == 'na') ele = 11
        if(trim(str) == 'mg') ele = 12
        if(trim(str) == 'al') ele = 13
        if(trim(str) == 'si') ele = 14
        if(trim(str) == 'p') ele = 15
        if(trim(str) == 's') ele = 16
        if(trim(str) == 'cl') ele = 17
        if(trim(str) == 'ar') ele = 18
        if(trim(str) == 'k') ele = 19
        if(trim(str) == 'ca') ele = 20
        if(trim(str) == 'sc') ele = 21
        if(trim(str) == 'ti') ele = 22
        if(trim(str) == 'v') ele = 23
        if(trim(str) == 'cr') ele = 24
        if(trim(str) == 'mn') ele = 25
        if(trim(str) == 'fe') ele = 26
        if(trim(str) == 'co') ele = 27
        if(trim(str) == 'ni') ele = 28
        if(trim(str) == 'cu') ele = 29
        if(trim(str) == 'zn') ele = 30
        if(trim(str) == 'ga') ele = 31
        if(trim(str) == 'ge') ele = 32
        if(trim(str) == 'as') ele = 33
        if(trim(str) == 'se') ele = 34
        if(trim(str) == 'br') ele = 35
        if(trim(str) == 'kr') ele = 36
        if(trim(str) == 'rb') ele = 37
        if(trim(str) == 'sr') ele = 38
        if(trim(str) == 'y') ele = 39
        if(trim(str) == 'zr') ele = 40
        if(trim(str) == 'nb') ele = 41
        if(trim(str) == 'mo') ele = 42
        if(trim(str) == 'tc') ele = 43
        if(trim(str) == 'ru') ele = 44
        if(trim(str) == 'rh') ele = 45
        if(trim(str) == 'pd') ele = 46
        if(trim(str) == 'ag') ele = 47
        if(trim(str) == 'cd') ele = 48
        if(trim(str) == 'in') ele = 49
        if(trim(str) == 'sn') ele = 50
        if(trim(str) == 'sb') ele = 51
        if(trim(str) == 'te') ele = 52
        if(trim(str) == 'i') ele = 53
        if(trim(str) == 'xe') ele = 54
        if(trim(str) == 'cs') ele = 55
        if(trim(str) == 'ba') ele = 56
        if(trim(str) == 'la') ele = 57
        if(trim(str) == 'ce') ele = 58
        if(trim(str) == 'pr') ele = 59
        if(trim(str) == 'nd') ele = 60
        if(trim(str) == 'pm') ele = 61
        if(trim(str) == 'sm') ele = 62
        if(trim(str) == 'eu') ele = 63
        if(trim(str) == 'gd') ele = 64
        if(trim(str) == 'tb') ele = 65
        if(trim(str) == 'dy') ele = 66
        if(trim(str) == 'ho') ele = 67
        if(trim(str) == 'er') ele = 68
        if(trim(str) == 'tm') ele = 69
        if(trim(str) == 'yb') ele = 70
        if(trim(str) == 'lu') ele = 71
        if(trim(str) == 'hf') ele = 72
        if(trim(str) == 'ta') ele = 73
        if(trim(str) == 'w') ele = 74
        if(trim(str) == 're') ele = 75
        if(trim(str) == 'os') ele = 76
        if(trim(str) == 'ir') ele = 77
        if(trim(str) == 'pt') ele = 78
        if(trim(str) == 'au') ele = 79
        if(trim(str) == 'hg') ele = 80
        if(trim(str) == 'tl') ele = 81
        if(trim(str) == 'pb') ele = 82
        if(trim(str) == 'bi') ele = 83
        if(trim(str) == 'po') ele = 84
        if(trim(str) == 'at') ele = 85
        if(trim(str) == 'rn') ele = 86
        if(trim(str) == 'fr') ele = 87
        if(trim(str) == 'ra') ele = 88
        if(trim(str) == 'ac') ele = 89
        if(trim(str) == 'th') ele = 90
        if(trim(str) == 'pa') ele = 91
        if(trim(str) == 'u') ele = 92
        if(trim(str) == 'np') ele = 93
        if(trim(str) == 'pu') ele = 94
        if(trim(str) == 'am') ele = 95
        if(trim(str) == 'cm') ele = 96
        if(trim(str) == 'bk') ele = 97
        if(trim(str) == 'cf') ele = 98
        if(trim(str) == 'es') ele = 99
        if(trim(str) == 'fm') ele = 100
        if(trim(str) == 'md') ele = 101
        if(trim(str) == 'no') ele = 102
        if(trim(str) == 'lr') ele = 103
        if(trim(str) == 'rf') ele = 104
        if(trim(str) == 'db') ele = 105
        if(trim(str) == 'sg') ele = 106
        if(trim(str) == 'bh') ele = 107
        if(trim(str) == 'hs') ele = 108
        if(trim(str) == 'mt') ele = 109
        if(trim(str) == 'ds') ele = 110
        if(trim(str) == 'rg') ele = 111
        if(trim(str) == 'cn') ele = 112
        if(trim(str) == 'uut') ele = 113
        if(trim(str) == 'fl') ele = 114
        if(trim(str) == 'uup') ele = 115
        if(trim(str) == 'lv') ele = 116
        if(trim(str) == 'uus') ele = 117
        if(trim(str) == 'uuo') ele = 118
        if(ele == 0) then
            write(6,'(A)') "Element ",trim(str)," is unknown"
            stop
        end if
        atom_index(ii) = ele
        if(trim(str) == trim(element)) atoms_to_do = atoms_to_do + 1
        counter(ele) = counter(ele) + 1
        if(counter(ele) < 10) then
            write(strN,'(I1)') counter(ele)
        else if(counter(ele) < 100) then
            write(strN,'(I2)') counter(ele)
        else if(counter(ele) < 1000) then
            write(strN,'(I3)') counter(ele)
        end if
        str = trim(str)//trim(strN)
        if(str(1:1) <= 'z' .and. str(1:1) >= 'a') str(1:1)=CHAR(ICHAR(str(1:1))-gap)
    end subroutine name_parse
    
    recursive function radial(input) result(res)
    implicit none
        real(kind=8),intent(in)::input(3)
        real(kind=8)::res
        res = dsqrt(input(1)*input(1)+input(2)*input(2)+input(3)*input(3))
    end function radial
    
    recursive function binom(n,r) result(res)
    implicit none
    integer,intent(in)::n,r
    real(kind=8)::res
    if(n < 0 .or. r < 0 .or. r > n) then
        res = 0.0_8
    else if(n == 0) then
        if(r == 0) then
            res = 1.0_8
        else
            res = 0.0_8
        end if
    else if(n==r .or. r==0) then
       res = 1.0_8
    else if (r==1) then
       res = dble(n)
    else
       res = dble(n)/dble(n-r)*binom(n-1,r)
    end if
    end function binom

END PROGRAM MOLCAS_INPUT_GEN