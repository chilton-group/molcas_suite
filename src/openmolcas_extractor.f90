!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   openmolcas_extractor.f90 
!
!   This program is a part of molcas_suite
!
!
!   Extracts energies, CFPs (and CFP calculated energies and wavefunctions), principal g values,
!    g tensors, angles of maximal g components to ground state, and spin-orbit weights of the 
!    first num_states spin orbit states in a given molcas output file and WRITEs to <file>_extracted.out
!
!   
!      Authors:
!       Jon Kragskow
!       Core code by Nicholas Chilton
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

PROGRAM EXTRACTOR
    USE, INTRINSIC::iso_fortran_env
    use matrix_tools
    IMPLICIT NONE

    CHARACTER(LEN = 5000)             :: input_file
    CHARACTER(LEN = 2)                :: element, orb_type
    REAL(KIND = 8)                    :: threshold, CFP_J(3,27), CFP_L(3,27)
    REAL(KIND = 8), ALLOCATABLE       :: energies_J(:), energies_L(:), g_vals(:,:), g_vecs(:,:,:),& 
                                         ang(:), composition_J(:,:), composition_L(:,:),&
                                         cf_percents_J(:,:), cf_percents_L(:,:), cf_energies_J(:),& 
                                         cf_energies_L(:), energies_all(:), Jz_vals(:), Lz_vals(:)
    LOGICAL                           :: orb_print, Kramers_J, SA_SKIP_PRINT
    INTEGER                           :: num_states_J, num_states_L, k_max_index_J, k_max_index_L, &
                                         num_so_states

    ! Read user input from command line
    CALL READ_USER_INPUT(element, orb_type, num_states_J, input_file, orb_print, threshold)

    ! Read in RASSCF information
    CALL EXTRACT_RASSCF(input_file, orb_type, orb_print, threshold)

    ! Read in RASSI information
    CALL EXTRACT_RASSI(input_file, g_vals, g_vecs, num_states_J, num_so_states, energies_J, &
                       energies_L, energies_all)

    ! Read in SINGLE_ANISO information
    CALL EXTRACT_SINGLE_ANISO(input_file, element, CFP_J, CFP_L, k_max_index_J, k_max_index_L, &
                              num_states_J, num_states_L, composition_J, &
                              composition_L, SA_SKIP_PRINT)

    ! If Kramers, calculate maximum and minimum of x,y, and z g values of each state
    IF(MODULO(num_states_J,2) == 0) THEN !Check if Kramers
        CALL CALC_G_PROPS(num_states_J, g_vals, g_vecs, ang)
        Kramers_J = .TRUE.
    ELSE
        Kramers_J = .FALSE.
    END IF

    IF (SA_SKIP_PRINT .eqv. .TRUE.) THEN
        CALL WRITE_OUTPUT_FILE_NO_SA(num_states_J, Kramers_J, input_file, energies_J, g_vals, &
                                     g_vecs, ang, num_so_states, energies_all)
        STOP
    END IF

    ! J Basis: Calculate crystal field energies using SINGLE_ANISO parameters with k <= 2J
    ! Returns unique energies only, i.e. Does not repeat degenerate levels
    CALL CALCULATE_CF_PROPS(CFP_J, cf_energies_J, k_max_index_J, num_states_J, cf_percents_J, &
                            Jz_vals)

    ! L Basis: Calculate crystal field energies using SINGLE_ANISO parameters with k <= 2L
    ! Returns unique energies only, i.e. Does not repeat degenerate levels
    CALL CALCULATE_CF_PROPS(CFP_L, cf_energies_L, k_max_index_L, num_states_L, cf_percents_L, &
                            Lz_vals)

    CALL WRITE_OUTPUT_FILE(num_states_J, num_states_L, Kramers_J, CFP_J, CFP_L, input_file, &
                           energies_J, energies_L, cf_energies_J, cf_energies_L, g_vals, g_vecs, &
                           ang, composition_J, composition_L, k_max_index_J, k_max_index_L, &
                           num_so_states, energies_all, Jz_vals, Lz_vals)

    CONTAINS

SUBROUTINE READ_USER_INPUT(element, orb_type, num_states_J, file, orb_print, threshold)
    USE version, ONLY : PRINT_VERSION
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(OUT) :: element, orb_type, file
    CHARACTER(LEN = 100)            :: Cdummy
    INTEGER, INTENT(OUT)            :: num_states_J
    INTEGER                         :: Idummy
    REAL(KIND = 8), INTENT(OUT)     :: threshold
    LOGICAL                         :: file_exists
    LOGICAL, INTENT(OUT)            :: orb_print

    !Read element 
    CALL GET_COMMAND_ARGUMENT(1,element)
    
    !Print help
    IF(TRIM(element) == '-h' .OR. TRIM(element) == '') THEN
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') 'openmolcas_extractor <element> <orb-type> <num_states> <file> [<orbital_print>] [<threshold>]'
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') 'Extracts energies, CFPs (and CFP calculated energies and wavefunctions), principal g values,'
        WRITE(6,'(A)')   ' g tensors, angles of maximal g components to ground state, and spin-orbit weights of the '
        WRITE(6,'(A)')   ' first num_states spin orbit states in a given molcas output file and writes to <file>_extracted.out'
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') 'element        : CHARACTER          e.g. dy'
        WRITE(6,'(A)') 'orb-type       : CHARACTER          e.g  4f'
        WRITE(6,'(A)') 'num_states     : INTEGER            e.g  16  for Dy3+ J=15/2 multiplet'
        WRITE(6,'(A)') 'file           : CHARACTER          e.g  dy.out'
        WRITE(6,'(A)') 'orbital_print  * OPTIONAL CHARACTER e.g  y/n for contrib. of orbs to active space'
        WRITE(6,'(A)') 'threshold      * OPTIONAL NUMBER    e.g  threshold for printing contrib. of orbs to active space'
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') 'Example ------ openmolcas_extractor dy 4f 16 dy.out y'
        WRITE(6,'(A)') ''

        STOP
    END IF

    ! Print version number
    IF (TRIM(element) == '-v' .OR. TRIM(element) == '-version' .OR. TRIM(element) == 'version') THEN
        CALL PRINT_VERSION('openmolcas_extractor')
        STOP
    END IF

    !Convert element to uppercase
    CALL uppercase(element)

    !Read orbital type
    CALL GET_COMMAND_ARGUMENT(2,orb_type)
    CALL ARG_CHECK('orb-type', orb_type, 'character')

    !Read number of states
    CALL GET_COMMAND_ARGUMENT(3,Cdummy)
    CALL ARG_CHECK('num_states',Cdummy, 'integer')
    READ(Cdummy,*) num_states_J

    !Read molcas .out file name
    CALL GET_COMMAND_ARGUMENT(4,file)
    CALL ARG_CHECK('file', file, 'character')

    !Check file exists
    INQUIRE(FILE=TRIM(file), EXIST=file_exists)
    IF (file_exists .EQV. .FALSE.) THEN
        WRITE(6,'(A)') 'Specified file does not exist'
        STOP
    END IF


    !Read orb_print for active space orbital printing
    CALL GET_COMMAND_ARGUMENT(5,Cdummy)
    orb_print = .FALSE.
    IF(TRIM(Cdummy) == 'y' .OR. TRIM(Cdummy) == 'Y') orb_print = .TRUE.

    !Read threshold value for printing active space orbital contributions
    CALL GET_COMMAND_ARGUMENT(6,Cdummy)
    !Check if temp is an integer, if it isnt THEN set threshold manually
    READ(Cdummy,*,IOSTAT=Idummy) threshold
    IF(Idummy /= 0 .and. TRIM(orb_type) == '4f') threshold = 0.01_8
    IF(Idummy /= 0 .and. TRIM(orb_type) == '3d') threshold = 0.025_8

END SUBROUTINE

SUBROUTINE ARG_CHECK(arg_name, arg_val, var_type)
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: arg_name, arg_val, var_type
    CHARACTER(LEN = 1000)          :: Cdummy
    INTEGER                        :: Idummy, iostatus
    REAL(KIND = 8)                 :: Rdummy

    IF (var_type == 'integer') THEN
        READ(arg_val,*,IOSTAT=iostatus) Idummy
    ELSE IF (var_type == 'real') THEN
        READ(arg_val,*,IOSTAT=iostatus) Rdummy
    ELSE IF (var_type == 'character') THEN
        READ(arg_val,*,IOSTAT=iostatus) Cdummy
    END IF

    IF (LEN_TRIM(arg_val) == 0) THEN
        WRITE(6,'(2A)') 'Missing argument ', arg_name
        WRITE(6,'(A)') 'Aborting'
        STOP
    END IF 

    IF (iostatus /= 0) THEN
        WRITE(6,'(2A)') 'Incorrect argument ', arg_name
        WRITE(6,'(A)') 'Aborting'
        STOP
    END IF 

END SUBROUTINE ARG_CHECK

SUBROUTINE EXTRACT_RASSCF(input_file, orb_type, orb_print, threshold)
    IMPLICIT NONE
    LOGICAL, INTENT(IN)              :: orb_print
    CHARACTER(LEN = *), INTENT(IN)   :: input_file, orb_type
    CHARACTER(LEN = 5000)            :: line
    CHARACTER(LEN = 10), ALLOCATABLE :: atoms(:)
    REAL(KIND = 8), ALLOCATABLE      :: wavefunc(:,:)
    REAL(KIND = 8)                   :: Rdummy, shift
    REAL(KIND = 8), INTENT(IN)       :: threshold
    INTEGER                          :: i, j, k, q, inactive, num_sections, curr_section, &
                                        found_section, converge, Idummy, active, impure, secondary,&
                                        basis, clean, iostatus, wfspec, roots, end_orbs, col, &
                                        reason, rasscf_line(100), line_number

    converge      = 2
    clean         = 2
    
    OPEN(30,FILE = TRIM(input_file), STATUS = 'OLD')
        line_number = 0
        num_sections  = 0
        ! Loop over file and count number of rasscf sections
        DO

            READ(30,'(A)', IOSTAT = reason) line
            line_number = line_number + 1
            ! If end of file reached unexpectedly then exit do loop
            IF (reason < 0) THEN
                EXIT
            END IF
            CALL CHECK_IO(reason)

            ! If end of rasscf section found then exit do loop
            ! IF (INDEX(TRIM(line), 'Stop Module: rasscf') /= 0) THEN
            !     EXIT
            ! END IF

            IF (INDEX(TRIM(line), 'Start Module: rasscf at') /= 0) THEN
                num_sections = num_sections + 1
                rasscf_line(num_sections) = line_number
            END IF

        END DO

        curr_section  = 0
        found_section = 1

        ! Loop over file and read in each rasscf section
        DO

            ! Quit when every section read in
            IF (curr_section == num_sections + 1) EXIT

            ! If a section has been found in the previous iteration then 
            ! loop over the file again
            IF (found_section == 1) THEN
                curr_section = curr_section + 1
                ! Go back to the start of the file
                REWIND(30)
                line_number = 0
                found_section = 0
            END IF

            ! Read in a line and advance the line counter
            READ(30,'(A)', IOSTAT = reason) line
            line_number = line_number + 1

            !Read RASSCF Section
            IF (rasscf_line(curr_section) == line_number) THEN
                
                ! Mark for reset
                found_section = 1

                ! Write info to screen
                WRITE(6,'(A)') '==============================================='
                WRITE(6,'(A)') 'RASSCF section: '//TRIM(line(29:LEN(TRIM(line)) - 4))

                ! Flags for convergence, orbital pruity and wfspec found
                converge = 0
                clean = 0
                wfspec = 0

                ! Loop over file and read in rasscf information
                DO 
                    READ(30,'(A)', IOSTAT = reason) line
                    CALL CHECK_IO(reason)


                    !Read wavefunction specifications
                    IF(wfspec == 0 .and. TRIM(line) == '++    Wave function specifications:') THEN
                        wfspec = 1

                        !Skip blank lines
                        DO i = 1,7
                            READ(30,'(A)', IOSTAT = reason) line
                            CALL CHECK_IO(reason)
                        END DO

                        !Read number of inactive orbitals
                        READ(line(34:),*) inactive
                        WRITE(6,'(A,I3)') 'Inactive:           ',inactive
                        READ(30,'(A)', IOSTAT = reason) line
                        CALL CHECK_IO(reason)

                        !Read number of active orbitals
                        READ(line(32:),*) active
                        WRITE(6,'(A,I2)') 'Active:              ',active
                        READ(30,'(A)', IOSTAT = reason) line
                        CALL CHECK_IO(reason)

                        !Read number of secondary orbitals and calculate basis
                        READ(line(35:),*) secondary
                        basis = inactive+active+secondary
                        WRITE(6,'(A,I4)') 'Basis:             ',basis
                        READ(30,'(A)', IOSTAT = reason) line
                        CALL CHECK_IO(reason)

                        !Read spin
                        READ(line(26:),*) Rdummy
                        IF (MODULO(NINT(2*Rdummy), 2) == 0) THEN
                            WRITE(6,'(A,I0)') 'Spin:                ',nint(2*Rdummy)
                        ELSE
                            WRITE(6,'(A,I0,A2)') 'Spin:               ',nint(2*Rdummy), '/2'
                        END IF
                    END IF

                    !Read molecular charge
                    IF (INDEX(TRIM(line), 'Total molecular charge') /= 0) THEN
                        READ(line(29:),*) Rdummy
                        WRITE(6,'(A8,F15.1)') 'Charge: ',Rdummy
                    END IF

                    !Read total energy shift
                    IF (INDEX(TRIM(line), 'Total energies have been shifted. Add') /= 0) THEN
                        READ(line(44:67),*) shift
                    END IF

                    ! Read convergence information and print to screen
                    IF (INDEX(TRIM(line), 'Convergence after') /= 0) THEN
                        converge = 1
                        WRITE(6,'(A)') 'Convergence:        YES'
                        READ(30,'(A)', IOSTAT = reason) line
                        READ(line,*, IOSTAT = reason) Idummy,Idummy,Idummy,Idummy,Rdummy
                        CALL CHECK_IO(reason)
                        WRITE(6,'(A8,F15.8)') 'Energy: ',Rdummy + shift
                    END IF

                    ! Read number of roots
                    IF (INDEX(TRIM(line), 'Number of root(s) required') /= 0) THEN
                        READ(line(33:),*) roots
                        WRITE(6,'(A7,I16)') 'Roots: ',roots
                    END IF

                    ! Check for instant convergence
                    IF (INDEX(TRIM(line), 'All orbitals are eigenfunctions of the PT2 Fock matrix') /= 0) THEN
                        WRITE(6,'(A)') '                                      CANONICAL'
                        clean = 2
                        converge = 2
                        EXIT
                    END IF

                    ! Read Molecular orbitals section
                    IF (INDEX(TRIM(line), 'Molecular orbitals for symmetry species 1: a') /= 0) THEN
                        IF(converge == 0) THEN
                            WRITE(6,'(A)') 'Convergence:         NO'
                        END IF

                        !Flag for impure orbitals
                        impure = 0

                        !Flag for reaching the end of the orbitals section
                        end_orbs = 0

                        !Allocate wavefunction array
                        ALLOCATE(wavefunc(0:basis,50),atoms(basis))
                        wavefunc = 0.0_8
                        
                        !Loop over the file and read in all contributions from AOs to MOs of the active space.
                        DO
                            READ(30,'(A)', IOSTAT = reason) line
                            ! If end of file reached then exit do loop
                            IF (reason < 0) THEN
                                EXIT
                            END IF
                            CALL CHECK_IO(reason)

                            IF(line(1:13) == '      Orbital') THEN

                                ! Read current highest orbital index - last index on line "Orbital 1 2 3 ... N"
                                READ(line(LEN_TRIM(line)-5:),*) Idummy

                                ! If orbital index is less than those of the active space keep reading until 
                                ! it is reached. When it is reached, read in the contributions to the MOs of the 
                                ! active space

                                IF(Idummy >= inactive+1) THEN  

                                    !Read MO indices
                                    DO q = 0,maxval((/ 0,((inactive+active - Idummy)/10)+1 /))*10,10

                                        READ(line(21:30),*,IOSTAT=iostatus) wavefunc(0,1+q)
                                        IF(iostatus == 0) READ(line(31:40)  ,*,IOSTAT=iostatus) wavefunc(0, 2+q)
                                        IF(iostatus == 0) READ(line(41:50)  ,*,IOSTAT=iostatus) wavefunc(0, 3+q)
                                        IF(iostatus == 0) READ(line(51:60)  ,*,IOSTAT=iostatus) wavefunc(0, 4+q)
                                        IF(iostatus == 0) READ(line(61:70)  ,*,IOSTAT=iostatus) wavefunc(0, 5+q)
                                        IF(iostatus == 0) READ(line(71:80)  ,*,IOSTAT=iostatus) wavefunc(0, 6+q)
                                        IF(iostatus == 0) READ(line(81:90)  ,*,IOSTAT=iostatus) wavefunc(0, 7+q)
                                        IF(iostatus == 0) READ(line(91:100) ,*,IOSTAT=iostatus) wavefunc(0, 8+q)
                                        IF(iostatus == 0) READ(line(101:110),*,IOSTAT=iostatus) wavefunc(0, 9+q)
                                        IF(iostatus == 0) READ(line(111:120),*,IOSTAT=iostatus) wavefunc(0,10+q)

                                        !Skip Energy, Occ. No. and blank lines
                                        DO i = 1, 3
                                            READ(30,'(A)', IOSTAT = reason) line
                                            CALL CHECK_IO(reason)
                                        END DO

                                        !Read coefficients of each AO
                                        DO j = 1, basis
                                            READ(30,'(A)', IOSTAT = reason) line
                                            IF (reason < 0) THEN
                                                EXIT
                                            END IF
                                            CALL CHECK_IO(reason)

                                            READ(line(7:20),'(A)',IOSTAT=iostatus) atoms(j)
                                            !n_entries = count_entries(line)

                                            IF(nint(wavefunc(0,10+q)) /= 0) THEN
                                                READ(line(21:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 10+q)
                                            ELSE IF(nint(wavefunc(0,9+q)) /= 0) THEN
                                                end_orbs = 1
                                                READ(line(21:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 9+q)
                                            ELSE IF(nint(wavefunc(0,8+q)) /= 0) THEN
                                                end_orbs = 1
                                                READ(line(21:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 8+q)
                                            ELSE IF(nint(wavefunc(0,7+q)) /= 0) THEN
                                                end_orbs = 1
                                                READ(line(21:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 7+q)
                                            ELSE IF(nint(wavefunc(0,6+q)) /= 0) THEN
                                                end_orbs = 1
                                                READ(line(21:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 6+q)
                                            ELSE IF(nint(wavefunc(0,5+q)) /= 0) THEN
                                                end_orbs = 1
                                                READ(line(21:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 5+q)
                                            ELSE IF(nint(wavefunc(0,4+q)) /= 0) THEN
                                                end_orbs = 1
                                                READ(line(21:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 4+q)
                                            ELSE IF(nint(wavefunc(0,3+q)) /= 0) THEN
                                                end_orbs = 1
                                                READ(line(21:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 3+q)
                                            ELSE IF(nint(wavefunc(0,2+q)) /= 0) THEN
                                                end_orbs = 1
                                                READ(line(21:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 2+q)
                                            ELSE IF(nint(wavefunc(0,1+q)) /= 0) THEN
                                                end_orbs = 1
                                                READ(line(21:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 1+q)
                                            END IF
                                            
                                        END DO

                                        !Skip blank lines
                                        DO i = 1, 3
                                            READ(30,'(A)', IOSTAT = reason) line
                                            CALL CHECK_IO(reason)
                                        END DO

                                    END DO

                                    !Calculate percentage contributions from each AO to the MOs of the active space
                                    !If they are requested, and are greater than the threshold, then print them to screen
                                    !If end has been reached then exit the infinite loop
                                    IF(end_orbs == 1) THEN
                                        DO j = 1,50
                                            IF(nint(wavefunc(0,j)) >= inactive+1 .and. nint(wavefunc(0,j)) <= inactive+active) THEN
                                                IF(orb_print) WRITE(6,*) 'orb',nint(wavefunc(0,j))
                                                Rdummy = 0.0_8
                                                DO k = 1,basis
                                                    Rdummy = Rdummy + wavefunc(k,j)*wavefunc(k,j)
                                                    wavefunc(k,j) = wavefunc(k,j)*wavefunc(k,j)
                                                END DO
                                                wavefunc(1:basis,j) = wavefunc(1:basis,j)/Rdummy
                                                DO k = 1,basis
                                                    IF(orb_print .and. wavefunc(k,j) >= threshold) WRITE(6,*) atoms(k),wavefunc(k,j)
                                                    !WRITE(6,'(A,F10.4)') atoms(k),wavefunc(k,j)
                                                    IF(wavefunc(k,j) >= threshold .and. atoms(k)(8:9) /= orb_type) impure = 1
                                                END DO
                                                IF(orb_print) WRITE(6,*)
                                            END IF
                                        END DO
                                    END IF
                                END IF
                            END IF
                        END DO

                        DEALLOCATE(wavefunc,atoms)

                        !Print warning if active space MOs are not purely the requested AOs
                        IF(impure == 1) WRITE(6,'(A)') 'Active space:     DIRTY'
                        
                        !Else print that the MOs are clean
                        IF(impure == 0) THEN
                            WRITE(6,'(A)') 'Active space:     CLEAN'
                            clean = 1
                        END IF

                        !If clean and converged
                        IF(clean == 1 .and. converge == 1) THEN
                            CALL SUCCESS 
                        !If either dirty or non-converged
                        ELSE IF(clean == 0 .or. converge == 0) THEN
                            CALL FAIL(clean, converge)
                        END IF

                        !Set clean and converged to 2 to indicate the entire loop is done
                        clean = 2
                        converge = 2
                        WRITE(6,'(A)') '==============================================='
                        EXIT
                    END IF
                END DO
            END IF
        END DO
    CLOSE(33)

END SUBROUTINE EXTRACT_RASSCF

! FUNCTION count_entries(line, n_preceeding_char) RESULT(num_entries)
!     IMPLICIT NONE
!     CHARACTER(LEN = *), INTENT(IN)    :: line
!     CHARACTER(LEN = 500), ALLOCATABLE :: Cdummy_arr(:)
!     INTEGER, INTENT(IN)               :: n_preceeding_char
!     INTEGER                           :: num_entries, iostatus

!     iostatus = 0

!     ALLOCATE(Cdummy_arr(n_preceeding_char))

!     DO WHILE(iostatus == 0)



! END FUNCTION count_entries

SUBROUTINE EXTRACT_RASSI(input_file, g_vals, g_vecs, num_states_J, num_so_states, energies_J, &
                         energies_L, energies_all)
    IMPLICIT NONE
    CHARACTER(LEN = 500)                      :: Cdummy
    CHARACTER(LEN = *), INTENT(IN)            :: input_file
    CHARACTER(LEN = 5000)                     :: line
    REAL(KIND = 8)                            :: Rdummy
    REAL(KIND = 8), ALLOCATABLE, INTENT(OUT)  :: energies_J(:), energies_L(:), g_vals(:,:), &
                                                 g_vecs(:,:,:), energies_all(:)
    INTEGER, INTENT(OUT)                      :: num_so_states
    INTEGER                                   :: i, j, p, num_sections, Idummy, multno, n_blocks, &
                                                 start_line(100), roots,col, row, reason, prev, &
                                                 num_spin_mult, SF_num_mult(100), SF_num_roots(100) 
    INTEGER, ALLOCATABLE                      :: root_nrs(:,:)
    INTEGER, INTENT(IN)                       :: num_states_J
    LOGICAL                                   :: config_found


    !Allocate energy and gvector arrays with number of states
    !If Kramers then only count degenerate states once
    IF (MODULO(num_states_J,2) == 0) THEN
        ALLOCATE(energies_J(num_states_J/2),g_vals(num_states_J/2,3),g_vecs(num_states_J/2,3,3))
    ELSE
        ALLOCATE(energies_J(num_states_J))
    END IF

    ALLOCATE(composition_J(num_states_J,num_states_J))

    config_found  = .FALSE.

    ! Open file
    OPEN(30,FILE = TRIM(input_file), STATUS = 'OLD')

        ! Set counters to zero
        j = 0
        num_sections = 0

        ! Read in RASSI Section, check for completion and count how many exist
        DO 
            READ(30,'(A)', IOSTAT = reason) line

            ! Check file isnt corrupted
            IF (reason > 0)  THEN
                WRITE(6,'(A)') '!!!!          Error reading molcas output file while counting RASSI sections          !!!!'
                STOP
            END IF

            ! If end of file reached then exit do loop
            IF (reason < 0) THEN
                EXIT
            END IF

            ! Add one to line counter
            j = j + 1

            IF (INDEX(TRIM(line), 'Start Module: rassi at') /= 0) THEN
                WRITE(6,'(A)') 'RASSI section: '//TRIM(line(28:))
                num_sections = num_sections + 1
                start_line(num_sections) = j
            END IF

            IF (INDEX(TRIM(line), 'Eigenvalues of complex Hamiltonian:') /= 0) THEN
            END IF
        END DO

        ! If multiple RASSI sections detected, ask user which one they want to use
        Idummy = num_sections
        
        IF(num_sections > 1) THEN
            WRITE(6,'(A6,I2,A38)') 'Found ',num_sections,' RASSI sections, which one to extract?'
            READ(*,*) Idummy
            IF(Idummy > num_sections) Idummy = num_sections
        ELSE IF (num_sections == 0) THEN
            WRITE(6,*)
            WRITE(6,'(A)') 'Cannot find any RASSI sections in this file'
            WRITE(6,'(A)') 'Aborting'
            STOP
        END IF

        ! Go back to start of file
        REWIND(30)

        ! set index to zero
        num_spin_mult = 0

        ! Use the chosen RASSI section
        ! Loop over and read in data
        p = 1
        IF(num_sections > 0) THEN
            j = 0
            DO 
                j = j + 1
                READ(30,'(A)', IOSTAT = reason) line
                CALL CHECK_IO(reason)
                IF(j < start_line(Idummy)) cycle

                ! read all spin free multiplicities and num roots
                IF (INDEX(TRIM(line), 'SPIN MULTIPLICITY:') /= 0) THEN
                    num_spin_mult = num_spin_mult + 1
                    READ(line(22:),*) SF_num_mult(num_spin_mult)
                END IF
                IF (INDEX(TRIM(line), 'NR OF CONFIG:') /= 0) THEN
                    READ(line(17:),*) SF_num_roots(num_spin_mult)
                END IF

                ! Read in total number of spin-free states and allocate spin-free energies array
                ! At this point program doesnt know L used by single aniso
                ! so best to read in all spin-free energies and ignore some
                ! Then Read in number of states actually used in rassi - these are truncated by an energy cutoff 
                ! which is hard coded into openmolcas_gen
                IF (INDEX(TRIM(line), 'Nr of states:') /= 0  .AND. (config_found .EQV. .FALSE.)) THEN
                    READ(line(16:),*) roots
                    ALLOCATE(energies_L(roots))
                    config_found = .TRUE.


                    ! Read in root numbers used in rassi
                    n_blocks = roots/20
                    IF (MODULO(roots, 20) > 1) THEN
                        ALLOCATE(root_nrs(n_blocks + 1, 20))
                    ELSE
                        ALLOCATE(root_nrs(n_blocks, 20))
                    END IF
                    root_nrs = 0
                    ! Read in all complete blocks of root numbers
                    DO row = 1, n_blocks
                        READ(30,*)
                        READ(30,*)
                        READ(30,*)
                        READ(30,*) Cdummy, Cdummy, (root_nrs(row, col), col = 1, 20)
                    END DO
                    ! Read remaining incomplete block of root numbers, if it exists
                    IF (MODULO(roots,20) > 0) THEN
                        READ(30,*)
                        READ(30,*)
                        READ(30,*)
                        READ(30,*) Cdummy, Cdummy, (root_nrs(n_blocks+1, col), col = 1, MODULO(roots, 20))
                        n_blocks = n_blocks + 1
                    END IF                 

                    multno = 1
                    prev = 0

                    ! Find max root number for each spin multiplicity
                    DO row = 1, n_blocks
                        DO col = 1, 20
                            ! Check if final root has been reached
                            IF (INT(0.5_8*(row * 20  + col)) == roots) THEN
                                SF_num_roots(multno) = root_nrs(row,col)
                            END IF
                            ! All other entries
                            ! Check if current root number is lower than previous
                            ! If it is then we have moved to a new spin-state
                            ! So record previous root number as total number of roots
                            ! for that spin state
                            ! And advance the spin state counter
                            IF (root_nrs(row, col) <= prev) THEN
                                SF_num_roots(multno) = prev
                                multno = multno + 1
                                prev = 1
                            ! Else skip over to next root number
                            ELSE
                                prev = root_nrs(row, col)
                            END IF
                        END DO
                    END DO     
                END IF


                ! Read in spin-free energies
                IF ((INDEX(TRIM(line), 'SF State') /= 0) .AND. (INDEX(TRIM(line), 'D:o, cm**(-1)') /= 0)) THEN
                    ! Skip blank line
                    READ(30,*, IOSTAT = reason)
                    CALL CHECK_IO(reason)
                    DO i = 1, roots
                        READ(30,*,IOSTAT = reason) Rdummy,Rdummy,Rdummy,energies_L(i)
                        CALL CHECK_IO(reason)
                    END DO
                END IF

                IF (INDEX(TRIM(line), 'Eigenvalues of complex Hamiltonian') /= 0) THEN
                    ! calculate total number of so states
                    num_so_states = 0
                    DO i = 1,num_spin_mult
                        num_so_states = num_so_states + SF_num_mult(i)*SF_num_roots(i)
                    END DO
                    ALLOCATE(energies_all(num_so_states))
                    ! Skip blank lines
                    DO i = 1,5
                        READ(30,*, IOSTAT = reason) 
                        CALL CHECK_IO(reason)
                    END DO

                    ! Read all SO Energies
                    DO i = 1,num_so_states
                        READ(30,*,IOSTAT = reason) Rdummy,Rdummy,Rdummy,energies_all(i)
                        CALL CHECK_IO(reason)
                    END DO

                    ! If Kramers copy half of energies into energies_J
                    IF (MODULO(num_states_J,2) == 0) THEN
                        energies_J = energies_all(1:num_states_J:2) 
                    ! If non-Kramers copy all of energies into energies_J
                    ELSE
                        energies_J = energies_all(1:num_states_J) 
                    END IF
                END IF

                !If Kramers ion then read in g-tensors
                IF (INDEX(TRIM(line), 'g-Matrix Approach II') /= 0) THEN
                    IF (MODULO(num_states_J,2) == 0) THEN
                        DO i = 1,7
                            READ(30,*, IOSTAT = reason) 
                            CALL CHECK_IO(reason)
                        END DO
                        DO i = 1,num_states_J/2
                            DO p = 1,5
                                READ(30,*, IOSTAT = reason) 
                                CALL CHECK_IO(reason)
                            END DO
                            READ(30,*, IOSTAT = reason) Cdummy,Rdummy,Rdummy,Rdummy,Cdummy,g_vals(i,1),Cdummy,g_vecs(i,1,1),g_vecs(i,2,1),g_vecs(i,3,1)
                            CALL CHECK_IO(reason)
                            READ(30,*, IOSTAT = reason) Cdummy,Rdummy,Rdummy,Rdummy,Cdummy,g_vals(i,2),Cdummy,g_vecs(i,1,2),g_vecs(i,2,2),g_vecs(i,3,2)
                            CALL CHECK_IO(reason)
                            READ(30,*, IOSTAT = reason) Cdummy,Rdummy,Rdummy,Rdummy,Cdummy,g_vals(i,3),Cdummy,g_vecs(i,1,3),g_vecs(i,2,3),g_vecs(i,3,3)
                            CALL CHECK_IO(reason)
                            IF(i/=num_states_J/2) READ(30,*)
                        END DO
                        !Quit
                        EXIT
                    !If non-Kramers quit
                    ELSE
                        EXIT
                    END IF
                END IF
            END DO
        END IF

        CALL SUCCESS

        ! Go back to the start of the file
        REWIND(30)


END SUBROUTINE EXTRACT_RASSI

SUBROUTINE EXTRACT_SINGLE_ANISO(input_file, element, CFP_J, CFP_L, k_max_index_J, k_max_index_L, &
                                num_states_J, num_states_L, composition_J, composition_L, &
                                SA_SKIP_PRINT)
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN)           :: input_file, element
    CHARACTER(LEN = 5000)                    :: line
    REAL(KIND = 8)                           :: CF_wf(4)
    REAL(KIND = 8), ALLOCATABLE, INTENT(OUT) :: composition_J(:,:), composition_L(:,:)
    REAL(KIND = 8), INTENT(OUT)              :: CFP_J(3,27), CFP_L(3,27)
    INTEGER, INTENT(OUT)                     :: k_max_index_J, k_max_index_L, num_states_L
    INTEGER                                  :: i, j, k, q, num_sections, Idummy, start_line(100), &
                                                reason, L
    INTEGER, INTENT(IN)                      :: num_states_J
    LOGICAL                                  :: found_CF_J, found_CF_L
    LOGICAL, INTENT(OUT)                     :: SA_SKIP_PRINT


    found_CF_J    = .FALSE.
    found_CF_L    = .FALSE.
    SA_SKIP_PRINT = .FALSE.

    ALLOCATE(composition_J(num_states_J, num_states_J))
    composition_J = 0.0_8

    ! Open file
    OPEN(30,FILE = TRIM(input_file), STATUS = 'OLD')
        ! Set counters to zero
        j = 0
        num_sections = 0

        ! Read in SINGLE_ANISO section and count how many there are
        DO 
            READ(30,'(A)', IOSTAT = reason) line

            ! Check file isnt corrupted
            IF (reason > 0)  THEN
                WRITE(6,'(A)') '!!!!          Error reading molcas output file while counting SINGLE_ANISO sections          !!!!'
                STOP
            END IF

            ! If end of file reached then exit do loop
            IF (reason < 0) THEN
                EXIT
            END IF

            ! Add one to line counter
            j = j + 1

            IF (INDEX(TRIM(line), 'Start Module: single_aniso at') /= 0) THEN
                WRITE(6,'(A)') '==============================================='
                WRITE(6,'(A)') 'SINGLE_ANISO section: '//TRIM(line(35:len(trim(line)) - 4))
            END IF
            
            IF (INDEX(TRIM(line), 'CALCULATION OF CRYSTAL-FIELD PARAMETERS OF THE GROUND ATOMIC MULTIPLET') /= 0) THEN
                num_sections = num_sections + 1
                start_line(num_sections) = j
            END IF
        END DO

        ! If multiple SINGLE_ANISO sections detected, ask user which one they want to use
        IF(num_sections > 1) THEN
            WRITE(6,'(A6,I2,A45)') 'Found ',num_sections,' SINGLE_ANISO sections, which one to extract?'
            READ(*,*) Idummy
            IF(Idummy > num_sections) Idummy = num_sections
        ELSE IF (num_sections == 0) THEN
            WRITE(6,*)
            WRITE(6,'(A)') 'Cannot find any SINGLE_ANISO sections with'
            WRITE(6,'(A)') 'crystal field parameters in this file'
            SA_SKIP_PRINT = .TRUE.
        ELSE
            Idummy = num_sections
        END IF

        ! Go back to the start of the file
        REWIND(30)

        ! Select chosen SINGLE_ANISO section and read in Crystal field parameters in J basis
        IF (SA_SKIP_PRINT .EQV. .FALSE.) THEN
            IF (num_sections > 0) THEN
                j = 0
                DO WHILE (.TRUE.)
                    j = j + 1
                    READ(30,'(A)', IOSTAT=reason) line
                    CALL CHECK_IO(reason)
                    IF(j < start_line(Idummy)) CYCLE
                    IF((TRIM(line) == '  k |  q  |    (K)^2    |         B(k,q)        |') .AND. (found_CF_J .EQV. .FALSE.)) THEN
                        found_CF_J = .TRUE.
                        READ(30,*)
                    ! Set max k values
                    ! Here k_max <= 2*J
                        IF(element == 'CE' .or. element == 'SM') THEN
                            k_max_index_J = 2
                        ELSE
                            k_max_index_J = 3
                        END IF

                    ! Read in Crystal field parameters
                        DO k = 1,k_max_index_J
                            DO q = 1,4*k+1
                                READ(30,'(A)', IOSTAT=reason) line
                                CALL CHECK_IO(reason)
                                READ(line(26:),*) CFP_J(k,q)
                            END DO
                            READ(30,*, IOSTAT=reason)
                            CALL CHECK_IO(reason)
                        END DO

                    END IF

                    !Read in decomposition of RASSI spin-orbit wavefunction in terms of pure mJ states
                    IF (INDEX(TRIM(line), 'DECOMPOSITION OF THE RASSI WAVE FUNCTIONS CORRESPONDING TO THE LOWEST ATOMIC MULTIPLET J') /= 0) THEN
                        ! Skip blank lines
                        DO i = 1,3
                            READ(30,'(A)', IOSTAT = reason) line
                            CALL CHECK_IO(reason)
                        END DO
                            
                        ! Read in contributions of pure mJ functions to each state
                        DO k = 1,num_states_J/2
                        ! Skip blank lines
                            !--------|----------------------------------------------------------|----------------------------------------------------------|
                            ! | JM > |                ab initio state  1                        |                ab initio state  2                        |
                            !--------|-------  Real  -------|------  Imaginary  -------|-Weight-|-------  Real  -------|------  Imaginary  -------|-Weight-|
                            DO i = 1,3
                                READ(30,'(A)', IOSTAT = reason) line
                                CALL CHECK_IO(reason)
                            END DO

                            DO q = 1, num_states_J
                                READ(30,'(A)', IOSTAT = reason) line
                                CALL CHECK_IO(reason)
                                READ(line(11:54),*, IOSTAT = reason) CF_wf(1:2)
                                CALL CHECK_IO_SA(reason)
                                READ(line(70:113),*, IOSTAT = reason) CF_wf(3:4)
                                CALL CHECK_IO_SA(reason)
                                composition_J(q,2*(k-1)+1) = (CF_wf(1)**2 + CF_wf(2)**2)
                                composition_J(q,2*(k-1)+2) = (CF_wf(3)**2 + CF_wf(4)**2)
                            END DO
                        END DO
                        
                        ! If non-kramers read the info for the last state which will be 
                        ! on its own in a single column
                        IF(mod(num_states_J,2) /= 0) THEN
                            CF_wf = 0.0_8
                            ! Skip blank lines
                            DO i = 1,3
                                READ(30,'(A)', IOSTAT = reason) line
                                CALL CHECK_IO(reason)
                            END DO
                            DO q = 1, num_states_J
                                READ(30,'(A)', IOSTAT = reason) line
                                CALL CHECK_IO(reason)
                                READ(line(11:54),*) CF_wf(1:2)
                                composition_J(q,num_states_J) = (CF_wf(1)**2 + CF_wf(2)**2)
                            END DO
                        END IF
                    EXIT
                    END IF
                END DO
            END IF

            ! Go back to the start of the file
            REWIND(30)

            ! Select chosen SINGLE_ANISO section and read in Crystal field parameters in L basis
            j = 0
            DO WHILE (.TRUE.)
                j = j + 1
                READ(30,'(A)', IOSTAT=reason) line
                CALL CHECK_IO(reason)
                IF(j < start_line(Idummy)) CYCLE

            ! Read in L value used by Single Aniso
                IF(INDEX(TRIM(line), 'CALCULATION OF CRYSTAL-FIELD PARAMETERS OF THE GROUND ATOMIC TERM, L =') /= 0) THEN
                    start_line(Idummy) = j
                    line = Cut_to_Char(Cut_Past_Char(line, '='), '.')
                    READ(line,*) L
                    num_states_L = 2 * L + 1
                    ALLOCATE(composition_L(num_states_L,num_states_L))
                    composition_L = 0.0_8

                    ! Set max index for max k values
                    ! Here k_L_max <= 2*L
                    IF (2*L<4) THEN
                        k_max_index_L = 1
                    ELSE IF(2*L<6) THEN
                        k_max_index_L = 2
                    ELSE
                        k_max_index_L = 3
                    END IF

                    ! Exit infinite loop
                    EXIT

                END IF
            END DO
            
            ! Go back to the start of the file
            REWIND(30)

            ! Start infinite loop again at L basis section of single aniso
            j = 0
            DO WHILE (.TRUE.)
                j = j + 1
                READ(30,'(A)', IOSTAT=reason) line
                CALL CHECK_IO(reason)
                IF(j < start_line(Idummy)) CYCLE

                ! Crystal Field parameters
                IF((TRIM(line) == '  k |  q  |    (K)^2    |         B(k,q)        |') .AND. (found_CF_L .EQV. .FALSE.)) THEN
                    found_CF_L = .TRUE.

                    ! Skip blank line
                    READ(30,*, IOSTAT=reason) 
                    CALL CHECK_IO(reason)

                    ! Read in crystal field parameters
                        DO k = 1,k_max_index_L
                            DO q = 1,4*k+1
                                READ(30,'(A)', IOSTAT=reason) line
                                CALL CHECK_IO(reason)
                                READ(line(26:),*) CFP_L(k,q)
                            END DO
                            READ(30,*, IOSTAT=reason)
                            CALL CHECK_IO(reason)
                        END DO

                END IF

                ! Read in decomposition of RASSI spin-orbit wavefunction in terms of pure mJ states
                IF (INDEX(TRIM(line), 'DECOMPOSITION OF THE RASSCF WAVE FUNCTIONS CORRESPONDING TO THE LOWEST ATOMIC MULTIPLET L') /= 0) THEN

                    ! Skip blank lines
                    DO i = 1,3
                        READ(30,'(A)', IOSTAT = reason) line
                        CALL CHECK_IO(reason)
                    END DO
                        
                    ! Read in contributions of pure mL functions to each state
                    DO k = 1,num_states_L/2
                    ! Skip blank lines
                        !--------|----------------------------------------------------------|----------------------------------------------------------|
                        ! | LM > |                ab initio state  1                        |                ab initio state  2                        |
                        !--------|-------  Real  -------|------  Imaginary  -------|-Weight-|-------  Real  -------|------  Imaginary  -------|-Weight-|
                        DO i = 1,3
                            READ(30,'(A)', IOSTAT = reason) line
                            CALL CHECK_IO(reason)
                        END DO

                        DO q = 1, num_states_L
                            READ(30,'(A)', IOSTAT = reason) line
                            CALL CHECK_IO(reason)
                            READ(line(11:54),*) CF_wf(1:2)
                            READ(line(70:113),*) CF_wf(3:4)
                            composition_L(q,2*(k-1)+1) = (CF_wf(1)**2 + CF_wf(2)**2)
                            composition_L(q,2*(k-1)+2) = (CF_wf(3)**2 + CF_wf(4)**2)
                        END DO
                    END DO

                    ! Exit infinite loop now all information has been read
                    EXIT
                END IF
            END DO
        END IF
    CALL SUCCESS
    WRITE(6,'(A)') '==============================================='
CLOSE(30)

END SUBROUTINE EXTRACT_SINGLE_ANISO

SUBROUTINE REARRANGE(array, companion)
    ! Rearrange array according to the order given in companion
    ! Returns rearranged array
    IMPLICIT NONE
    REAL(KIND = 8), INTENT(INOUT) :: array(:,:)
    REAL(KIND = 8), ALLOCATABLE   :: temp(:,:)
    INTEGER       , INTENT(IN)    :: companion(:,:)
    INTEGER                       :: i

    ! Make temporary copy of array
    ALLOCATE(temp(SIZE(array,1), SIZE(array,2)))
    temp = array

    ! Rearrange array 
    DO i  = 1, SIZE(companion,1)
        array(i,:) = temp(companion(i,2),:)
    END DO

END SUBROUTINE REARRANGE


SUBROUTINE CHECK_IO(IOREPORT)
    !Checks IOSTAT return variable for errors
    IMPLICIT NONE
    INTEGER, INTENT(IN)   :: IOREPORT

    !Check file isnt corrupted or ends too soon
        IF (IOREPORT > 0)  THEN
            WRITE(6,'(A)') '!!!!                         Error reading molcas output file                              !!!!'
            STOP
        ELSE IF (IOREPORT < 0) THEN
            WRITE(6,'(A)') '!!!!                Error: Reached end of molcas output file too soon                      !!!!'
            STOP
        END IF

END SUBROUTINE CHECK_IO

SUBROUTINE CHECK_IO_SA(IOREPORT)
    !Checks IOSTAT return variable for errors
    !Specific for reading wavefunction contributions from single aniso in J basis
    IMPLICIT NONE
    INTEGER, INTENT(IN)   :: IOREPORT

    !Check file isnt corrupted or ends too soon
        IF (IOREPORT > 0)  THEN
            WRITE(6,'(A)') '==============================================='
            WRITE(6,'(A)') '  Error: Incorrect number of states specified  '
            WRITE(6,'(A)') '==============================================='
            STOP
        END IF

END SUBROUTINE CHECK_IO_SA

SUBROUTINE CALC_G_PROPS(num_states_J, g_vals, g_vecs, ang)
    USE matrix_tools
    IMPLICIT NONE
    REAL(KIND = 8), INTENT(INOUT)            :: g_vals(:,:), g_vecs(:,:,:)
    REAL(KIND = 8), INTENT(OUT), ALLOCATABLE :: ang(:)
    REAL(KIND = 8)                           :: ground(3), current(3)
    INTEGER                                  :: i
    INTEGER, INTENT(IN)                      :: num_states_J
    INTEGER, ALLOCATABLE                     :: companion(:,:)


    ! Allocate and zero arrays for ordering of g-values, and gz angles
    ALLOCATE(companion(3,2), ang(num_states_J))
    ang = 0.0_8

    ! Loop over states - halved due to Kramers
    DO i = 1,num_states_J/2

        ! Zero array for ordering of g-values
        companion = 0

        ! Order g values for this state
        ! max to min
        CALL BUBBLE(g_vals(i,:), companion, 'max')

        ! Use above order (companion) to order g vectors
        CALL REARRANGE(g_vecs(i,:,:), companion)

        IF(i == 1) CYCLE

        ! Calculate angle between gz vector of ground doublet and all other doublets
        ground  = g_vecs(1,1,:)
        current = g_vecs(i,1,:)
        ang(i) = G_ANGLE(ground, current)
        IF(ang(i) > 90.0_8) ang(i) = 180.0_8 - ang(i)
        
    END DO

END SUBROUTINE CALC_G_PROPS

SUBROUTINE CALCULATE_CF_PROPS(CFP, trunc_cf_energies, k_max_index, num_states, trunc_cf_percents, &
                              Jz_vals)
    USE matrix_tools
    USE stev_ops
    IMPLICIT NONE
    REAL(KIND = 8), INTENT(IN)              :: CFP(:,:)
    REAL(KIND = 8),ALLOCATABLE              :: cf_energies(:), cf_percents(:,:)
    REAL(KIND = 8),ALLOCATABLE, INTENT(OUT) :: trunc_cf_energies(:), trunc_cf_percents(:,:), &
                                               Jz_vals(:)
    COMPLEX(KIND = 8),ALLOCATABLE           :: Jz(:,:)
    INTEGER, INTENT(IN)                     :: k_max_index, num_states
    INTEGER                                 :: k, q, i
    COMPLEX(KIND = 8),ALLOCATABLE           :: HCF(:,:), HCF_vecs(:,:), StevensOperators(:,:,:,:), &
                                               JP(:,:), JM(:,:), J2(:,:), JZed(:,:)

    ! Allocate arrays for hamiltonian, eigenvectors, and eigenvalues
    ALLOCATE(HCF(num_states, num_states),HCF_vecs(num_states, num_states), &
             cf_energies(num_states), cf_percents(num_states, num_states))

    HCF = (0.0_8, 0.0_8)
    HCF_vecs = (0.0_8, 0.0_8)
    cf_energies = 0.0_8
    cf_percents = 0.0_8

    ! Calculate stevens operators
    CALL GET_STEVENS_OPERATORS(num_states - 1, StevensOperators, JP, JM, J2, JZed)

    ! Calculate HCF
    DO k = 1, k_max_index
        DO q = 1, 4*k+1
            HCF = HCF + CFP(k,q) * StevensOperators(k,q,:,:)
        END DO
    END DO

    ! For Kramers ions
    ! Add small zeeman field in z direction
    ! Uses field strength of 25 micro Tesla = Earth's magnetic field
    ! Stops linear combination of +- states
    IF (MODULO(num_states,2) == 0) THEN
        HCF = HCF + 0.5_8*(JP+JM)*0.0_8 &
                  + (1.0_8/(2.0_8*(0.0_8, 1.0_8)))*(JP-JM)*0.0_8 &
                  + JZed*25000.0_8*10.0_8**-9.0_8 
    END IF

    ! Diagonalise HCF
    CALL DIAGONALISE('U', HCF, HCF_vecs, cf_energies)

    ! Set first CF Energy as zero
    cf_energies = cf_energies - cf_energies(1) 

    IF (MODULO(num_states,2) == 0) THEN
        ! For Kramers systems print every 2nd energy as degenerate

        ALLOCATE(trunc_cf_energies(num_states/2))
        trunc_cf_energies = 0.0_8

        q = 1
        DO k = 1, num_states
            IF (MODULO(k,2) > 0) THEN
                trunc_cf_energies(q) = cf_energies(k)
                q = q + 1
            END IF
        END DO
    ELSE
        ! For non-Kramers systems print all energies
        ALLOCATE(trunc_cf_energies(num_states))
        trunc_cf_energies = 0.0_8
        trunc_cf_energies = cf_energies
    END IF

    !Calculate % contributions of each state to each eigenvector

    DO k = 1, num_states
        cf_percents(k,:) = ABS(REAL(HCF_vecs(:,k),8))**2 + ABS(DIMAG(HCF_vecs(:,k)))**2
    END DO

    IF (MODULO(num_states,2) == 0) THEN
        !For Kramers systems print every 2nd percentage vector as states are degenerate

        ALLOCATE(trunc_cf_percents(num_states/2, num_states))
        trunc_cf_percents = 0.0_8

        q = 0
        DO k = 1, num_states/2
            trunc_cf_percents(k,:) = cf_percents(2*k-1,:)
        END DO
    ELSE
        !For non-Kramers systems print all energies
        ALLOCATE(trunc_cf_percents(num_states, num_states))
        trunc_cf_percents = 0.0_8
        trunc_cf_percents = cf_percents
    END IF

    ! Calculate Jz expectation value in CF eigenbasis

    ALLOCATE(Jz(num_states, num_states), Jz_vals(num_states))
    Jz = (0.0_8, 0.0_8)

    DO i = 1, num_states
        Jz(i,i) = (num_states-1.0_8)/2.0_8 - i + 1.0_8
    END DO

    CALL EXPECTATION('F', HCF_vecs, Jz)

    Jz_vals = REAL(DIAG(Jz), 8)

END SUBROUTINE CALCULATE_CF_PROPS

 SUBROUTINE UPPERCASE(str)
    IMPLICIT NONE
    INTEGER            :: i, gap
    CHARACTER(LEN = *) :: str
    
    GAP = ICHAR('A')-ICHAR('a')

    IF(LEN(str) > 0) THEN
        DO i = 1, LEN(str)
            IF(str(i:i) <= 'z') THEN
                IF(str(i:i) >= 'a') str(i:i) = CHAR(ICHAR(str(i:i)) + gap)
            END IF
        END DO
    END IF

END SUBROUTINE UPPERCASE

RECURSIVE FUNCTION G_ANGLE(A,B) RESULT(X)
    IMPLICIT NONE
    REAL(KIND = 8),INTENT(IN) :: A(3),B(3)
    REAL(KIND = 8)            :: X, pie
    pie = 3.141592653589793238462643383279502884197_8
    X = 180.0_8*dacos((A(1)*B(1)+A(2)*B(2)+A(3)*B(3))/(G_RADIAL(A)*G_RADIAL(B)))/pie
END FUNCTION G_ANGLE
    
RECURSIVE FUNCTION G_RADIAL(input) RESULT(res)
    IMPLICIT NONE
    REAL(KIND = 8),INTENT(IN) :: input(3)
    REAL(KIND = 8)            :: res
    res = dsqrt(input(1)*input(1)+input(2)*input(2)+input(3)*input(3))
END FUNCTION G_RADIAL


SUBROUTINE WRITE_OUTPUT_FILE(num_states_J, num_states_L, Kramers_J, CFP_J, CFP_L, input_file, &
                             energies_J, energies_L, cf_energies_J, cf_energies_L, g_vals, &
                             g_vecs, ang, composition_J, composition_L, k_max_index_J, &
                             k_max_index_L, num_so_states, energies_all, Jz_vals, Lz_vals)
    IMPLICIT NONE
    INTEGER, INTENT(IN)             :: num_states_J, num_states_L, k_max_index_J, k_max_index_L, &
                                       num_so_states
    INTEGER                         :: row, col, i, k, p, q, c
    INTEGER, ALLOCATABLE            :: mJ_heads(:), mL_heads(:)
    LOGICAL, INTENT(IN)             :: Kramers_J
    REAL(KIND = 8), INTENT(IN)      :: CFP_J(:,:), CFP_L(:,:), energies_J(:), energies_L(:), &
                                       cf_energies_J(:), cf_energies_L(:), g_vals(:,:), &
                                       g_vecs(:,:,:), ang(:), composition_J(:,:), &
                                       composition_L(:,:), energies_all(:), Jz_vals(:), &
                                       Lz_vals(:)
    REAL(KIND = 8), ALLOCATABLE     :: SA_Jz_vals(:), mJ(:)
    REAL(KIND = 8)                  :: J, L
    CHARACTER(LEN = *)              :: input_file
    CHARACTER(LEN = 3)              :: cartesian(3)
    CHARACTER(LEN = 300)            :: states_char, FMT, FMTT, Cdummy

    ! Calculate J from num states (2*J+1)
    J = (REAL(num_states_J, 8) - 1.0_8) / 2.0_8
    ALLOCATE(mJ_heads(num_states_J))

    ! Calculate mJ values
    ! If Kramers then these are the numerator of the fraction - '/2' is added when printed
    ! If non-Kramers they are 2* the mJ value
    mJ_heads(1) = J*2.0_8
    DO i = 1, num_states_J-1
        mJ_heads(i + 1) = J*2.0_8 - 2*real(i,8)
    END DO


    ! Calculate L from num states (2*L+1)
    L = (REAL(num_states_L, 8) - 1.0_8) / 2.0_8
    ALLOCATE(mL_heads(num_states_L))

    ! Calculate mL values
    mL_heads(1) = L
    DO i = 1, num_states_L-1
        mL_heads(i + 1) = L - real(i,8)
    END DO

    !Open output file
    OPEN(66,FILE = TRIM(input_file) //"_extracted", STATUS = 'UNKNOWN')

        ! Write header for file
        WRITE(66,'(A, A)') 'Extracted information from ', TRIM(input_file)
        WRITE(66, '(A)') ''
        IF(Kramers_J .EQV. .TRUE.) WRITE(66, '(A)') '!!!! Kramers ion -- all mJ states doubly degenerate !!!!'
        WRITE(66, '(A)') ''
        WRITE(66, '(A)') ''

        ! If Kramers - write molcas energies, J basis CF params, J basis CF energies, principal g values, and gz angles
            IF(Kramers_J .EQV. .TRUE.) THEN 
                WRITE(66,'(A)') '     Molcas       Crystal Field               Principal G values'
                WRITE(66,'(A)') '  Energy (cm-1)   Energy (cm-1)         Gx             Gy             Gz           Angle'
                WRITE(66,'(A)') REPEAT('‾',90)
                DO i = 1,num_states_J/2
                    IF(i /= 1) THEN
                        WRITE(66,'(6F15.7)') energies_J(i), cf_energies_J(i), g_vals(i,3), g_vals(i,2), g_vals(i,1), ang(i)
                    ELSE
                        WRITE(66,'(5F15.7,A)') energies_J(i), cf_energies_J(i), g_vals(i,3), g_vals(i,2), g_vals(i,1),'        --     '
                    END IF
                END DO
    
                cartesian = ['x  ', 'y  ', 'z  ']
    
                ! Write g tensors for each state
                CALL SUB_SECTION_HEADER('G tensors:', 66)

                c = 1
                DO i = 1,num_states_J/2
                    WRITE(66, '(A, I0, A, I0, A)')  'States ', c ,',', c + 1, ' :'
                    c = c + 2
                    WRITE(66, '(A)') '            Gx              Gy             Gz'
                    IF(i /= 1) THEN
                        WRITE(66,'(A, 3F15.7)') (cartesian(col), g_vecs(i,3,col),g_vecs(i,2,col),g_vecs(i,1,col), col = 1, 3)
                    ELSE
                        WRITE(66,'(A, 3F15.7)') (cartesian(col), g_vecs(i,3,col),g_vecs(i,2,col),g_vecs(i,1,col), col = 1, 3)
                    END IF
                    WRITE(66,'(A)')
                END DO
                WRITE(66,'(A)')
    
                IF (Kramers_J .EQV. .TRUE.) THEN
                    WRITE(Cdummy, '(A, I2, A)') 'Crystal Field in the J = ', NINT(J*2), '/2 basis:'
                    CALL SUB_SECTION_HEADER(Cdummy, 66)
                ELSE
                    WRITE(Cdummy, '(A, I2, A)') 'Crystal Field in the J = ', NINT(J), ' basis:'
                    CALL SUB_SECTION_HEADER(Cdummy, 66)
                END IF

                ! Write J basis CF contributions from each state from SINGLE ANISO
                ! This uses all CF parameters
                CALL SUB_SECTION_HEADER('SINGLE_ANISO:  % contributions from mJ states to '//&
                                    'crystal field eigenstates:', 66)
                WRITE(66,'(A, I0)') 'This is calculated using operators of all valid ranks, in this case up to k = ', kmax(INT(num_states_J))

                WRITE(66,'(A)') ''

                WRITE(FMTT,*) '(SP, I3, A',(', I3, A', i = 2, num_states_J), ')'
                WRITE(states_char,TRIM(FMTT)) (INT(mj_heads(i)),'/2     ',i = 1, num_states_J)
                WRITE(66,'(A,A)') ' mJ =             ', TRIM(states_char)
        
                WRITE(FMT, '(A, I0, A)')'(',num_states_J+1,'A)'
                WRITE(66,FMT) REPEAT('‾',18+LEN_TRIM(states_char))
        
                WRITE(FMT, '(A, I0, A)') '(A, I2, A, I3, ', num_states_J,'F10.3)'
                c = 1
                DO i = 1, num_states_J/2
                    WRITE(66,FMT) 'States ', c ,',', c + 1, (100.0_8*composition_J(row, 2*i-1), row = 1, num_states_J)
                    c = c + 2
                END DO
        
                ! Write J basis crystal field contributions from each state
                ! This uses CF parameters upto k = 6
                CALL SUB_SECTION_HEADER('Crystal Field: % contributions from mJ states to '//&
                                    'crystal field eigenstates:', 66)
                WRITE(66,'(A, I0)') 'This is calculated using operators up to k = ', INT(k_max_index_J*2)
                
                IF (kmax(INT(num_states_J)) <= 6) THEN
                    WRITE(66,'(A)') 'In this case these numbers should match those in the above table'
                END IF

                WRITE(66,'(A)') ''

                WRITE(66,'(A,A)') ' mJ =             ', TRIM(states_char)
        
                WRITE(FMT, '(A, I0, A)')'(',num_states_J+1,'A)'
                WRITE(66,FMT) REPEAT('‾',18+LEN_TRIM(states_char))
        
                WRITE(FMT, '(A, I0, A)') '(A, I2, A, I3, ', num_states_J,'F10.3)'
                c = 1
                DO i = 1, num_states_J/2
                    WRITE(66,FMT) 'States ', c ,',', c + 1, (100.0_8*cf_percents_J(i,col),col = 1, num_states_J)
                    c = c + 2
                END DO
    
            ! If non-Kramers write molcas energies, cf energies, and wavefunction compositions
            ELSE 
                WRITE(66,'(A)') '     Molcas       Crystal Field'
                WRITE(66,'(A)') '  Energy (cm-1)   Energy (cm-1)'
                WRITE(66,'(A)') REPEAT('‾',31)
                DO i = 1,num_states_J
                    IF(i /= 1) THEN
                        WRITE(66,'(2F15.7)') energies_J(i), cf_energies_J(i)
                    ELSE
                        WRITE(66,'(2F15.7,A)') energies_J(i), cf_energies_J(i)
                    END IF
                END DO
    
                ! Write J basis CF contributions from each state from SINGLE ANISO
                ! This uses all CF parameters
                CALL SUB_SECTION_HEADER('SINGLE_ANISO:  % contributions from mJ states to '//&
                                    'crystal field eigenstates:', 66)
                WRITE(66,'(A, I0)') 'This is calculated using operators of all valid ranks, in this case up to k = ', kmax(INT(num_states_J))
                WRITE(66,'(A)') ''
        
                WRITE(FMTT,*) '(I3,A',(',I3,A',i = 2, num_states_J),')'
                WRITE(states_char,TRIM(FMTT)) (INT(mj_heads(i))/2,'       ',i = 1, num_states_J)
                WRITE(66,'(A,A)') ' mJ =          ', TRIM(states_char)
        
                WRITE(FMT, '(A, I0, A)')'(',num_states_J+1,'A)'
                WRITE(66,FMT) REPEAT('‾',15+LEN_TRIM(states_char))
        
                WRITE(FMT, '(A, I0, A)') '(A, I2, ', num_states_J,'F10.3)'
                DO i = 1, num_states_J
                    WRITE(66,FMT) 'State ', i, (100.0_8*composition_J(row, i), row = 1, num_states_J)
                END DO
        
                ! Write J basis crystal field contributions from each state
                ! This uses CF parameters upto k = 6
                CALL SUB_SECTION_HEADER('Crystal Field: % contributions from mJ states to '//&
                                    'crystal field eigenstates:', 66)

                WRITE(66,'(A, I0)') 'This is calculated using operators up to k = ', INT(k_max_index_J*2)
                IF (kmax(INT(num_states_J)) <= 6) THEN
                    WRITE(66,'(A)') 'In this case these numbers should match those in the above table'
                    WRITE(66,*) 
                END IF
        
                WRITE(66,'(A,A)') ' mJ =          ', TRIM(states_char)
        
                WRITE(FMT, '(A, I0, A)')'(',num_states_J+1,'A)'
                WRITE(66,FMT) REPEAT('‾',15+LEN_TRIM(states_char))
        
                WRITE(FMT, '(A, I0, A)') '(A, I2, ', num_states_J,'F10.3)'
                DO i = 1, num_states_J
                    WRITE(66,FMT) 'State ', i, (100.0_8*cf_percents_J(i,col),col = 1, num_states_J)
                END DO
            END IF

            !Write Jz values

            CALL SUB_SECTION_HEADER('SINGLE_ANISO: Jz Expectation Values', 66)

            ALLOCATE(mJ(num_states_J))
            mJ = 0.0_8

            DO i = 1, num_states_J
                mJ(i) = (num_states_J-1.0_8)/2.0_8 - i + 1.0_8
            END DO

            ALLOCATE(SA_Jz_vals(num_states_J))
            SA_Jz_vals = 0.0_8
            DO row = 1, num_states_J
                DO col = 1, num_states_J
                    SA_Jz_vals(row) = SA_Jz_vals(row) + mJ(col)*composition_J(col,row)
                END DO
            END DO

            DO row = 1, num_states_J
                WRITE(66, '(F6.3)') SA_Jz_vals(row)
            END DO
            WRITE(66,'(A)') ''

            CALL SUB_SECTION_HEADER('Crystal Field: Jz Expectation Values', 66)

            DO row = 1, num_states_J
                WRITE(66, '(F6.3)') Jz_vals(row)
            END DO
            WRITE(66,'(A)') ''
    
            ! Write J Basis crystal field parameters in PHI format

            CALL SUB_SECTION_HEADER('J Basis Crystal field parameters in PHI format -'//&
                                    'these include OEFs :', 66)

            DO k = 1,k_max_index_J
                p = -1
                DO q = 1,4*k+1
                    p = p + 1
                    WRITE(66,'(3I3,E22.14E2)') 1,2*k,-2*k+p,CFP_J(k,q)
                END DO
            END DO
            WRITE(66,'(A)') ''

            ! Write J Basis crystal field parameters
            CALL SUB_SECTION_HEADER('J Basis Crystal field parameters in Stevens form -'//&
                                    'these include OEFs :', 66)
            WRITE(66,'(A)') '  k     q     B_k^q'
            DO k = 1,k_max_index_J
                p = -1
                DO q = 1,4*k+1
                    p = p + 1
                    WRITE(66,'(I3, A, I3, A ,E22.14E2)') 2*k, '   ', -2*k+p, '   ', CFP_J(k,q)
                END DO
            END DO
            WRITE(66,'(A)') ''

            ! Write L basis crystal field information
            WRITE(Cdummy, '(A, I2, A)') 'Crystal Field in the L = ', NINT(L), ' basis:'
            CALL SUB_SECTION_HEADER(Cdummy, 66)

            WRITE(66,'(A)') '     Molcas       Crystal Field'
            WRITE(66,'(A)') '  Energy (cm-1)   Energy (cm-1)'
            WRITE(66,'(A)') REPEAT('‾',31)

            DO i = 1,num_states_L
                IF(i /= 1) THEN
                    WRITE(66,'(2F15.7)') energies_L(i), cf_energies_L(i)
                ELSE
                    WRITE(66,'(2F15.7,A)') energies_L(i), cf_energies_L(i)
                END IF
            END DO
            WRITE(66,'(A)')
    
            ! Write L basis CF contributions from each state from SINGLE ANISO
            ! This uses all CF parameters
            CALL SUB_SECTION_HEADER('SINGLE_ANISO:  % contributions from mL states to '//&
                                    'crystal field eigenstates:', 66)
            WRITE(66,'(A, I0)') 'This is calculated using operators of all valid ranks, in this case up to k = ', kmax(INT(num_states_L))
            WRITE(66,'(A)') ''
        
            WRITE(FMTT,*) '(SP, I2,A',(',I2,A',i = 2, num_states_L/2),',SS,I2,A',(',SP,I2,A',i = 1+num_states_L/2, num_states_L),')'
            WRITE(states_char,TRIM(FMTT)) (INT(mL_heads(i)),'        ',i = 1, num_states_L)
            WRITE(66,'(A,A)') ' mL =           ', TRIM(states_char)
        
            WRITE(FMT, '(A, I0, A)')'(',num_states_L+1,'A)'
            WRITE(66,FMT) REPEAT('‾',16+LEN_TRIM(states_char))
        
            WRITE(FMT, '(A, I0, A)') '(A, I2, ', num_states_L,'F10.3)'
            DO i = 1, num_states_L
                WRITE(66,FMT) 'State ', i, (100.0_8*composition_L(row, i), row = 1, num_states_L)
            END DO
        
            ! Write L basis crystal field contributions from each state
            ! This uses CF parameters upto k = 6
            CALL SUB_SECTION_HEADER('Crystal Field: % contributions from mL states to '//&
                                    'crystal field eigenstates:', 66)
            WRITE(66,'(A, I0)') 'This is calculated using operators up to k = ', INT(k_max_index_L*2)
            IF (kmax(INT(num_states_L)) <= 6) THEN 
                WRITE(66,'(A)') 'In this case these numbers should match those in the above table'
            END IF
            WRITE(66,'(A)') ''
        
            WRITE(66,'(A,A)') ' mL =           ', TRIM(states_char)
        
            WRITE(FMT, '(A, I0, A)')'(',num_states_L+1,'A)'
            WRITE(66,FMT) REPEAT('‾',16+LEN_TRIM(states_char))
        
            WRITE(FMT, '(A, I0, A)') '(A, I2, ', num_states_L,'F10.3)'
            DO i = 1, num_states_L
                WRITE(66,FMT) 'State ', i, (100.0_8*cf_percents_L(i,col),col = 1, num_states_L)
            END DO
    
            ! Write L Basis crystal field parameters in PHI format
            CALL SUB_SECTION_HEADER('L Basis Crystal field parameters in PHI format -'//&
                                    'these include OEFs :', 66)
            DO k = 1, k_max_index_L
                p = -1
                DO q = 1,4*k+1
                    p = p + 1
                    WRITE(66,'(3I3,E22.14E2)') 1,2*k,-2*k+p,CFP_L(k,q)
                END DO
            END DO

            ! Write L Basis crystal field parameters
            CALL SUB_SECTION_HEADER('L Basis Crystal field parameters in Stevens form -'//&
                                    'these include OEFs :', 66)
            WRITE(66,'(A)') '  k     q     B_k^q'
            DO k = 1, k_max_index_L
                p = -1
                DO q = 1,4*k+1
                    p = p + 1
                    WRITE(66,'(I3, A, I3, A ,E22.14E2)') 2*k, '   ', -2*k+p, '   ', CFP_L(k,q)
                END DO
            END DO

            !Write Lz values
            CALL SUB_SECTION_HEADER('Lz Expectation Values', 66)
            DO i = 1, num_states_L
                WRITE(66, '(F6.3)') Lz_vals(i)
            END DO

            ! Write all SO states
            CALL SUB_SECTION_HEADER('All spin-orbit state energies from MOLCAS:', 66)
            DO k = 1, num_so_states
                WRITE(66,'(F15.7)') energies_all(k)
            END DO


    CLOSE(66)

END SUBROUTINE WRITE_OUTPUT_FILE

SUBROUTINE WRITE_OUTPUT_FILE_NO_SA(num_states_J, Kramers_J,input_file, energies_J, g_vals, g_vecs,&
                                   ang, num_so_states, energies_all)
    IMPLICIT NONE
    INTEGER, INTENT(IN)             :: num_states_J, num_so_states
    INTEGER                         :: col, i, c
    LOGICAL, INTENT(IN)             :: Kramers_J
    REAL(KIND = 8), INTENT(IN)      :: energies_J(:), g_vals(:,:), g_vecs(:,:,:), ang(:), energies_all(:)
    CHARACTER(LEN = *)              :: input_file
    CHARACTER(LEN = 3)              :: cartesian(3)


    !Open output file
    OPEN(66,FILE = TRIM(input_file) //"_extracted", STATUS = 'UNKNOWN')

        ! Write header for file
        WRITE(66,'(A, A)') 'Extracted information from ', TRIM(input_file)
        WRITE(66, '(A)') ''
        IF(Kramers_J .EQV. .TRUE.) WRITE(66, '(A)') '!!!! Kramers ion -- all mJ states doubly degenerate !!!!'
        WRITE(66, '(A)') ''
        WRITE(66, '(A)') ''

        ! If Kramers - write molcas energies, J basis CF params, J basis CF energies, principal g values, and gz angles
            IF(Kramers_J .EQV. .TRUE.) THEN 
                WRITE(66,'(A)') '     Molcas                Principal G values'
                WRITE(66,'(A)') '  Energy (cm-1)      Gx             Gy             Gz           Angle'
                WRITE(66,'(A)') REPEAT('‾',69)
                DO i = 1,num_states_J/2
                    IF(i /= 1) THEN
                        WRITE(66,'(5F15.7)')   energies_J(i), g_vals(i,3), g_vals(i,2), g_vals(i,1), ang(i)
                    ELSE
                        WRITE(66,'(4F15.7,A)') energies_J(i), g_vals(i,3), g_vals(i,2), g_vals(i,1),'        --     '
                    END IF
                END DO
    
                cartesian = ['x  ', 'y  ', 'z  ']
    
                ! Write g tensors for each state
                WRITE(66,'(A)') ''
                WRITE(66,'(A)') 'G tensors:'
                c = 1
                DO i = 1,num_states_J/2
                    WRITE(66, '(A, I0, A, I0, A)')  'States ', c ,',', c + 1, ' :'
                    c = c + 2
                    WRITE(66, '(A)') '            Gx              Gy             Gz'
                    IF(i /= 1) THEN
                        WRITE(66,'(A, 3F15.7)') (cartesian(col), g_vecs(i,3,col),g_vecs(i,2,col),g_vecs(i,1,col), col = 1, 3)
                    ELSE
                        WRITE(66,'(A, 3F15.7)') (cartesian(col), g_vecs(i,3,col),g_vecs(i,2,col),g_vecs(i,1,col), col = 1, 3)
                    END IF
                    WRITE(66,'(A)')
                END DO
                WRITE(66,'(A)')
            ELSE
                WRITE(66,'(A)') '     Molcas    '
                WRITE(66,'(A)') '  Energy (cm-1)'
                WRITE(66,'(A)') REPEAT('‾',15)

                DO i = 1,num_states_J
                    IF(i /= 1) THEN
                        WRITE(66,'(F15.7)') energies_J(i)
                    ELSE
                        WRITE(66,'(F15.7,A)') energies_J(i)
                    END IF
                END DO
            END IF

            ! Write all SO states
            WRITE(66,'(A)')
            WRITE(66,'(A)') 'All spin-orbit state energies from MOLCAS:'
            WRITE(66,'(A)') REPEAT('‾',42)
            DO i = 1, num_so_states
                WRITE(66,'(F15.7)') energies_all(i)
            END DO

    CLOSE(66)

END SUBROUTINE WRITE_OUTPUT_FILE_NO_SA

FUNCTION kmax(num_states) RESULT(out)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: num_states
    INTEGER             :: out, temp

    temp = num_states - 1

    IF (modulo(temp, 2) == 0) THEN
        out = temp
    ELSE
        out = temp - 1
    END IF

END FUNCTION kmax

FUNCTION Cut_Past_Char (s,cutchar)  RESULT(outs)
    !Trims a string past to a given character
    !e.g Cut_Past_Char('Hello_World','_') = World

    CHARACTER(*)        :: s,cutchar
    CHARACTER(LEN(s)+100) :: outs
    INTEGER             :: i

    outs = s 
    DO WHILE(.TRUE.)
       i = INDEX(outs,cutchar) 
       IF (i == 0) EXIT
       outs = outs(i+1:LEN(ADJUSTL(TRIM(s))))
    END DO
END FUNCTION Cut_Past_Char

FUNCTION Cut_To_Char (s,cutchar)  RESULT(outs)
    !Truncates a string at a given character
    !e.g Cut_To_Char('Hello_World','_') = Hello

    CHARACTER(*)        :: s,cutchar
    CHARACTER(LEN(s)+100) :: outs
    INTEGER             :: i

    outs = s 
    DO WHILE(.TRUE.)
       i = INDEX(outs,cutchar) 
       IF (i == 0) EXIT
       outs = outs(1:i-1)
    END DO
END FUNCTION Cut_To_Char

SUBROUTINE SUCCESS()
    IMPLICIT NONE
    WRITE(6,'(A)') '                                        SUCCESS'
END SUBROUTINE SUCCESS

SUBROUTINE FAIL(clean, converge)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: clean, converge
    WRITE(6,'(A, I0)') '                                   converge = ', converge
    WRITE(6,'(A, I0)') '                                      clean = ', clean
    WRITE(6,'(A)')     '                                           FAIL'
END SUBROUTINE FAIL

SUBROUTINE SUB_SECTION_HEADER(title, unit)
    ! Writes a subsection section header to unit
    ! Two lines
    !   title
    !   ‾‾‾‾‾
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: title
    INTEGER, INTENT(IN)            :: unit

    INTEGER                        :: num_l_spaces, num_r_spaces

    num_l_spaces = (80 - LEN_TRIM(title)) / 2
    num_r_spaces = (80 - LEN_TRIM(title)) / 2

    IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
        num_l_spaces = num_l_spaces + 1
    END IF

    WRITE(unit,*)

    WRITE(unit, '(3A)') REPEAT(' ',num_l_spaces), &
                        TRIM(title), &
                        REPEAT(' ',num_r_spaces)

    WRITE(unit,'(3A)')  REPEAT(' ',num_l_spaces), &
                        REPEAT('‾',LEN_TRIM(title)), &
                        REPEAT(' ',num_r_spaces)

END SUBROUTINE SUB_SECTION_HEADER

END PROGRAM EXTRACTOR
