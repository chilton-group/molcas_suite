!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   molcas_basis.f90
!
!   This program is a part of molcas_suite
!
!   Extracts list of atomic orbitals (basis functions) from 
!   either the last or the first RASSCF section of a given
!   molcas output file
!
!   
!      Authors:
!       Nicholas Chilton
!       Commented by Jon Kragskow
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


PROGRAM MOLCAS_BASIS
    IMPLICIT NONE
    
    CHARACTER(LEN = 500)    :: line, l_or_f, input_file
    INTEGER                 :: active,inactive,basis,secondary,i,count,location(500),mol_version,ras_count,ras_loc(500)
        !Read user input
        CALL READ_USER_INPUTS(mol_version, input_file, l_or_f)
            
        !Set counters to zero
        count = 0
        location = 0
        ras_count = 0
        ras_loc = 0
        i = 0

        !Open molcas output file
        OPEN(30,FILE = TRIM(ADJUSTL(input_file)), STATUS = 'OLD')

    
        !Read in output file and count number of rasscf sections
        DO WHILE(.TRUE.)
            READ(30,'(A)',END=31) line
            i = i + 1
            IF(line(1:24) == '--- Start Module: rasscf') THEN
                ras_count = ras_count + 1
                ras_loc(ras_count) = i
            END IF
            IF(mol_version == 7) THEN
                IF(TRIM(line) == '++       Wave function specifications:') THEN
                    count = count + 1
                    location(count) = i
                END IF
            ELSE IF(mol_version == 8) THEN
                IF(TRIM(line) == '++    Wave function specifications:') THEN
                    count = count + 1
                    location(count) = i
                END IF
            END IF
        END DO
        31 CONTINUE
        REWIND(30)
        
        !WRITE(6,*) "count",count
        !WRITE(6,*) "locs",location(1:count)
        
        !Read first or last RASSCF section depending on user selection
        IF(ras_count > 1) THEN
            !k = 1
            !DO i = 1,count
            !   IF(location(i) > ras_loc(k) .and. location(i) < ras_loc(k+1)
            !END DO
            !WRITE(6,*) 'Found',ras_count,'RASSCF sections, using first as basis'
            IF(l_or_f(1:1) == 'f' .or. l_or_f(1:1) == 'F') count = 1
            IF(l_or_f(1:1) == 'l' .or. l_or_f(1:1) == 'L') count = count
        ELSE
            count = 1
        END IF
        
        
        !Read basis information and write basis file
        i = 0
        DO while(.TRUE.)
            READ(30,'(A)',END=30) line
            i = i + 1
            IF(i < location(count)) cycle
            IF(mol_version == 7) THEN
                IF(TRIM(line) == '++       Wave function specifications:') THEN
                            READ(30,'(A)',end=30) line
                            READ(30,'(A)',end=30) line
                            READ(30,'(A)',end=30) line
                            READ(30,'(A)',end=30) line
                            READ(30,'(A)',end=30) line
                            READ(30,'(A)',end=30) line
                            READ(30,'(A)',end=30) line
                            READ(line(34:),*) inactive
                            READ(30,'(A)',end=30) line
                            READ(line(32:),*) active
                            READ(30,'(A)',end=30) line
                            READ(line(35:),*) secondary
                            basis = inactive+active+secondary
                            WRITE(6,*) "basis",basis
                ELSE IF(TRIM(line) == '      MOLECULAR ORBITALS FOR SYMMETRY SPECIES 1: a') THEN
                    WRITE(6,'(A)') 'Condensed format detected, ending'
                    CLOSE(30)
                    STOP
                ELSE IF(TRIM(line) == '      Molecular orbitals for symmetry species 1: a') THEN
                    DO while (.TRUE.)
                        READ(30,'(A)',end=30) line
                        IF(line(1:13) == '      Orbital') exit
                    END DO
                    READ(30,'(A)',end=30) line
                    READ(30,'(A)',end=30) line
                    READ(30,'(A)',end=30) line
                    open(31,file='basis')
                    DO i = 1,basis
                        READ(30,'(A)') line
                        WRITE(31,'(A)') TRIM(line(7:19))
                    END DO
                    CLOSE(30)
                    CLOSE(31)
                    STOP
                END IF
            ELSE IF(mol_version == 8) THEN
                IF(TRIM(line) == '++    Wave function specifications:') THEN
                                                READ(30,'(A)',end=30) line
                                                READ(30,'(A)',end=30) line
                                                READ(30,'(A)',end=30) line
                                                READ(30,'(A)',end=30) line
                                                READ(30,'(A)',end=30) line
                                                READ(30,'(A)',end=30) line
                                                READ(30,'(A)',end=30) line
                                                READ(line(34:),*) inactive
                                                READ(30,'(A)',end=30) line
                                                READ(line(32:),*) active
                                                READ(30,'(A)',end=30) line
                                                READ(line(35:),*) secondary
                                                basis = inactive+active+secondary
                        ELSE IF(TRIM(line) == '      MOLECULAR ORBITALS FOR SYMMETRY SPECIES 1: a') THEN
                                WRITE(6,'(A)') 'Condensed format detected, ending'
                                CLOSE(30)
                                STOP
                        ELSE IF(TRIM(line) == '      Molecular orbitals for symmetry species 1: a') THEN
                                DO while (.TRUE.)
                                        READ(30,'(A)',end=30) line
                                        IF(line(1:13) == '      Orbital') exit
                                END DO
                                READ(30,'(A)',end=30) line
                                READ(30,'(A)',end=30) line
                                READ(30,'(A)',end=30) line
                                OPEN(31,file='basis')
                                DO i = 1,basis
                                        READ(30,'(A)') line
                                        WRITE(31,'(A)') TRIM(line(7:19))
                                END DO
                                CLOSE(30)
                                CLOSE(31)
                                STOP
                        END IF
            END IF
        END DO
        30 CONTINUE
        CLOSE(30)
    
        WRITE(6,'(A)') 'Basis information written to basis'
    
    CONTAINS

    SUBROUTINE READ_USER_INPUTS(mol_version, input_file, l_or_f)
        USE version, ONLY : PRINT_VERSION
        IMPLICIT NONE
        CHARACTER(LEN = 500)              :: CDUMMY
        CHARACTER(LEN = 500), INTENT(OUT) :: input_file, l_or_f
        INTEGER, INTENT(OUT)              :: mol_version
        LOGICAL                           :: file_exists

        CALL GET_COMMAND_ARGUMENT(1,CDUMMY)

        IF(TRIM(CDUMMY) == '-h' .OR. TRIM(CDUMMY) == '') THEN
            WRITE(6,'(A)') "molcas_basis <mol_version> <file> <(L)ast/(F)irst>"
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Creates basis file from molcas output file'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'mol_version       : INTEGER                                                                          e.g 8 for CSF'
            WRITE(6,'(A)') 'file              : CHARACTER      molcas output file                                                e.g  dy.out'
            WRITE(6,'(A)') '(L)ast/(F)irst    : CHARACTER      use first(F) or last(L) RASSCF section if multiple present        e.g  L or F'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Example ------ molcas_basis 8 dy.out L'
            WRITE(6,'(A)') ''
            STOP
        END IF

        ! Print mol_version number
        IF (TRIM(CDUMMY) == '-v' .OR. TRIM(CDUMMY) == '-version' .OR. TRIM(CDUMMY) == 'version') THEN
            CALL PRINT_VERSION('molcas_basis')
            STOP
        END IF

        READ(CDUMMY,*) mol_version
    
        CALL GET_COMMAND_ARGUMENT(2,input_file)

        CALL GET_COMMAND_ARGUMENT(3,l_or_f)

        IF(l_or_f(1:1) /= 'f' .and. l_or_f(1:1) /= 'F' .and. l_or_f(1:1) /= 'l' .and. l_or_f(1:1) /= 'L') THEN
            WRITE(6,'(A)') "incorrect arguments!"
            STOP
        END IF

        !Check file exists

        INQUIRE(FILE=input_file, EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(3A)') 'Specified file ', TRIM(ADJUSTL(input_file)),' does not exist'
            STOP
        END IF


    END SUBROUTINE READ_USER_INPUTS

END PROGRAM molcas_basis
