!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   openmolcas_compare.f90 
!
!   This program is a part of molcas_suite
!
!   Compares orb files?
!
!   
!      Authors:
!       Nicholas Chilton
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program openmolcas_compare
USE version, ONLY : PRINT_VERSION
use prec
use matrix_tools
implicit none

character(len=500)::arg,line,arg2
integer::basis,basis2,i,j,k,wrote,num_orb_list, found,IOstatus
integer,allocatable::order(:)
real(kind=WP)::length,thresh,dot
real(kind=WP),allocatable::coeffs1(:,:),occ1(:),energy1(:),coeffs2(:,:),occ2(:),energy2(:),C_mat(:,:),S_mat(:,:),mat1(:,:),mat2(:,:),D_mat(:,:),V_mat(:,:),eigs(:),rootS_mat(:,:)
character(len=1),allocatable::index1(:),index2(:)
character(len=11),allocatable::labels(:)
character(len=2)::Cdummy,type
!character(len=9)::basischar
character(len=20)::FMT

call get_command_argument(1,arg)
if(trim(arg) == '-h') then
  write(6,*) 'openmolcas_compare <orbital_file1> <orbital_file2>'
  stop
end if

! Print version number
IF (TRIM(arg) == '-v' .OR. TRIM(arg) == '-version' .OR. TRIM(arg) == 'version') THEN
    CALL PRINT_VERSION('openmolcas_compare')
    STOP
END IF

call get_command_argument(2,arg2)

open(30,file=trim(adjustl(arg)))
read(30,*)
read(30,*)
read(30,*)
read(30,*)
read(30,*) basis
allocate(coeffs1(basis,basis),occ1(basis),energy1(basis),index1(basis),C_mat(basis,basis),labels(basis),S_mat(basis,basis),mat1(basis,basis),D_mat(basis,basis),V_mat(basis,basis),eigs(basis),rootS_mat(basis,basis))
write(6,*) 'basis =',basis
do while(.true.)
  read(30,'(A)',END=60) line
  if(line(1:4) == '#ORB') then
    write(6,*) 'Reading MO1s...'
    do j = 1,basis
      read(30,*)
      do i = 1,basis/5
        read(30,'(5E22.14E2)') coeffs1(j,i*5-4:i*5)
      end do
      if(mod(basis,5) == 1) read(30,'(1E22.14E2)') coeffs1(j,basis)
      if(mod(basis,5) == 2) read(30,'(2E22.14E2)') coeffs1(j,basis-1:basis)
      if(mod(basis,5) == 3) read(30,'(3E22.14E2)') coeffs1(j,basis-2:basis)
      if(mod(basis,5) == 4) read(30,'(4E22.14E2)') coeffs1(j,basis-3:basis)
    end do
    write(6,*) '...done'
  else if(line(1:4) == '#occ') then
    write(6,*) 'Reading occ1s...'
    read(30,*)
    do i = 1,basis/5
      read(30,'(5E22.14E2)') occ1(i*5-4:i*5)
    end do
    if(mod(basis,5) == 1) read(30,'(1E22.14E2)') occ1(basis)
    if(mod(basis,5) == 2) read(30,'(2E22.14E2)') occ1(basis-1:basis)
    if(mod(basis,5) == 3) read(30,'(3E22.14E2)') occ1(basis-2:basis)
    if(mod(basis,5) == 4) read(30,'(4E22.14E2)') occ1(basis-3:basis)
    write(6,*) '...done'
  else if(line(1:4) == '#ONE') then
    write(6,*) 'Reading ENERGIES...'
    read(30,*)
    do i = 1,basis/10
      read(30,'(10E12.4E2)') energy1(i*10-9:i*10)
    end do
    if(mod(basis,10) == 1) read(30,'(1E12.4E2)') energy1(basis)
    if(mod(basis,10) == 2) read(30,'(2E12.4E2)') energy1(basis-1:basis)
    if(mod(basis,10) == 3) read(30,'(3E12.4E2)') energy1(basis-2:basis)
    if(mod(basis,10) == 4) read(30,'(4E12.4E2)') energy1(basis-3:basis)
    if(mod(basis,10) == 5) read(30,'(5E12.4E2)') energy1(basis-4:basis)
    if(mod(basis,10) == 6) read(30,'(6E12.4E2)') energy1(basis-5:basis)
    if(mod(basis,10) == 7) read(30,'(7E12.4E2)') energy1(basis-6:basis)
    if(mod(basis,10) == 8) read(30,'(8E12.4E2)') energy1(basis-7:basis)
    if(mod(basis,10) == 9) read(30,'(9E12.4E2)') energy1(basis-8:basis)
    write(6,*) '...done'
  else if(line(1:6) == '#index') then
    write(6,*) 'Reading INDICIES...'
    read(30,*)
    do i = 1,basis/10
      read(30,'(A2,10A1)') Cdummy,index1(i*10-9:i*10)
    end do
    if(mod(basis,10) == 1) read(30,'(A2,1A1)') Cdummy,index1(basis)
    if(mod(basis,10) == 2) read(30,'(A2,2A1)') Cdummy,index1(basis-1:basis)
    if(mod(basis,10) == 3) read(30,'(A2,3A1)') Cdummy,index1(basis-2:basis)
    if(mod(basis,10) == 4) read(30,'(A2,4A1)') Cdummy,index1(basis-3:basis)
    if(mod(basis,10) == 5) read(30,'(A2,5A1)') Cdummy,index1(basis-4:basis)
    if(mod(basis,10) == 6) read(30,'(A2,6A1)') Cdummy,index1(basis-5:basis)
    if(mod(basis,10) == 7) read(30,'(A2,7A1)') Cdummy,index1(basis-6:basis)
    if(mod(basis,10) == 8) read(30,'(A2,8A1)') Cdummy,index1(basis-7:basis)
    if(mod(basis,10) == 9) read(30,'(A2,9A1)') Cdummy,index1(basis-8:basis)
    write(6,*) '...done'
  end if
end do
60 continue
close(30)

open(30,file=trim(adjustl(arg2)))
read(30,*)
read(30,*)
read(30,*)
read(30,*)
read(30,*) basis2
if(basis2 /= basis) then
	write(6,*) "Basis sets do not match"
	stop
end if
allocate(coeffs2(basis,basis),occ2(basis),energy2(basis),index2(basis),mat2(basis,basis))
write(6,*) 'basis =',basis
do while(.true.)
  read(30,'(A)',END=62) line
  if(line(1:4) == '#ORB') then
    write(6,*) 'Reading MO2s...'
    do j = 1,basis
      read(30,*)
      do i = 1,basis/5
        read(30,'(5E22.14E2)') coeffs2(j,i*5-4:i*5)
      end do
      if(mod(basis,5) == 1) read(30,'(1E22.14E2)') coeffs2(j,basis)
      if(mod(basis,5) == 2) read(30,'(2E22.14E2)') coeffs2(j,basis-1:basis)
      if(mod(basis,5) == 3) read(30,'(3E22.14E2)') coeffs2(j,basis-2:basis)
      if(mod(basis,5) == 4) read(30,'(4E22.14E2)') coeffs2(j,basis-3:basis)
    end do
    write(6,*) '...done'
  else if(line(1:4) == '#occ') then
    write(6,*) 'Reading occ2s...'
    read(30,*)
    do i = 1,basis/5
      read(30,'(5E22.14E2)') occ2(i*5-4:i*5)
    end do
    if(mod(basis,5) == 1) read(30,'(1E22.14E2)') occ2(basis)
    if(mod(basis,5) == 2) read(30,'(2E22.14E2)') occ2(basis-1:basis)
    if(mod(basis,5) == 3) read(30,'(3E22.14E2)') occ2(basis-2:basis)
    if(mod(basis,5) == 4) read(30,'(4E22.14E2)') occ2(basis-3:basis)
    write(6,*) '...done'
  else if(line(1:4) == '#ONE') then
    write(6,*) 'Reading ENERGIES...'
    read(30,*)
    do i = 1,basis/10
      read(30,'(10E12.4E2)') energy2(i*10-9:i*10)
    end do
    if(mod(basis,10) == 1) read(30,'(1E12.4E2)') energy2(basis)
    if(mod(basis,10) == 2) read(30,'(2E12.4E2)') energy2(basis-1:basis)
    if(mod(basis,10) == 3) read(30,'(3E12.4E2)') energy2(basis-2:basis)
    if(mod(basis,10) == 4) read(30,'(4E12.4E2)') energy2(basis-3:basis)
    if(mod(basis,10) == 5) read(30,'(5E12.4E2)') energy2(basis-4:basis)
    if(mod(basis,10) == 6) read(30,'(6E12.4E2)') energy2(basis-5:basis)
    if(mod(basis,10) == 7) read(30,'(7E12.4E2)') energy2(basis-6:basis)
    if(mod(basis,10) == 8) read(30,'(8E12.4E2)') energy2(basis-7:basis)
    if(mod(basis,10) == 9) read(30,'(9E12.4E2)') energy2(basis-8:basis)
    write(6,*) '...done'
  else if(line(1:6) == '#index') then
    write(6,*) 'Reading INDICIES...'
    read(30,*)
    do i = 1,basis/10
      read(30,'(A2,10A1)') Cdummy,index2(i*10-9:i*10)
    end do
    if(mod(basis,10) == 1) read(30,'(A2,1A1)') Cdummy,index2(basis)
    if(mod(basis,10) == 2) read(30,'(A2,2A1)') Cdummy,index2(basis-1:basis)
    if(mod(basis,10) == 3) read(30,'(A2,3A1)') Cdummy,index2(basis-2:basis)
    if(mod(basis,10) == 4) read(30,'(A2,4A1)') Cdummy,index2(basis-3:basis)
    if(mod(basis,10) == 5) read(30,'(A2,5A1)') Cdummy,index2(basis-4:basis)
    if(mod(basis,10) == 6) read(30,'(A2,6A1)') Cdummy,index2(basis-5:basis)
    if(mod(basis,10) == 7) read(30,'(A2,7A1)') Cdummy,index2(basis-6:basis)
    if(mod(basis,10) == 8) read(30,'(A2,8A1)') Cdummy,index2(basis-7:basis)
    if(mod(basis,10) == 9) read(30,'(A2,9A1)') Cdummy,index2(basis-8:basis)
    write(6,*) '...done'
  end if
end do
62 continue
close(30)



open(30,file='basis')
	write(6,*) 'Reading basis...'
	j = 0
	wrote = 0
	do i = 1,basis
	  read(30,'(A)',END=61) line
	  j = j+1
	  labels(i) = line(1:11)
	end do
	write(6,*) '...done'
	write(6,*) 'Reading overlap matrix...'
    S_mat = 0.0_WP
    write(FMT,'(I10,A10)') basis,'E25.15E3'
    FMT = adjustl(FMT)
    open(33,file="overlap",status="old")
      do i = 1,basis
        read(33,'('//trim(FMT)//')') S_mat(i,1:basis)
      end do
    close(33)
	write(6,*) '...done'
    write(6,*) 'Orthogonalising MO1s as (S^1/2).C ...'
    ! Orbitals are orthonormal as:
	! (C^T).(S^1/2).(S^1/2).C
    ! Calc S^1/2
    call DIAGONALISE('U',S_mat,V_mat,eigs)
    rootS_mat = 0.0_WP
    do j = 1,basis
      rootS_mat(j,j) = sqrt(eigs(j))
    end do
    rootS_mat = matmul(matmul(V_mat,rootS_mat),transpose(V_mat))
    ! Coordinate transform using S^(1/2) to get orthonormal MOs
	C_mat = transpose(coeffs1)
    mat1 = matmul(rootS_mat,C_mat)
    C_mat = transpose(coeffs2)
    mat2 = matmul(rootS_mat,C_mat)

	do j = 1,basis
		write(6,'(A8,I3,A9,F4.2,A12,E12.4E2,A11,A1,A9,F4.2,A12,E12.4E2,A11,A1,A5)') '---- MO ',j,', occ1 = ',occ1(j),', energy1 = ',energy1(j),', index1 = ',index1(j),', occ2 = ',occ2(j),', energy2 = ',energy2(j),', index2 = ',index2(j),' ----'
		dot = 0.0_8
		do i = 1,basis
			dot = dot + mat1(j,i)*mat2(j,i)
		end do
		write(6,'(A6,F4.2,A10,F6.2)') "Dot = ",dot,", Angle = ",dacos(dot)*180.0_8/pi
	end do
	61 continue
close(30)

contains

recursive subroutine re_order(num,values,list,way)
  implicit none
  integer,intent(in)::num
  integer,intent(out),dimension(:)::list(:)
  integer::j,k
  real(kind=WP),intent(in),dimension(:)::values(:)
  real(kind=WP)::low
  integer,allocatable::done(:)
  character(len=3),intent(in)::way
  allocate(done(num))
  list(:) = 0
  done(:) = 0
  list(1) = minloc(values,1)
  done(list(1)) = 1
  do j=2,num
    low = 1D99
    do k=1,num
      if((values(k) - values(list(1))) >= (values(list(j-1)) - values(list(1))) .and. (values(k) - values(list(1))) < low .and. done(k) == 0) then
        low = (values(k) - values(list(1)))
        list(j) = k
      end if
    end do
    done(list(j)) = 1
  end do
  if(way == 'max') then
    do j = 1,num
      done(j) = list(num-j+1)
    end do
    list = done
  end if
  deallocate(done)
end subroutine re_order
end program openmolcas_compare
