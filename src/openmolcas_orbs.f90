!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   openmolcas_orbs.f90 
!
!   This program is a part of molcas_suite
!
!
!   Extracts orbital compositions in terms of atomic orbitals 
!   from openmolcas RasOrb file and prints to screen
!
!   
!      Authors:
!       Nicholas Chilton
!       Jon Kragskow
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

PROGRAM openmolcas_orbs
    USE prec
    USE matrix_tools
    IMPLICIT NONE
    
    CHARACTER(LEN = 500)             :: input_file, line
    INTEGER                          :: basis, i, j, k, wrote, num_orb_list, found, IOstatus, col, status
    INTEGER,ALLOCATABLE              :: order(:,:)
    REAL(KIND = WP)                  :: length, thresh
    REAL(KIND = WP),ALLOCATABLE      :: coeffs(:,:), occ(:), energy(:), percents(:,:), orb_percents(:), C_mat(:,:), S_mat(:,:), mat(:,:), V_mat(:,:), eigs(:), rootS_mat(:,:)
    CHARACTER(LEN = 1),ALLOCATABLE   :: index_list(:)
    CHARACTER(LEN = 12),ALLOCATABLE  :: labels(:)
    CHARACTER(LEN = 10),ALLOCATABLE  :: orb_list(:), char_temp(:)
    CHARACTER(LEN = 2)               :: Cdummy,type
    CHARACTER(LEN = 20)              :: FMT
    
    ! Read user inputs
    CALL READ_USER_INPUT(input_file, type, thresh)
    
    ! Open RasOrb file
    OPEN(30,FILE = TRIM(ADJUSTL(input_file)), STATUS = 'UNKNOWN')
    
        ! Skip file header
        DO i = 1, 4
            READ(30,*, IOSTAT = status)
            CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
        END DO

        ! Read in number of atomic orbitals 
        READ(30,*, IOSTAT = status) basis
        CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

        ! Allocate arrays
        ALLOCATE(coeffs(basis,basis),occ(basis),energy(basis),index_list(basis),percents(basis,basis),labels(basis),orb_list(basis),C_mat(basis,basis),S_mat(basis,basis),mat(basis,basis),V_mat(basis,basis),eigs(basis),rootS_mat(basis,basis))

        ! Status update
        WRITE(6,'(A, I0)') 'Basis = ', basis

        ! Loop over file, read every orbital section, and store
        DO WHILE(.TRUE.)
            READ(30,'(A)',END=60, IOSTAT = status) line
            CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

            !If orb section is found then inform user and read in data
            IF(line(1:4) == '#ORB') THEN
                WRITE(6,'(A)') 'Reading MOs...'
                DO j = 1,basis
                    READ(30,*, IOSTAT = status)
                    CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

                    ! Read information on MO coefficients for all lines except from the last one
                    DO i = 1,basis/5
                        READ(30,'(5E22.14E2)', IOSTAT = status) (coeffs(j,col), col = i*5-4,i*5)
                        CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                    END DO

                    ! Read final line
                    ! Molcas prints with an 80 character limit
                    ! so this calculates how many entries will be on the final line
                    ! and then reads in the data
                    IF(mod(basis,5) == 1) READ(30,'(1E22.14E2)', IOSTAT = status) coeffs(j,basis)
                    CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

                    IF(mod(basis,5) == 2) READ(30,'(2E22.14E2)', IOSTAT = status) (coeffs(j,col), col = basis-1,basis)
                    CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

                    IF(mod(basis,5) == 3) READ(30,'(3E22.14E2)', IOSTAT = status) (coeffs(j,col), col = basis-2,basis)
                    CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

                    IF(mod(basis,5) == 4) READ(30,'(4E22.14E2)', IOSTAT = status) (coeffs(j,col), col = basis-3,basis)
                    CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))


                END DO

                ! Status update
                WRITE(6,'(A)') '...done'

            ! If occupancy section is found then inform user and read in data
            ELSE IF(line(1:4) == '#OCC') THEN

                ! Status update
                WRITE(6,'(A)') 'Reading OCCs...'

                ! Skip line
                READ(30,*, IOSTAT = status)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

                ! Read information on occupancies for all lines except from the last one
                DO i = 1,basis/5
                    READ(30,'(5E22.14E2)', IOSTAT = status) (occ(col), col = i*5-4,i*5)
                    CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                END DO

                ! Read final line
                ! Molcas prints with an 80 character limit
                ! so this calculates how many entries will be on the final line
                ! and then reads in the data
                IF(mod(basis,5) == 1) READ(30,'(1E22.14E2)', IOSTAT = status) occ(basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,5) == 2) READ(30,'(2E22.14E2)', IOSTAT = status) (occ(col), col = basis-1,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,5) == 3) READ(30,'(3E22.14E2)', IOSTAT = status) (occ(col), col = basis-2,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,5) == 4) READ(30,'(4E22.14E2)', IOSTAT = status) (occ(col), col = basis-3,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

                ! Status update
                WRITE(6,'(A)') '...done'

            ! If one-electron energies section is found then inform user and read in data
            ELSE IF(line(1:4) == '#ONE') THEN

                ! Status update
                WRITE(6,'(A)') 'Reading ENERGIES...'

                ! Skip line
                READ(30,*, IOSTAT = status)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

                ! Read information on one-electron energies for all lines except from the last one
                DO i = 1,basis/10
                    READ(30,'(10E12.4E2)', IOSTAT = status) (energy(col), col = i*10-9, i*10)
                    CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                END DO

                ! Read final line
                ! Molcas prints with an 80 character limit
                ! so this calculates how many entries will be on the final line
                ! and then reads in the data
                IF(mod(basis,10) == 1) READ(30,'(1E12.4E2)', IOSTAT = status) energy(basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 2) READ(30,'(2E12.4E2)', IOSTAT = status) (energy(col), col = basis-1,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 3) READ(30,'(3E12.4E2)', IOSTAT = status) (energy(col), col = basis-2,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 4) READ(30,'(4E12.4E2)', IOSTAT = status) (energy(col), col = basis-3,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 5) READ(30,'(5E12.4E2)', IOSTAT = status) (energy(col), col = basis-4,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 6) READ(30,'(6E12.4E2)', IOSTAT = status) (energy(col), col = basis-5,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 7) READ(30,'(7E12.4E2)', IOSTAT = status) (energy(col), col = basis-6,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 8) READ(30,'(8E12.4E2)', IOSTAT = status) (energy(col), col = basis-7,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 9) READ(30,'(9E12.4E2)', IOSTAT = status) (energy(col), col = basis-8,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

                !Status update
                WRITE(6,'(A)') '...done'

            ! If orbital index_listing section is found then inform user and read in data
            ELSE IF(line(1:6) == '#INDEX') THEN

                !Status update
                WRITE(6,'(A)') 'Reading INDICIES...'
                
                ! Skip line
                READ(30,*, IOSTAT = status)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))

                ! Read information on one-electron energies for all lines except from the last one
                DO i = 1,basis/10
                    READ(30,'(A2,10A1)', IOSTAT = status) Cdummy,(index_list(col), col = i*10-9,i*10)
                    CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                END DO

                ! Read final line
                ! Molcas prints with an 80 character limit
                ! so this calculates how many entries will be on the final line
                ! and then reads in the data
                IF(mod(basis,10) == 1) READ(30,'(A2,1A1)', IOSTAT = status) Cdummy,index_list(basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 2) READ(30,'(A2,2A1)', IOSTAT = status) Cdummy,(index_list(col), col = basis-1,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 3) READ(30,'(A2,3A1)', IOSTAT = status) Cdummy,(index_list(col), col = basis-2,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 4) READ(30,'(A2,4A1)', IOSTAT = status) Cdummy,(index_list(col), col = basis-3,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 5) READ(30,'(A2,5A1)', IOSTAT = status) Cdummy,(index_list(col), col = basis-4,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 6) READ(30,'(A2,6A1)', IOSTAT = status) Cdummy,(index_list(col), col = basis-5,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 7) READ(30,'(A2,7A1)', IOSTAT = status) Cdummy,(index_list(col), col = basis-6,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 8) READ(30,'(A2,8A1)', IOSTAT = status) Cdummy,(index_list(col), col = basis-7,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                IF(mod(basis,10) == 9) READ(30,'(A2,9A1)', IOSTAT = status) Cdummy,(index_list(col), col = basis-8,basis)
                CALL CHECK_IO(status, TRIM(ADJUSTL(input_file)))
                
                !Status update
                WRITE(6,'(A)') '...done'
            END IF
        END DO
        60 CONTINUE
    ! Close RasOrb file
    CLOSE(30)

    ! Check basis file exists
    CALL CHECK_EXISTS('basis')

    !Open basis file - generated by molcas_basis - usually automatically run 
    ! in submision script made by openmolcas_gen
    OPEN(30,FILE = 'basis', STATUS = 'UNKNOWN')
        ! Status update
        WRITE(6,'(A)') 'Reading BASIS...'
        
        ! Loop over file and read in atomic orbital labels

        ! Counters
        j = 0
        wrote = 0

        DO i = 1,basis
            READ(30,'(A)',IOSTAT = status) line
            line = ADJUSTL(line)
            CALL CHECK_IO(status, 'basis')
            j = j+1
            labels(i) = line(1:11)
        END DO

        ! Status update
        WRITE(6,'(A)') '...done'

        ! Loop over all atomic orbitals starting at the second one and 
        ! count how many different orbitals there are
        num_orb_list = 1

        !Set first atomic orbital name as first element of label array
        orb_list(1) = labels(1)(1:9)
        DO j = 2,basis

            ! Variable used to mark double counting
            found = 0

            ! Check for double counting of current atomic orbital
            ! If so, skip this orbital
            DO i = 1,num_orb_list
                IF(labels(j)(1:9) == orb_list(i)) THEN
                    found = 1
                    EXIT
                END IF
            END DO

            ! If orbital isnt being doubly counted then add one to total
            ! and add orbital name to the list
            IF(found == 0) THEN
                num_orb_list = num_orb_list + 1
                orb_list(num_orb_list) = labels(j)(1:9)
            END IF
        END DO

    ! Cut down orb_list to correct size - removes garbage elements at end
    ALLOCATE(char_temp(num_orb_list))
    char_temp = orb_list(:num_orb_list)
    DEALLOCATE(orb_list)
    ALLOCATE(orb_list(num_orb_list))
    orb_list = char_temp
    DEALLOCATE(char_temp)
    ! Close basis file
    CLOSE(30)
        
    ! Check overlap file exists
    CALL CHECK_EXISTS('overlap')

    ! Open overlap file
    OPEN(30,FILE = 'overlap', STATUS = 'OLD')
        ! Status update
        WRITE(6,'(A)') 'Reading overlap matrix...'

            ! Set overlap matrix to zero
            S_mat = 0.0_WP

            ! Create format identifier for overlap matrix
            WRITE(FMT,'(I10,A10)') basis,'E25.15E3'
            FMT = adjustl(FMT)

            ! Loop over overlap file and read in the overlap matrix
            DO i = 1,basis
                READ(30,'('//trim(FMT)//')', IOSTAT = status) (S_mat(i,col), col = 1, basis)
                CALL CHECK_IO(status, 'overlap')
            END DO

        ! Status update
        WRITE(6,'(A)') '...done'

    ! Close overlap file
    CLOSE(30)

    ! Status update
    WRITE(6,'(A)') 'Orthogonalising MOs as (S^1/2).C ...'

    ! Orbitals are orthonormal as:
    ! $C^T \cdot S^{1/2} \cdot S^{1/2} \cdot C$
    ! Where S is the overlap matrix in the AO basis, and C
    ! is a vector of coefficients specifying the MO expansion
    ! $\psi = \sum_{i}c_{i} \phi_{i}$
    
    ! Calculate S^1/2
    ! Get Eigenvectors and Eigenvalues of S
    CALL DIAGONALISE('U',S_mat,V_mat,eigs)

    ! Set up array for S^1/2
    rootS_mat = 0.0_WP
    ! Set S^1/2 as sqrt of S in eigenbasis
    DO j = 1,basis
        rootS_mat(j,j) = sqrt(eigs(j))
    END DO

    ! Convert S^1/2 back to basis of AOs
    rootS_mat = matmul(matmul(V_mat,rootS_mat),transpose(V_mat))

    ! Use S^(1/2) on C to give a set of orthonormal MOs
    C_mat = transpose(coeffs)
    mat = matmul(rootS_mat,C_mat)

    ! Calculate percentage of each AO in each MO
    DO j = 1,basis
        DO i = 1,basis
            percents(j,i) = 100.0_WP*mat(i,j)*mat(i,j)
        END DO
    END DO

    ! If raw wavefunction values are requested then print to screen
    IF(trim(type) == 'wf') THEN

        !Allocate array used to reorder AO coefficients
        ALLOCATE(order(basis,2))


        DO j = 1,basis

            !Reorder AO raw contributions for current MO to be high (top) to low (bottom)
            CALL bubble(percents(j,:),order,'max')

            !Print header for MO to screen
            WRITE(6,'(A,I3,A,F4.2,A,E12.4E2,A,A,A)') '---- MO ',j,', occ = ',occ(j),', energy = ',energy(j),', index = ',index_list(j),' ----'
            
            !Print coefficients to screen
            DO i = 1,basis
                IF(percents(j,i) >= thresh) THEN
                    WRITE(6,'(A14,F12.3,E21.7E2)') labels(order(i,2)),percents(j,i),coeffs(j,order(i,2))
                ELSE
                    EXIT
                END IF
            END DO
        END DO
        wrote = 1

    ! Else if percentage contributions to each MO are requested then print to screen
    ELSE IF(trim(type) == '%') THEN

        !Allocate array used to reorder percentage AO contributions
        ALLOCATE(order(num_orb_list,2))

        ALLOCATE(orb_percents(num_orb_list))
        DO j = 1,basis
            orb_percents = 0.0_WP
            
            !Print header for MO to screen
            WRITE(6,'(A,I3,A,F4.2,A,E12.4E2,A,A,A)') '---- MO ',j,'    occ = ',occ(j),'    energy = ',energy(j),'    index = ',index_list(j),' ----'
            
            !Calculate percentage AO contributions for current MO
            ! Adds contributions for orbitals of the same atom with same n, l but different ml
            ! i.e. Ce_2px + Ce_2py + Ce_2pz 
            DO i = 1,basis
                DO k = 1,num_orb_list
                    IF(labels(i)(1:9) == orb_list(k)) THEN
                        orb_percents(k) = orb_percents(k) + percents(j,i)
                    END IF
                END DO
            END DO


            !Reorder percentage AO contributions for current MO to be high (top) to low (bottom)
            !and return an additional list specifying the new order
            CALL bubble(orb_percents,order,'max')

            !Print percentages to screen
            DO k = 1,num_orb_list
                IF(orb_percents(k) >= thresh) WRITE(6,'(A10,F8.3, I6)') orb_list(order(k,2)),orb_percents(k)
            END DO
        END DO
        wrote = 1
        DEALLOCATE(orb_percents)
    END IF

    ! Catch errors with basis size - I dont think this will work JK
    IF(wrote == 0) WRITE(6,'(A, I0, A, I0, A)') 'Basis expected =',basis,'; Basis found =',j,' or other error'

    !Deallocate arrays
    DEALLOCATE(coeffs,occ,energy,index_list,percents,order,labels,orb_list,C_mat,S_mat,mat,rootS_mat)
    
CONTAINS

SUBROUTINE READ_USER_INPUT(input_file, type, thresh)
    USE version, ONLY : PRINT_VERSION
    IMPLICIT NONE
    REAL(KIND = WP)                 :: thresh
    CHARACTER(LEN = 500)            :: line
    CHARACTER(LEN = *), INTENT(OUT) :: input_file, type
    LOGICAL                         :: file_exists

    CALL GET_COMMAND_ARGUMENT(1,input_file)

    !Print help
    IF(TRIM(input_file) == '' .OR. TRIM(input_file) == '-h' .OR. TRIM(input_file) == '-H') THEN
        WRITE(6,'(A)') 'openmolcas_orbs <orbital_file> [<type=%/wf> <threshold>]'
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') 'Extracts orbital information from molcas RasOrb file and prints to screen'
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') 'orbital_file      : CHARACTER          RasOrb file                                                        e.g  1.RasOrb'
        WRITE(6,'(A)') 'type              * OPTIONAL CHARACTER print % AO contributions or the raw values of the AO coefficients  e.g  % or wf'
        WRITE(6,'(A)') 'threshold         * OPTIONAL NUMBER    threshold for printing orbital contributions (default 1%)          e.g  0.5'
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') 'Example ------ openmolcas_orbs 1.RasOrb % 0.5'
        WRITE(6,'(A)') ''
        STOP
    END IF

    ! Print version number
    IF (TRIM(input_file) == '-v' .OR. TRIM(input_file) == '-version' .OR. TRIM(input_file) == 'version') THEN
        CALL PRINT_VERSION('openmolcas_orbs')
        STOP
    END IF

    !Check if specified RasOrb file exists
    CALL CHECK_EXISTS(input_file)

    CALL GET_COMMAND_ARGUMENT(2,type)
    !Check IF type is correct
    IF(TRIM(type) /= '') THEN
        IF(TRIM(type) /= '%' .and. TRIM(type) /= 'wf') THEN
            WRITE(6,'(A)') 'TYPE must be % or wf'
            STOP
        END IF
    ELSE
        type = '%'
    END IF

    !Check IF thresh provided
    CALL GET_COMMAND_ARGUMENT(3,line)
    IF(TRIM(line) /= '') THEN
        READ(line,*,IOSTAT=IOstatus) thresh
        IF(IOstatus /= 0) thresh = 1.0_WP
    ELSE
        thresh = 1.0_WP
    END IF

END SUBROUTINE READ_USER_INPUT

SUBROUTINE CHECK_EXISTS(file_name)
    ! Checks if specified file exists
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: file_name
    LOGICAL                        :: file_exists

    INQUIRE(FILE=TRIM(file_name), EXIST=file_exists)
    IF (file_exists .EQV. .FALSE.) THEN
        WRITE(6,'(A, A, A)') 'File "', TRIM(file_name),'" cannot be found in the current directory'
        STOP
    END IF

END SUBROUTINE CHECK_EXISTS

SUBROUTINE CHECK_IO(IOREPORT, file_name)
    ! Checks IOREPORT returned by IOSTAT argument in READ statement
    IMPLICIT NONE
    INTEGER, INTENT(IN)               :: IOREPORT
    CHARACTER(LEN = *), INTENT(IN)    :: file_name

    ! Check file isnt corrupted
        IF (IOREPORT > 0)  THEN
            WRITE(6,'(A, A, A)') '!!!!                         Error reading ',file_name, ' file                              !!!!'
            STOP
    ! Check file doesnt end too soon
        ELSE IF (IOREPORT < 0) THEN
            WRITE(6,'(A, A, A)') '!!!!               Error reached end of file ',file_name, ' too soon                        !!!!'
            STOP
        END IF

END SUBROUTINE CHECK_IO


END PROGRAM openmolcas_orbs