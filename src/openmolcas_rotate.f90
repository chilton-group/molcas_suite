!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   openmolcas_rotate.f90 
!
!   This program is a part of molcas_suite
!
!
!   Rotates orbitals in and out of the different spaces in a 
!   CASSCF calculation and prints to a new Orb file.
!   x, y, and z indicate the indices of the orbitals you want 
!   to move c indicates the orb_type of orbital you want to 
!   change your selected orbital into.
!
!   
!      Authors:
!       Nicholas Chilton
!       Commented by Jon Kragskow
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


PROGRAM OPENMOLCAS_ROTATE
    IMPLICIT NONE

    CHARACTER(LEN = 500)           :: input_file, output_file, line, orblist
    INTEGER                        :: basis, name_len, count, switches, i, orb, j, k
    CHARACTER(LEN = 1),ALLOCATABLE :: orb_type(:)
    CHARACTER(LEN = 2)             :: Cdummy

    !Read user input
    CALL READ_USER_INPUT(input_file, orblist)

    !WRITE(6,'(A)') TRIM(adjustl(orblist))
    !WRITE(6,'(A)') '12345678901234567890'

    !Create name for output file by appending ModOrb after .
    name_len = LEN_TRIM(input_file)-6
    output_file = input_file(1:name_len)//'ModOrb'

    !Open input and output files
    OPEN(30,file=trim(adjustl(input_file)))
    OPEN(31,file=trim(adjustl(output_file)))

    !Read input file
    READ(30,'(A)',END=60) line
    WRITE(31,'(A)') TRIM(line)
    READ(30,'(A)',END=60) line
    WRITE(31,'(A)') TRIM(line)
    READ(30,'(A)',END=60) line
    WRITE(31,'(A)') TRIM(line)
    READ(30,'(A)',END=60) line
    WRITE(31,'(A)') TRIM(line)
    READ(30,'(A)',END=60) line
    WRITE(31,'(A)') TRIM(line)
    READ(line,*) basis
    ALLOCATE(orb_type(basis))

    !Read file and move orbitals
    DO while(.true.)
        READ(30,'(A)',END=60) line
        IF(line(1:6) == '#INDEX') THEN
            WRITE(31,'(A)') TRIM(line)
            READ(30,'(A)',END=60) line
            WRITE(31,'(A)') TRIM(line)
            DO i = 1,basis/10
                READ(30,'(A2,10A1)') Cdummy,orb_type(i*10-9:i*10)
            END DO
            IF(mod(basis,10) == 1) READ(30,'(A2,1A1)') Cdummy,orb_type(basis)
            IF(mod(basis,10) == 2) READ(30,'(A2,2A1)') Cdummy,orb_type(basis-1:basis)
            IF(mod(basis,10) == 3) READ(30,'(A2,3A1)') Cdummy,orb_type(basis-2:basis)
            IF(mod(basis,10) == 4) READ(30,'(A2,4A1)') Cdummy,orb_type(basis-3:basis)
            IF(mod(basis,10) == 5) READ(30,'(A2,5A1)') Cdummy,orb_type(basis-4:basis)
            IF(mod(basis,10) == 6) READ(30,'(A2,6A1)') Cdummy,orb_type(basis-5:basis)
            IF(mod(basis,10) == 7) READ(30,'(A2,7A1)') Cdummy,orb_type(basis-6:basis)
            IF(mod(basis,10) == 8) READ(30,'(A2,8A1)') Cdummy,orb_type(basis-7:basis)
            IF(mod(basis,10) == 9) READ(30,'(A2,9A1)') Cdummy,orb_type(basis-8:basis)
            count = 0
            DO i = 1,len_trim(orblist)
                IF(orblist(i:i) == ',') count = count + 1
            END DO
            count = count + 1
            j = 1
            i = 0
            DO k = 1,count
                !WRITE(6,*) '---'
                i = index(TRIM(orblist(j:)),'>') + j - 1
                !WRITE(6,*) j,i
                READ(orblist(j+1:i-1),*) orb
                IF(k /= count) THEN
                    j = index(orblist(i:),',') + i - 1
                ELSE
                    j = len_trim(orblist)
                END IF
                !WRITE(6,*) i,j
                !WRITE(6,*) orb,'goes to ',orblist(i+1:j-1)
                orb_type(orb) = orblist(i+1:j-1)
            END DO
            DO i = 0,basis/10-1
                WRITE(31,'(I1,A1,10A1)') mod(i,10),' ',orb_type((i+1)*10-9:(i+1)*10)
            END DO
            IF(mod(basis,10) == 1) WRITE(31,'(I1,A1,1A1)') mod(i,10),' ',orb_type(basis)
            IF(mod(basis,10) == 2) WRITE(31,'(I1,A1,2A1)') mod(i,10),' ',orb_type(basis-1:basis)
            IF(mod(basis,10) == 3) WRITE(31,'(I1,A1,3A1)') mod(i,10),' ',orb_type(basis-2:basis)
            IF(mod(basis,10) == 4) WRITE(31,'(I1,A1,4A1)') mod(i,10),' ',orb_type(basis-3:basis)
            IF(mod(basis,10) == 5) WRITE(31,'(I1,A1,5A1)') mod(i,10),' ',orb_type(basis-4:basis)
            IF(mod(basis,10) == 6) WRITE(31,'(I1,A1,6A1)') mod(i,10),' ',orb_type(basis-5:basis)
            IF(mod(basis,10) == 7) WRITE(31,'(I1,A1,7A1)') mod(i,10),' ',orb_type(basis-6:basis)
            IF(mod(basis,10) == 8) WRITE(31,'(I1,A1,8A1)') mod(i,10),' ',orb_type(basis-7:basis)
            IF(mod(basis,10) == 9) WRITE(31,'(I1,A1,9A1)') mod(i,10),' ',orb_type(basis-8:basis)
        else
            WRITE(31,'(A)') TRIM(line)
        END IF
    END DO
    60 CONTINUE
    CLOSE(30)
    CLOSE(31)
    DEALLOCATE(orb_type)

    WRITE(6,'(A, A)') 'Rotated orbitals have been written to ', TRIM(output_file)

    CONTAINS


    SUBROUTINE READ_USER_INPUT(input_file, orblist)
        USE version, only : PRINT_VERSION
        IMPLICIT NONE
        CHARACTER(LEN = *) :: input_file, orblist
        LOGICAL            :: file_exists

        CALL GET_COMMAND_ARGUMENT(1,input_file)

        !Write help instructions
        IF (TRIM(input_file) == '' .OR. TRIM(input_file) == '-h' .OR. TRIM(input_file) == '-H') THEN
            WRITE(6,'(A)') 'openmolcas_rotate <file> <replacement>'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Rotates orbitals in and out of the different spaces in a CASSCF '// &
                           'calculation and prints to a new Orb file'
            WRITE(6,'(A)')   'x, y, and z indicate the indices of the orbitals you want to move'
            WRITE(6,'(A)')   'c indicates the orb_type of orbital you want to change your '// &
                             'selected orbital into.'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'file         : CHARACTER   RasOrb file from calculation          '// &
                           '         e.g  1.RasOrb'
            WRITE(6,'(A)') 'replacement  : CHARACTER   String which describes your orbital '// &
                            ' rotation - of the form "x>c,y>c,z>c" - where c = i,1,2,3,s'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Example replacement string --- "1>i, 2>s, 5>2" will move orb 1 to '// &
                           'inactive, 2 to secondary, and 5 to ras2'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Example ------ molcas_rotate 1.RasOrb "1>i, 2>s, 5>2"'
            WRITE(6,'(A)') ''
            STOP
        END IF

        ! Print version number
        IF (TRIM(input_file) == '-v' .OR. TRIM(input_file) == '-version' .OR. TRIM(input_file) == 'version') THEN
            CALL PRINT_VERSION('openmolcas_rotate')
            STOP
        END IF

        !Check file exists
        INQUIRE(FILE=input_file, EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(A)') 'Specified file does not exist'
            STOP
        END IF

        CALL GET_COMMAND_ARGUMENT(2,orblist)

    orblist = '('//trim(adjustl(orblist))//')'


    END SUBROUTINE READ_USER_INPUT

END PROGRAM OPENMOLCAS_ROTATE
