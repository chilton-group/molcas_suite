!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


program molcas_identity
use version, only : PRINT_VERSION
implicit none

character(len=500)::arg1,arg2
integer::i,j,k,h,num_basis,num_electrons,len_contraction,num_L,prev,num_mad
integer,allocatable::Lvals(:),Lcount(:),madelung(:,:),order(:)
real(kind=8),allocatable::vector(:),realmad(:)


call get_command_argument(1,arg1)
if(trim(arg1) == '-h' .or. trim(arg1) == '') then
    write(6,*) 'openmolcas_identity <contraction> <num_electrons>'
    stop
end if

! Print version number
IF (TRIM(arg1) == '-v' .OR. TRIM(arg1) == '-version' .OR. TRIM(arg1) == 'version') THEN
    CALL PRINT_VERSION('openmolcas_identity')
    STOP
END IF

len_contraction = len_trim(arg1)
num_L = 0
do i = 1, len_contraction
    if((iachar(arg1(i:i)) >= 65 .and. iachar(arg1(i:i)) <= 90) .or. (iachar(arg1(i:i)) >= 97 .and. iachar(arg1(i:i)) <= 122)) num_L = num_L + 1
end do
allocate(Lvals(num_L),Lcount(num_L))
num_L = 0
prev = 0
do i = 1, len_contraction
    if((iachar(arg1(i:i)) >= 65 .and. iachar(arg1(i:i)) <= 90) .or. (iachar(arg1(i:i)) >= 97 .and. iachar(arg1(i:i)) <= 122)) then
        num_L = num_L + 1
        if(arg1(i:i) == 'S' .or. arg1(i:i) == 's') Lvals(num_L) = 0
        if(arg1(i:i) == 'P' .or. arg1(i:i) == 'p') Lvals(num_L) = 1
        if(arg1(i:i) == 'D' .or. arg1(i:i) == 'd') Lvals(num_L) = 2
        if(arg1(i:i) == 'F' .or. arg1(i:i) == 'f') Lvals(num_L) = 3
        if(arg1(i:i) == 'G' .or. arg1(i:i) == 'g') Lvals(num_L) = 4
        if(arg1(i:i) == 'H' .or. arg1(i:i) == 'h') Lvals(num_L) = 5
        if(arg1(i:i) == 'I' .or. arg1(i:i) == 'i') Lvals(num_L) = 6
        read(arg1(prev+1:i-1),*) Lcount(num_L)
        prev = i
    end if
end do
num_basis = 0
write(6,*) trim(arg1)
do i = 1,num_L
    num_basis = num_basis + Lcount(i)*(2*Lvals(i)+1)
    write(6,*) Lcount(i),Lvals(i)
end do
write(6,*) num_basis
num_mad = sum(Lcount)
allocate(vector(num_basis),madelung(num_basis,3),order(num_basis),realmad(num_basis))
k = 0
do i = 1,num_L
    do h = -Lvals(i),Lvals(i),1
        do j = 1,Lcount(i)
            k = k + 1
            madelung(k,1) = j+Lvals(i)
            madelung(k,2) = Lvals(i)
            madelung(k,3) = h
            realmad(k) = madelung(k,1)+madelung(k,2)+0.01_8*madelung(k,1)
            !write(6,'(I2)',advance='no') madelung(k,1)
            !if(madelung(k,2) == 0) write(6,'(A1)',advance='no') 's'
            !if(madelung(k,2) == 1) write(6,'(A1)',advance='no') 'p'
            !if(madelung(k,2) == 2) write(6,'(A1)',advance='no') 'd'
            !if(madelung(k,2) == 3) write(6,'(A1)',advance='no') 'f'
            !if(madelung(k,2) == 4) write(6,'(A1)',advance='no') 'g'
            !if(madelung(k,2) == 5) write(6,'(A1)',advance='no') 'h'
            !if(madelung(k,2) == 6) write(6,'(A1)',advance='no') 'i'
            !write(6,'(I3)',advance='no') madelung(k,3)
            !write(6,'(F8.2)') realmad(k)
        end do
    end do
end do
call re_order(num_basis,realmad,order,'min')
! do i = 1,num_basis
    ! write(6,'(I2)',advance='no') madelung(order(i),1)
    ! if(madelung(order(i),2) == 0) write(6,'(A1)',advance='no') 's'
    ! if(madelung(order(i),2) == 1) write(6,'(A1)',advance='no') 'p'
    ! if(madelung(order(i),2) == 2) write(6,'(A1)',advance='no') 'd'
    ! if(madelung(order(i),2) == 3) write(6,'(A1)',advance='no') 'f'
    ! if(madelung(order(i),2) == 4) write(6,'(A1)',advance='no') 'g'
    ! if(madelung(order(i),2) == 5) write(6,'(A1)',advance='no') 'h'
    ! if(madelung(order(i),2) == 6) write(6,'(A1)',advance='no') 'i'
    ! write(6,'(I3)',advance='no') madelung(order(i),3)
    ! write(6,'(I5)') order(i)
! end do
call get_command_argument(2,arg2)
read(arg2,*) num_electrons
open(33,file='identity.IdnOrb',status='unknown')
write(33,'(A)') "#INPORB 2.2"
write(33,'(A)') "#INFO"
write(33,'(A)') "* IDENTITY ORBITALS"
write(33,'(A)') "       0       1       0"
write(33,'(I8)') num_basis
write(33,'(I8)') num_basis
write(33,'(A)') "*BC:HOST cerberus PID 4444 DATE Tue Nov 21 04:44:44 1989"
write(33,'(A)') "#EXTRAS"
write(33,'(A)') "* ACTIVE TWO-EL ENERGY"
write(33,'(A)') "0.000000000000E+00"
write(33,'(A)') "#ORB"
vector = 0.0_8
do i = 1,num_basis
    vector(order(i)) = 1.0_8
    write(33,'(A,I5)') "* ORBITAL    1",i
    write(33,'(5E22.14E2)') vector
    vector(order(i)) = 0.0_8
end do
write(33,'(A)') "#OCC"
write(33,'(A)') "* OCCUPATION NUMBERS"
vector = 0.0_8
vector(1:num_electrons/2) = 2.0_8
if((num_electrons/2)*2 /= num_electrons) vector(num_electrons/2+1) = 1.0_8
write(33,'(5E22.14E2)') vector
write(33,'(A)') "#OCHR"
write(33,'(A)') "* OCCUPATION NUMBERS (HUMAN-READABLE)"
write(33,'(10F8.4)') vector

write(33,'(A)') "#ONE"
write(33,'(A)') "* ONE ELECTRON ENERGIES"
vector = 0.0_8
write(33,'(10E12.4E2)') vector
write(33,'(A)') "#INDEX"
write(33,'(A)') "* 1234567890"
i = 0
j = 0
do while(i < num_electrons/2)
    i = i + 1
    if(mod(i,10) == 1) then
        write(33,'(I1,A2)',advance='no') mod(j,10)," i"
        j = j + 1
    else if(mod(i,10) > 0) then
        write(33,'(A1)',advance='no') "i"
    else if(mod(i,10) == 0) then
        write(33,'(A1)',advance='yes') "i"
    end if
end do
if((num_electrons/2)*2 /= num_electrons) then
    i = i + 1
    if(mod(i,10) == 1) then
        write(33,'(I1,A2)',advance='no') mod(j,10)," 2"
        j = j + 1
    else if(mod(i,10) > 0) then
        write(33,'(A1)',advance='no') "2"
    else if(mod(i,10) == 0) then
        write(33,'(A1)',advance='yes') "2"
    end if
end if
do while(i < num_basis)
    i = i + 1
    if(mod(i,10) == 1) then
        write(33,'(I1,A2)',advance='no') mod(j,10)," s"
        j = j + 1
    else if(mod(i,10) > 0) then
        write(33,'(A1)',advance='no') "s"
    else if(mod(i,10) == 0) then
        write(33,'(A1)',advance='yes') "s"
    end if
end do
close(33)
deallocate(vector,Lvals,Lcount,madelung,order,realmad)

contains

recursive subroutine re_order(num,values,list,way)
! returns ordered list from input
implicit none
integer,intent(in)::num
integer,intent(out),dimension(:)::list(:)
integer::j,k
real(kind=8),intent(in),dimension(:)::values(:)
real(kind=8)::low
integer,allocatable::done(:)
character(len=3),intent(in)::way
allocate(done(num))
list(:) = 0
done(:) = 0
list(1) = minloc(values,1)
done(list(1)) = 1
if(any(values /= values)) then
    write(6,*) "NaN or Infinity on entry to re_order"
    stop
end if
do j=2,num
    low = 1D99
    do k=1,num
        if((values(k) - values(list(1))) >= (values(list(j-1)) - values(list(1))) .and. (values(k) - values(list(1))) < low .and. done(k) == 0) then
            low = (values(k) - values(list(1)))
            list(j) = k
        end if
    end do
    done(list(j)) = 1
end do
if(way == 'max') then
    do j = 1,num
        done(j) = list(num-j+1)
    end do
    list = done
end if
deallocate(done)
end subroutine re_order
end program molcas_identity