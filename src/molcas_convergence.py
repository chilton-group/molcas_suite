#! /usr/bin/env python

#┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
#││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
#┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


#############################################################
#
#
#   molcas_convergence.py
#
#   This program is a part of molcas_suite
#
#   Plots SCF energies for each iteration of a casscf 
#   calcualtion
#
#   
#      Author:
#       Jon Kragskow
#
#
#############################################################

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
import matplotlib.ticker as mtick
from matplotlib.ticker import ScalarFormatter, MaxNLocator

def read_in_command_line():
    #!!! Read in command line arguments

    parser = argparse.ArgumentParser(description='Plots energies from molcas CASSCF.')
    parser.add_argument('output_file', type = str, metavar ='molcas output file',
                        help = 'Name for saved plot')
    parser.add_argument('--save', action='store_true',
                        help = 'save figure window instead of showing plot.')
    parser.add_argument('--from_step', type = int,
                        help = 'plot energy changes and energies from specified step')
    parser.add_argument('--separate', action = 'store_true',
                        help = 'plot energy changes and energies on separate graphs')
    args = parser.parse_args()
    
    molcas_output = args.output_file

    if args.from_step == None:
        from_step = 1
    else:
        from_step = args.from_step

    if os.path.isfile(molcas_output) == False:
        print('File not found!')
        exit()



    return molcas_output, args.save, from_step, args.separate

def read_file(molcas_output):
    #!!! Read in molcas output file and extract convergence energies

    energies = []
    diffs = []

    with open(molcas_output,'r') as f:
        for line in f:
            if line.find('This run of MOLCAS is using the pymolcas driver') != -1:
                open_molcas = True
            else:
                open_molcas = False
            if line.find('Total energies have been shifted') != -1:
                line = next(f)
                for i in range(10000):

                        #If convergence occurs or fails then read final line and return
                        if line.find('Convergence after') != -1 or line.find('No convergence after') != -1:
                            #read 1 more line and then exit
                            line = next(f)
                            energies.append(float(line.split()[4]))
                            if line.split()[5][-1] == '*':
                                nostar = line.split()[5][:-1]
                            else:
                                nostar = line.split()[5]
                            diffs.append(float(nostar))
                            return np.asarray(energies), np.asarray(diffs)

                        #If warnings are encountered, skip over the warning box - .e.g large orbital rotation
                        elif line.find('###############################################################################') != -1:
                            #read 1 more line and then exit
                            for j in range(10):
                                line = next(f)

                        #If blank line then skip over
                        elif len(line.split()) == 0:
                            line=next(f)

                        #= marked line then check for divergence warning
                        elif line.find('========================================================================================================================') != -1:
                            for j in range(5):
                                line=next(f)
                            if line.find('ERROR: Rasscf energy diverges.') != -1:
                                print('Energies have diverged')
                                return np.asarray(energies), np.asarray(diffs)

                        #Otherwise read the file as normal
                        else: 
                            #read energies
                            energies.append(float(line.split()[4]))
                            if line.split()[5][-1] == '*':
                                nostar = line.split()[5][:-1]
                            else:
                                nostar = line.split()[5]
                            diffs.append(float(nostar))
                            #Try catches output files which terminate abruptly - e.g. csf killed the job
                            try:
                                line = next(f)
                            except StopIteration:
                                return np.asarray(energies), np.asarray(diffs)

def plot_data(ax1, ax2, energies, diffs, from_step):
    #!!! Plots data - convergence energies and energy changes
    iterations = np.arange(1,np.size(energies)+1)

    #Draw energy trace
    plot1 = ax1.plot(iterations[from_step-1:], energies[from_step-1:], linewidth = 2, color = 'blue')

    #Draw energy differences trace
    plot2 = ax2.plot(iterations[from_step-1:], diffs[from_step-1:], linewidth = 2, color = 'red')

    return plot1, plot2

def adjust_plot(ax, separate):
    #!!! Function to adjust plot options - axis, labels, style etc.

    if ax == ax1:
        #Set x axis options
        ax.set_xlabel(r' Iteration number ')
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.tick_params(axis = 'both', which = 'both', length = 2.0)
    
        #Set y axis options for cm-1
        ax.set_ylabel(r' Convergence energy / a.u.')
        ax.yaxis.label.set_color('blue')


    if ax == ax2:
        #Set x axis options
        ax.tick_params(axis = 'both', which = 'both', length = 2.0)

        #Set y axis options for cm-1
        ax.set_ylabel(r' Energy shift / a.u.')
        ax.yaxis.label.set_color('red')

    if separate == True:

        #Remove right and top parts of plot box
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.set_xlabel(r' Iteration number ')
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))



    return

#---------------------------------------------------------------------------------#

if __name__ == '__main__':

    #Read in command line
    molcas_output, save, from_step, separate = read_in_command_line()
    
    #Read in data
    energies, diffs = read_file(molcas_output)
    
    #Change plot font size - needs to be done before plot is created
    plt.rcParams.update({'font.size': 12})
    
    #Create plot and axes
    if separate == True:
        fig, (ax1, ax2) = plt.subplots(1, 2, sharex='all', sharey=False, figsize=(12,6), num='RASSCF energies of '+molcas_output)
    else:
        fig, (ax1) = plt.subplots(1, 1, sharex='all', sharey=False, figsize=(8,6), num='RASSCF energies of '+molcas_output)
        ax2 = ax1.twinx()
    
    #Plot data
    plot1, plot2 = plot_data(ax1, ax2, energies, diffs, from_step)
    
    #Adjust plot axes
    adjust_plot(ax1, separate)
    adjust_plot(ax2, separate)
    
    fig.tight_layout()
    
    #Show or save plot
    
    if save == False:
        plt.show()
    elif save == True:
        plot_name = molcas_output+'_converged.svg'
        fig.savefig(plot_name, dpi=fig.dpi) 