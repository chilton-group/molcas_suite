#! /usr/bin/env python3


#┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
#││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
#┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘

#############################################################
#
#
#   molcas_barrier.py
#
#   This program is a part of molcas_suite
#
#   Plots barrier figures using an (open)molcas output file
#
#   Transition intensities are computed using the magnetic
#    transition moment operator
#   
#      Author:
#       Jon Kragskow
#
#
#############################################################

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import argparse
from numpy import linalg as la
import os
import sys
import copy

mpl.rcParams["savefig.directory"] = ""


def read_user_args():
    """
    Read in user arguments from command line

    Positional arguments:
        None

    Keyword arguments:
        None

    Returns:
        user_args (argparse parser object) : Object containing the parsed command line arguments
    """

    parser = argparse.ArgumentParser(
        description="""
        Creates barrier figure of lowest lying J multiplet.
        Transition probabilities are given by magnetic moment operator.
        Note a small quantisation field is applied along the z axis to give
        correct <Jz> values
        """
    )
    parser.add_argument("output_file", type=str, metavar="<output_file>",
                        help="Molcas output file")
    parser.add_argument("--save", type=str, metavar="<png/svg/pdf>", choices=["svg","pdf","png"],
                        help="Save figure with given format", default="pdf")
    parser.add_argument("--colour", type=str, metavar="<rgb hex code without # or the words red/green/blue>",
                        default="red",
                        help="Colour of transition arrows e.g. --colour 000066 or --colour green")
    user_args = parser.parse_args()

    # Check colour option - if none then set to red
    if user_args.colour.lower() == 'red':

        user_args.colour = 'ff0000'

    elif user_args.colour.lower() == 'green':

        user_args.colour = '00ff00'

    elif user_args.colour.lower() == 'blue':

        user_args.colour = '0000ff'

    return user_args


def read_molcas_file(f_name):
    """
    Read in molcas output file and extract info

    Positional arguments:
        f_name (str) :: Name of output file

    Keyword arguments:
        None

    Returns:
        CFPs (list)    :: Stevens crystal field parameters up to k=6
        n_elec (int)   :: Number of electrons in active space
        n_orbs (int)   :: Number of orbitals in active space
        L (float)      :: L quantum number
        S (float)      :: S quantum number
        J (float)      :: J quantum number
    """

    # Check file exists
    if not os.path.isfile(f_name):
        print('File not found!')
        sys.exit()

    # Flags for job completion, molcas_version, and flags for 
    # successful location of number of electrons and orbitals
    # in the active space, and CFPs
    finished = False
    open_molcas = False
    found_orbs = False
    found_elec = False
    found_params = False

    # Find happy landing and pymolcas driver lines
    with open(f_name, 'r') as f:
        for line in f:
            if 'This run of MOLCAS is using the pymolcas driver' in line:
                open_molcas = True
            if 'Happy landing!' in line:
                finished = True

    # Ask user if they want to continue if happy landing is missing
    if not finished and not open_molcas:
        print('Molcas has not terminated properly')
        answer = input('Do you want to continue? (Y/N)')
        if answer != 'Y' and answer != 'N':
            answer = input('Do you want to continue? (Y/N)')
        if answer != 'Y':
            exit()
    f.close()

    # Check how many single_aniso sections exist in the file
    # If more than one, ask user which they would like to use
    with open(f_name, 'r') as f:
        single_aniso = 0
        for line in f:
            if 'Start Module: single_aniso' in line:
                single_aniso += 1
        if single_aniso == 0:
            print('Error: no SINGLE_ANISO modules found in specified file')
            sys.exit()

        if single_aniso >= 2:
            section_sinani = input(
                'There are {} single_aniso sections. Which section do you want to use? \n'.format(single_aniso))
            section_sinani = int(section_sinani)

            # Check for stupid input
            if section_sinani > single_aniso:
                print('Error there are not that many SINGLE_ANISO modules present')
                sys.exit()
        else:
            section_sinani = 1

    f.close()

    # Begin reading molcas output file
    single_aniso = 0
    CFPs = []

    with open(f_name, 'r') as f:
        for line in f:

            # Read number of electrons in active space 
            if 'Nactel' in line:
                if open_molcas:
                    # n_elec = int(line.split()[1])
                    n_elec = int(line.split('=')[1].split()[0])
                else:
                    line = next(f)
                    n_elec = int(line.split()[0])
                found_elec = True

            # Read number of active orbitals
            if 'Ras2' in line:
                if open_molcas:
                    # n_orbs = int(line.split()[1])
                    n_orbs = int(line.split('=')[1].split()[0])
                else:
                    line = next(f)
                    n_orbs = int(line.split()[0])
                found_orbs = True

            # Calculate J and limit number of CFPs if necessary
            if found_orbs and found_elec:
                L, S, J = ang_mom_nums(n_elec, n_orbs)

                if int(2 * J) >= 6:
                    num_CFP_lines = 30
                else:
                    num_CFP_lines = 17

            # Read crystal field parameters
            if 'Start Module: single_aniso' in line :
                single_aniso += 1

            if "Hcf = SUM_{k,q} * [ B(k,q) * O(k,q) ];" in line \
                and single_aniso == section_sinani \
                    and found_orbs and found_elec:
                if not found_params:
                    found_params = True
                    # Skip over garbage header
                    # extra skip is for cases where CF terms
                    # with k > 6 are required
                    # in that case two more lines are printed by molcas
                    for i in range(10):
                        line = next(f)
                    if 'J. Chem. Phys., 137, 064112 (2012).' in line:
                        for i in range(2):
                            next(f)
                    # Read in parameters
                    # 27 CFPs plus the separating lines
                    for i in range(num_CFP_lines):
                        line = next(f)
                        if line.split()[0] == '2' \
                            or line.split()[0] == '4' \
                                or line.split()[0] == '6':
                            CFPs.append(float(line.split()[6]))

    # Check number of active orbitals has been found
    if not found_orbs:
        print(
            'Error: could not find number of active orbitals in specified file'
        )
        print('Please add the line:')
        print('Ras2=NUMBER_OF_ORBITALS')
        sys.exit()

    # Check number of active electrons has been found
    if not found_elec:
        print('Error: could not find number of active electrons in specified file')
        print('Please add the line:')
        print('Nactel=NUMBER_OF_ELECTRONS')
        sys.exit()

    return CFPs, n_elec, n_orbs, L, S, J


def krodelta(arg1, arg2):
    """
    Kronecker delta function

    Positional arguments:
        arg1 (float/int) :: First variable
        arg2 (float/int) :: Second variable

    Keyword arguments:
        None

    Returns:
        val (int) :: 1 if arg1 = arg, else 0
    """

    if arg1 == arg2:
        val = 1
    else:
        val = 0

    return val


def ang_mom_nums(n_elec, n_orbs):
    """
    Calculates quantum numbers S, J, L, from a set of orbitals and electrons

    Positional arguments:
        n_elec (int) :: Number of electrons
        n_orbs (int) :: Number of orbitals

    Keyword arguments:
        None

    Returns:
        L (float)    :: L quantum number
        S (float)    :: S quantum number
        J (float)    :: J quantum number
    """

    # ml values of 4f orbitals
    ml_4f = np.array([3, 2, 1, 0, -1, -2, -3])

    if n_orbs == 7:
        # More than half filled
        if n_elec > 7.:
            S = 3.5 - float(n_elec - 7) * 0.5
            L = float(sum(ml_4f[0:n_elec - 7]))
            J = L + S
        # Less than half filled
        elif n_elec < 7.:
            S = 0.5 * float(n_elec)
            L = float(sum(ml_4f[0:n_elec]))
            J = L - S
        # Half filled print error as no crystal field
        elif n_elec == 7:
            print('7 electrons detected - no orbital component of J')
            sys.exit()
    else:
        print('Incorrect number of orbitals in active space: should be 7')
        sys.exit()

    return L, S, J


def ang_mom_ops(J):
    """
    Calculates the angular momentum operators Jx Jy Jz Jp Jm J2

    Positional arguments:
        J (float)      :: J quantum number

    Keyword arguments:
        None

    Returns:
        Jx (np.array)  :: Matrix representation of angular momentum operator
        Jy (np.array)  :: Matrix representation of angular momentum operator
        Jz (np.array)  :: Matrix representation of angular momentum operator
        Jp (np.array)  :: Matrix representation of angular momentum operator
        Jm (np.array)  :: Matrix representation of angular momentum operator
        J2 (np.array)  :: Matrix representation of angular momentum operator
    """

    # Create vector of mJ values
    mJ = np.arange(-J, J + 1, 1)
    # calculate number of states
    n_states = int(2 * J + 1)

    # Jz operator - diagonal in Jz basis - entries are mJ
    Jz = np.diag(mJ)

    # Jp and Jm operators
    Jp = np.zeros([n_states, n_states])
    Jm = np.zeros([n_states, n_states])
    for it1, mJp in enumerate(mJ):
        for it2, mJq in enumerate(mJ):
            Jp[it1, it2] = (np.real(np.sqrt(J * (J + 1) - mJq * (mJq + 1))) * krodelta(mJp, mJq + 1))
            Jm[it1, it2] = (np.real(np.sqrt(J * (J + 1) - mJq * (mJq - 1))) * krodelta(mJp, mJq - 1))

    # Jx, Jy, and J2
    Jx = 0.5 * (Jp + Jm)
    Jy = 1. / (2. * 1j) * (Jp - Jm)
    J2 = Jx @ Jx + Jy @ Jy + Jz @ Jz

    return Jx, Jy, Jz, Jp, Jm, J2


def calc_stev_ops(J, Jp, Jm, Jz, J2):
    """
    Calculates Stevens operators up to 6th rank (k=6)

    Positional arguments:
        J (float)      :: J quantum number
        Jp (np.array)  :: Matrix representation of angular momentum operator
        Jm (np.array)  :: Matrix representation of angular momentum operator
        Jz (np.array)  :: Matrix representation of angular momentum operator
        J2 (np.array)  :: Matrix representation of angular momentum operator

    Keyword arguments:
        None

    Returns:
        stevops (list of np.arrays) :: List containing each stevens operator
                                        indexed k=2 q=-k->k, k=4 q=-k->k ...

    """


    # !!! Calculates Stevens operators upto 6th rank (k=6)
    # !!!  returns single array with all ops

    I = np.diag(np.ones(int(2 * J + 1)))

    O2m2 = 1j * 0.5 * (np.matmul(Jp, Jp) - np.matmul(Jm, Jm))
    O2m1 = 1j * 0.25 * (np.matmul(Jz, (Jp - Jm)) + np.matmul((Jp - Jm), Jz))
    O20 = 3.0 * np.matmul(Jz, Jz) - J2
    O21 = 0.25 * (np.matmul(Jz, (Jp + Jm)) + np.matmul((Jp + Jm), Jz))
    O22 = 0.5 * (np.matmul(Jp, Jp) + np.matmul(Jm, Jm))

    O4m4 = 0.5 * 1j * (np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)) - np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm)))
    O4m3 = 0.25 * 1j * (np.matmul((np.matmul(np.matmul(Jp, Jp), Jp) - (np.matmul(np.matmul(Jm, Jm), Jm))), Jz) + np.matmul(Jz, ((np.matmul(np.matmul(Jp, Jp), Jp) - (np.matmul(np.matmul(Jm, Jm), Jm))))))
    O4m2 = 0.25 * 1j * (np.matmul((7.0 * np.matmul(Jz, Jz) - J2 - 5.0 * I),(np.matmul(Jp, Jp) - np.matmul(Jm, Jm))) + np.matmul((np.matmul(Jp, Jp) - np.matmul(Jm, Jm)), (7.0 * np.matmul(Jz, Jz) - J2 - 5.0 * I)))
    O4m1 = 0.25 * 1j * (np.matmul((7.0 * np.matmul(np.matmul(Jz, Jz), Jz) - 3.0 * np.matmul(J2, Jz) - Jz),(Jp - Jm)) + np.matmul((Jp - Jm), (7.0 * np.matmul(np.matmul(Jz, Jz), Jz) - 3.0 * np.matmul(J2, Jz) - Jz)))
    O40 = 35.0 * np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)) - 30.0 * np.matmul(J2, np.matmul(Jz,Jz)) + 25.0 * np.matmul(Jz, Jz) + 3.0 * np.matmul(J2, J2) - 6.0 * J2
    O41 = 0.25 * (np.matmul((Jp + Jm),(7.0 * np.matmul(np.matmul(Jz, Jz), Jz) - 3.0 * np.matmul(J2, Jz) - Jz)) + np.matmul((7.0 * np.matmul(np.matmul(Jz, Jz), Jz) - 3.0 * np.matmul(J2, Jz) - Jz), (Jp + Jm)))
    O42 = 0.25 * (np.matmul((7.0 * np.matmul(Jz, Jz) - J2 - 5.0 * I),(np.matmul(Jp, Jp) + np.matmul(Jm, Jm))) + np.matmul((np.matmul(Jp, Jp) + np.matmul(Jm, Jm)), (7.0 * np.matmul(Jz, Jz) - J2 - 5.0 * I)))
    O43 = 0.25 * (np.matmul((np.matmul(np.matmul(Jp, Jp), Jp) + (np.matmul(np.matmul(Jm, Jm), Jm))), Jz) + np.matmul(Jz,((np.matmul(np.matmul(Jp,Jp),Jp) + (np.matmul(np.matmul(Jm,Jm),Jm))))))
    O44 = 0.5 * (np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)) + np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm)))

    O6M6 = 0.5 * 1j * (np.matmul(np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)), np.matmul(Jp, Jp)) - np.matmul(np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm)), np.matmul(Jm, Jm)))
    O6M5 = 0.25 * 1j * (np.matmul(np.matmul(np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)), Jp) - np.matmul(np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm)), Jm), Jz) + np.matmul(Jz, (np.matmul(np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)), Jp) - np.matmul(np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm)), Jm))))
    O6M4 = 0.25 * 1j * (np.matmul((np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)) - np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm))),(11.0 * np.matmul(Jz, Jz) - J2 - 38.0 * I)) + np.matmul((11.0 * np.matmul(Jz, Jz) - J2 - 38.0 * I), (np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)) - np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm)))))
    O6M3 = 0.25 * 1j * (np.matmul(np.matmul(np.matmul(Jp, Jp), Jp) - np.matmul(np.matmul(Jm, Jm), Jm), (11.0 * np.matmul(Jz, np.matmul(Jz, Jz)) - 3.0 * np.matmul(J2, Jz) - 59.0 * Jz)) + np.matmul((11.0 * np.matmul(Jz, np.matmul(Jz, Jz)) - 3.0 * J2 * Jz - 59.0 * Jz),(np.matmul(np.matmul(Jp, Jp), Jp) - np.matmul(np.matmul(Jm, Jm), Jm))))
    O6M2 = 0.25 * 1j * (np.matmul(np.matmul(Jp, Jp) - np.matmul(Jm, Jm), 33.0 * np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)) - 18.0 * np.matmul(J2,np.matmul(Jz,Jz)) - 123.0 * np.matmul(Jz, Jz) + np.matmul(J2, J2) + 10.0 * J2 + 102.0 * I) + np.matmul(33.0 * np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)) - 18.0 * np.matmul(J2, np.matmul(Jz,Jz)) - 123.0 * np.matmul(Jz, Jz) + np.matmul(J2, J2) + 10.0 * J2 + 102.0 * I, np.matmul(Jp, Jp) - np.matmul(Jm, Jm)))
    O6M1 = 0.25 * 1j * (np.matmul((Jp - Jm), (33.0 * np.matmul(np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)), Jz) - 30.0 * np.matmul(J2, np.matmul(np.matmul(Jz, Jz), Jz)) + 15.0 * np.matmul(np.matmul(Jz, Jz), Jz) + 5.0 * np.matmul(np.matmul(J2, J2),Jz) - 10.0 * np.matmul(J2, Jz) + 12.0 * Jz)) + np.matmul((33.0 * np.matmul(np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)),Jz) - 30.0 * np.matmul(J2, np.matmul(np.matmul(Jz, Jz),Jz)) + 15.0 * np.matmul(np.matmul(Jz, Jz), Jz) + 5.0 * np.matmul(np.matmul(J2, J2), Jz) - 10.0 * np.matmul(J2, Jz) + 12.0 * Jz),(Jp - Jm)))
    O60 = 231.0 * np.matmul(np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)),np.matmul(Jz, Jz)) - 315.0 * J2 * np.matmul(np.matmul(Jz, Jz),np.matmul(Jz, Jz)) + 735.0 * np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)) + 105.0 * np.matmul(np.matmul(J2, J2),np.matmul(Jz, Jz)) - 525.0 * np.matmul(J2, np.matmul(Jz,Jz)) + 294.0 * np.matmul(Jz, Jz) - 5.0 * np.matmul(np.matmul(J2, J2), J2) + 40.0 * np.matmul(J2, J2) - 60.0 * J2
    O61 = 0.25 * (np.matmul((Jp + Jm), (33.0 * np.matmul(np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)), Jz) - 30.0 * np.matmul(J2, np.matmul(np.matmul(Jz, Jz), Jz)) + 15.0 * np.matmul(np.matmul(Jz, Jz), Jz) + 5.0 * np.matmul(np.matmul(J2, J2),Jz) - 10.0 * np.matmul(J2, Jz) + 12.0 * Jz)) + np.matmul((33.0 * np.matmul(np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)),Jz) - 30.0 * np.matmul(J2, np.matmul(np.matmul(Jz, Jz),Jz)) + 15.0 * np.matmul(np.matmul(Jz, Jz), Jz) + 5.0 * np.matmul(np.matmul(J2, J2), Jz) - 10.0 * np.matmul(J2, Jz) + 12.0 * Jz),(Jp + Jm)))
    O62 = 0.25 * (np.matmul(np.matmul(Jp, Jp) + np.matmul(Jm, Jm),33.0 * np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)) - 18.0 * np.matmul(J2, np.matmul(Jz,Jz)) - 123.0 * np.matmul(Jz, Jz) + np.matmul(J2, J2) + 10.0 * J2 + 102.0 * I) + np.matmul(33.0 * np.matmul(np.matmul(Jz, Jz), np.matmul(Jz, Jz)) - 18.0 * np.matmul(J2, np.matmul(Jz,Jz)) - 123.0 * np.matmul(Jz, Jz) + np.matmul(J2, J2) + 10.0 * J2 + 102.0 * I, np.matmul(Jp, Jp) + np.matmul(Jm, Jm)))
    O63 = 0.25 * (np.matmul(np.matmul(np.matmul(Jp, Jp), Jp) + np.matmul(np.matmul(Jm, Jm), Jm), (11.0 * np.matmul(Jz, np.matmul(Jz, Jz)) - 3.0 * np.matmul(J2, Jz) - 59.0 * Jz)) + np.matmul((11.0 * np.matmul(Jz, np.matmul(Jz, Jz)) - 3.0 * J2 * Jz - 59.0 * Jz),(np.matmul(np.matmul(Jp, Jp), Jp) + np.matmul(np.matmul(Jm, Jm), Jm))))
    O64 = 0.25 * (np.matmul((np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)) + np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm))),(11.0 * np.matmul(Jz, Jz) - J2 - 38.0 * I)) + np.matmul((11.0 * np.matmul(Jz, Jz) - J2 - 38.0 * I), (np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)) + np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm)))))
    O65 = 0.25 * (np.matmul(np.matmul(np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)), Jp) + np.matmul(np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm)), Jm), Jz) + np.matmul(Jz, (np.matmul(np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)), Jp) + np.matmul(np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm)), Jm))))
    O66 = 0.5 * (np.matmul(np.matmul(np.matmul(Jp, Jp), np.matmul(Jp, Jp)), np.matmul(Jp, Jp)) + np.matmul(np.matmul(np.matmul(Jm, Jm), np.matmul(Jm, Jm)), np.matmul(Jm, Jm)))

    stev_ops = np.array(
        [O2m2, O2m1, O20, O21, O22, O4m4, O4m3, O4m2, O4m1, O40, O41, O42, O43, O44, O6M6, O6M5, O6M4, O6M3, O6M2, O6M1,
         O60, O61, O62, O63, O64, O65, O66])
    return stev_ops


def calc_HCF(J, Jp, Jm, Jz, CFPs, stev_ops):
    """
    Calculates crystal field Hamiltonian (HCF) using CFPs and Stevens operators

    Positional arguments:
        J (float)      :: J quantum number
        Jp (np.array)  :: Matrix representation of angular momentum operator
        Jm (np.array)  :: Matrix representation of angular momentum operator
        Jz (np.array)  :: Matrix representation of angular momentum operator
        CFPs (np.array)  :: Matrix representation of angular momentum operator
        stevops (list of np.arrays) :: List containing each stevens operator
                                        indexed k=2 q=-k->k, k=4 q=-k->k ...
    
    Keyword arguments:
        None

    Returns:
        HCF (np.array)    :: Matrix representation of Crystal Field Hamiltonian
        CF_val (np.array) :: Eigenvalues of HCF (lowest eigenvalue is zero)
        CF_vec (np.array) :: Eigenvectors of HCF

    """

    # calculate number of states
    n_states = int(2 * J + 1)

    # Form Hamiltonian
    # No operator equivalent factors are needed as they are included in the single_aniso CFPs
    HCF = np.zeros([n_states, n_states], dtype=np.complex128)
    for it, parameter in enumerate(CFPs):
        HCF += stev_ops[it, :, :] * CFPs[it]

    # For Kramers ions
    # Add small zeeman field in z direction
    # Uses field strength of 25 micro Tesla = Earth's magnetic field
    # Stops linear combination of +- states
    if (n_states%2 == 0) :
        HCF += 0.5*(Jp + Jm)*0.0 
        HCF += (1.0/(2.0*1j))*(Jp - Jm)*0.0 
        HCF += Jz*25000.0*10.0**-9.0

    # Solve for CF_vec and eigenvalues
    CF_val, CF_vec = la.eigh(HCF)

    # Set ground energy to zero
    CF_val -= CF_val[0]

    return HCF, CF_val, CF_vec


def calc_trans_mat(L, S, J, Jx, Jy, Jz, CF_vec):
    """
    Calculates magnetic transition dipole moment operator

    Positional arguments:
        L (float)      :: L quantum number
        S (float)      :: S quantum number
        J (float)      :: J quantum number
        Jx (np.array)  :: Matrix representation of angular momentum operator
        Jy (np.array)  :: Matrix representation of angular momentum operator
        Jz (np.array)  :: Matrix representation of angular momentum operator
        CF_vec (np.array) :: Eigenvectors of HCF
    
    Keyword arguments:
        None

    Returns:
        trans (np.array)    :: Matrix representation of magnetic transition 
                              dipole moment operator

    """

    # Calculate gJ (Lande g factor) for a given L, S, J
    gJ = 1.5 + (S * (S + 1) - L * (L + 1)) / (2 * J * (J + 1))

    # Calculate expectation values of magnetic moment operators for each direction in the eigenbasis of crystal field
    # here we transform Jx, Jy and Jz into CF eigenbasis - ignore hbar and muB as they are constants
    Mux = gJ * la.inv(CF_vec) @ Jx @ CF_vec
    Muy = gJ * la.inv(CF_vec) @ Jy @ CF_vec
    Muz = gJ * la.inv(CF_vec) @ Jz @ CF_vec

    # Calculate overall transition probabilties as average of each dipole moment squared
    trans = (np.abs(Mux) ** 2 + np.abs(Muy) ** 2 + np.abs(Muz) ** 2) * 1. / 3.

    return trans


def evolve_trans_mat(J, Jz_expect, trans):
    """
    Scales transition matrix by "amount" coming into each state
    and removes backwards or downwards transitions

    Positional arguments:
        J (float)             :: J quantum number
        Jz_expect (np.array)  :: 1D array of <Jz> in eigenbasis of HCF
        trans (np.array)      :: Matrix representation of magnetic transition 
                                 dipole moment operator
    Keyword arguments:
        None

    Returns:
        output_trans (np.array)    :: Matrix representation of magnetic transition 
                                      dipole moment operator after scaling

    """

    # Calculate number of states
    n_states = int(2 * J + 1)

    # Remove self transitions
    np.fill_diagonal(trans, 0.)

    # Remove all transitions backwards over the barrier or downwards between states
    for i in np.arange(n_states):  # from
        for f in np.arange(n_states):  # to
            if Jz_expect[i] > Jz_expect[f]:
                trans[f, i] = 0.  # No backwards or downwards steps

    # Normalise each column so transition probability is a fraction of 1
    for i in np.arange(n_states):
        total = 0.
        total = sum(trans[:, i])
        if total > 0.:
            trans[:, i] = trans[:, i] / total

    # Find indexing which relates the current arrrangement of the array Jz_expect to the arrangement it would
    # have if it was written in descending order (largest first)
    # This is done because our pathway consists only of transitions which increase (-ve to eventually +ve) <Jz>
    index = Jz_expect.argsort()

    # Scale transition probability by amount coming into each state
    # Assume unit "population" of ground state (Jz=-J)
    # i.e. trans[:,index[0]] is already 1
    for ind in index:
        if ind == 0:
            continue
        else:
            # scale outward by inward
            trans[:,ind] *= np.sum(trans[ind,:])

    # Scale matrix to be a percentage
    trans = 100. * trans

    # Find transitions with >1% probability
    # write their indices to an array along with the probabilty as a decimal
    # this is used to set the transparency of the arrows on the plot
    num_trans = 0
    output_trans = []
    for row in np.arange(n_states):
        for col in np.arange(n_states):
            if trans[row, col] > 1.:
                alpha = float(trans[row, col] / 100.0)
                if alpha > 1.:
                    alpha = 1.
                output_trans.append([row, col, alpha])
                num_trans += 1

    return output_trans, num_trans


def hex_to_rgb(value):
    """
    Convert hex code to rgb list

    Positional arguments:
        value (str)             :: Hex code

    Keyword arguments:
        None

    Returns:
        rgb (list)    :: [red,green,blue]
    """

    value = value.lstrip('# ')
    lv = len(value)
    rgb=[int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3)]

    return rgb


def plot_barrier(J, energy, Jz_expect, trans, colour='ff0000', name='', font_size = 10,
                 figsize = [7,5.5], second_K_ax = True, show=False, save=True, 
                 save_name="barrier.svg"):
    """
    Plots barrier figure with transition intensities from user provided matrix
    Y axis is Energy in cm-1, x axis is <Jz> of each state
    Arrows are transitions with intensity specified by specified by trans array

    Positional arguments:
        J (float)         : J quantum number
        energy (list)     : List of state energies
        Jz_expect (list)  : List of <Jz> for each state
        trans (list)      : List of lists of transitions [initial, final, %intensity]   

    Keyword arguments:
        colour (str)       : Hex code specifying arrow colours
        name (str)         : Name for plot window title
        font_size (str)    : Font size
        figsize (list)     : Size of figure [width, height] in inches
        second_K_ax (bool) : If True, add second y axis in units of K
        show (bool)        : If True, show plot on screen
        save (bool)        : If True, save plot to file
        save_name (str)    : Name of file for final plot 

    Returns:
        fig (matplotlib figure object) : Matplotlib figure object for figure
        axes (list of matplotlib axis objects) : List containing axes objects for main axis
                                                  and secondary Kelvin axis
    """


    # Change plot font size - needs to be done before plot is created
    plt.rcParams.update({'font.size': font_size})

    # Create plot and axes
    fig, ax = plt.subplots(1, 1, sharey='all', figsize=figsize, num=name)
    
    if second_K_ax:
        K_ax = ax.twinx()
        axes = [ax, K_ax]
    else:
        axes = [ax]

    # Draw energy level lines
    ax.plot(Jz_expect, energy, marker='_', markersize='25', mew='2.5', linewidth=0, color='black')

    # Final <Jz>
    Jz_expect_final = [Jz_expect[trans[row][1]] for row in range(num_trans)]

    # Difference between initial and final <Jz>
    Jz_expect_diff = [Jz_expect[trans[row][0]]-Jz_expect[trans[row][1]] for row in range(num_trans)]

    # Final energies
    energies_final = [energy[trans[row][1]] for row in range(num_trans)]

    # Difference between initial and final energies
    energies_diff = [energy[trans[row][0]] - energy[trans[row][1]] for row in range(num_trans)]

    # Alpha channel values
    alphas = [trans[row][2] for row in range(num_trans)]

    # Make colours array
    # Columns are red, green, blue, alpha
    rgba_colors = np.zeros((num_trans, 4))

    # Convert user hex to rgb
    colour_rgb = hex_to_rgb(colour)
    rgba_colors[:, 0] = float(colour_rgb[0]) / 255.
    rgba_colors[:, 1] = float(colour_rgb[1]) / 255.
    rgba_colors[:, 2] = float(colour_rgb[2]) / 255.

    rgba_colors[:, 3] = np.asarray(alphas)

    # Draw lines between levels
    ax.quiver(Jz_expect_final, energies_final, Jz_expect_diff, energies_diff, 
              scale_units='xy', angles='xy', scale=1, color=rgba_colors)

    # Set x axis options
    ax.set_xlabel(r'$\langle \ \hat{J}_{z} \ \rangle$')
    ax.tick_params(axis='both', which='both', length=2.0)
    ax.xaxis.set_major_locator(plt.MaxNLocator(8))

    # Set y axis options for cm-1
    ax.set_ylabel(r'Energy $($cm$^{-1})$')
    ax.yaxis.set_major_locator(plt.MaxNLocator(7))

    # Set y axis options for K
    if second_K_ax:
        K_ax.set_ylabel(r'Energy $($K$)$')
        K_ax.set_ylim(ax.get_ylim()[0] * 1.4, ax.get_ylim()[1] * 1.4)
        K_ax.yaxis.set_major_locator(plt.MaxNLocator(7))

    # Set axis limits
    ax.set_xlim([-J * 1.1, J * 1.1])

    # Set number and position of x axis ticks
    ax.set_xticks(np.arange(-J, J + 1, 1))

    # Set x axis tick labels
    labels = []

    # Fractions if J non-integer
    if J % 2 != 0:
        for it in np.arange(0, int(2 * J + 1)):
            labels.append(str(-int(2 * J) + 2 * it) + '/2')
    else:
        for it in np.arange(0, int(2 * J + 1)):
            labels.append(str(-int(J) + it))

    ax.set_xticklabels(labels, rotation=45)

    fig.tight_layout()

    # Save or show plot
    if save:
        fig.savefig(save_name, dpi=500)
    if show:
        plt.show()

    return fig, axes

if __name__ == '__main__':

    # Read in command line arguments
    user_args = read_user_args()

    # Read in data from molcas output file
    CFPs, n_elec, n_orbs, L, S, J = read_molcas_file(user_args.output_file)

    # Calculate Jx, Jy, Jz, Jp, Jm, J2 operators
    Jx, Jy, Jz, Jp, Jm, J2 = ang_mom_ops(J)

    # Calculate Stevens operators
    stev_ops = calc_stev_ops(J, Jp, Jm, Jz, J2)

    # Calculate crystal field Hamiltonian and diagonalise
    HCF, CF_val, CF_vec = calc_HCF(J, Jp, Jm, Jz, CFPs, stev_ops)

    # Calculate transition matrix using magnetic moment operators
    trans = calc_trans_mat(L, S, J, Jx, Jy, Jz, CF_vec)

    # Calculate expectation value of Jz in CF eigenbasis
    # and only take real part
    Jz_expect = np.real(np.diag(la.inv(CF_vec) @ Jz @ CF_vec))

    # Scale trnasition matrix to only give transitions over the barrier
    trans, num_trans = evolve_trans_mat(J, Jz_expect, trans)

    # Plot barrier figure
    save_name = "{}_barrier.{}".format(user_args.output_file.split('.')[0], user_args.save)
    fig, axes = plot_barrier(J, CF_val, Jz_expect, trans, colour=user_args.colour, 
                             name=user_args.output_file, show=True, save=True, save_name = save_name)
