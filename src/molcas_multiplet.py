#! /usr/bin/env python3

#┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
#││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
#┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import argparse
from numpy import linalg as LA
import os
import sys

mpl.rcParams["savefig.directory"] = ""
mpl.rcParams['savefig.dpi'] = 300

def read_in_command_line():
    # Read in command line arguments

    parser = argparse.ArgumentParser(description='Plots molcas multiplet energies for multiple molcas output files')
    parser.add_argument('output_files', type = str, nargs='*', metavar ='file',
                        help = 'Molcas output files')
    parser.add_argument('--save', type = str, metavar = '<png/svg>',
                        help = 'Save figure with given format e.g. --save png or svg')
    args = parser.parse_args()

    # Check for no arguments
    if not any(args.output_files):
            parser.error('Missing molcas output files')
    return args


    # Check format is png or svg
    if args.save != None:
        save_format = args.save.lower()
        # If format not png or svg then exit with error
        if save_format != 'png' and save_format != 'svg' and save_format != '.png' and save_format != '.svg':
            print('*----------          Save format is unsupported          ----------*')
            sys.exit(1)
        # Remove dot from file extension if user adds it
        if save_format[0] == '.':
            args.save = save_format[1:]


    return args


def read_molcas_file(molcas_output):
    # !!! Read in molcas output file and extract info

    # Check file exists
    if os.path.isfile(molcas_output) == False:
        print('File not found!')
        exit()

    # Flags for job completion, molcas_version, and flags for 
    # successful location of number of electrons and orbitals
    # in the active space
    finished = False
    open_molcas = False
    found_orbs = False
    found_elec = False

    # Find happy landing and pymolcas driver lines
    with open(molcas_output,'r') as f:
        for line in f:
            if line.find('This run of MOLCAS is using the pymolcas driver') != -1:
                open_molcas = True
            if line.find('Happy landing!') != -1:
                finished = True

    # Ask user if they want to continue if happy landing is missing
    if finished != True and open_molcas == False:
        print('Molcas has not terminated properly')
        answer = input('Do you want to continue? (Y/N)')
        answer = str(answer)
        if answer != 'Y' and answer != 'N':
            answer = input('Do you want to continue? (Y/N)')
            answer = str(answer)
        elif answer == 'N':
            exit()

    f.close()

    # Check how many single_aniso sections exist in the file
    # If more than one, ask user which they would like to use
    with open(molcas_output, 'r') as f:
        single_aniso = 0
        for line in f:
            if line.find('Start Module: single_aniso') != -1:
                single_aniso += 1
        if single_aniso == 0:
            print('Error: no SINGLE_ANISO modules found in specified file')
            sys.exit(1)
        
        if single_aniso >= 2:
            section_sinani = input('There are {} single_aniso sections. Which section do you want to use? \n'.format(single_aniso))
            section_sinani = int(section_sinani)

            # Check for stupid input
            if section_sinani > single_aniso :
                print('Error there are not that many SINGLE_ANISO modules present')
                sys.exit(1)
        else:
            section_sinani = 1

    f.close()

    # Begin reading molcas output file
    count_sinani = 0
    CFPs = []

    with open(molcas_output,'r') as f:
        for line in f:

            # Read number of electrons in active space 
            if line.find('Nactel') != -1:
                line = next(f)
                found_elec = True
                num_elec = int(line.split()[0])

            # Read number of active orbitals
            if line.find('Ras2') != -1:
                line = next(f)
                found_orbs = True
                num_orbs = int(line.split()[0])
            
            # Calculate J and limit number of CFPs if necessary
            if (found_orbs == True and found_elec == True):
                L, S, J = ang_mom_nums(num_elec, num_orbs)


                if (int(2 * J) >= 6) :
                    num_CFP_lines = 30
                else:
                    num_CFP_lines = 17

            # Read crystal field parameters
            if line.find('Start Module: single_aniso') != -1:
                count_sinani += 1

            if line.find('Hcf = SUM_{k,q} * [ B(k,q) * O(k,q) ];') != -1 and count_sinani == section_sinani:
                # Skip over garbage header - extra skip is for cases where CF terms 
                # with k > 6 are required - in that case two more lines are printed by molcas
                for i in range(10):
                    line = next(f)
                if line.find('J. Chem. Phys., 137, 064112 (2012).') != -1:
                    for i in range(2):
                        line = next(f)
                # Read in parameters
                for i in range(num_CFP_lines): # 27 CFPs plus the separating lines
                    line = next(f)
                    if line.split()[0] == '2' or line.split()[0] == '4' or line.split()[0] == '6':
                        CFPs.append(float(line.split()[6])) # add cfps to array
                
    # Check number of active orbitals has been found
    if found_orbs == False :
        print('Error: could not find number of active orbitals in specified file')
        print('Please add the two lines:')
        print('Ras2')
        print('Number_of_active_orbitals')
        sys.exit(1)

    # Check number of active electrons has been found
    if found_elec == False :
        print('Error: could not find number of active electrons in specified file')
        print('Please add the two lines:')
        print('Nactel')
        print('Number_of_active_electrons')
        sys.exit(1)

    return np.array(CFPs), num_elec, num_orbs, L, S, J

def krodelta(arg1, arg2):
    if arg1 == arg2:
        return 1
    else:
        return 0

def ang_mom_nums(num_elec, num_orbs):
    # !!! Calculates quantum numbers L, S, J


    # Calculate J for given number of electrons and orbitals
    # assuming orbitals are 4f

    # ml values of 4f orbitals
    ml_4f = np.array([3, 2, 1 ,0, -1, -2, -3])

    if num_orbs == 7:
        # More than half filled
        if num_elec > 7.:
            S = 3.5 - float(num_elec - 7)*0.5
            L = float(sum(ml_4f[0:num_elec - 7]))
            J = L + S
        # Less than half filled
        elif num_elec < 7.:
            S = 0.5 * float(num_elec)
            L = float(sum(ml_4f[0:num_elec]))
            J = L - S
        # Half filled print error as no crystal field
        elif num_elec == 7:
            print('7 electrons detected - no orbital component of J')
            sys.exit(1)
    else:
        print('Incorrect number of orbitals in active space: should be 7')
        sys.exit(1)

    return L, S, J


def ang_mom_ops(num_elec, J, num_orbs):
    # !!! Calculates the angular momentum operators Jx Jy Jz Jp Jm J2

    # Create vector of mJ values
    mJ = np.arange(-J,J+1,1) 
    # calculate number of states
    num_states = int(2*J + 1)

    # Jz operator - diagonal in Jz basis - entries are mJ
    Jz = np.diag(mJ)

    # Jp and Jm operators
    Jp = np.zeros([num_states, num_states])
    Jm = np.zeros([num_states, num_states])
    for it1, mJp in enumerate(mJ):
        for it2, mJq in enumerate(mJ):
            Jp[it1,it2] = (np.real(np.sqrt(J*(J+1)-mJq*(mJq+1)))*krodelta(mJp,mJq+1))
            Jm[it1,it2] = (np.real(np.sqrt(J*(J+1)-mJq*(mJq-1)))*krodelta(mJp,mJq-1))

    # Jx, Jy, and J2
    Jx = 0.5*(Jp+Jm);
    Jy = 1./(2.*1j)*(Jp-Jm);
    J2 = np.matmul(Jx,Jx) + np.matmul(Jy,Jy) + np.matmul(Jz,Jz)

    return Jx, Jy, Jz, Jp, Jm, J2

def calc_stev_ops(J, Jp, Jm, Jz, J2):
    # !!! Calculates Stevens operators upto 6th rank (k=6)
    # !!!  returns single array with all ops

    I = np.diag(np.ones(int(2*J + 1)))

    O2m2 = 1j*0.5*(np.matmul(Jp,Jp) - np.matmul(Jm,Jm))
    O2m1 = 1j*0.25*(np.matmul(Jz,(Jp-Jm)) + np.matmul((Jp-Jm),Jz))
    O20 = 3.0*np.matmul(Jz,Jz) - J2
    O21 = 0.25*(np.matmul(Jz,(Jp+Jm)) + np.matmul((Jp+Jm),Jz))
    O22 = 0.5*(np.matmul(Jp,Jp) + np.matmul(Jm,Jm))

    O4m4 = 0.5*1j*(np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp)) - np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm)))
    O4m3 = 0.25*1j*(np.matmul((np.matmul(np.matmul(Jp,Jp),Jp)-(np.matmul(np.matmul(Jm,Jm),Jm))),Jz)+np.matmul(Jz,((np.matmul(np.matmul(Jp,Jp),Jp)-(np.matmul(np.matmul(Jm,Jm),Jm))))))
    O4m2 = 0.25*1j*(np.matmul((7.0*np.matmul(Jz,Jz)-J2-5.0*I),(np.matmul(Jp,Jp)-np.matmul(Jm,Jm)))+np.matmul((np.matmul(Jp,Jp)-np.matmul(Jm,Jm)),(7.0*np.matmul(Jz,Jz)-J2-5.0*I)))
    O4m1 = 0.25*1j*(np.matmul((7.0*np.matmul(np.matmul(Jz,Jz),Jz) - 3.0*np.matmul(J2,Jz)-Jz),(Jp-Jm))+np.matmul((Jp-Jm),(7.0*np.matmul(np.matmul(Jz,Jz),Jz) - 3.0*np.matmul(J2,Jz)-Jz)))
    O40  = 35.0*np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz)) - 30.0*np.matmul(J2,np.matmul(Jz,Jz))+ 25.0*np.matmul(Jz,Jz) + 3.0*np.matmul(J2,J2) - 6.0*J2
    O41  = 0.25*(np.matmul((Jp+Jm),(7.0*np.matmul(np.matmul(Jz,Jz),Jz)-3.0*np.matmul(J2,Jz)-Jz))+np.matmul((7.0*np.matmul(np.matmul(Jz,Jz),Jz)-3.0*np.matmul(J2,Jz)-Jz),(Jp+Jm)))
    O42  = 0.25*(np.matmul((7.0*np.matmul(Jz,Jz)-J2-5.0*I),(np.matmul(Jp,Jp)+np.matmul(Jm,Jm)))+np.matmul((np.matmul(Jp,Jp)+np.matmul(Jm,Jm)),(7.0*np.matmul(Jz,Jz)-J2-5.0*I)))
    O43  = 0.25*(np.matmul((np.matmul(np.matmul(Jp,Jp),Jp)+(np.matmul(np.matmul(Jm,Jm),Jm))),Jz)+np.matmul(Jz,((np.matmul(np.matmul(Jp,Jp),Jp)+(np.matmul(np.matmul(Jm,Jm),Jm))))))
    O44  = 0.5*(np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp)) + np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm)))

    O6M6 = 0.5*1j*(np.matmul(np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp)),np.matmul(Jp,Jp)) - np.matmul(np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm)),np.matmul(Jm,Jm))) 
    O6M5 = 0.25*1j*(np.matmul(np.matmul(np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp)),Jp) - np.matmul(np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm)),Jm),Jz)+np.matmul(Jz,(np.matmul(np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp)),Jp) - np.matmul(np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm)),Jm))))
    O6M4 = 0.25*1j*(np.matmul((np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp))-np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm))),(11.0*np.matmul(Jz,Jz)-J2-38.0*I))+np.matmul((11.0*np.matmul(Jz,Jz)-J2-38.0*I),(np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp))-np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm)))))
    O6M3 = 0.25*1j*(np.matmul(np.matmul(np.matmul(Jp,Jp),Jp)-np.matmul(np.matmul(Jm,Jm),Jm),(11.0*np.matmul(Jz,np.matmul(Jz,Jz))-3.0*np.matmul(J2,Jz)-59.0*Jz))+np.matmul((11.0*np.matmul(Jz,np.matmul(Jz,Jz))-3.0*J2*Jz-59.0*Jz),(np.matmul(np.matmul(Jp,Jp),Jp)-np.matmul(np.matmul(Jm,Jm),Jm))))
    O6M2 = 0.25*1j*(np.matmul(np.matmul(Jp,Jp)-np.matmul(Jm,Jm),33.0*np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz))-18.0*np.matmul(J2,np.matmul(Jz,Jz))-123.0*np.matmul(Jz,Jz)+np.matmul(J2,J2)+10.0*J2+102.0*I)+np.matmul(33.0*np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz))-18.0*np.matmul(J2,np.matmul(Jz,Jz))-123.0*np.matmul(Jz,Jz)+np.matmul(J2,J2)+10.0*J2+102.0*I,np.matmul(Jp,Jp)-np.matmul(Jm,Jm)))
    O6M1 = 0.25*1j*(np.matmul((Jp-Jm),(33.0*np.matmul(np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz)),Jz)-30.0*np.matmul(J2,np.matmul(np.matmul(Jz,Jz),Jz))+15.0*np.matmul(np.matmul(Jz,Jz),Jz)+5.0*np.matmul(np.matmul(J2,J2),Jz)-10.0*np.matmul(J2,Jz)+12.0*Jz))+np.matmul((33.0*np.matmul(np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz)),Jz)-30.0*np.matmul(J2,np.matmul(np.matmul(Jz,Jz),Jz))+15.0*np.matmul(np.matmul(Jz,Jz),Jz)+5.0*np.matmul(np.matmul(J2,J2),Jz)-10.0*np.matmul(J2,Jz)+12.0*Jz),(Jp-Jm)))
    O60 = 231.0*np.matmul(np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz)),np.matmul(Jz,Jz)) - 315.0*J2*np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz)) + 735.0*np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz)) + 105.0*np.matmul(np.matmul(J2,J2),np.matmul(Jz,Jz)) - 525.0*np.matmul(J2,np.matmul(Jz,Jz)) + 294.0*np.matmul(Jz,Jz) - 5.0*np.matmul(np.matmul(J2,J2),J2) + 40.0*np.matmul(J2,J2) - 60.0*J2
    O61 = 0.25*(np.matmul((Jp+Jm),(33.0*np.matmul(np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz)),Jz)-30.0*np.matmul(J2,np.matmul(np.matmul(Jz,Jz),Jz))+15.0*np.matmul(np.matmul(Jz,Jz),Jz)+5.0*np.matmul(np.matmul(J2,J2),Jz)-10.0*np.matmul(J2,Jz)+12.0*Jz))+np.matmul((33.0*np.matmul(np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz)),Jz)-30.0*np.matmul(J2,np.matmul(np.matmul(Jz,Jz),Jz))+15.0*np.matmul(np.matmul(Jz,Jz),Jz)+5.0*np.matmul(np.matmul(J2,J2),Jz)-10.0*np.matmul(J2,Jz)+12.0*Jz),(Jp+Jm)))
    O62 = 0.25*(np.matmul(np.matmul(Jp,Jp)+np.matmul(Jm,Jm),33.0*np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz))-18.0*np.matmul(J2,np.matmul(Jz,Jz))-123.0*np.matmul(Jz,Jz)+np.matmul(J2,J2)+10.0*J2+102.0*I)+np.matmul(33.0*np.matmul(np.matmul(Jz,Jz),np.matmul(Jz,Jz))-18.0*np.matmul(J2,np.matmul(Jz,Jz))-123.0*np.matmul(Jz,Jz)+np.matmul(J2,J2)+10.0*J2+102.0*I,np.matmul(Jp,Jp)+np.matmul(Jm,Jm)))
    O63 = 0.25*(np.matmul(np.matmul(np.matmul(Jp,Jp),Jp)+np.matmul(np.matmul(Jm,Jm),Jm),(11.0*np.matmul(Jz,np.matmul(Jz,Jz))-3.0*np.matmul(J2,Jz)-59.0*Jz))+np.matmul((11.0*np.matmul(Jz,np.matmul(Jz,Jz))-3.0*J2*Jz-59.0*Jz),(np.matmul(np.matmul(Jp,Jp),Jp)+np.matmul(np.matmul(Jm,Jm),Jm))))
    O64 = 0.25*(np.matmul((np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp))+np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm))),(11.0*np.matmul(Jz,Jz)-J2-38.0*I))+np.matmul((11.0*np.matmul(Jz,Jz)-J2-38.0*I),(np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp))+np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm)))))
    O65 = 0.25*(np.matmul(np.matmul(np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp)),Jp) + np.matmul(np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm)),Jm),Jz)+np.matmul(Jz,(np.matmul(np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp)),Jp) + np.matmul(np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm)),Jm))))
    O66 = 0.5*(np.matmul(np.matmul(np.matmul(Jp,Jp),np.matmul(Jp,Jp)),np.matmul(Jp,Jp)) + np.matmul(np.matmul(np.matmul(Jm,Jm),np.matmul(Jm,Jm)),np.matmul(Jm,Jm))) 

    stev_ops = np.array([O2m2, O2m1, O20, O21, O22, O4m4, O4m3, O4m2, O4m1, O40, O41, O42, O43, O44, O6M6, O6M5, O6M4, O6M3, O6M2, O6M1, O60, O61, O62, O63, O64, O65, O66])
    return stev_ops

def calc_HCF(J, CFPs, stev_ops):
    # !!! Calculates crystal field Hamiltonian (HCF) using CFPs and Stevens operators
    # !!!  Returns HCF and its eigenvalues and eigenvectors

    # calculate number of states
    num_states = int(2*J + 1)

    # Form Hamiltonian
    # No operator equivalent factors are needed as they are included in the single_aniso CFPs
    HCF = np.zeros([num_states, num_states], dtype = np.complex128)
    for it,parameter in enumerate(CFPs):
        HCF += stev_ops[it,:,:]*CFPs[it]

    # Solve for CF_vec and eigenvalues
    CF_val, CF_vec = LA.eigh(HCF)

    # Set ground energy to zero
    CF_val -= CF_val[0]

    return HCF, CF_val, CF_vec


def calc_moment(J, CF_vec):

    # Calculate number of states
    num_states = int(2*J + 1)

    # Calculate vector of mJ values -ve to +ve
    mJ = np.arange(-J,J+1,1) 

    # Calculate CF_vec as percentage compositions
    CF_vec = np.real(CF_vec)**2 + np.imag(CF_vec)**2

    # Make vector of mJ values
    # -mJ to +mJ
    for i in np.arange(num_states):
        mJ[i] = -np.real(J*2)/2.0 + i


    # Calculate how magnetic each state is by summing eigenvector component * mJ value for each state/eigenvector
    moment = np.zeros(num_states,)
    for i in np.arange(num_states):
        for j in np.arange(num_states):
            moment[i] += mJ[j]*CF_vec[j,i]

            return moment

def hex_to_rgb(value):
    # Convert hex code to rgb tuple
    value = value.lstrip('# ')
    lv = len(value)
    return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))

def plot_data(ax, energy, moment):
    # !!!Plots data - energy levels and transition arrows

    # Draw energy level lines
    ax.plot(moment, energy, marker='_', markersize='25', mew='2.5', linewidth=0, color = 'black')

def adjust_plot(ax, K_ax, num_trans, J):
    # !!!Function to adjust plot options - axis, labels, style etc.

    # Set x axis options
    ax.set_xlabel(r'$\langle \ \hat{J}_{z} \ \rangle$')
    ax.tick_params(axis = 'both', which = 'both', length = 2.0)
    ax.xaxis.set_major_locator(plt.MaxNLocator(8))

    # Set y axis options for cm-1
    ax.set_ylabel(r'Energy $($cm$^{-1})$')
    ax.yaxis.set_major_locator(plt.MaxNLocator(7))

    # Set y axis options for K
    K_ax.set_ylabel(r'Energy $($K$)$')
    K_ax.set_ylim(ax.get_ylim()[0]*1.4, ax.get_ylim()[1]*1.4)
    K_ax.yaxis.set_major_locator(plt.MaxNLocator(7))

    # Set axis limits
    ax.set_xlim([-J * 1.1, J * 1.1])

    # Set number and position of x axis ticks
    ax.set_xticks(np.arange(-J, J + 1, 1))

    # Set x axis tick labels
    labels = []

    # Fractions if J non-integer
    if J%2 != 0:
        for it in np.arange(0,int(2*J + 1)):
            labels.append(str(-int(2*J) + 2*it)+'/2')
    else:
        for it in np.arange(0,int(2*J + 1)):
            labels.append(str(-int(J) + it))

    ax.set_xticklabels(labels, rotation = 45)

    return

# Read in command line arguments
args = read_in_command_line()

# Create plot and axes

fig, (ax) = plt.subplots( 1, 1, sharex=False, sharey='all', figsize=(8,6), num = 'multiplets')
K_ax = ax.twinx()

# Read in data from molcas output file

for output_file in args.output_files:
    CFPs, num_elec, num_orbs, L, S, J = read_molcas_file(output_file)

    # Calculate J, L, and S quantum numbers, and Jx, Jy, Jz, Jp, Jm, J2 operators
    Jx, Jy, Jz, Jp, Jm, J2 = ang_mom_ops(num_elec, J, num_orbs)
    
    # Calculate Stevens operators
    stev_ops = calc_stev_ops(J, Jp, Jm, Jz, J2)
    
    # Calculate crystal field Hamiltonian and diagonalise
    HCF, CF_val, CF_vec = calc_HCF(J, CFPs, stev_ops)

    moment = calc_moment(J, CF_vec)
    
    # Change plot font size - needs to be done before plot is created
    plt.rcParams.update({'font.size': 12})
    
    # Plot data
    plot_data(ax, CF_val, moment)
    
# Adjust plot axes
adjust_plot(ax, K_ax, J)
fig.tight_layout()

# Save or show plot
if args.save != None:
    file_name = 'multiplets.'+args.save
    fig.savefig(file_name, dpi=fig.dpi)
else:
    plt.show()


