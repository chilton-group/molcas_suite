#┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
#││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
#┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


#Makefile for molcas_suite
	#Chilton Group

#-----------------------Usage-------------------------------------------------------------------#

#To build molcas_suite use:
# make all install

#NFC only - to build on cerberus for all users
# make all install master=1

# To build in debug mode
# make XXXX debug=1
# Where XXXX is all, or a target name, and may or may not be followed by install
# e.g. make all install debug=1

# To build for AWS
# make XXXX aws=1

# To compile fortran objects with static linking
# make XXXX static=1

# To change compiler
# make XXXX compiler=YYYY
# where YYYY is either intel or gcc

#-----------------------Configuration-----------------------------------------------------------#

UNAME_S  := $(shell uname -s)
HOST     := $(shell hostname)
CURR_DIR  = $(shell pwd -P)

PIP=pip3

#LINUX compile commands
ifeq ($(UNAME_S),Linux)
	F90COMP=ifort
	MKL=-mkl
	SUDO=sudo
	PERMS=chmod +x
	MOLCAS_GEN_FLAG=-Dlocal
	BIN=/usr/local/bin/
	PROC_OPT=-xHost
	FLAGS=-O2 -fp-model source -nowarn -ip -fpp -msse4.2
	MODLINK=-module  # This MUST have a space after it
	HEAPARRAYS=-heap-arrays
endif

#MacOS compile commands
ifeq ($(UNAME_S),Darwin)
	F90COMP=gfortran
	MKL=-framework Accelerate
	BINDIR=~/bin/MOLCAS_PROGRAMS
	PERMS=chmod +x
	MOLCAS_GEN_FLAG=-Dlocal
	BIN=/usr/local/bin/
	FLAGS=-O2 -g -ffree-line-length-3500 -cpp
	MODLINK=-J # This MUST have a space after it
endif

#-----------------------Machine specific settings------------------------------------------------------#

#CSF specific compile commands
ifeq ($(findstring csf3, $(HOST)),csf3)
	SUDO=#not necessary as only user directories are used
	PERMS=chmod +x
	MOLCAS_GEN_FLAG=-Dcsf
	BIN=~/bin/
	PROC_OPT=-msse4.2 -axCORE-AVX512,CORE-AVX2,AVX
	CSF_PIP=--user
	PIP=pip
endif

#Cerberus specific compile commands
ifeq ($(HOST),$(filter %cerberus, $(HOST)))
	#MKL location
	MKL=$(MKLROOT)/lib/intel64/libmkl_blas95_lp64.a $(MKLROOT)/lib/intel64/libmkl_lapack95_lp64.a -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl -I$(MKLROOT)/include -I$(MKLROOT)/include/intel64/lp64
	#Modify install bin location and sudo for cerberus master build or user build
	ifeq ($(master), 1)
		BIN=/usr/local/bin/
		SUDO=sudo
		PERMS=sudo chmod 555
	else
		BIN=~/bin/
		SUDO=
		PERMS=chmod +x
	endif

endif

#Medusa specific compile commands
ifeq ($(HOST),$(filter %medusa, $(HOST)))
	MOLCAS_GEN_FLAG=-Dmedusa
	#Modify install bin location and sudo for medusa master build or user build
	ifeq ($(master), 1)
		BIN=/usr/local/bin/
		SUDO=sudo
		PERMS=sudo chmod 555
	else
		BIN=~/bin/
		SUDO=
		PERMS=chmod +x
	endif

endif

#-----------------------Compiler Override------------------------------------------------------#

#Set different compiler flags and link to lapack/mkl
ifeq ($(compiler), intel)
	F90COMP=ifort
	FLAGS=-O2 -fp-model source -nowarn -ip -fpp -msse4.2
	MKL=-mkl
	MODLINK=-module
	PROC_OPT=-xHost
	HEAPARRAYS=-heap-arrays
else ifeq ($(compiler),gcc)
	F90COMP=gfortran
	FLAGS=-O2 -g -ffree-line-length-3500 -cpp
	MKL=-llapack -lblas
	MODLINK=-J
	PROC_OPT=
	#CSF lapack location
	ifeq ($(findstring csf3, $(HOST)),csf3)
		MKL=-L/opt/gridware/depots/8e896c5a/el7/pkg/libs/lapack/3.5.0/gcc-4.8.5/lib -llapack
	endif
endif

#-----------------------Static compilation----------------------------------------------------#
ifeq ($(static), 1)
	ifneq ($(findstring csf3, $(HOST)),csf3)
		ifeq ($(compiler), intel)
			PROC_OPT=-msse4.2 -axCORE-AVX512,CORE-AVX2,AVX
		else
			PROC_OPT=
		endif
		FLAGS:=$(FLAGS) -static
	endif
endif
#-----------------------Debug-----------------------------------------------------------------#

#Change flags for debug options
ifeq ($(debug),1)
	ifeq ($(F90COMP), ifort)
		FLAGS=-fp-model source -traceback -check all -ip
	else ifeq ($(F90COMP),gfortran)
		FLAGS=-Og -g -fcheck=all -fbacktrace -fno-range-check -ffree-line-length-3500
	endif
endif


#-----------------------AWS-----------------------------------------------------------------#

#Change for AWS
ifeq ($(debug),1)
	F90COMP=ifort
	FLAGS=-O2 -fp-model source -nowarn -ip -fpp
	MKL=-mkl
	MODLINK=-module
	PROC_OPT=-xHost
	HEAPARRAYS=-heap-arrays
endif


#Bin for source code to use when compiled
ROOTBIN=../bin/

# Libraries folder and dependencies
LDIR =../libs
DEPNAMES = prec matrix_tools stev_ops random_numbers
OBJ=$(patsubst %,$(LDIR)/%.o,$(DEPNAMES))
MOD=$(patsubst %,$(LDIR)/%.mod,$(DEPNAMES))

# Git version module file
define GIT_VER
module version
  implicit none
  
  contains
    SUBROUTINE PRINT_VERSION(program_name)
      IMPLICIT NONE
      CHARACTER(LEN = *) :: program_name
      CHARACTER(LEN=1000), parameter :: commit_tag = '$(shell git describe --abbrev=10 --dirty --always --tags)'

      WRITE(6,'(3A)') TRIM(program_name), ' version commit hash ::  ', TRIM(commit_tag)

    END SUBROUTINE PRINT_VERSION

end module
endef
export GIT_VER
# Remove old version .o and .mod files to force make to create new ones!
# Yes, this is not a clever approach!
# No, I do not care!
$(shell rm -f version.*)

# Combine all preprocessor flags
D=$(MOLCAS_GEN_FLAG)

#----------------------------------------------------------------------------------------------#

#-----------------------Targets----------------------------------------------------------------#

.PHONY: all fort_progs py_progs install move_all clean

all: fort_progs py_progs

fort_progs: cfe molcas_extractor molcas_gen molcas_orbs molcas_basis molcas_rotate openmolcas_gen openmolcas_orbs openmolcas_compare openmolcas_extractor openmolcas_rotate molcas_identity openmolcas_identity

py_progs: molcas_convergence molcas_barrier cpol

install: move_all

#----------------------------------------------------------------------------------------------#

#-----------------------Molcas_suite-----------------------------------------------------------#

#-----------------------Fortran----------------------------------------------------------------#


# General target for most named targets in fort_progs - includes all dependencies, flags and proprocessor variables
%: $(OBJ) version.o %.f90 
	$(F90COMP) $(FLAGS) $(PROC_OPT) $(D) -o $@ $^ $(MKL) $(MODLINK)$(LDIR); $(PERMS) $@; $(SUDO) mv $@ $(ROOTBIN);

# Target for orbs - needs --heap arrays which should NOT be enabled for all programs
# this wont work for gfortran - really I think the code needs rewriting to be better but I cant remember where the problem is...! JK
openmolcas_orbs: $(OBJ) version.o openmolcas_orbs.f90 
	$(F90COMP) $(HEAPARRAYS) $(FLAGS) $(PROC_OPT) $(D) -o $@ $^ $(MKL) $(MODLINK)$(LDIR); $(PERMS) $@; $(SUDO) mv $@ $(ROOTBIN);

# Module for repository/code version 
version.o: 
	echo "$$GIT_VER" > version.f90
	$(F90COMP) $(FLAGS) -c version.f90 -o $*.o
	rm version.f90

# Rule for any object file
$(OBJ) : %.o : %.f90
	$(F90COMP) $(FLAGS) $(PROC_OPT) -c $< -o $*.o $(MKL) $(MODLINK)$(LDIR)

#-----------------------Python-----------------------------------------------------------------#
# General Rule for all named targets in py_progs
%: %.py
	chmod +x $^; cp $^ $(ROOTBIN)$@
#----------------------------------------------------------------------------------------------#

#-----------------------Misc.------------------------------------------------------------------#
clean:
	rm -rf *.mod *.dSYM *.o; rm -rf $(LDIR)/*.mod $(LDIR)/*.dSYM $(LDIR)/*.o
#----------------------------------------------------------------------------------------------#


#-----------------------Clean bin directory----------------------------------------------------#
clean_bin:
	rm -f $(ROOTBIN)*
#----------------------------------------------------------------------------------------------#


#-----------------------Move compiled files to user bin----------------------------------------#
move_all:
	$(SUDO) mv $(ROOTBIN)* $(BIN)
#----------------------------------------------------------------------------------------------#
