#! /usr/bin/env python3



#┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
#││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
#┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘

#############################################################
#
#
#   cpol.py
#
#   This program is a part of molcas_suite
#
#   Calculates circularly polarised light transition intensities
#   
#      Author:
#       Julia Evans
#
#
#############################################################

import h5py
import numpy as np
import numpy.linalg as la
import argparse

def read_in_command_line():
    # Read in command line arguments

    parser = argparse.ArgumentParser(description='Calculates circularly polarised light transition intensities')
    parser.add_argument('input_file', type=str, metavar='<input_file>',
                        help='Molcas hdf5 rassi file')

    user_args = parser.parse_args()

    return user_args

# Read input from command line
user_args = read_in_command_line()

#input from user to get filename
f = h5py.File(user_args.input_file, 'r')

#Spin-orbit energies
ESOC = f['SOS_ENERGIES'][:]

#Number of spin-orbit states
nSOC = ESOC.size
print("Number of spin-orbit coupled state = ")
print(nSOC)

#Spin-orbit Hamiltonian
HSOC = f['HSO_MATRIX_REAL'][:] + 1j*f['HSO_MATRIX_IMAG'][:]

#Angular momentum components between the spin-orbit states stored as <SOS1|iL(x,y,z)|SOS2> in [3,nSOC,nSOC]
LxyzSOC = f['SOS_ANGMOM_REAL'][:] + 1j*f['SOS_ANGMOM_IMAG'][:]

#Spin momentum components between the spin-orbit states stored as <SOS1|SPIN(x,y,z)|SOS2> in [3,nSOC,nSOC]
SxyzSOC = f['SOS_SPIN_REAL'][:] + 1j*f['SOS_SPIN_IMAG'][:]

#Electric dipole moment components between the spin-orbit states stored as <SOS1|ED(x,y,z)|SOS2> in [3,nSOC,nSOC]
DipoleSOC = f['SOS_EDIPMOM_REAL'][:] + 1j*f['SOS_EDIPMOM_IMAG'][:]

#Eigenvectors of the spin-orbit Hamiltonian, expressed as linear combinations of the spin-free states in [nSOC,nSOC], fast index corresponds to spin-free states.
USOC = f['SOS_COEFFICIENTS_REAL'][:] + 1j*f['SOS_COEFFICIENTS_IMAG'][:]
#Need to take conjugae transpose as molcas vecs are not in standard notation
USOC = np.conjugate(np.transpose(USOC))

#H = U.D.U^-1
# U is unitary, so U^-1 = U^T^*
#U^-1.H.U = D

HSOC_check = USOC@np.diag(ESOC)@la.inv(USOC)

diff = np.sum(np.sum(np.abs(HSOC_check-HSOC), axis=0), axis=0)/(nSOC*nSOC)
# print("diff {:e}".format(diff))

#Hzee = mu_B*(L + g*S).B
#(L + S).B = (Lx+Sx)Bx + (Ly+Sy)By + (Lz+Sz)Bz

mu_B = 2.12719144e-6
g = 2.0023193043617
B = [0,0,1]
Hzee = mu_B*((LxyzSOC[0] + g*SxyzSOC[0])*B[0] + (LxyzSOC[1] + g*SxyzSOC[1])*B[1] + (LxyzSOC[2] + g*SxyzSOC[2])*B[2])
#Hzee = mu_B*(LxyzSOC + g*SxyzSOC)*np.transpose(B)

# write total hamiltonian in same basis as HSOC
Htot = HSOC + Hzee

# Find eigenvalues (D) and eigenvectors (Ptot) of total hamiltonian
# (Ptot^-1)*Htot*Ptot = D (Ptot is a matrix where each column is an eigenvector of Htot)
D, Ptot = la.eigh(Htot)
# for i in range(16):
# print(Ptot[:, i])
# print("_")

# Transform DipoleSOC matrices into eigenbasis of Htotal
DipoleTEx = la.inv(Ptot)@DipoleSOC[0]@Ptot
DipoleTEy = la.inv(Ptot)@DipoleSOC[1]@Ptot
DipoleTEz = la.inv(Ptot)@DipoleSOC[2]@Ptot

# since we added a magnetic field the states aren't the same as in rassi, need to check coefficients of Ptot?
# matrix to heatmap function from leti?


# evaluate circularly polarised transition intensities
# loop over states i 0 to 13 (1-14) and j 14 and 15 (15 and 16) -- python indexes from 0
SOstates_4f = range(14)
SOstates_5d = [14, 15]
for i in SOstates_4f:
    for k in SOstates_5d:
        # x + iy element:
        print("Transition from state " + str(i) + " to state " + str(k))
        print("Right handed: ")
        print(abs(DipoleTEx[i, k] + 1j * DipoleTEy[i, k])**2)
        print("_")
        # x - iy element:
        print("Left handed: ")
        print(abs(DipoleTEx[i, k] - 1j * DipoleTEy[i, k])**2)
        print("_")

# save into empty array?
# print out intensities


