!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!   molcas_orbs.f90 
!
!   This program is a part of molcas_suite
!
!
!   Extracts orbital compositions in terms of atomic orbitals 
!   from molcas RasOrb file and prints to screen
!
!   
!      Authors:
!       Nicholas Chilton
!       Commented by Jon Kragskow
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

PROGRAM molcas_orbs
    USE prec
    USE matrix_tools
    IMPLICIT NONE
    
    CHARACTER(LEN = 500)            :: input_file, line
    INTEGER                         :: basis,i,j,k,wrote,num_orb_list, found,IOstatus, col
    INTEGER,ALLOCATABLE             :: order(:)
    REAL(KIND = WP)                 :: length,thresh
    REAL(KIND = WP),ALLOCATABLE     :: coeffs(:,:),occ(:),energy(:),percents(:,:),orb_percents(:),C_mat(:,:),I_mat(:,:),S_mat(:,:),mat(:,:),D_mat(:,:),V_mat(:,:),eigs(:),rootS_mat(:,:)
    CHARACTER(LEN = 1),ALLOCATABLE  :: index(:)
    CHARACTER(LEN = 13),ALLOCATABLE :: labels(:)
    CHARACTER(LEN = 8),ALLOCATABLE  :: orb_list(:)
    CHARACTER(LEN = 2)              :: Cdummy,type
    CHARACTER(LEN = 8)              :: basischar
    
    !Read user inputs
    CALL READ_USER_INPUT(input_file, type, thresh)
    
    ! Write deprecation warning
    CALL WARNING('Warning: molcas_orbs is deprecated - use openmolcas_orbs', 6)
    
    OPEN(30,FILE = TRIM(ADJUSTL(input_file)), STATUS = 'UNKNOWN')
    READ(30,*)
    READ(30,*)
    READ(30,*)
    READ(30,*)
    READ(30,*) basis
    ALLOCATE(coeffs(basis,basis),occ(basis),energy(basis),index(basis),percents(basis,basis),order(basis),labels(basis),orb_list(basis),C_mat(basis,basis),I_mat(basis,basis),S_mat(basis,basis),mat(basis,basis),D_mat(basis,basis),V_mat(basis,basis),eigs(basis),rootS_mat(basis,basis))
    WRITE(6,*) 'Basis =',basis
    DO while(.true.)
        READ(30,'(A)',END=60) line
        IF(line(1:4) == '#ORB') then
            WRITE(6,*) 'Reading MOs...'
            DO j = 1,basis
                READ(30,*)
                DO i = 1,basis/4
                    READ(30,'(4E18.12E2)') (coeffs(j,col), col = i*4-3,i*4)
                END DO
                IF(mod(basis,4) == 1) READ(30,'(1E18.12E2)') coeffs(j,basis)
                IF(mod(basis,4) == 2) READ(30,'(2E18.12E2)') (coeffs(j,col), col = basis-1,basis)
                IF(mod(basis,4) == 3) READ(30,'(3E18.12E2)') (coeffs(j,col), col = basis-2,basis)
                length = 0.0_WP
                DO i = 1,basis
                    length = length + coeffs(j,i)*coeffs(j,i)
                    percents(j,i) = coeffs(j,i)*coeffs(j,i)
                END DO
                percents(j,:) = percents(j,:)/length
                !coeffs(j,:) = coeffs(j,:)/dsqrt(length)
            END DO
            percents = 100.0_WP*percents
            WRITE(6,*) '...done'
        ELSE IF(line(1:4) == '#OCC') then
            WRITE(6,*) 'Reading OCCs...'
            READ(30,*)
            DO i = 1,basis/4
                READ(30,'(4E18.12E2)') occ(i*4-3:i*4)
            END DO
            IF(mod(basis,4) == 1) READ(30,'(1E18.12E2)') occ(basis)
            IF(mod(basis,4) == 2) READ(30,'(2E18.12E2)') (occ(col), col = basis-1,basis)
            IF(mod(basis,4) == 3) READ(30,'(3E18.12E2)') (occ(col), col = basis-2,basis)
            WRITE(6,*) '...done'
        ELSE IF(line(1:4) == '#ONE') then
            WRITE(6,*) 'Reading ENERGIES...'
            READ(30,*)
            DO i = 1,basis/4
                READ(30,'(4E18.12E2)') energy(i*4-3:i*4)
            END DO
            IF(mod(basis,4) == 1) READ(30,'(1E18.12E2)') energy(basis)
            IF(mod(basis,4) == 2) READ(30,'(2E18.12E2)') (energy(col), col = basis-1,basis)
            IF(mod(basis,4) == 3) READ(30,'(3E18.12E2)') (energy(col), col = basis-2,basis)
            WRITE(6,*) '...done'
        ELSE IF(line(1:6) == '#INDEX') then
            WRITE(6,*) 'Reading INDICIES...'
            READ(30,*)
            DO i = 1,basis/10
                READ(30,'(A2,10A1)') Cdummy,index(i*10-9:i*10)
            END DO
            IF(mod(basis,10) == 1) READ(30,'(A2,1A1)') Cdummy,index(basis)
            IF(mod(basis,10) == 2) READ(30,'(A2,2A1)') Cdummy,(index(col), col = basis-1,basis)
            IF(mod(basis,10) == 3) READ(30,'(A2,3A1)') Cdummy,(index(col), col = basis-2,basis)
            IF(mod(basis,10) == 4) READ(30,'(A2,4A1)') Cdummy,(index(col), col = basis-3,basis)
            IF(mod(basis,10) == 5) READ(30,'(A2,5A1)') Cdummy,(index(col), col = basis-4,basis)
            IF(mod(basis,10) == 6) READ(30,'(A2,6A1)') Cdummy,(index(col), col = basis-5,basis)
            IF(mod(basis,10) == 7) READ(30,'(A2,7A1)') Cdummy,(index(col), col = basis-6,basis)
            IF(mod(basis,10) == 8) READ(30,'(A2,8A1)') Cdummy,(index(col), col = basis-7,basis)
            IF(mod(basis,10) == 9) READ(30,'(A2,9A1)') Cdummy,(index(col), col = basis-8,basis)
            WRITE(6,*) '...done'
        END IF
    END DO
    60 CONTINUE
    CLOSE(30)
    OPEN(30,FILE = 'basis', STATUS = 'UNKNOWN')
    WRITE(6,*) 'Reading BASIS...'
    j = 0
    wrote = 0
    DO i = 1,basis
        READ(30,'(A)',END=61) line
        j = j+1
        labels(i) = line(1:13)
    END DO
    WRITE(6,*) '...done'
    num_orb_list = 1
    orb_list(1) = labels(1)(1:8)
    DO j = 2,basis
        found = 0
        DO i = 1,num_orb_list
            IF(labels(j)(1:8) == orb_list(i)) then
                found = 1
                EXIT
            END IF
        END DO
        IF(found == 0) then
            num_orb_list = num_orb_list + 1
            orb_list(num_orb_list) = labels(j)(1:8)
        END IF
    END DO
    IF(TRIM(type) == 'wf') then
        DO j = 1,basis
            call re_order(basis,percents(j,:),order,'max')
            WRITE(6,'(A8,I3,A8,F4.2,A11,E19.12E2,A10,A1,A5)') '---- MO ',j,', occ = ',occ(j),', energy = ',energy(j),', index = ',index(j),' ----'
            DO i = 1,basis
                IF(percents(j,order(i)) >= thresh) then
                    WRITE(6,'(A14,F8.3,E20.12E2)') labels(order(i)),percents(j,order(i)),coeffs(j,order(i))
                ELSE
                    EXIT
                END IF
            END DO
        END DO
        wrote = 1
    ELSE IF(TRIM(type) == '%') then
        ALLOCATE(orb_percents(num_orb_list))
        DO j = 1,basis
            orb_percents = 0.0_WP
            WRITE(6,'(A8,I3,A8,F4.2,A11,E19.12E2,A10,A1,A5)') '---- MO ',j,'  occ = ',occ(j),'  energy = ',energy(j),'  index = ',index(j),' ----'
            DO i = 1,basis
                DO k = 1,num_orb_list
                    IF(labels(i)(1:8) == orb_list(k)) orb_percents(k) = orb_percents(k) + percents(j,i)
                END DO
            END DO
            call re_order(num_orb_list,orb_percents,order,'max')
            DO k = 1,num_orb_list
                IF(orb_percents(order(k)) >= thresh) WRITE(6,'(A9,F8.3)') orb_list(order(k)),orb_percents(order(k))
            END DO
        END DO
        wrote = 1
        DEALLOCATE(orb_percents)
    END IF
    61 CONTINUE
    IF(wrote == 0) WRITE(6,*) "Basis expected =",basis,"; Basis found =",j," or other error"
    CLOSE(30)
    DEALLOCATE(coeffs,occ,energy,index,percents,order,labels,orb_list,C_mat,I_mat,S_mat,mat)

CONTAINS

SUBROUTINE READ_USER_INPUT(input_file, type, thresh)
    USE version, ONLY : PRINT_VERSION
    IMPLICIT NONE
    REAL(KIND = WP)                 :: thresh
    CHARACTER(LEN = 500)            :: line
    CHARACTER(LEN = *), INTENT(OUT) :: input_file, type
    LOGICAL                         :: file_exists

    CALL GET_COMMAND_ARGUMENT(1,input_file)

    !Print help
    IF(TRIM(input_file) == '' .OR. TRIM(input_file) == '-h' .OR. TRIM(input_file) == '-H') THEN
        ! Write deprecation warning
        CALL WARNING('Warning: molcas_orbs is deprecated - use openmolcas_orbs', 6)
        WRITE(6,*) 'molcas_orbs <orbital_file> [<type=%/wf> <threshold>]'
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') 'Extracts orbital compositions in terms of atomic orbitals from molcas RasOrb file and prints to screen'
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') 'orbital_file      : CHARACTER          RasOrb file                                               e.g  1.RasOrb'
        WRITE(6,'(A)') 'type              * OPTIONAL CHARACTER print % contribution or the raw value of the wavefunction e.g  % or wf'
        WRITE(6,'(A)') 'threshold         * OPTIONAL NUMBER    threshold for printing orbital contributions (default 1%) e.g  0.5'
        WRITE(6,'(A)') ''
        WRITE(6,'(A)') 'Example ------ molcas_orbs 1.RasOrb % 0.5'
        WRITE(6,'(A)') ''
        STOP
    END IF

    ! Print version number
    IF (TRIM(input_file) == '-v' .OR. TRIM(input_file) == '-version' .OR. TRIM(input_file) == 'version') THEN
        CALL PRINT_VERSION('molcas_orbs')
        STOP
    END IF

    !Check if file exists
    INQUIRE(FILE=TRIM(input_file), EXIST=file_exists)
    IF (file_exists .EQV. .FALSE.) THEN
        WRITE(6,'(A)') 'Specified file does not exist'
        STOP
    END IF


    CALL GET_COMMAND_ARGUMENT(2,type)
    !Check if type is correct
    IF(TRIM(type) /= '') then
        IF(TRIM(type) /= '%' .and. TRIM(type) /= 'wf') then
            WRITE(6,*) 'TYPE must be % or wf'
            STOP
        END IF
    ELSE
        type = '%'
    END IF

    !Check if thresh provided
    CALL GET_COMMAND_ARGUMENT(3,line)
    IF(TRIM(line) /= '') then
        READ(line,*,IOSTAT=IOstatus) thresh
        IF(IOstatus /= 0) thresh = 1.0_WP
    ELSE
        thresh = 1.0_WP
    END IF

END SUBROUTINE READ_USER_INPUT

SUBROUTINE WARNING(title, unit)
    ! Writes a warning header to unit
    ! Three lines
    ! *********
    ! * title *
    ! *********
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: title
    INTEGER, INTENT(IN)            :: unit

    INTEGER                        :: num_l_spaces, num_r_spaces

    num_l_spaces = (70 - LEN_TRIM(title)) / 2
    num_r_spaces = (70 - LEN_TRIM(title)) / 2

    IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
        num_l_spaces = num_l_spaces + 1

    END IF

    WRITE(unit,'(A)')   REPEAT('*',80)
    WRITE(unit, '(5A)') REPEAT('*',5), REPEAT(' ',num_l_spaces), TRIM(title), &
                        REPEAT(' ',num_r_spaces), REPEAT('*',5)
    WRITE(unit,'(A)')   REPEAT('*',80)

END SUBROUTINE WARNING

RECURSIVE SUBROUTINE re_order(num,values,list,way)
    IMPLICIT NONE
    INTEGER,INTENT(IN)                      :: num
    INTEGER,INTENT(OUT),DIMENSION(:)        :: list(:)
    INTEGER                                 :: j,k
    REAL(KIND = WP),INTENT(IN),DIMENSION(:) :: values(:)
    REAL(KIND = WP)                         :: low
    INTEGER,ALLOCATABLE                     :: done(:)
    CHARACTER(LEN = 3),INTENT(IN)           :: way

    ALLOCATE(done(num))
    list(:) = 0
    done(:) = 0
    list(1) = minloc(values,1)
    done(list(1)) = 1
    DO j=2,num
        low = 1D99
        DO k=1,num
            IF((values(k) - values(list(1))) >= (values(list(j-1)) - values(list(1))) .and. (values(k) - values(list(1))) < low .and. done(k) == 0) then
                low = (values(k) - values(list(1)))
                list(j) = k
            END IF
        END DO
        done(list(j)) = 1
    END DO
    IF(way == 'max') then
        DO j = 1,num
            done(j) = list(num-j+1)
        END DO
        list = done
    END IF
    deallocate(done)
END SUBROUTINE re_order


END PROGRAM molcas_orbs
