!┌┬┐┌─┐┬  ┌─┐┌─┐┌─┐  ┌─┐┬ ┬┬┌┬┐┌─┐
!││││ ││  │  ├─┤└─┐  └─┐│ ││ │ ├┤ 
!┴ ┴└─┘┴─┘└─┘┴ ┴└─┘  └─┘└─┘┴ ┴ └─┘


program barrier_figure
implicit none

character(len=500)::arg,line,history_save_char
character(len=9)::col
character(len=3)::prob
character(len=10)::label
integer::i,j,k,Iprob,pass,counter,sideA,sideB,num_f,num_states,debug,step_limit,skip_ueff,skip_rotate
integer,allocatable::history(:),history_save(:),list(:)
real(kind=8)::sum,angles(3),rando,temp,gJ,Ueff,threshold,store,total_prob,prob_Ueff(2)
real(kind=8),allocatable::energy(:),moment(:),matrix(:,:),basis(:),probability(:)
logical::new

new = .true.
call get_command_argument(1,arg)
if(trim(arg) == '-h') then
    write(6,'(A)') 'barrier_figure <file> <num_f_electrons> [<old/new>] [<skip-rotate?>] [<skip_Ueff?>] [<-d>]'
    stop
end if
call get_command_argument(2,line)
read(line,*) num_f
if(num_f < 1 .or. num_f > 13) then
    write(6,'(A)') 'Number of f electrons must be > 0 and < 14'
    write(6,'(A)') 'And currently limited to 8, 9, 10 or 11'
    stop
end if
line = ''
debug = 0
skip_ueff = 0
skip_rotate = 0
step_limit = 30
call get_command_argument(3,line)
if(trim(line) == 'old') new = .false.
call get_command_argument(4,line)
if(trim(line) == 'y') skip_rotate = 1
call get_command_argument(5,line)
if(trim(line) == 'y') skip_ueff = 1
call get_command_argument(6,line)
if(trim(line) == '-d') debug = 1
if(num_f == 1) num_states = 6
if(num_f == 2) num_states = 9
if(num_f == 3) num_states = 10
if(num_f == 4) num_states = 9
if(num_f == 5) num_states = 6
if(num_f == 6) num_states = 1
if(num_f == 7) num_states = 8
if(num_f == 8) num_states = 13
if(num_f == 9) num_states = 16
if(num_f == 10) num_states = 17
if(num_f == 11) num_states = 16
if(num_f == 12) num_states = 13
if(num_f == 13) num_states = 8

if(num_f == 8) then
    gJ = 1.5_8
else if(num_f == 9) then
    gJ = 4.0_8/3.0_8
else if(num_f == 10) then
    gJ = 5.0_8/4.0_8
else if(num_f == 11) then
    gJ = 6.0_8/5.0_8
else if(num_f == 13) then
    gJ = 8.0_8/7.0_8
else
    write(6,'(A)') 'Number of f electrons must be > 0 and < 14'
    write(6,'(A)') 'And currently limited to 8, 9, 10, 11 or 13'
    stop
end if

allocate(energy(num_states),moment(num_states),matrix(num_states,num_states),basis(num_states),probability(num_states-1),history(step_limit),history_save(step_limit))

if(skip_rotate == 0) then
    !run PHI and rotate CFPs to get ground state quantized along z-axis
    if(debug == 1) write(6,'(A)') "Starting CFP rotation"
    pass = 0
    counter = 0
    open(31,file=trim(arg)//'_fit_G.exp',status='unknown')
    ! if(num_f == 1) write(31,'(A)') '0 0 20 1 0 0 0 1 0 0 0 1'
    ! if(num_f == 2) write(31,'(A)') '0 0 20 1 0 0 0 1 0 0 0 1'
    ! if(num_f == 3) write(31,'(A)') '0 0 20 1 0 0 0 1 0 0 0 1'
    ! if(num_f == 4) write(31,'(A)') '0 0 20 1 0 0 0 1 0 0 0 1'
    ! if(num_f == 5) write(31,'(A)') '0 0 20 1 0 0 0 1 0 0 0 1'
    ! if(num_f == 6) write(31,'(A)') '0 0 20 1 0 0 0 1 0 0 0 1'
    ! if(num_f == 7) write(31,'(A)') '0 0 20 1 0 0 0 1 0 0 0 1'
    if(num_f == 8) write(31,'(A)') '0 0 18 1 0 0 0 1 0 0 0 1'
    if(num_f == 9) write(31,'(A)') '0 0 20 1 0 0 0 1 0 0 0 1'
    if(num_f == 10) write(31,'(A)') '0 0 20 1 0 0 0 1 0 0 0 1'
    if(num_f == 11) write(31,'(A)') '0 0 18 1 0 0 0 1 0 0 0 1'
    ! if(num_f == 12) write(31,'(A)') '0 0 20 1 0 0 0 1 0 0 0 1'
    if(num_f == 13) write(31,'(A)') '0 0 8 1 0 0 0 1 0 0 0 1'
    close(31)
    do while(pass == 0)
        counter = counter + 1
        if(counter > 10) then
            write(6,'(A)') 'WARNING - more than 10 attempts needed for '//trim(arg)
        end if
        open(31,file=trim(arg)//'_fit.input',status='unknown')
        write(31,'(A)') '****spin'
        write(31,'(I2)') num_states-1
        write(31,'(A)') '****gfactor'
        write(31,'(A2,F13.10)') '1 ',gJ
        write(31,'(A)') '****crys'
        open(30,file=trim(arg))
        read(30,'(A)') line
        if((num_f/2)*2 /= num_f) k = num_states/2 + 1
        if((num_f/2)*2 == num_f) k = num_states + 1
        if(line(1:9) == '   Energy') then
            do i = 1,k
                read(30,'(A)') line
            end do
            do i = 1,27
                read(30,'(A)') line
                write(31,'(A)') trim(line)
            end do
        else
            write(31,'(A)') trim(line)
            do i = 1,26
                read(30,'(A)') line
                write(31,'(A)') trim(line)
            end do
        end if
        close(30)
        write(31,'(A)') '****fit'
        write(31,'(A)') 'simplex'
        call random_number(rando)
        write(31,'(F6.3)') rando*10.0_8
        write(31,'(A)') 'rc 1 1 0'
        write(31,'(A)') '----'
        call random_number(rando)
        write(31,'(F6.3)') rando*10.0_8
        write(31,'(A)') 'rc 1 2 0'
        write(31,'(A)') '----'
        call random_number(rando)
        write(31,'(F6.3)') rando*10.0_8
        write(31,'(A)') 'rc 1 3 0'
        write(31,'(A)') '----'
        write(31,'(A)') '****params'
        write(31,'(A)') 'opmode fit d'
        write(31,'(A)') 'fullwf'
        write(31,'(A)') 'nooef'
        write(31,'(A)') 'noprint'
        write(31,'(A)') '****end'
        close(31)
        call system('phi_dHCF '//trim(arg)//'_fit > tmp')
        open(30,file='tmp',status='unknown')
        do i = 1,29
            read(30,*)
        end do
        read(30,'(A)') line
        if(line(1:21) == 'Finished Simplex with') then
            pass = 1
            read(30,*)
            read(30,*) angles(1)
            read(30,*)
            read(30,*)
            read(30,*) angles(2)
            read(30,*)
            read(30,*)
            read(30,*) angles(3)
        else
            continue
        end if
        close(30)
    end do
    call system('rm '//trim(arg)//'_fit_G.res')
    call system('rm '//trim(arg)//'_fit_G.exp')
    call system('rm tmp')
    call system('rm '//trim(arg)//'_fit.input')
end if
    
!run PHI with rotated CFPs to get transition elements etc
if(debug == 1) write(6,'(A)') "Calculating transition probs"
open(31,file=trim(arg)//'.input',status='unknown')
write(31,'(A)') '****spin'
write(31,'(I2)') num_states-1
write(31,'(A)') '****gfactor'
write(31,'(A2,F13.10)') '1 ',gJ
write(31,'(A)') '****crys'
open(30,file=trim(arg))
read(30,'(A)') line
if((num_f/2)*2 /= num_f) k = num_states/2 + 1
if((num_f/2)*2 == num_f) k = num_states + 1
if(line(1:9) == '   Energy') then
    do i = 1,k
        read(30,'(A)') line
    end do
    do i = 1,27
        read(30,'(A)') line
        write(31,'(A)') trim(line)
    end do
else
    write(31,'(A)') trim(line)
    do i = 1,26
        read(30,'(A)') line
        write(31,'(A)') trim(line)
    end do
end if
close(30)
write(31,'(A)') '****params'
write(31,'(A)') 'opmode sim l'
write(31,'(A)') 'fullwf'
write(31,'(A)') 'nooef'
write(31,'(A)') 'noprint'
!write(31,'(A)') 'staticb 0.01 0 0 1'
if(skip_rotate == 0) write(31,'(A9,3F14.8)') 'rotate 1 ',angles
write(31,'(A)') '****end'
close(31)
call system('phi_dHCF '//trim(arg)//' > /dev/null')
!call system('rm '//trim(arg)//'_levels.res')
!call system('rm '//trim(arg)//'_G.res')

if(debug == 1) write(6,'(A)') "Reading transition probs"
open(30,file=trim(arg)//'_states.res')
read(30,*)
read(30,*)
read(30,*)
read(30,*)
read(30,'(A)') line
do i = 1,num_states
    read(line((i-1)*12+7:(i-1)*12+18),'(E12.4E2)') energy(i)
end do
do i = 1,12+2*num_states
    read(30,*)
end do
if(debug == 1) write(6,'(A)') "States:"
do j = 1,num_states
    read(30,'(A)') line
    if(debug == 1) write(6,'(A)') trim(line)
    read(line(1:6),'(F6.1)') basis(j)
    if(debug == 1) write(6,'(F6.1)') basis(j)
    do i = 1,num_states
        read(line((i-1)*12+7:(i-1)*12+18),'(F12.7)') matrix(j,i)
        if(debug == 1) write(6,'(F12.6)') matrix(j,i)
    end do
end do
matrix = matrix*0.01_8
moment = 0.0_8
do i = 1,num_states
    do j = 1,num_states
        moment(i) = moment(i) + basis(j)*matrix(j,i)
    end do
end do
!moment = moment*gJ !(without this you have <Jz>)
do while(.true.)
    read(30,'(A)') line
    if(line(1:24) == 'Transition Probabilities') exit
end do
read(30,'(A)') line
read(30,'(A)') line
read(30,'(A)') line
read(30,'(A)') line
matrix = 0.0_8
do j = 1,num_states
    read(30,'(A)') line
    do i = 1,num_states
        if(i /= j) read(line((i-1)*12+13:(i-1)*12+24),'(E12.4E2)') matrix(j,i)
    end do
end do
if(debug == 1) write(6,'(A)') 'Input transition matrix:'
do j = 1,num_states
    if(debug == 1) write(6,'(16E10.3E2)') matrix(j,:)
end do
sideA = 1
sideB = 2
if(moment(2) < moment(1)) then
    sideA = 2
    sideB = 1
end if
!matrix(:,sideB) = 0.0_8
do i = 1,num_states !from
    do j = 1,num_states !to
        if(i == j) then
            matrix(j,i) = 0.0_8
        !else if(moment(i) < 0.0_8 .and. energy(j) < energy(i)) then
        !   matrix(j,i) = 0.0_8
        !else if(moment(i) > 0.0_8 .and. energy(j) > energy(i)) then
        !   matrix(j,i) = 0.0_8
        !else if(moment(i) == 0.0_8 .and. energy(j) > energy(i)) then
        !   matrix(j,i) = 0.0_8
        end if
        if(moment(i) > 0.0_8 .and. moment(j) < 0.0_8) matrix(j,i) = 0.0_8           ! exclude backwards steps across barrier
        !if(moment(j) < moment(i) .and. moment(i) < moment(sideB)) matrix(j,i) = 0.0_8                              ! exclude most backwards steps
        if(moment(i) > moment(j)) matrix(j,i) = 0.0_8   !no backwards steps!
    end do
end do

if(new) then
    if(debug == 1) then
        write(6,*) "Moments"
        do i = 1,num_states
            write(6,*) i,moment(i)
        end do
    end if
    do i = 1,num_states
        sum = 0.0_8
        do j = 1,num_states
            sum = sum + matrix(j,i)
        end do
        if(sum > 0.0_8) matrix(:,i) = matrix(:,i)/sum
    end do
    if(debug == 1) then
        write(6,*) "Unscaled"
        do i = 1,num_states
            write(6,'(16E24.14E3)') matrix(i,:)
        end do
    end if
    allocate(list(num_states))
    call re_order(num_states,moment,list,'min')
    if(debug == 1) then
        write(6,*) "List"
        do i = 1,num_states
            write(6,*) list(i)
        end do
    end if
    if(debug == 1) write(6,*) "scaling..."
    do i = 2,num_states
        sum = 0.0_8
        do j = i-1,1,-1
            sum = sum + matrix(list(i),list(j))
            if(debug == 1) write(6,*) i,j,matrix(list(i),list(j)),sum
        end do
        if(debug == 1)write(6,*) "scaling",list(i)
        matrix(:,list(i)) = matrix(:,list(i))*sum
        if(debug == 1) then
            write(6,*)
            do j = 1,num_states
                write(6,'(16E24.14E3)') matrix(j,:)
            end do
            write(6,*)
            write(6,*)
        end if
    end do
    deallocate(list)
    if(debug == 1) then
        write(6,*) "Scaled"
        do i = 1,num_states
            write(6,'(16E24.14E3)') matrix(i,:)
        end do
    end if
else
    do i = 1,num_states
        if(moment(i) < moment(sideA) .and. i /= sideA) matrix(:,i) = 0.0_8
        if(moment(i) > moment(sideB) .and. i /= sideB) matrix(i,:) = 0.0_8
    end do
    do i = 1,num_states
        sum = 0.0_8
        do j = 1,num_states
            sum = sum + matrix(j,i)
        end do
        if(sum > 0.0_8) matrix(:,i) = matrix(:,i)/sum
    end do
end if

if(debug == 1) write(6,'(A)') 'Final transition matrix:'
do j = 1,num_states
    if(debug == 1) write(6,'(16I4)') nint(100.0_8*matrix(j,:))
end do
if(debug == 1) write(6,*)

if(skip_ueff == 0) then
    if(debug == 1) write(6,'(A)') "Calculating Ueff (Largest step)"
    sum = 0.0_8
    history(1) = sideA
    history(2) = maxloc(matrix(:,history(1)),1)
    k = 2
    if(debug == 1) write(6,*) history(1)
    if(debug == 1) write(6,*) history(2)
    do while(.true.)
        k = k+1
        if(k > step_limit) then
            write(6,'(A)') 'Step limit exceeded in History search, aborting'
            sum = 0.0_8
            exit
        end if
        history(k) = maxloc(matrix(:,history(k-1)),1)
        if(debug == 1) write(6,*) history(k)
        !write(6,*) history(k)
        !pause
        if(history(k) == history(k-2)) then
            temp = 0.0_8
            do i = 1,num_states
                if(matrix(i,history(k-1)) > temp .and. matrix(i,history(k-1)) < maxval(matrix(:,history(k-1)),1)) then
                    history(k) = i
                    temp = matrix(i,history(k-1))
                end if
            end do
        end if
        !write(6,*) history(k),Energy(history(k)),sum
        if(Energy(history(k)) > sum) sum = Energy(history(k))
        if(history(k) == sideB) exit
    end do

    if(debug == 1) write(6,'(A)') "Calculating Ueff (Weighted)"
    do j = 20,20
        !write(6,*) '%%%%',j,'%%%%'
        Ueff = 0.0_8
        prob_Ueff = 0.0_8
        total_prob = 0.0_8
        history(1) = sideA
        threshold = 10.0_8**(-j)
        !write(6,'(2E24.15E2,3I3)') 0.0_8,matrix(sideB,sideA),2,sideA,sideB
        total_prob = total_prob + matrix(sideB,sideA)
        prob_Ueff(1) = matrix(sideB,sideA)
        do i = 3,20
            call step(i,2,sideA,0.0_8,1.0_8)
        end do
        write(6,'(A,3I5)') 'cm-1 ',nint(sum),nint(prob_Ueff(2)),nint(Ueff/total_prob)
        write(6,'(A,3I5)') 'K    ',nint(sum/0.6950387_8),nint(prob_Ueff(2)/0.6950387_8),nint((Ueff/total_prob)/0.6950387_8)
        !write(6,'(A)') trim(history_save_char)
    end do
end if

matrix = 100.0_8*matrix

if(debug == 1) write(6,'(A)') "Writing .plt"
open(30,file=trim(arg)//'.plt',status='unknown')
write(30,'(A)') 'set terminal pdf background "#ffffff" font "Arial,12"'
write(30,'(A)') 'set pointsize 1.2'
!write(30,'(A)') 'set xlabel "Magnetic moment ({/Symbol m}_B)"'
write(30,'(A)') 'set xlabel "<J_z>"'
write(30,'(A)') 'set ylabel "Energy (cm^{-1})"'
write(30,'(A)') 'set y2label "Energy (K)"'
!set y2tics 1000 nomirror textcolor lt 3;
write(30,'(A)') "set output '"//trim(arg)//".pdf'" 
write(30,'(A11,F6.2,A1,F6.2,A1)') 'set xrange[',1.2*(minval(moment(:))-0.4),':',1.2*(maxval(moment(:))+0.4),']'
write(30,'(A12,F7.1,A1,F7.1,A1)') 'set yrange[-',maxval(energy(:))*0.08_8,':',maxval(energy(:))*1.08_8,']'
write(30,'(A13,F7.1,A1,F7.1,A1)') 'set y2range[-',maxval(energy(:))*0.08_8/0.6950387_8,':',maxval(energy(:))*1.08_8/0.6950387_8,']'
write(30,'(A)') 'set ytics 200 nomirror'
write(30,'(A)') 'set y2tics 200 nomirror'
if(2*(num_f/2) == num_f) then
    write(30,'(A)') 'set xtics (-8,-6,-4,-2,0,2,4,6,8)'
else
    write(30,'(A)') 'set xtics (-7.5,-5.5,-3.5,-1.5,0,1.5,3.5,5.5,7.5)'
end if
write(30,'(A)')
do i = 1,num_states
    write(30,'(A15,F6.2,A1,F7.1,A4,F6.2,A1,F7.1,A7)') 'set arrow from ',moment(i)-0.4,',',energy(i),' to ',moment(i)+0.4,',',energy(i),' nohead'
    !write(label,'(I2,A1,F7.1)') nint(dble(i)/2),' ',energy(i)
    write(label,'(I2,A1,F7.1)') i,' ',energy(i)
    if(moment(i) < 0.0_8) write(30,'(A25,F6.2,A1,F7.1,A21)') 'set label "'//label//'" at ',moment(i)-1.6,',',energy(i),' front font "Arial,6"'
    if(moment(i) > 0.0_8) write(30,'(A256,F6.2,A1,F7.1,A21)') 'set label "'//label//'" at ',moment(i)+0.6,',',energy(i),' front font "Arial,6"'
end do
k = 0
do i = 1,num_states
    do j = 1,num_states
        !if(matrix(j,i) >= 1.0_8) then
            k = k + 1
            Iprob = nint(matrix(j,i))
            write(prob,'(I3)') Iprob
            !write(30,'(A19,F6.2,A1,F7.1,A21)') 'set label "'//prob//'" at ',(moment(j)+moment(i))/2,',',(energy(j)+energy(i))/2,' front font "Arial,6"'
            col = '#C6FF0000'
            write(col(2:3),'(Z2)') 255-nint(255.0_8*matrix(j,i)/100.0_8)
            !write(6,'(I3,A1,Z2)') nint(255.0_8*matrix(j,i)/100.0_8),' ',nint(255.0_8*matrix(j,i)/100.0_8)
            if(col(2:2) == ' ') col(2:2) = '0'
            write(30,'(A15,F6.2,A1,F7.1,A4,F6.2,A1,F7.1,A21,A9,A1)') 'set arrow from ',moment(i),',',energy(i),' to ',moment(j),',',energy(j),' head filled lc rgb "',col,'"'
        !end if
    end do
end do
write(30,'(A)') 'plot "'//"<echo '0 0'"//'" w p ls 1 notitle'
write(30,'(A)')
close(30)
call system('gnuplot '//trim(arg)//'.plt')
call system('rm '//trim(arg)//'.plt')

if(debug == 1) write(6,'(A)') "Complete"

contains

recursive subroutine step(total,current,previous,max_en,prob)
implicit none
integer::total,i,current,previous
character(2)::fmt
real(kind=8)::max_en,prob,pass_en,pass_prob
if(dabs(prob) < threshold) return
if(current == total) then
    if(previous == sideB) return
    history(total) = sideB
    if(prob*matrix(sideB,previous) >= threshold) then
        write(fmt,'(I2)') total+1
        !write(6,'(2E24.15E2,'//fmt//'I3)') max_en,prob*matrix(sideB,previous),total,history(1:total)
        Ueff = Ueff + prob*matrix(sideB,previous)*max_en
        total_prob = total_prob + prob*matrix(sideB,previous)
        if(prob*matrix(sideB,previous) > prob_Ueff(1)) then
            prob_Ueff(1) = prob*matrix(sideB,previous)
            prob_Ueff(2) = max_en
            history_save = history
            write(history_save_char,'('//fmt//'I3)') history(1:total)
        end if
    end if
else
    do i=1,num_states
        if(i == sideB) cycle
        if(i == previous) cycle
        if(matrix(i,previous) == 0.0_8) cycle
        history(current) = i
        pass_en = max(Energy(i),max_en)
        pass_prob = prob*matrix(i,previous)
        call step(total,current+1,i,pass_en,pass_prob)
    end do
end if
end subroutine step

recursive subroutine step_down(steps_to_do,upper_index,max_en,prob)
implicit none
integer::steps_to_do,i,upper_index
real(kind=8)::max_en,prob,pass_en,pass_prob
!write(6,*) '----'
!write(6,*) steps_to_do,upper_index,max_en,prob
if(dabs(prob) < threshold) return
if(steps_to_do > 1) then
    do i=1,num_states
        !if(i == upper_index) cycle
        if(matrix(i,upper_index) == 0.0_8) cycle
        pass_en = max(Energy(i),max_en)
        pass_prob = prob*matrix(i,upper_index)
        write(6,*) '|'
        write(6,*) steps_to_do-1,i,pass_en,pass_prob
        call step_down(steps_to_do-1,i,pass_en,pass_prob)
    end do
else
    do i=1,num_states
        if(i == upper_index) cycle
        if(matrix(i,upper_index) == 0.0_8) cycle
        if(matrix(sideB,i) == 0.0_8) cycle
        !write(6,*) i,prob*matrix(i,upper_index),max(Energy(i),max_en)
        !write(6,*) sideB,prob*matrix(i,upper_index)*matrix(sideB,i),max(Energy(i),max_en)
        write(6,'(3I3,F6.1,F7.1)') upper_index,i,sideB,100*prob*matrix(i,upper_index)*matrix(sideB,i),max(Energy(i),max_en)
        Ueff = Ueff + prob*matrix(i,upper_index)*matrix(sideB,i)*max(Energy(i),max_en)
    end do 
end if
write(6,*) '===='
end subroutine step_down

subroutine re_order(num,values,list,way)
implicit none
integer,intent(in)::num
integer,intent(out),dimension(:)::list(:)
integer::j,k
real(kind=8),intent(in),dimension(:)::values(:)
real(kind=8)::low
integer,allocatable::done(:)
character(len=3),intent(in)::way
allocate(done(num))
list(:) = 0
done(:) = 0
list(1) = minloc(values,1)
done(list(1)) = 1
do j=2,num
    low = 1D99
    do k=1,num
        if((values(k) - values(list(1))) >= (values(list(j-1)) - values(list(1))) .and. (values(k) - values(list(1))) < low .and. done(k) == 0) then
            low = (values(k) - values(list(1)))
            list(j) = k
        end if
    end do
    done(list(j)) = 1
end do
if(way == 'max') then
    do j = 1,num
        done(j) = list(num-j+1)
    end do
    list = done
end if
deallocate(done)
end subroutine re_order

end program barrier_figure
