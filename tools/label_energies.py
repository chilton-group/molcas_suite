#!/usr/bin/env python3
'''
This is a script designed to plot energy level diagrams with individual level labels. 
The input file(s) should be plain-text and have energies in the first column and labels in the second column.
Lines that only contain energies will be plotted as unlabelled levels.
To label groups, include the label on the first line of the group, then use * to label subseqent levels.
LaTeX-style formatting of the labels is supported e.g. Greek letters ($\phi$),
subscripts ($_{subscript}$) and superscripts ($^{superscript}$).
Data from multiple files will be plotted side by side.
'''
import numpy as np
import matplotlib.pyplot as plt
import argparse


def energylvls_main(filelist, legend = None,
        linecoverage = 0.5, figsize = (8, 6), textsize = 10,
        units = 'cm-1', xwidth = 10):
    labels = []
    energies = []
    if legend is None:
        autolegend = []
    for fname in filelist:
        contents = read_energies_labels(fname.strip())
        energies.append(contents[0])
        labels.append(contents[1])
        if legend is None:
            autolegend.append(fname.split('.')[0])
    # Determine horizontal line bounds
    (xmins, xmaxs, xmids) = get_hline_bounds(len(filelist), 
            xwidth = xwidth, linecoverage = linecoverage)
    # Initialise figure
    fig = plt.figure(figsize = figsize)
    ax = fig.add_subplot(1, 1, 1)
    # Set limits of xaxis so linecoverage actually takes effect
    ax.set_xlim(0, xwidth)
    # Set label for energy axis
    if units == 'cm-1':
        ax.set_ylabel(r'Energy (cm$^{-1}$)', fontsize = 14)
    else:
        ax.set_ylabel(r'Energy (' + units + ')', fontsize = 14)
    # Set xticks at midpoint of each set of hlines
    ax.set_xticks(xmids)
    # Set ticklabels
    if legend is None:
        ax.set_xticklabels(autolegend, fontdict={'fontsize': textsize, 
            'fontweight': 'normal', 'verticalalignment': 'top', 
            'horizontalalignment': 'center'})
    else:
        ax.set_xticklabels(legend, fontdict={'fontsize': textsize, 
            'fontweight': 'normal', 'verticalalignment': 'top', 
            'horizontalalignment': 'center'})
    for i in range(len(filelist)):
        plot_labelled_energies(ax, xmins[i], xmaxs[i], energies[i],
                labels[i], textsize = textsize)
    # Display figure
    plt.show()


def plot_labelled_energies(ax, xmin, xmax, ydata, lbls, textsize):
    ''' Plots a simple energy level diagram from a single dataset
    Input:
        ax = Matplotlib Axis object'''
    # Plot a set of nroots lines
    ax.hlines(ydata, xmin, xmax, linestyles = 'solid', linewidth = 1.25)
    idx = 0
    for lbl in lbls:
        if lbl is not None:
            label_x = xmin - 0.2 * (xmax - xmin)
            label_y = (ydata[idx] + ydata[idx + lbl[1] - 1]) / 2
            label = r'' + lbl[0]
            ax.text(label_x, label_y, label, horizontalalignment = 'right', 
                    verticalalignment = 'center', fontsize = textsize)
            idx += lbl[1]
        else:
            idx += 1


def read_energies_labels(fname):
    with open(fname, 'r') as f:
        # Read energies and labels (one pair on each line)
        energies = []
        labels = []
        line = f.readline()
        while line is not None and len(line.strip()) > 0:
            # Line is split at first space only
            if ' ' in line.strip():
                [estr, lblstr] = line.strip().split(' ', 1)
                if lblstr.strip() == '*':
                    labels[len(labels) - 1][1] += 1
                else:
                    labels.append([lblstr.strip(), 1])
            else:
                estr = line.strip()
                labels.append(None)
            energies.append(float(estr))
            line = f.readline()
    # No label parsing. User will have to write their own LaTeX.
    return (energies, labels)


def get_hline_bounds(nsamples, xwidth, linecoverage):
    # Define x values by equally dividing the available width between datasets, then
    # setting xmin and xmax for each set using the linecoverage parameter.
    (xbounds, xstep) = np.linspace(0, xwidth, num = nsamples + 1, 
            endpoint = True, retstep = True)
    xmin = np.zeros(nsamples)
    xmax = np.zeros(nsamples)
    xmid = np.zeros(nsamples)
    for i in range(0, nsamples):
        xmin[i] = xbounds[i] + 0.5 * (1 - float(linecoverage)) * xstep
        xmax[i] = xbounds[i + 1] - 0.5 * (1 - float(linecoverage)) * xstep
        xmid[i] = 0.5 * (xmin[i] + xmax[i])
    return (xmin, xmax, xmid)


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description = __doc__)
    parser.add_argument('fnames', nargs = '+',
            help = 'Names of files containing term symbols and energies')
    parser.add_argument('--legend', nargs = '+',
            help = 'User-defined labels for datasets in each file. If not provided, \
                    the file names (minus extension) will be used.')
    parser.add_argument('--spread', default = 0.5,
            help = 'Float between 0 and 1, fraction of the allocated horizontal \
                    space that the hline will occupy. Default is 0.5.')
    parser.add_argument('--figsize', nargs = 2, default = [8, 6],
            help = 'Figure size in inches; 2 numbers should be provided, width and height. \
                    Default is 8, 6.')
    parser.add_argument('--textsize', default = 10,
            help = 'Font size for level labels and dataset labels. Default is 10.')
    parser.add_argument('--units', default = 'cm-1', help = 'Energy units. Default is cm-1.')
    args = parser.parse_args()
    energylvls_main(args.fnames, legend = args.legend, linecoverage = float(args.spread), 
            figsize = (args.figsize[0], args.figsize[1]), textsize = float(args.textsize),
            units = args.units.strip())
