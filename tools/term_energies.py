#!/usr/bin/env python3
'''
This is a script designed to plot Dieke-style energy level diagrams. 
The input file(s) should be plain-text. The first line should include all term symbols (space
 separated); no formatting is necessary (e.g. a doublet F term with j = 5/2 is specified as
2F5/2 or as 2F2.5). Subsequent lines should each contain one energy value.
Data from multiple files will be plotted side by side.
'''
import numpy as np
import re
import matplotlib.pyplot as plt
import argparse


l_multiplicities = {'S' : 1, 'A' : 1, 'B' : 1, 'E' : 2, 'T' : 3,
        'P' : 3, 'D' : 5, 'F' : 7, 'G' : 9, 'H' : 11, 'I' : 13}


def energylvls_main(filelist, spinfree = False, legend = None,
        linecoverage = 0.5, figsize = (8, 6), textsize = 10,
        units = 'cm-1', xwidth = 10):
    jterms = []
    term_parts = []
    energies = []
    if legend is None:
        autolegend = []
    for fname in filelist:
        contents = read_terms_energies(str(fname))
        jterms.append(contents[0])
        term_parts.append(contents[1])
        energies.append(contents[2])
        if legend is None:
            autolegend.append(fname.split('.')[0])
    # Determine horizontal line bounds
    (xmins, xmaxs, xmids) = get_hline_bounds(len(filelist), 
            xwidth = xwidth, linecoverage = linecoverage)
    # Initialise figure
    fig = plt.figure(figsize = figsize)
    ax = fig.add_subplot(1, 1, 1)
    # Set limits of xaxis so linecoverage actually takes effect
    ax.set_xlim(0, xwidth)
    # Set label for energy axis
    if units == 'cm-1':
        ax.set_ylabel(r'Energy (cm$^{-1}$)', fontsize = 14)
    else:
        ax.set_ylabel(r'Energy (' + units + ')', fontsize = 14)
    # Set xticks at midpoint of each set of hlines
    ax.set_xticks(xmids)
    # Set ticklabels
    if legend is None:
        ax.set_xticklabels(autolegend, fontdict={'fontsize': textsize, 
            'fontweight': 'normal', 'verticalalignment': 'top', 
            'horizontalalignment': 'center'})
    else:
        ax.set_xticklabels(legend, fontdict={'fontsize': textsize, 
            'fontweight': 'normal', 'verticalalignment': 'top', 
            'horizontalalignment': 'center'})
    for i in range(len(filelist)):
        plot_term_energies(ax, xmins[i], xmaxs[i], energies[i], term_parts[i],
                jterms[i], spinfree = spinfree, textsize = textsize)
    # Display figure
    plt.show()


def plot_term_energies(ax, xmin, xmax, ydata, terms, jterms, spinfree,
        textsize):
    ''' Plots a simple energy level diagram from a single dataset
    Input:
        ax = Matplotlib Axis object'''
    # Plot a set of nroots lines
    ax.hlines(ydata, xmin, xmax, linestyles = 'solid', linewidth = 1.25)
    # Determine overall multiplicity of each term
    nterms = len(terms)
    cml_mult = 0
    for i in range(nterms):
        if jterms:
            mult = int(2 * terms[i][2] + 1)
        elif spinfree:
            mult = l_multiplicities[terms[i][1]]
        else:
            mult = terms[i][0] * l_multiplicities[terms[i][1]]
        label_x = xmin - 0.2 * (xmax - xmin)
        label_y = (ydata[cml_mult] + ydata[cml_mult + mult - 1]) / 2
        if terms[i][2] - int(terms[i][2]) >= 0.01:
            jstr = str(int(2 * terms[i][2])) + '/2'
        else:
            jstr = str(int(terms[i][2]))
        label = r'$^{' + str(terms[i][0]) + '}$' + terms[i][1] + '$_{' + \
                jstr + '}$'
        ax.text(label_x, label_y, label, horizontalalignment = 'right', 
                verticalalignment = 'center', fontsize = textsize)
        cml_mult += mult


def read_terms_energies(fname):
    with open(fname, 'r') as f:
        # First line is a list of term symbols
        line = f.readline()
        termsymb = line.split()
        # Check if using L-terms or J-terms
        m = re.search('([0-9]+)([a-zA-Z])([0-9/\.]*)', termsymb[0])
        if len(m.group(3)) == 0:
            jterms = False
        else:
            jterms = True
        # Store quantum number in 2-tuples (L terms) / 3-tuples (J terms)
        term_parts = []
        for i in range(len(termsymb)):
            m = re.search('([0-9]+)([a-zA-Z])([0-9/\.]*)', termsymb[i])
            if jterms:
                if '/' in m.group(3):
                    frac = m.group(3).split('/')
                    j = round(float(frac[0]) / float(frac[1]), 1)
                else:
                    j = round(float(m.group(3)), 1)
                term_parts.append((int(m.group(1)), m.group(2).upper(), j))
            else:
                term_parts.append((int(m.group(1)), m.group(2).upper()))
        # Read energies (one on each line)
        energies = []
        line = f.readline()
        while line is not None and len(line.strip()) > 0:
            energies.append(float(line))
            line = f.readline()
    return (jterms, term_parts, energies)


def get_hline_bounds(nsamples, xwidth, linecoverage):
    # Define x values by equally dividing the available width between datasets, then
    # setting xmin and xmax for each set using the linecoverage parameter.
    (xbounds, xstep) = np.linspace(0, xwidth, num = nsamples + 1, 
            endpoint = True, retstep = True)
    xmin = np.zeros(nsamples)
    xmax = np.zeros(nsamples)
    xmid = np.zeros(nsamples)
    for i in range(0, nsamples):
        xmin[i] = xbounds[i] + 0.5 * (1 - float(linecoverage)) * xstep
        xmax[i] = xbounds[i + 1] - 0.5 * (1 - float(linecoverage)) * xstep
        xmid[i] = 0.5 * (xmin[i] + xmax[i])
    return (xmin, xmax, xmid)


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description = __doc__)
    parser.add_argument('fnames', nargs = '+',
            help = 'Names of files containing term symbols and energies')
    parser.add_argument('--sf', action = 'store_true', default = False,
            help = 'Flag for spin-free states. If specified, spin multiplicity \
                    is not considered when counting states.')
    parser.add_argument('--legend', nargs = '+',
            help = 'User-defined labels for datasets in each file. If not provided, \
                    the file names (minus extension) will be used.')
    parser.add_argument('--spread', default = 0.5,
            help = 'Float between 0 and 1, fraction of the allocated horizontal \
                    space that the hline will occupy. Default is 0.5.')
    parser.add_argument('--figsize', nargs = 2, default = [8, 6],
            help = 'Figure size in inches; 2 numbers should be provided, width and height. \
                    Default is 8, 6.')
    parser.add_argument('--textsize', default = 10,
            help = 'Font size for term labels and dataset labels. Default is 10.')
    parser.add_argument('--units', default = 'cm-1', help = 'Energy units. Default is cm-1.')
    args = parser.parse_args()
    energylvls_main(args.fnames, spinfree = args.sf, legend = args.legend,
            linecoverage = float(args.spread), figsize = (args.figsize[0], args.figsize[1]),
            textsize = float(args.textsize), units = args.units.strip())
