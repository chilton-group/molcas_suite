MODULE matrix_tools
    USE, INTRINSIC :: iso_fortran_env
    IMPLICIT NONE

    INTERFACE EXPECTATION
        !MODULE PROCEDURE EXPECTATION_R_DP
        MODULE PROCEDURE EXPECTATION_R_QP
        !MODULE PROCEDURE EXPECTATION_C_DP
        MODULE PROCEDURE EXPECTATION_C_QP
    END INTERFACE EXPECTATION
    
    INTERFACE DIAGONALISE
        MODULE PROCEDURE DIAG_R_DP
        MODULE PROCEDURE DIAG_C_DP
    END INTERFACE DIAGONALISE

    INTERFACE NUM_GREATER_THAN_EQ_TO
    !Calculates how many elements in a vector are greater than or equal to a given value
        MODULE PROCEDURE GTEQ_R_DP
        MODULE PROCEDURE GTEQ_R_QP
        MODULE PROCEDURE GTEQ_I_SP
        MODULE PROCEDURE GTEQ_I_DP
    END INTERFACE NUM_GREATER_THAN_EQ_TO

    INTERFACE BUBBLE
        !Sorts a vector of reals or integers high to low
        !Returns sorted list and a companion describing the order of the new list in terms of the old one
        MODULE PROCEDURE BUBBLE_R_DP
        MODULE PROCEDURE BUBBLE_R_QP
        MODULE PROCEDURE BUBBLE_I_SP
        MODULE PROCEDURE BUBBLE_I_DP
    END INTERFACE BUBBLE

    INTERFACE ALT_MATMUL
    !Matrix Multiplication at variable precision
        MODULE PROCEDURE MATMUL_R_DP
        MODULE PROCEDURE MATMUL_R_QP
        MODULE PROCEDURE MATVECMUL_R_DP
        MODULE PROCEDURE MATVECMUL_R_QP
        MODULE PROCEDURE MATMUL_C_DP
        MODULE PROCEDURE MATMUL_C_QP
        MODULE PROCEDURE MATVECMUL_C_DP
        MODULE PROCEDURE MATVECMUL_C_QP
    END INTERFACE ALT_MATMUL

    INTERFACE ARRAY_APPEND
    !Appends scalar/vector to vector/matrix for complex/real/integer
        MODULE PROCEDURE ARRAY_APPEND_R_V_S_DP
        MODULE PROCEDURE ARRAY_APPEND_R_V_S_QP
        MODULE PROCEDURE ARRAY_APPEND_C_V_S_DP
        MODULE PROCEDURE ARRAY_APPEND_C_V_S_QP
        MODULE PROCEDURE ARRAY_APPEND_I_V_S_SP
        MODULE PROCEDURE ARRAY_APPEND_I_V_S_DP
        MODULE PROCEDURE ARRAY_APPEND_R_V_V_DP
        MODULE PROCEDURE ARRAY_APPEND_R_V_V_QP
        MODULE PROCEDURE ARRAY_APPEND_C_V_V_DP
        MODULE PROCEDURE ARRAY_APPEND_C_V_V_QP
        MODULE PROCEDURE ARRAY_APPEND_I_V_V_SP
        MODULE PROCEDURE ARRAY_APPEND_I_V_V_DP
        MODULE PROCEDURE ARRAY_APPEND_R_M_V_DP
        MODULE PROCEDURE ARRAY_APPEND_R_M_V_QP
        MODULE PROCEDURE ARRAY_APPEND_C_M_V_DP
        MODULE PROCEDURE ARRAY_APPEND_C_M_V_QP
        MODULE PROCEDURE ARRAY_APPEND_I_M_V_SP
        MODULE PROCEDURE ARRAY_APPEND_I_M_V_DP
    END INTERFACE ARRAY_APPEND

    INTERFACE FLIPUD
    !Flip arrays
        MODULE PROCEDURE FLIPUD_R_V_DP
        MODULE PROCEDURE FLIPUD_R_V_QP
        MODULE PROCEDURE FLIPUD_C_V_DP
        MODULE PROCEDURE FLIPUD_C_V_QP
        MODULE PROCEDURE FLIPUD_I_V_SP
        MODULE PROCEDURE FLIPUD_I_V_DP
    END INTERFACE FLIPUD

    INTERFACE RADIAL
    !Calculates euclidian norm
        MODULE PROCEDURE EUC_NORM_R_DP
        MODULE PROCEDURE EUC_NORM_R_QP
    END INTERFACE RADIAL

    INTERFACE ANGLE
    !Calculates angle between two vectors
        MODULE PROCEDURE ANGLE_R_DP
        MODULE PROCEDURE ANGLE_R_QP
    END INTERFACE ANGLE

    INTERFACE CHECK_HERMITIAN
    !Returns SUM(matrix * matrix^+)
        MODULE PROCEDURE CHECK_HERMITIAN_DP
        MODULE PROCEDURE CHECK_HERMITIAN_QP
    END INTERFACE CHECK_HERMITIAN

    CONTAINS

!    SUBROUTINE EXPECTATION_R_DP(FB,vectors,pert)
!        !Performs change of basis for a double precision real matrix
!        INTEGER                         :: dim
!        CHARACTER(LEN = *), INTENT(IN)  :: FB
!        REAL(KIND = 8), INTENT(IN)     :: vectors(:,:)
!        REAL(KIND = 8), INTENT(INOUT)  :: pert(:,:)
!        REAL(KIND = 8)                 :: temp(size(vectors,1), size(vectors,2))
!      
!        dim = size(vectors,1)
!        
!        !Perform forwards or backwards change of basis
!        IF(FB == 'F' .or. FB == 'f') then     ! (vectors^T).(pert.vectors)
!              CALL DGEMM('N','N',dim,dim,dim,1.0_8,pert,dim,vectors,dim,0.0_8,temp,dim)
!              CALL DGEMM('C','N',dim,dim,dim,1.0_8,vectors,dim,temp,dim,0.0_8,pert,dim)
!        ELSE IF(FB == 'B' .or. FB == 'b') then  ! (vectors.pert).(vectors^T)
!              CALL DGEMM('N','N',dim,dim,dim,1.0_8,vectors,dim,pert,dim,0.0_8,temp,dim)
!              CALL DGEMM('N','C',dim,dim,dim,1.0_8,temp,dim,vectors,dim,0.0_8,pert,dim)
!        END IF
!    END SUBROUTINE EXPECTATION_R_DP
    
    SUBROUTINE EXPECTATION_R_QP(FB,vectors,pert)
        !Performs change of basis for a quadruple precision real matrix
        INTEGER                         :: dim
        CHARACTER(LEN = *), INTENT(IN)  :: FB
        REAL(KIND = 16), INTENT(IN)     :: vectors(:,:)
        REAL(KIND = 16), INTENT(INOUT)  :: pert(:,:)
        REAL(KIND = 16)                 :: temp(size(vectors,1), size(vectors,2))
      
        dim = size(vectors,1)
        
        !Perform forwards or backwards change of basis
        IF(FB == 'F' .or. FB == 'f') then     ! (vectors^T).(pert.vectors)
              CALL DGEMM('N','N',dim,dim,dim,1.0_16,pert,dim,vectors,dim,0.0_16,temp,dim)
              CALL DGEMM('C','N',dim,dim,dim,1.0_16,vectors,dim,temp,dim,0.0_16,pert,dim)
        ELSE IF(FB == 'B' .or. FB == 'b') then  ! (vectors.pert).(vectors^T)
              CALL DGEMM('N','N',dim,dim,dim,1.0_16,vectors,dim,pert,dim,0.0_16,temp,dim)
              CALL DGEMM('N','C',dim,dim,dim,1.0_16,temp,dim,vectors,dim,0.0_16,pert,dim)
        END IF
    END SUBROUTINE EXPECTATION_R_QP
    
!    SUBROUTINE EXPECTATION_C_DP(FB,vectors,pert)
!        !Performs change of basis for a double precision complex matrix
!        INTEGER                            :: dim
!        CHARACTER(LEN = *), INTENT(IN)     :: FB
!        COMPLEX(KIND = 8), INTENT(IN)      :: vectors(:,:)
!        COMPLEX(KIND = 8), INTENT(INOUT)   :: pert(:,:)
!        COMPLEX(KIND = 8)                  :: temp(size(vectors,1), size(vectors,2))
!      
!        dim = size(vectors,1)
!        
!        !Perform forwards or backwards change of basis
!        IF(FB == 'F' .or. FB == 'f') then     ! (vectors^T).(pert.vectors)
!              CALL ZGEMM('N','N',dim,dim,dim,(1.0_8,0.0_8),pert,dim,vectors,dim,(0.0_8,0.0_8),temp,dim)
!              CALL ZGEMM('C','N',dim,dim,dim,(1.0_8,0.0_8),vectors,dim,temp,dim,(0.0_8,0.0_8),pert,dim)
!        ELSE IF(FB == 'B' .or. FB == 'b') then  ! (vectors.pert).(vectors^T)
!              CALL ZGEMM('N','N',dim,dim,dim,(1.0_8,0.0_8),vectors,dim,pert,dim,(0.0_8,0.0_8),temp,dim)
!              CALL ZGEMM('N','C',dim,dim,dim,(1.0_8,0.0_8),temp,dim,vectors,dim,(0.0_8,0.0_8),pert,dim)
!        END IF
!    END SUBROUTINE EXPECTATION_C_DP
    
    SUBROUTINE EXPECTATION_C_QP(FB,vectors,pert)
        !Performs change of basis for a quadruple precision complex matrix
        INTEGER                            :: dim
        CHARACTER(LEN = *), INTENT(IN)     :: FB
        COMPLEX(KIND = 16), INTENT(IN)     :: vectors(:,:)
        COMPLEX(KIND = 16), INTENT(INOUT)  :: pert(:,:)
        COMPLEX(KIND = 16)                 :: temp(size(vectors,1), size(vectors,2))
      
        dim = size(vectors,1)
        
        !Perform forwards or backwards change of basis
        IF(FB == 'F' .or. FB == 'f') THEN     ! (vectors^T).(pert.vectors)
              CALL ZGEMM('N','N',dim,dim,dim,(1.0_16,0.0_16),pert,dim,vectors,dim,(0.0_16,0.0_16),temp,dim)
              CALL ZGEMM('C','N',dim,dim,dim,(1.0_16,0.0_16),vectors,dim,temp,dim,(0.0_16,0.0_16),pert,dim)
        ELSE IF(FB == 'B' .or. FB == 'b') THEN  ! (vectors.pert).(vectors^T)
              CALL ZGEMM('N','N',dim,dim,dim,(1.0_16,0.0_16),vectors,dim,pert,dim,(0.0_16,0.0_16),temp,dim)
              CALL ZGEMM('N','C',dim,dim,dim,(1.0_16,0.0_16),temp,dim,vectors,dim,(0.0_16,0.0_16),pert,dim)
        END IF

    END SUBROUTINE EXPECTATION_C_QP

    FUNCTION GTEQ_R_DP(vector, value) RESULT(GTEQ)
        !Returns the number of numbers greater than or equal to a given value
        !Both vector and value are double precision reals
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)  :: vector(:), value
        INTEGER                     :: row, GTEQ
      
        GTEQ = 0
      
        DO row = 1, size(vector)
              IF(vector(row) >= value) GTEQ = GTEQ + 1
        END DO
      
    END FUNCTION GTEQ_R_DP

    FUNCTION GTEQ_R_QP(vector, value) RESULT(GTEQ)
        !Returns the number of numbers greater than or equal to a given value
        !Both vector and value are quadruple precision reals
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)  :: vector(:), value
        INTEGER                      :: row, GTEQ
      
        GTEQ = 0
      
        DO row = 1, size(vector)
              IF(vector(row) >= value) GTEQ = GTEQ + 1
        END DO
      
    END FUNCTION GTEQ_R_QP

    FUNCTION GTEQ_I_SP(vector, value)  RESULT(GTEQ)
        !Returns the number of numbers greater than or equal to a given value
        !Both vector and value are single precision integers
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: vector(:), value
        INTEGER             :: row, GTEQ
      
        GTEQ = 0
      
        DO row = 1, size(vector)
              IF(vector(row) >= value) GTEQ = GTEQ + 1
        END DO
      
    END FUNCTION GTEQ_I_SP

    FUNCTION GTEQ_I_DP(vector, value)  RESULT(GTEQ)
        !Returns the number of numbers greater than or equal to a given value
        !Both vector and value are double precision integers
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(IN) :: vector(:), value
        INTEGER(KIND = 8)             :: row, GTEQ
      
        GTEQ = 0
      
        DO row = 1, size(vector)
              IF(vector(row) >= value) GTEQ = GTEQ + 1
        END DO
      
    END FUNCTION GTEQ_I_DP

    SUBROUTINE DIAG_R_DP(NV,UL,matrix,eigenvectors,eigenvalues)
        !Real Hermitian matrix eigensolver using LAPACK's ZHEEV SUBROUTINE
        !NV = 'N' (eigenvalues only) OR 'V' (eigenvalues and eigenvectors)
        !UL = 'U' (Upper triangle of matrix is stored) OR 'L' (Lower triangle of matrix is stored)
        IMPLICIT NONE
            CHARACTER(LEN = *),INTENT(IN)    :: NV,UL
            REAL(KIND = 8),INTENT(IN)        :: matrix(:,:)
            REAL(KIND = 8),INTENT(OUT)       :: eigenvectors(size(matrix,1),size(matrix,2))
            REAL(KIND = 8),INTENT(OUT)       :: eigenvalues(size(matrix,1))
            REAL(KIND = 8),ALLOCATABLE       :: rwork(:)
            INTEGER                          :: dim,lwork,diaginfo

            !Perform workspace query to calculate optimum workspace size
            dim = SIZE(matrix,1)
            eigenvectors = matrix
            ALLOCATE(rwork(4))
            CALL DSYEV(NV,UL,dim,eigenvectors,dim,eigenvalues,rwork,-1,diaginfo)

            !Reallocate work based on lwork returned by query, then perform diagonalisation
            lwork = NINT(rwork(1))
            DEALLOCATE(rwork)
            ALLOCATE(rwork(lwork))
            CALL DSYEV(NV,UL,dim,eigenvectors,dim,eigenvalues,rwork,lwork,diaginfo)
            IF(diaginfo /= 0) WRITE(6,'(A21,I10)') "Diagonalisation Error",diaginfo
            DEALLOCATE(rwork)

    END SUBROUTINE DIAG_R_DP

    SUBROUTINE DIAG_C_DP(NV,UL,matrix,eigenvectors,eigenvalues)
        !Complex Hermitian matrix eigensolver using LAPACK's ZHEEV SUBROUTINE
        !NV = 'N' (eigenvalues only) OR 'V' (eigenvalues and eigenvectors)
        !UL = 'U' (Upper triangle of matrix is stored) OR 'L' (Lower triangle of matrix is stored)
        IMPLICIT NONE
        CHARACTER(LEN = *),INTENT(IN)       :: NV,UL
        COMPLEX(KIND = 8),INTENT(IN)        :: matrix(:,:)
        COMPLEX(KIND = 8),INTENT(OUT)       :: eigenvectors(size(matrix,1),size(matrix,2))
        REAL(KIND = 8),INTENT(OUT)          :: eigenvalues(size(matrix,1))
        REAL(KIND = 8),ALLOCATABLE          :: rwork(:)
        COMPLEX(KIND = 8),ALLOCATABLE       :: work(:)
        INTEGER                             :: dim,lwork,diaginfo


        !Perform workspace query to calculate optimum workspace size
        dim = SIZE(matrix,1)
        eigenvectors = matrix
        ALLOCATE(work(4),rwork(3*dim-2))
        CALL ZHEEV(NV,UL,dim,eigenvectors,dim,eigenvalues,work,-1,rwork,diaginfo)
        
        !Reallocate work based on lwork returned by query, then perform diagonalisation
        lwork = NINT(REAL(work(1),8))
        DEALLOCATE(work)
        ALLOCATE(work(lwork))
        CALL ZHEEV(NV,UL,dim,eigenvectors,dim,eigenvalues,work,lwork,rwork,diaginfo)
        IF(diaginfo /= 0) WRITE(6,'(A21,I10)') "Diagonalisation Error",diaginfo
        DEALLOCATE(work,rwork)

    END SUBROUTINE DIAG_C_DP

    SUBROUTINE BUBBLE_R_DP(list, companion, top)
    !Bubble sort for a list of double precision reals
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(INOUT)  :: list(:) 
        REAL(KIND = 8)                 :: RTEMP
        INTEGER, INTENT(INOUT)         :: companion(:,:)
        INTEGER                        :: J, ITEMP
        LOGICAL                        :: swapped
        CHARACTER(LEN = 3)             :: top
    
        companion = 0
        companion(:,1) = [(J, J = 1, SIZE(list))]
        companion(:,2) = companion(:,1)
    
        swapped = .TRUE.
    

    IF (top == 'max') THEN
        DO WHILE (swapped .EQV. .TRUE.)
        !Loop until no swaps occur
        swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) < list(J + 1)) THEN
                    !Swap list elements
                    RTEMP        = list(J) 
                    list(J)    = list(J + 1) 
                    list(J + 1)  = RTEMP
                    !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP
                    swapped = .TRUE.
                END IF
            END DO
        END DO
    ELSE IF (top == 'min') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) > list(J + 1)) THEN
                    !Swap list elements
                    RTEMP        = list(J) 
                    list(J)    = list(J + 1) 
                    list(J + 1)  = RTEMP
                    !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP
                    swapped = .TRUE.
                END IF
            END DO
        END DO
    END IF
    
    END SUBROUTINE BUBBLE_R_DP
    
    SUBROUTINE BUBBLE_R_QP(list, companion, top)
    !Bubble sort for a list of quadruple precision reals
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(INOUT) :: list(:) 
        REAL(KIND = 16)                :: RTEMP
        INTEGER, INTENT(INOUT)         :: companion(:,:)
        INTEGER                        :: J, ITEMP
        LOGICAL                        :: swapped
        CHARACTER(LEN = 3)             :: top
    
        companion = 0
        companion(:,1) = [(J, J = 1, SIZE(list))]
        companion(:,2) = companion(:,1)
    
        swapped = .TRUE.
    

    IF (top == 'max') THEN
        DO WHILE (swapped .EQV. .TRUE.)
        !Loop until no swaps occur
        swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) < list(J + 1)) THEN
                    !Swap list elements
                    RTEMP        = list(J) 
                    list(J)    = list(J + 1) 
                    list(J + 1)  = RTEMP
                    !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP
                    swapped = .TRUE.
                END IF
            END DO
        END DO
    ELSE IF (top == 'min') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) > list(J + 1)) THEN
                    !Swap list elements
                    RTEMP        = list(J) 
                    list(J)    = list(J + 1) 
                    list(J + 1)  = RTEMP
                    !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP
                    swapped = .TRUE.
                END IF
            END DO
        END DO
    END IF
    
    END SUBROUTINE BUBBLE_R_QP

    SUBROUTINE BUBBLE_I_SP(list,companion, top)
    !Bubble sort for a list of single precision integers
        IMPLICIT NONE
        INTEGER,   INTENT(INOUT) :: list(:), companion(:,:)
        INTEGER                  :: J, TEMP, ITEMP
        LOGICAL                  :: swapped
        CHARACTER(LEN = 3)       :: top

        companion = 0
        companion(:,1) = [(J, J = 1, SIZE(list))]
        companion(:,2) = companion(:,1)
    
        swapped = .TRUE.
    
    IF (top == 'max') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) < list(J + 1)) THEN
              !Swap list elements
                    TEMP        = list(J) 
                    list(J)     = list(J + 1) 
                    list(J + 1) = TEMP
              !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP

                    swapped = .TRUE.
                END IF
            END DO
        END DO
    ELSE IF (top == 'min') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) > list(J + 1)) THEN
              !Swap list elements
                    TEMP        = list(J) 
                    list(J)     = list(J + 1) 
                    list(J + 1) = TEMP
              !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP

                    swapped = .TRUE.
                END IF
            END DO
        END DO
    END IF
    END SUBROUTINE BUBBLE_I_SP

    SUBROUTINE BUBBLE_I_DP(list,companion, top)
    !Bubble sort for a list of double precision integers
        IMPLICIT NONE
        INTEGER,   INTENT(INOUT)           :: companion(:,:)
        INTEGER(KIND = 8),   INTENT(INOUT) :: list(:)
        INTEGER                            :: J, TEMP, ITEMP
        LOGICAL                            :: swapped
        CHARACTER(LEN = 3)                 :: top

        companion = 0
        companion(:,1) = [(J, J = 1, SIZE(list))]
        companion(:,2) = companion(:,1)
    
        swapped = .TRUE.
    
    IF (top == 'max') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) < list(J + 1)) THEN
              !Swap list elements
                    TEMP        = list(J) 
                    list(J)     = list(J + 1) 
                    list(J + 1) = TEMP
              !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP

                    swapped = .TRUE.
                END IF
            END DO
        END DO
    ELSE IF (top == 'min') THEN
        !Loop until no swaps occur
        DO WHILE (swapped .EQV. .TRUE.)
            swapped = .FALSE.
            DO J = 1, SIZE(list) - 1
                IF (list(J) > list(J + 1)) THEN
              !Swap list elements
                    TEMP        = list(J) 
                    list(J)     = list(J + 1) 
                    list(J + 1) = TEMP
              !Swap companion elements
                    ITEMP            = companion(J,2)
                    companion(J,2)     = companion(J + 1,2)
                    companion(J + 1,2) = ITEMP

                    swapped = .TRUE.
                END IF
            END DO
        END DO
    END IF
    END SUBROUTINE BUBBLE_I_DP

    FUNCTION MATMUL_R_QP(A,B) RESULT(C)
        !Multiplication of real matrix and real matrix
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)  :: A(:,:), B(:,:)
        REAL(KIND = 16)              :: C(size(A,1),size(B,2))
        INTEGER                     :: K,L,P

        IF (SIZE(A,2) /= SIZE(B,1)) THEN
          WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
          STOP
        END IF

        C = 0.0_16
    
        DO K = 1, SIZE(A,1)
            DO L = 1, SIZE(B,2)
                DO P = 1, SIZE(A,2)
                    C(K,L) = C(K,L) + A(K,P)*B(P,L)
                END DO
            END DO
        END DO

    END FUNCTION MATMUL_R_QP

    FUNCTION MATMUL_R_DP(A,B) RESULT(C)
        !Multiplication of real matrix and real matrix
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)  :: A(:,:), B(:,:)
        REAL(KIND = 8)              :: C(size(A,1),size(B,2))
        INTEGER                     :: K,L,P

        IF (SIZE(A,2) /= SIZE(B,1)) THEN
          WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
          STOP
        END IF

        C = 0.0_8
    
        DO K = 1, SIZE(A,1)
            DO L = 1, SIZE(B,2)
                DO P = 1, SIZE(A,2)
                    C(K,L) = C(K,L) + A(K,P)*B(P,L)
                END DO
            END DO
        END DO

    END FUNCTION MATMUL_R_DP

    FUNCTION MATVECMUL_R_QP(A,B) RESULT(C)
        !Multiplication of real vector and real matrix
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)  :: A(:,:), B(:)
        REAL(KIND = 16)              :: C(SIZE(B))
        INTEGER                      :: K,L,P

        IF (SIZE(A,2) /= SIZE(B)) THEN
          WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
          STOP
        END IF

        C = 0.0_16   

        DO K = 1, SIZE(A,1)
            DO P = 1, SIZE(A,2)
                C(K) = C(K) + A(K,P)*B(P)
            END DO
        END DO

    END FUNCTION MATVECMUL_R_QP

    FUNCTION MATVECMUL_R_DP(A,B) RESULT(C)
        !Multiplication of real vector and real matrix
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)   :: A(:,:), B(:)
        REAL(KIND = 8)               :: C(SIZE(B))
        INTEGER                      :: K,L,P

        IF (SIZE(A,2) /= SIZE(B)) THEN
          WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
          STOP
        END IF

        C = 0.0_8   

        DO K = 1, SIZE(A,1)
            DO P = 1, SIZE(A,2)
                C(K) = C(K) + A(K,P)*B(P)
            END DO
        END DO

    END FUNCTION MATVECMUL_R_DP

    FUNCTION MATMUL_C_DP(A,B) RESULT(C)
        !Multiplication of complex matrix and complex matrix
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN) :: A(:,:), B(:,:)
        COMPLEX(KIND = 8)             :: C(SIZE(A,1),SIZE(B,2))
        INTEGER                        :: K,L,P

        IF (SIZE(A,2) /= SIZE(B,1)) THEN
            WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
            STOP
        END IF

        C = (0.0_8, 0.0_8)    

        DO K = 1, SIZE(A,1)
            DO L = 1, SIZE(B,2)
                DO P = 1, SIZE(A,2)
                    C(K,L) = C(K,L) + A(K,P)*B(P,L)
                END DO
            END DO
        END DO

    END FUNCTION MATMUL_C_DP

    FUNCTION MATMUL_C_QP(A,B) RESULT(C)
        !Multiplication of complex matrix and complex matrix
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN) :: A(:,:), B(:,:)
        COMPLEX(KIND = 16)             :: C(SIZE(A,1),SIZE(B,2))
        INTEGER                        :: K,L,P

        IF (SIZE(A,2) /= SIZE(B,1)) THEN
            WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
            STOP
        END IF

        C = (0.0_16, 0.0_16)  

        DO K = 1, SIZE(A,1)
            DO L = 1, SIZE(B,2)
                DO P = 1, SIZE(A,2)
                    C(K,L) = C(K,L) + A(K,P)*B(P,L)
                END DO
            END DO
        END DO

    END FUNCTION MATMUL_C_QP

    FUNCTION MATVECMUL_C_DP(A,B) RESULT(C)
        !Multiplication of complex vector and complex matrix
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)   :: A(:,:), B(:)
        COMPLEX(KIND = 8)               :: C(SIZE(B))
        INTEGER                         :: K,L,P

        IF (SIZE(A,2) /= SIZE(B)) THEN
            WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
            STOP
        END IF

        C = 0.0_8   

        DO K = 1, SIZE(A,1)
            DO P = 1, SIZE(A,2)
                C(K) = C(K) + A(K,P)*B(P)
            END DO
        END DO

    END FUNCTION MATVECMUL_C_DP

    FUNCTION MATVECMUL_C_QP(A,B) RESULT(C)
        !Multiplication of complex vector and complex matrix
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)  :: A(:,:), B(:)
        COMPLEX(KIND = 16)              :: C(SIZE(B))
        INTEGER                         :: K,L,P

        IF (SIZE(A,2) /= SIZE(B)) THEN
            WRITE(6,*) 'MISMATCH IN INNER PRODUCT'
            STOP
        END IF

        C = 0.0_16   

        DO K = 1, SIZE(A,1)
            DO P = 1, SIZE(A,2)
                C(K) = C(K) + A(K,P)*B(P)
            END DO
        END DO

    END FUNCTION MATVECMUL_C_QP
    
    SUBROUTINE ARRAY_APPEND_I_V_S_SP(array, element)
        !Appends an integer scalar to a vector of integers
        IMPLICIT NONE
        INTEGER, INTENT(INOUT), ALLOCATABLE :: array(:)
        INTEGER, INTENT(IN)                 :: element
        INTEGER, ALLOCATABLE                :: tarray(:)
        INTEGER                             :: array_length, i

        IF (ALLOCATED(array) .EQV. .TRUE.) THEN

            array_length = SIZE(array)

            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length
                tarray(i) = array(i)
            END DO

            tarray(array_length+1) = element

        ELSE
            ALLOCATE(tarray(1))
            tarray(1) = element
        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_I_V_S_SP
    
    SUBROUTINE ARRAY_APPEND_I_V_S_DP(array, element)
        !Appends an integer scalar to a vector of integers
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array(:)
        INTEGER(KIND = 8), INTENT(IN)                 :: element
        INTEGER(KIND = 8), ALLOCATABLE                :: tarray(:)
        INTEGER(KIND = 8)                             :: array_length, i


        IF (ALLOCATED(array) .EQV. .TRUE.) THEN

            array_length = SIZE(array)

            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length
                tarray(i) = array(i)
            END DO

            tarray(array_length+1) = element

        ELSE
            ALLOCATE(tarray(1))
            tarray(1) = element
        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_I_V_S_DP

    SUBROUTINE ARRAY_APPEND_R_V_S_DP(array, element)
        !Appends a real scalar to a vector of reals
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(INOUT), ALLOCATABLE    :: array(:)
        REAL(KIND = 8), INTENT(IN)                    :: element
        REAL(KIND = 8), ALLOCATABLE                   :: tarray(:)
        INTEGER                             :: array_length, i

        array_length = SIZE(array)

        IF (ALLOCATED(array) .EQV. .TRUE.) THEN
            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length  
                tarray(i) = array(i)
            END DO

            tarray(array_length + 1) = element
            
        ELSE
            
            ALLOCATE(tarray(1))
            tarray(1) = element

        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_R_V_S_DP

    SUBROUTINE ARRAY_APPEND_R_V_S_QP(array, element)
        !Appends a real scalar to a vector of reals
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(INOUT), ALLOCATABLE    :: array(:)
        REAL(KIND = 16), INTENT(IN)                    :: element
        REAL(KIND = 16), ALLOCATABLE                   :: tarray(:)
        INTEGER                             :: array_length, i

        array_length = SIZE(array)

        IF (ALLOCATED(array) .EQV. .TRUE.) THEN
            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length  
                tarray(i) = array(i)
            END DO

            tarray(array_length + 1) = element
            
        ELSE
            
            ALLOCATE(tarray(1))
            tarray(1) = element

        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_R_V_S_QP


    SUBROUTINE ARRAY_APPEND_C_V_S_DP(array, element)
        !Appends a complex scalar to a vector of complex values
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array(:)
        COMPLEX(KIND = 8), INTENT(IN)                 :: element
        COMPLEX(KIND = 8), ALLOCATABLE                :: tarray(:)
        INTEGER                             :: array_length, i

        array_length = SIZE(array) + 1

        IF (ALLOCATED(array) .EQV. .TRUE.) THEN
            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length 
                tarray(i) = array(i)
            END DO
            tarray(array_length + 1) = element
        ELSE
            ALLOCATE(tarray(1))
            tarray(1) = element
        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_C_V_S_DP

    SUBROUTINE ARRAY_APPEND_C_V_S_QP(array, element)
        !Appends a complex scalar to a vector of complex values
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(INOUT), ALLOCATABLE :: array(:)
        COMPLEX(KIND = 16), INTENT(IN)                 :: element
        COMPLEX(KIND = 16), ALLOCATABLE                :: tarray(:)
        INTEGER                             :: array_length, i

        array_length = SIZE(array) + 1

        IF (ALLOCATED(array) .EQV. .TRUE.) THEN
            ALLOCATE(tarray(array_length + 1))
            DO i = 1, array_length 
                tarray(i) = array(i)
            END DO
            tarray(array_length + 1) = element
        ELSE
            ALLOCATE(tarray(1))
            tarray(1) = element
        END IF

        CALL MOVE_ALLOC(tarray, array)

    END SUBROUTINE ARRAY_APPEND_C_V_S_QP

    SUBROUTINE ARRAY_APPEND_I_V_V_SP(array_1, array_2)
        !Appends a vector of integers to a vector of integers
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        INTEGER, INTENT(INOUT), ALLOCATABLE :: array_1(:)
        INTEGER, INTENT(IN)                 :: array_2(:)
        INTEGER, ALLOCATABLE                :: tarray(:)
        INTEGER                             :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_I_V_V_SP

    SUBROUTINE ARRAY_APPEND_I_V_V_DP(array_1, array_2)
        !Appends a vector of integers to a vector of integers
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array_1(:)
        INTEGER(KIND = 8), INTENT(IN)                 :: array_2(:)
        INTEGER(KIND = 8), ALLOCATABLE                :: tarray(:)
        INTEGER(KIND = 8)                             :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_I_V_V_DP

    SUBROUTINE ARRAY_APPEND_R_V_V_DP(array_1, array_2)
        !Appends a vector of reals to a vector of reals
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(INOUT), ALLOCATABLE  :: array_1(:)
        REAL(KIND = 8), INTENT(IN)                  :: array_2(:)
        REAL(KIND = 8), ALLOCATABLE                 :: tarray(:)
        INTEGER                                     :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO
        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_R_V_V_DP

SUBROUTINE ARRAY_APPEND_R_V_V_QP(array_1, array_2)
        !Appends a vector of reals to a vector of reals
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(INOUT), ALLOCATABLE :: array_1(:)
        REAL(KIND = 16), INTENT(IN)                 :: array_2(:)
        REAL(KIND = 16), ALLOCATABLE                :: tarray(:)
        INTEGER                                     :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO
        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_R_V_V_QP

    SUBROUTINE ARRAY_APPEND_C_V_V_DP(array_1, array_2)
        !Appends a vector of reals to a vector of reals
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array_1(:)
        COMPLEX(KIND = 8), INTENT(IN)                 :: array_2(:)
        COMPLEX(KIND = 8), ALLOCATABLE                :: tarray(:)
        INTEGER                                     :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO
        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_C_V_V_DP

    SUBROUTINE ARRAY_APPEND_C_V_V_QP(array_1, array_2)
        !Appends a vector of reals to a vector of reals
        !array_2 is appended to the bottom of array_1
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(INOUT), ALLOCATABLE :: array_1(:)
        COMPLEX(KIND = 16), INTENT(IN)                 :: array_2(:)
        COMPLEX(KIND = 16), ALLOCATABLE                :: tarray(:)
        INTEGER                                     :: array_1_length, array_2_length, i

        array_1_length = SIZE(array_1)
        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN
            
            !Allocate temporary array to hold new combined array
            ALLOCATE(tarray(array_1_length + array_2_length))

            !Add elements from array_1
            DO i = 1, array_1_length
                tarray(i) = array_1(i)
            END DO

            !Add elements from array_2
            DO i = array_1_length + 1, array_2_length
                tarray(i) = array_2(i)
            END DO
        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(array_2_length))
            DO i = 1, array_2_length
                tarray(i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_C_V_V_QP

    SUBROUTINE ARRAY_APPEND_R_M_V_DP(array_1, array_2)
        !Appends a vector of reals to a matrix of reals
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        REAL(KIND = 8), INTENT(IN)                 :: array_2(:)
        REAL(KIND = 8), ALLOCATABLE                :: tarray(:,:)
        INTEGER                                    :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = 0.0_8

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = 0.0_8

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_R_M_V_DP

    SUBROUTINE ARRAY_APPEND_R_M_V_QP(array_1, array_2)
        !Appends a vector of reals to a matrix of reals
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        REAL(KIND = 16), INTENT(IN)                 :: array_2(:)
        REAL(KIND = 16), ALLOCATABLE                :: tarray(:,:)
        INTEGER                                    :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = 0.0_16

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = 0.0_16

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_R_M_V_QP

    SUBROUTINE ARRAY_APPEND_C_M_V_DP(array_1, array_2)
        !Appends a vector of complex numbers to a matrix of complex numbers
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        COMPLEX(KIND = 8), INTENT(IN)                 :: array_2(:)
        COMPLEX(KIND = 8), ALLOCATABLE                :: tarray(:,:)
        INTEGER                                       :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = (0.0_8, 0.0_8)

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = (0.0_8, 0.0_8)

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_C_M_V_DP

    SUBROUTINE ARRAY_APPEND_C_M_V_QP(array_1, array_2)
        !Appends a vector of complex numbers to a matrix of complex numbers
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        COMPLEX(KIND = 16), INTENT(IN)                 :: array_2(:)
        COMPLEX(KIND = 16), ALLOCATABLE                :: tarray(:,:)
        INTEGER                                       :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = (0.0_16, 0.0_16)

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = (0.0_16, 0.0_16)

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_C_M_V_QP

    SUBROUTINE ARRAY_APPEND_I_M_V_SP(array_1, array_2)
        !Appends a vector of integers to a matrix of integers
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        INTEGER, INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        INTEGER, INTENT(IN)                 :: array_2(:)
        INTEGER, ALLOCATABLE                :: tarray(:,:)
        INTEGER                             :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = 0

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = 0

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_I_M_V_SP

    SUBROUTINE ARRAY_APPEND_I_M_V_DP(array_1, array_2)
        !Appends a vector of integers to a matrix of integers
        !array_2 is appended as a row to the bottom of array_1
        !If array_2 has more columns than array_1 then array_1
        ! is adjusted to match this, and its new empty elements are set to zero
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(INOUT), ALLOCATABLE :: array_1(:,:)
        INTEGER(KIND = 8), INTENT(IN)                 :: array_2(:)
        INTEGER(KIND = 8), ALLOCATABLE                :: tarray(:,:)
        INTEGER(KIND = 8)                             :: array_1_rows, array_1_cols, array_2_length, i

        array_2_length = SIZE(array_2)

        !If the first array is allocated then append the two
        IF (ALLOCATED(array_1) .EQV. .TRUE.) THEN

            array_1_rows = SIZE(array_1,1)
            array_1_cols = SIZE(array_1,2)
            
            !If more columns in array_2
            IF (array_1_cols < array_2_length) THEN

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_2_length))
                tarray = 0

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,1:array_1_cols) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,:) = array_2

            !If more columns in array_1
            ELSE

                !Allocate temporary array to hold new combined array
                ALLOCATE(tarray(array_1_rows + 1, array_1_cols))
                tarray = 0

                !Add elements from array_1
                DO i = 1, array_1_rows
                    tarray(i,:) = array_1(i,:)
                END DO

                !Add elements from array_2
                tarray(array_1_rows + 1,1:array_2_length) = array_2

            END IF

        !If the first array is not allocated then allocate 
        ! it to the length of array_2 and add elements
        ELSE
            ALLOCATE(tarray(1, array_2_length))
            DO i = 1, array_2_length
                tarray(1,i) = array_2(i)
            END DO        
        END IF

        CALL MOVE_ALLOC(tarray, array_1)

    END SUBROUTINE ARRAY_APPEND_I_M_V_DP


    FUNCTION FLIPUD_R_V_QP(input) RESULT(output)
        !Flips a vector of quadruple precision reals 
        IMPLICIT NONE
        REAL(KIND = 16), INTENT(IN)       :: input(:)
        REAL(KIND = 16)                   :: output(SIZE(input))
        INTEGER                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_R_V_QP

    FUNCTION FLIPUD_R_V_DP(input) RESULT(output)
        !Flips a vector of double precision reals 
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)        :: input(:)
        REAL(KIND = 8)                    :: output(SIZE(input))
        INTEGER                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_R_V_DP

    FUNCTION FLIPUD_C_V_QP(input) RESULT(output)
        !Flips a vector of quadruple precision complexes
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)    :: input(:)
        COMPLEX(KIND = 16)                :: output(SIZE(input))
        INTEGER                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_C_V_QP

    FUNCTION FLIPUD_C_V_DP(input) RESULT(output)
        !Flips a vector of double precision complexes
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)     :: input(:)
        COMPLEX(KIND = 8)                 :: output(SIZE(input))
        INTEGER                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_C_V_DP

    FUNCTION FLIPUD_I_V_SP(input) RESULT(output)
        !Flips a vector of single precision integers
        IMPLICIT NONE
        INTEGER, INTENT(IN)               :: input(:)
        INTEGER                           :: output(SIZE(input))
        INTEGER                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_I_V_SP

    FUNCTION FLIPUD_I_V_DP(input) RESULT(output)
        !Flips a vector of double precision integers
        IMPLICIT NONE
        INTEGER(KIND = 8), INTENT(IN)               :: input(:)
        INTEGER(KIND = 8)                           :: output(SIZE(input))
        INTEGER(KIND = 8)                           :: i

    !Fill output with input backwards  
        DO i = 0, SIZE(input)-1
            output(i+1) = input(SIZE(input) - i)
        END DO

    END FUNCTION FLIPUD_I_V_DP

    FUNCTION EUC_NORM_R_DP(vector) RESULT(out)
        !Calculates the euclidian norm of a double precision real vector
        REAL(KIND = 8)  :: out, vector(:)
        INTEGER         :: i

        out = 0.0_8
        DO i = 1,size(vector)
              out = out + vector(i)*vector(i)
        END DO

        out = sqrt(out)
    END FUNCTION EUC_NORM_R_DP
    
    FUNCTION EUC_NORM_R_QP(vector) RESULT(out)
        !Calculates the euclidian norm of a quadruple precision real vector
        REAL(KIND = 16) :: out, vector(:)
        INTEGER         :: i

        out = 0.0_16
        DO i = 1,size(vector)
              out = out + vector(i)*vector(i)
        END DO

        out = sqrt(out)
    END FUNCTION EUC_NORM_R_QP

    FUNCTION ANGLE_R_DP(v,u) RESULT(out)
        !Calculates the angle between two double precision real vectors
        REAL(KIND = 8), INTENT(IN)  :: v(:), u(:)
        REAL(KIND = 8)              :: out
        INTEGER                     :: i

        out = 0.0_8
        IF(size(v) /= size(u)) THEN
              write(6,'(A)') "Vectors of unequal length in call to angle"
              STOP
        ELSE IF(all(u == 0.0_8) .OR. all(v == 0.0_8)) THEN
              !write(6,'(A)') "One vector is null in call to angle"
              RETURN
        END IF

        DO i = 1, size(v)
              out = out + v(i)*u(i)
        END DO

        out = out/(radial(v)*radial(u))
        out = acos(out)
    END FUNCTION ANGLE_R_DP
    
    FUNCTION ANGLE_R_QP(v,u) RESULT(out)
        !Calculates the angle between two quadruple precision real vectors
        REAL(KIND = 16), INTENT(IN)  :: v(:), u(:)
        REAL(KIND = 16)              :: out
        INTEGER                     :: i

        out = 0.0_16
        IF(size(v) /= size(u)) THEN
              write(6,'(A)') "Vectors of unequal length in call to angle"
              STOP
        ELSE IF(all(u == 0.0_16) .OR. all(v == 0.0_16)) THEN
              !write(6,'(A)') "One vector is null in call to angle"
              RETURN
        END IF

        DO i = 1, size(v)
              out = out + v(i)*u(i)
        END DO

        out = out/(radial(v)*radial(u))
        out = acos(out)
    END FUNCTION ANGLE_R_QP

    FUNCTION CHECK_HERMITIAN_DP(matrix) RESULT(out)
        !Returns SUM(matrix * matrix^+) for a double precision matrix of complexes
        IMPLICIT NONE
        COMPLEX(KIND = 8) :: matrix(:,:)
        REAL(KIND = 8)    :: temp(size(matrix,1), size(matrix,2))
        REAL(KIND = 8)    :: out

        temp = abs(conjg(transpose(matrix)) - matrix)
        out = sum(temp)

    END FUNCTION CHECK_HERMITIAN_DP
    
    FUNCTION CHECK_HERMITIAN_QP(matrix) RESULT(out)
        !Returns SUM(matrix * matrix^+) for a quadruple precision matrix of complexes
        IMPLICIT NONE
        COMPLEX(KIND = 16) :: matrix(:,:)
        REAL(KIND = 16)    :: temp(size(matrix,1), size(matrix,2))
        REAL(KIND = 16)    :: out

        temp = abs(conjg(transpose(matrix)) - matrix)
        out = sum(temp)

    END FUNCTION CHECK_HERMITIAN_QP

    FUNCTION FACTORIAL(IN) RESULT(OUT)
        !Calculates factorials
        !$n!$
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: IN
        INTEGER             :: OUT, N

        IF (IN == 0) THEN
            OUT = 0
            RETURN
        END IF

        OUT = 1
        N = IN

        DO WHILE (IN /= 0)
            OUT = OUT * N
            N   = N - 1
        END DO
    END FUNCTION FACTORIAL

    FUNCTION NCR(N, R) RESULT(C)
        !Calulates the number of combinations of objects (n) in a sample (r)
        !$C(n,r) = \frac{n!}{r!(n-r)!}$
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: N, R
        INTEGER             :: C

        C = FACTORIAL(N)/(FACTORIAL(R) * (FACTORIAL(N - R)))

    END FUNCTION NCR

    FUNCTION NPR(N, R) RESULT(P)
        !Calulates the number of permutations of objects (n) in a sample (r)
        !$P(n,r) = \frac{n!}{(n-r)!}$
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: N, R
        INTEGER             :: P

        P = FACTORIAL(N)/FACTORIAL(N - R)

    END FUNCTION NPR


END MODULE matrix_tools