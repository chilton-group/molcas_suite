PROGRAM EXTRACTOR
    USE, INTRINSIC::iso_fortran_env
    USE matrix_tools
    USE stev_ops
    IMPLICIT NONE

    CHARACTER(LEN = 5000)             :: input_file
    CHARACTER(LEN = 2)                :: element, orb_type
    CHARACTER(LEN = 1)                :: yesno
    REAL(KIND = 8)                    :: threshold, CFP(3,27)
    REAL(KIND = 8), ALLOCATABLE       :: energy(:),g(:,:),g_vec(:,:,:),ang(:),composition(:,:), cf_percents(:,:), cf_energies(:)
    LOGICAL                           :: Kramers
    INTEGER                           :: num_states, k_max_index
    INTEGER, ALLOCATABLE              :: g_extr_locs(:,:)

    !Read user input from command line
    CALL READ_USER_INPUT(element, orb_type, num_states, input_file, yesno, threshold)

    !Extract info from input_file (molcas output file)
    CALL EXTRACT_INFO(input_file, element, orb_type, yesno, threshold, g, g_vec, CFP, k_max_index, num_states, energy, composition)

    !Calculate crystal field energies using SINGLE_ANISO parameters with k <= 2J
    ! Returns unique energies only, i.e. does not repeat degenerate levels
    CALL CALCULATE_CF_PROPS(CFP, cf_energies, k_max_index, num_states, cf_percents)
    
    !If Kramers, calculate maximum and minimum of x,y, and z g values of each state
    IF(MODULO(num_states,2) == 0) THEN !Check if Kramers
        CALL ORDER_G(num_states, g, g_vec, ang, g_extr_locs)
        Kramers = .TRUE.
    ELSE
        Kramers = .FALSE.
    END IF

    CALL WRITE_OUTPUT_FILE(num_states, Kramers, CFP, input_file, energy, cf_energies, g, g_extr_locs, ang, composition)
    
    CONTAINS

    SUBROUTINE READ_USER_INPUT(element, orb_type, num_states, file, yesno, threshold)
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(OUT) :: element, orb_type, file, yesno
        CHARACTER(LEN = 100)            :: dummyC
        INTEGER, INTENT(OUT)            :: num_states
        INTEGER                         :: dummyI
        REAL(KIND = 8), INTENT(OUT)     :: threshold
        LOGICAL                         :: file_exists

        !Read element 
        CALL GET_COMMAND_ARGUMENT(1,element)
        
        !Print help
        IF(TRIM(element) == '-h' .OR. TRIM(element) == '') THEN
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'molcas_extractor_new <element> <orb-type> <num_states> <file> [<orbital_print>] [<threshold>]'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Extracts energies, CFPs (and CFP calculated energies and wavefunctions), principal g values,'
            WRITE(6,'(A)')   ' g tensors, angles of maximal g components to ground state, and spin-orbit weights of the '
            WRITE(6,'(A)')   ' first num_states in a given molcas output file and writes to <file>_extracted.out'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'element        : CHARACTER          e.g. dy'
            WRITE(6,'(A)') 'orb-type       : CHARACTER          e.g  4f'
            WRITE(6,'(A)') 'num_states     : INTEGER            e.g  16  for Dy3+ J=15/2 multiplet'
            WRITE(6,'(A)') 'file           : CHARACTER          e.g  dy.out'
            WRITE(6,'(A)') 'orbital_print  * OPTIONAL CHARACTER e.g  y/n for contrib. of orbs to active space'
            WRITE(6,'(A)') 'threshold      * OPTIONAL NUMBER    e.g  threshold for printing contrib. of orbs to active space'
            WRITE(6,'(A)') ''
            WRITE(6,'(A)') 'Example ------ molcas_extractor_new dy 4f 16 dy.out y'
            WRITE(6,'(A)') ''

            STOP
        END IF

        !Convert element to uppercase
        CALL uppercase(element)

        !Read orbital type
        CALL GET_COMMAND_ARGUMENT(2,orb_type)

        !Read number of states
        CALL GET_COMMAND_ARGUMENT(3,dummyC)
        READ(dummyC,*) num_states

        !Read molcas .out file name
        CALL GET_COMMAND_ARGUMENT(4,file)

        !Check file exists
        INQUIRE(FILE=TRIM(file), EXIST=file_exists)
        IF (file_exists .EQV. .FALSE.) THEN
            WRITE(6,'(A)') 'Specified file does not exist'
            STOP
        END IF
    

        !Read yesno for active space orbital printing
        CALL GET_COMMAND_ARGUMENT(5,yesno)
        IF(TRIM(yesno) /= 'y' .AND. TRIM(yesno) /= 'Y') yesno = 'n'

        !Read threshold value for printing active space orbital contributions
        CALL GET_COMMAND_ARGUMENT(6,dummyC)
        !Check if temp is an integer, if it isnt THEN set threshold manually
        READ(dummyC,*,IOSTAT=dummyI) threshold
        IF(dummyI /= 0 .and. TRIM(orb_type) == '4f') threshold = 0.01_8
        IF(dummyI /= 0 .and. TRIM(orb_type) == '3d') threshold = 0.025_8

    END SUBROUTINE

    SUBROUTINE EXTRACT_INFO(input_file, element, orb_type, yesno, threshold, g, g_vec, CFP, k_max_index, num_states, energy, composition)
        IMPLICIT NONE
        CHARACTER(LEN = 500)                        :: Sdummy
        CHARACTER(LEN = *), INTENT(IN)              :: input_file, element, orb_type, yesno
        CHARACTER(LEN = 5000)                       :: line
        CHARACTER(LEN = 10), ALLOCATABLE            :: atoms(:)
        REAL(KIND = 8)                              :: Rdummy, dummyR, shift, wf(8)
        REAL(KIND = 8), INTENT(IN)                  :: threshold
        REAL(KIND = 8), ALLOCATABLE, INTENT(OUT)    :: energy(:), g(:,:), g_vec(:,:,:), composition(:,:)
        REAL(KIND = 8), INTENT(OUT)                 :: CFP(3,27)
        REAL(KIND = 8), ALLOCATABLE                 :: ang(:), wavefunc(:,:)
        INTEGER, INTENT(OUT)                        :: k_max_index
        INTEGER                                     :: i, j, k, p, q, inactive, count, converge, dummyI, active, impure, start_line(100), secondary, basis, clean, iostatus, wfspec, roots, rasscf_files, END_orbs, col, row, reason
        INTEGER, ALLOCATABLE                        :: g_extr_locs(:,:)
        INTEGER, INTENT(IN)                         :: num_states



        !Allocate energy and gvector arrays with number of states
        !If Kramers then only count degenerate states once
        IF (MODULO(num_states,2) == 0) THEN
            ALLOCATE(energy(num_states/2),g(num_states/2,3),g_vec(num_states/2,3,3))
        ELSE
            ALLOCATE(energy(num_states))
        END IF

        ALLOCATE(composition(num_states,num_states))


        converge = 2
        clean = 2
        rasscf_files = 0

        !Read in file and extract information on calculation
        OPEN(30,FILE = TRIM(input_file), STATUS = 'OLD')
            DO WHILE (.TRUE.)
                READ(30,'(A)',END=30, IOSTAT = reason) line
                CALL CHECK_IO(reason)

                !Read RASSCF Section
                IF(TRIM(line(1:27)) == '--- Start Module: rasscf at') THEN
                    WRITE(6,'(A)') 'RASSCF section: '//TRIM(line(29:))
                    converge = 0
                    clean = 0
                    wfspec = 0
                    DO WHILE(.TRUE.)
                        READ(30,'(A)',END=30, IOSTAT = reason) line
                        CALL CHECK_IO(reason)
                        !WRITE(6,'(A)') '#!#!#! '//TRIM(line)

                        !Read wavefunction specifications
                        IF(wfspec == 0 .and. TRIM(line) == '++    Wave function specifications:') THEN
                            wfspec = 1
                            DO i = 1,7
                                READ(30,'(A)',END=30, IOSTAT = reason) line
                                CALL CHECK_IO(reason)
                            END DO
                            !Read number of inactive orbitals
                            READ(line(34:),*) inactive
                            WRITE(6,'(A10,I11)') 'Inactive: ',inactive
                            READ(30,'(A)',END=30, IOSTAT = reason) line
                            CALL CHECK_IO(reason)

                            !Read number of active orbitals
                            READ(line(32:),*) active
                            WRITE(6,'(A8,I11)') 'Active: ',active
                            READ(30,'(A)',END=30, IOSTAT = reason) line
                            CALL CHECK_IO(reason)

                            !Read number of secondary orbitals
                            READ(line(35:),*) secondary
                            basis = inactive+active+secondary
                            WRITE(6,'(A7,I14)') 'Basis: ',basis
                            READ(30,'(A)',END=30, IOSTAT = reason) line
                            CALL CHECK_IO(reason)

                            !Read spin
                            READ(line(26:),*) dummyR
                            WRITE(6,'(A6,I13,A2)') 'Spin: ',nint(2*dummyR),'/2'

                        END IF

                        !Read molecular charge
                        IF(TRIM(line(1:28)) == '      Total molecular charge') THEN
                            READ(line(29:),*) dummyR
                            WRITE(6,'(A8,F13.1)') 'Charge: ',dummyR
                        END IF

                        !Read total energy shift
                        IF(TRIM(line(1:41)) == '      Total energies have been shifted by') THEN
                            READ(line(42:),*) shift
                        END IF

                        !Read convergence information and print to screen
                        IF(TRIM(line(1:23)) == '      Convergence after') THEN
                            converge = 1
                            WRITE(6,'(A)') 'Convergence:      YES'
                            READ(30,'(A)',END=30, IOSTAT = reason) line
                            CALL CHECK_IO(reason)

                            !Read ground state energy and print to screen
                            READ(line,*) dummyI,dummyI,dummyI,dummyI,dummyR
                            WRITE(6,'(A8,F24.8)') 'Energy: ',dummyR+shift
                        END IF

                        !Read number of roots and print to screen
                        IF(TRIM(line(1:32)) == '      Number of root(s) required') THEN
                            READ(line(33:),*) roots
                            WRITE(6,'(A7,I14)') 'Roots: ',roots
                        END IF

                        !Read Molecular orbitals section
                        IF(TRIM(line) == '      Molecular orbitals for symmetry species 1: a') THEN
                            IF(converge == 0) THEN
                                WRITE(6,'(A)') 'Convergence:      NO'
                            END IF

                            !Flag for impure orbitals
                            impure = 0

                            !Flag for reaching the end of the orbitals section
                            END_orbs = 0

                            !Allocate wavefunction array
                            ALLOCATE(wavefunc(0:basis,50),atoms(basis))
                            wavefunc = 0.0_8
                            
                            !Loop over the file and read in all contributions from AOs to MOs
                            ! for the MOs of the active space.
                            DO WHILE (.TRUE.)
                                READ(30,'(A)',END=30, IOSTAT = reason) line
                                CALL CHECK_IO(reason)

                                !Read in contributions to each molecular orbital
                                IF(line(1:13) == '      Orbital') THEN

                                    !If iostatus is 0 then the end of the line has been reached, and therefore 
                                    ! all orbitals have been read
                                    READ(line(107:116),*,IOSTAT=iostatus) dummyI
                                    IF(iostatus /= 0) READ(line(97:106),*,IOSTAT=iostatus) dummyI
                                    IF(iostatus /= 0) READ(line(87:96),*,IOSTAT=iostatus) dummyI
                                    IF(iostatus /= 0) READ(line(77:86),*,IOSTAT=iostatus) dummyI
                                    IF(iostatus /= 0) READ(line(67:76),*,IOSTAT=iostatus) dummyI
                                    IF(iostatus /= 0) READ(line(57:66),*,IOSTAT=iostatus) dummyI
                                    IF(iostatus /= 0) READ(line(47:56),*,IOSTAT=iostatus) dummyI
                                    IF(iostatus /= 0) READ(line(37:46),*,IOSTAT=iostatus) dummyI
                                    IF(iostatus /= 0) READ(line(27:36),*,IOSTAT=iostatus) dummyI
                                    IF(iostatus /= 0) READ(line(17:26),*,IOSTAT=iostatus) dummyI

                                    ! If the orbital index is less than those of the active space keep reading until 
                                    ! it is reached. When it is reaced, read in the contributions to the MOs of the 
                                    ! active space

                                    IF(dummyI >= inactive+1) THEN 

                                        !Read contributions
                                        DO q = 0,40,10
                                            READ(line(17:26),*,IOSTAT=iostatus) wavefunc(0,1+q)
                                            IF(iostatus == 0) READ(line(27:36),*,IOSTAT=iostatus) wavefunc(0,2+q)
                                            IF(iostatus == 0) READ(line(37:46),*,IOSTAT=iostatus) wavefunc(0,3+q)
                                            IF(iostatus == 0) READ(line(47:56),*,IOSTAT=iostatus) wavefunc(0,4+q)
                                            IF(iostatus == 0) READ(line(57:66),*,IOSTAT=iostatus) wavefunc(0,5+q)
                                            IF(iostatus == 0) READ(line(67:76),*,IOSTAT=iostatus) wavefunc(0,6+q)
                                            IF(iostatus == 0) READ(line(77:86),*,IOSTAT=iostatus) wavefunc(0,7+q)
                                            IF(iostatus == 0) READ(line(87:96),*,IOSTAT=iostatus) wavefunc(0,8+q)
                                            IF(iostatus == 0) READ(line(97:106),*,IOSTAT=iostatus) wavefunc(0,9+q)
                                            IF(iostatus == 0) READ(line(107:116),*,IOSTAT=iostatus) wavefunc(0,10+q)

                                            DO i = 1, 3
                                                READ(30,'(A)',END=33, IOSTAT = reason) line
                                                CALL CHECK_IO(reason)
                                            END DO


                                            DO j = 1,basis
                                                READ(30,'(A)',END=33, IOSTAT = reason) line
                                                CALL CHECK_IO(reason)
                                                !WRITE(6,'(A)') "¬¬  "//TRIM(line)
                                                READ(line(7:16),'(A)',IOSTAT=iostatus) atoms(j)
                                                IF(nint(wavefunc(0,10+q)) /= 0) THEN
                                                    READ(line(17:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 10+q)
                                                ELSE IF(nint(wavefunc(0,9+q)) /= 0) THEN
                                                    END_orbs = 1
                                                    READ(line(17:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 9+q)
                                                ELSE IF(nint(wavefunc(0,8+q)) /= 0) THEN
                                                    END_orbs = 1
                                                    READ(line(17:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 8+q)
                                                ELSE IF(nint(wavefunc(0,7+q)) /= 0) THEN
                                                    END_orbs = 1
                                                    READ(line(17:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 7+q)
                                                ELSE IF(nint(wavefunc(0,6+q)) /= 0) THEN
                                                    END_orbs = 1
                                                    READ(line(17:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 6+q)
                                                ELSE IF(nint(wavefunc(0,5+q)) /= 0) THEN
                                                    END_orbs = 1
                                                    READ(line(17:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 5+q)
                                                ELSE IF(nint(wavefunc(0,4+q)) /= 0) THEN
                                                    END_orbs = 1
                                                    READ(line(17:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 4+q)
                                                ELSE IF(nint(wavefunc(0,3+q)) /= 0) THEN
                                                    END_orbs = 1
                                                    READ(line(17:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 3+q)
                                                ELSE IF(nint(wavefunc(0,2+q)) /= 0) THEN
                                                    END_orbs = 1
                                                    READ(line(17:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 2+q)
                                                ELSE IF(nint(wavefunc(0,1+q)) /= 0) THEN
                                                    END_orbs = 1
                                                    READ(line(17:),'(10F10.4)',IOSTAT=iostatus) (wavefunc(j,col), col = 1+q, 1+q)
                                                END IF

                                            END DO
                                            DO i = 1, 3
                                                READ(30,'(A)',END=33, IOSTAT = reason) line
                                                CALL CHECK_IO(reason)
                                            END DO

                                            !If end has been reached then exit the infinite loop
                                            IF(END_orbs == 1) EXIT
                                        END DO
    33                                  CONTINUE
                                        !Calculate percentage contributions from each AO to the MOs of the active space
                                        !If they are requested, and are greater than the threshold, then print them to screen
                                        DO j = 1,50
                                            IF(nint(wavefunc(0,j)) >= inactive+1 .and. nint(wavefunc(0,j)) <= inactive+active) THEN
                                                IF(yesno == 'y') WRITE(6,*) 'orb',nint(wavefunc(0,j))
                                                Rdummy = 0.0_8
                                                DO k = 1,basis
                                                    Rdummy = Rdummy + wavefunc(k,j)*wavefunc(k,j)
                                                    wavefunc(k,j) = wavefunc(k,j)*wavefunc(k,j)
                                                END DO
                                                wavefunc(1:basis,j) = wavefunc(1:basis,j)/Rdummy
                                                DO k = 1,basis
                                                    IF(yesno == 'y' .and. wavefunc(k,j) >= threshold) WRITE(6,*) atoms(k),wavefunc(k,j)
                                                    !WRITE(6,'(A,F10.4)') atoms(k),wavefunc(k,j)
                                                    IF(wavefunc(k,j) >= threshold .and. atoms(k)(7:8) /= orb_type) impure = 1
                                                END DO
                                                IF(yesno == 'y') WRITE(6,*)
                                            END IF
                                        END DO
                                        EXIT
                                    END IF
                                END IF
                            END DO
                            DEALLOCATE(wavefunc,atoms)

                            !Print warning if active space MOs are not purely the requested AOs
                            IF(impure == 1) WRITE(6,'(A)') 'Active space:     DIRTY'
                            
                            !Else print that the MOs are clean
                            IF(impure == 0) THEN
                                WRITE(6,'(A)') 'Active space:     CLEAN'
                                clean = 1
                            END IF

                            !If clean and converged
                            IF(clean == 1 .and. converge == 1) THEN
                                WRITE(6,'(A)') '                                        SUCCESS'
                            !If either dirty or non-converged
                            ELSE IF(clean == 0 .or. converge == 0) THEN
                                WRITE(6,'(A)') '                                           FAIL'
                            END IF

                            !Set clean and converged to 2 to indicate the entire loop is done
                            clean = 2
                            converge = 2
                            WRITE(6,'(A)') '==============================================='
                        END IF
                    END DO
                END IF
    66          CONTINUE
            END DO
    30      CONTINUE



            !Go back to the start of the file
            REWIND(30)
            j = 0
            count = 0

            !Read in RASSI Section, check for completion and count how many exist
            DO WHILE (.TRUE.)
                READ(30,'(A)',END=31, IOSTAT = reason) line
                CALL CHECK_IO(reason)
                j = j + 1
                IF(TRIM(line(1:26)) == '--- Start Module: rassi at') THEN
                    WRITE(6,'(A)') 'RASSI section: '//TRIM(line(28:))
                END IF
                IF(TRIM(line) == '   Eigenvalues of complex Hamiltonian:') THEN
                    count = count + 1
                    start_line(count) = j
                    WRITE(6,'(A)') '                                        SUCCESS'
                END IF
            END DO

    31      CONTINUE

            !If multiple RASSI sections detected, ask user which one they want to use
            dummyI = count
            
            IF(count > 1) THEN
                WRITE(6,'(A6,I2,A38)') 'Found ',count,' RASSI sections, which one to extract?'
                READ(*,*) dummyI
                IF(dummyI > count) dummyI = count
            END IF
            REWIND(30)

            !Use the chosen RASSI section
            !Loop over and read in data
            IF(count > 0) THEN
                j = 0
                DO WHILE (.TRUE.)
                    j = j + 1
                    READ(30,'(A)') line
                    IF(j < start_line(dummyI)) CYCLE

                    !Read in spin-orbit energies
                    IF(TRIM(line) == '   Eigenvalues of complex Hamiltonian:') THEN
                        DO i = 1,6
                            READ(30,*, IOSTAT = reason) 
                            CALL CHECK_IO(reason)
                        END DO
                        !If Kramers read degenerate states once
                        IF (MODULO(num_states,2) == 0) THEN
                            DO i = 1,num_states/2
                                READ(30,*,IOSTAT = reason) Rdummy,Rdummy,Rdummy,energy(i)
                                CALL CHECK_IO(reason)
                                READ(30,*,IOSTAT = reason)
                                CALL CHECK_IO(reason)
                            END DO
                        !If non-Kramers read all states
                        ELSE
                            DO i = 1,num_states
                                READ(30,*,IOSTAT = reason) Rdummy,Rdummy,Rdummy,energy(i)
                                CALL CHECK_IO(reason)
                            END DO
                        END IF
                    END IF

                    !If Kramers ion then read in g-tensors
                    IF (MODULO(num_states,2) == 0) THEN
                        IF(TRIM(line) == '   g-Matrix Approach II') THEN
                            DO i = 1,7
                                READ(30,*, IOSTAT = reason) 
                                CALL CHECK_IO(reason)
                            END DO
                            DO i = 1,num_states/2
                                DO p = 1,5
                                    READ(30,*, IOSTAT = reason) 
                                    CALL CHECK_IO(reason)
                                END DO
                                READ(30,*, IOSTAT = reason) Sdummy,Rdummy,Rdummy,Rdummy,Sdummy,g(i,1),Sdummy,g_vec(i,1,1),g_vec(i,2,1),g_vec(i,3,1)
                                CALL CHECK_IO(reason)
                                READ(30,*, IOSTAT = reason) Sdummy,Rdummy,Rdummy,Rdummy,Sdummy,g(i,2),Sdummy,g_vec(i,1,2),g_vec(i,2,2),g_vec(i,3,2)
                                CALL CHECK_IO(reason)
                                READ(30,*, IOSTAT = reason) Sdummy,Rdummy,Rdummy,Rdummy,Sdummy,g(i,3),Sdummy,g_vec(i,1,3),g_vec(i,2,3),g_vec(i,3,3)
                                CALL CHECK_IO(reason)
                                IF(i/=num_states/2) READ(30,*)
                            END DO
                            !Quit
                            EXIT
                        END IF
                    !If non-Kramers quit
                    ELSE
                        EXIT
                    END IF

                END DO
            END IF

            !Go back to the start of the file
            REWIND(30)
            j = 0
            count = 0

            !Read in SINGLE_ANISO section and count how many there are
            DO WHILE (.TRUE.)
                READ(30,'(A)',END=32, IOSTAT=reason) line
                CALL CHECK_IO(reason)
                j = j + 1
                IF(TRIM(line(1:33)) == '--- Start Module: single_aniso at') THEN
                    IF(clean == 1 .and. converge == 1) THEN
                        WRITE(6,'(A)') '                                        SUCCESS'
                    ELSE IF(clean == 0 .or. converge == 0) THEN
                        WRITE(6,'(A)') '                                           FAIL'
                    END IF
                    clean = 2
                    converge = 2
                    WRITE(6,'(A)') '==============================================='
                    WRITE(6,'(A)') 'SINGLE_ANISO section: '//TRIM(line(34:))
                END IF
                
                IF(line(1:44) == '     CALCULATION OF CRYSTAL-FIELD PARAMETERS') THEN
                    count = count + 1
                    start_line(count) = j
                END IF
            END DO

    32      CONTINUE

            !If multiple SINGLE_ANISO sections detected, ask user which one they want to use
            IF(count > 1) THEN
                WRITE(6,'(A6,I2,A45)') 'Found ',count,' SINGLE_ANISO sections, which one to extract?'
                READ(*,*) dummyI
                IF(dummyI > count) dummyI = count
            END IF

            !Go back to the start of the file
            REWIND(30)

            !Select chosen SINGLE_ANISO section and read in Crystal field parameters in J basis
            IF(count > 0) THEN
                j = 0
                DO WHILE (.TRUE.)
                    j = j + 1
                    READ(30,'(A)', IOSTAT=reason) line
                    CALL CHECK_IO(reason)
                    IF(j < start_line(dummyI)) CYCLE
                    IF(TRIM(line) == '  k |  q  |    (Knm)^2  |         B(k,q)        |') THEN
                        READ(30,*)

                        !Set max k values
                        ! Here k_max <= 2*J
                        IF(element == 'CE' .or. element == 'SM') THEN
                            k_max_index = 2
                        ELSE
                            k_max_index = 3
                        END IF

                        !Read in Crystal field parameters
                        DO k = 1,k_max_index
                            DO q = 1,4*k+1
                                READ(30,'(A)', IOSTAT=reason) line
                                CALL CHECK_IO(reason)
                                READ(line(26:),*) CFP(k,q)
                            END DO
                            READ(30,*, IOSTAT=reason)
                            CALL CHECK_IO(reason)
                        END DO

                    END IF

                    !Read in decomposition of RASSI spin-orbit wavefunction in terms of mJ states
                    IF(line(1:43) == '  DECOMPOSITION OF THE RASSI WAVE FUNCTIONS') THEN
                        
                        READ(30,'(A)', IOSTAT = reason) line
                        CALL CHECK_IO(reason)
                        DO k = 1,num_states/4
                            DO i = 1, 4
                                READ(30,'(A)', IOSTAT = reason) line
                                CALL CHECK_IO(reason)
                            END DO
                            DO q = 1,num_states
                                READ(30,'(A)', IOSTAT = reason) line
                                CALL CHECK_IO(reason)
                                READ(line(10:29),*, IOSTAT = reason) wf(1:2)
                                CALL CHECK_IO_SA(reason)
                                READ(line(31:50),*, IOSTAT = reason) wf(3:4)
                                CALL CHECK_IO_SA(reason)
                                READ(line(52:71),*, IOSTAT = reason) wf(5:6)
                                CALL CHECK_IO_SA(reason)
                                READ(line(73:92),*, IOSTAT = reason) wf(7:8)
                                CALL CHECK_IO_SA(reason)
                                composition(q,4*(k-1)+1) = (wf(1)**2 + wf(2)**2)
                                composition(q,4*(k-1)+2) = (wf(3)**2 + wf(4)**2)
                                composition(q,4*(k-1)+3) = (wf(5)**2 + wf(6)**2)
                                composition(q,4*(k-1)+4) = (wf(7)**2 + wf(8)**2)
                            END DO
                            READ(30,'(A)', IOSTAT = reason) line
                            CALL CHECK_IO(reason)
                        END DO
                        IF(mod(num_states,4) > 0) THEN
                            wf = 0.0_8
                            DO i = 1, 4
                                READ(30,'(A)', IOSTAT = reason) line
                                CALL CHECK_IO(reason)
                            END DO
                            DO q = 1,num_states
                                READ(30,'(A)', IOSTAT = reason) line
                                CALL CHECK_IO(reason)
                                IF(mod(num_states,4) >= 1) READ(line(10:29),*) wf(1:2)
                                IF(mod(num_states,4) >= 2) READ(line(31:50),*) wf(3:4)
                                IF(mod(num_states,4) >= 3) READ(line(52:71),*) wf(5:6)
                                IF(mod(num_states,4) >= 1) composition(q,4*(num_states/4)+1) = (wf(1)**2 + wf(2)**2)
                                IF(mod(num_states,4) >= 2) composition(q,4*(num_states/4)+2) = (wf(3)**2 + wf(4)**2)
                                IF(mod(num_states,4) >= 3) composition(q,4*(num_states/4)+3) = (wf(5)**2 + wf(6)**2)
                            END DO
                            READ(30,'(A)', IOSTAT = reason) line
                            CALL CHECK_IO(reason)
                        END IF
                        EXIT
                    END IF
                END DO
            END IF
            WRITE(6,'(A)') '==============================================='
        CLOSE(30)

    END SUBROUTINE EXTRACT_INFO



    FUNCTION NOT_EITHER(num1, num2) RESULT(num3)
        !Returns the number which neither num1 and num2 are out of a list of three
        IMPLICIT NONE
        INTEGER, INTENT(IN)  :: num1, num2
        INTEGER              :: num3

        IF(num1 == 1) THEN
            IF(num2 == 2) THEN
                num3 = 3
            ELSE
                num3 = 2
            END IF
        ELSE IF(num1 == 2) THEN
            IF(num2 == 1) THEN
                num3 = 3
            ELSE
                num3 = 1
            END IF
        ELSE
            IF(num2 == 1) THEN
                num3 = 2
            ELSE
                num3 = 1
            END IF
        END IF

    END FUNCTION NOT_EITHER

    SUBROUTINE CHECK_IO(IOREPORT)
        !Checks IOSTAT return variable for errors
        IMPLICIT NONE
        INTEGER, INTENT(IN)   :: IOREPORT

        !Check file isnt corrupted or ends too soon
            IF (IOREPORT > 0)  THEN
                WRITE(6,'(A)') '!!!!                         Error reading molcas output file                              !!!!'
                STOP
            ELSE IF (IOREPORT < 0) THEN
                WRITE(6,'(A)') '!!!!                Error: Reached end of molcas output file too soon                      !!!!'
                STOP
            END IF

    END SUBROUTINE CHECK_IO

    SUBROUTINE CHECK_IO_SA(IOREPORT)
        !Checks IOSTAT return variable for errors
        !Specific for reading wavefunction contributions from single aniso
        IMPLICIT NONE
        INTEGER, INTENT(IN)   :: IOREPORT

        !Check file isnt corrupted or ends too soon
            IF (IOREPORT > 0)  THEN
                WRITE(6,'(A)') '==============================================='
                WRITE(6,'(A)') '  Error: Incorrect number of states specified  '
                WRITE(6,'(A)') '==============================================='
                STOP
            END IF

    END SUBROUTINE CHECK_IO_SA

    SUBROUTINE ORDER_G(num_states, g, g_vec, ang, g_extr_locs)
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)               :: g(:,:), g_vec(:,:,:)
        REAL(KIND = 8), INTENT(OUT), ALLOCATABLE :: ang(:)
        REAL(KIND = 8)                           :: ground_g_vec(3), current_g_vec(3)
        INTEGER                                  :: i
        INTEGER, INTENT(IN)                      :: num_states
        INTEGER, INTENT(OUT), ALLOCATABLE        :: g_extr_locs(:,:)


        ALLOCATE(g_extr_locs(num_states,3), ang(num_states))
        g_extr_locs = 0
        ang = 0.0_8
    
        DO i = 1,num_states/2
    
            !Find min, max, and other g values
            g_extr_locs(i,1) = minloc(g(i,:),1)
            g_extr_locs(i,2) = maxloc(g(i,:),1)
            g_extr_locs(i,3) = NOT_EITHER(g_extr_locs(i,1),g_extr_locs(i,2))
    
            !Catch error if min==max
            IF(g_extr_locs(i,1) == g_extr_locs(i,2)) THEN
                WRITE(6,*) "Error : min = max"
                STOP
            END IF
    
            !Catch error if min==other or max==other
            IF(g_extr_locs(i,3) == g_extr_locs(i,1) .or. g_extr_locs(i,3) == g_extr_locs(i,2)) THEN
                WRITE(6,'(A, I0)') "ERROR IN STATE ", i
                WRITE(6,'(A)') "other = min or max"
                STOP
            END IF
    
            !Calculate angle between gound doublet maximal g direction and current state (i) maximal g direction
            IF(i /= 1) THEN
                ground_g_vec = g_vec(1,g_extr_locs(1,2),:)
                current_g_vec = g_vec(i,g_extr_locs(i,2),:)
                ang(i) = G_ANGLE(ground_g_vec,current_g_vec)
                IF(ang(i) > 90.0_8) ang(i) = 180.0_8 - ang(i)
            END IF
    
        END DO

    END SUBROUTINE ORDER_G

    SUBROUTINE CALCULATE_CF_PROPS(CFP, trunc_cf_energies, k_max_index, num_states, trunc_cf_percents)
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)              :: CFP(:,:)
        REAL(KIND = 8),ALLOCATABLE              :: cf_energies(:), cf_percents(:,:)
        REAL(KIND = 8),ALLOCATABLE, INTENT(OUT) :: trunc_cf_energies(:), trunc_cf_percents(:,:)
        INTEGER, INTENT(IN)                     :: k_max_index, num_states
        INTEGER                                 :: k, q
        COMPLEX(KIND = 8),ALLOCATABLE           :: HCF(:,:), HCF_vecs(:,:), StevensOperators(:,:,:,:)

        !Allocate arrays for hamiltonian, eigenvectors, and eigenvalues
        ALLOCATE(HCF(num_states, num_states),HCF_vecs(num_states, num_states), cf_energies(num_states), cf_percents(num_states, num_states))

        !Calculate stevens operators
        CALL GET_STEVENS_OPERATORS(num_states - 1, StevensOperators)

        !Form HCF
        DO k = 1, k_max_index
            DO q = 1, 4*k+1
                HCF = HCF + CFP(k,q) * StevensOperators(k,q,:,:)
            END DO
        END DO

        !Diagonalise
        CALL DIAGONALISE('V', 'U', HCF, HCF_vecs, cf_energies)

        !Set first CF Energy as zero
	cf_energies = cf_energies - cf_energies(1) 
        IF (MODULO(num_states,2) == 0) THEN
            !For Kramers systems print every 2nd energy as degenerate

            ALLOCATE(trunc_cf_energies(num_states/2))
    
            q = 1
            DO k = 1, num_states
                IF (MODULO(k,2) > 0) THEN
                    trunc_cf_energies(q) = cf_energies(k)
                    q = q + 1
                END IF
            END DO
        ELSE
            !For non-Kramers systems print all energies
            ALLOCATE(trunc_cf_energies(num_states))
            trunc_cf_energies = cf_energies
        END IF

        !Calculate % contributions of each state to each eigenvector

        DO k = 1, num_states
            cf_percents(k,:) = DABS(REAL(HCF_vecs(:,k),8))**2 + DABS(DIMAG(HCF_vecs(:,k)))**2
        END DO

        IF (MODULO(num_states,2) == 0) THEN
            !For Kramers systems print every 2nd percentage vector as degenerate

            ALLOCATE(trunc_cf_percents(num_states/2, num_states))
    
            q = 0
            DO k = 1, num_states
                IF (MODULO(k,2) > 0) THEN
                    q = q + 1
                    trunc_cf_percents(q,:) = cf_percents(k,:)
                END IF
            END DO
        ELSE
            !For non-Kramers systems print all energies
            ALLOCATE(trunc_cf_percents(num_states, num_states))
            trunc_cf_percents = cf_percents
        END IF

    END SUBROUTINE CALCULATE_CF_PROPS

     SUBROUTINE UPPERCASE(str)
        IMPLICIT NONE
        INTEGER            :: i, gap
        CHARACTER(LEN = *) :: str
        
        GAP = ICHAR('A')-ICHAR('a')

        IF(LEN(str) > 0) THEN
            DO i = 1, LEN(str)
                IF(str(i:i) <= 'z') THEN
                    IF(str(i:i) >= 'a') str(i:i) = CHAR(ICHAR(str(i:i)) + gap)
                END IF
            END DO
        END IF

    END SUBROUTINE UPPERCASE

    RECURSIVE FUNCTION G_ANGLE(A,B) RESULT(X)
        IMPLICIT NONE
        REAL(KIND = 8),INTENT(IN) :: A(3),B(3)
        REAL(KIND = 8)            :: X, pie
        pie = 3.141592653589793238462643383279502884197_8
        X = 180.0_8*dacos((A(1)*B(1)+A(2)*B(2)+A(3)*B(3))/(G_RADIAL(A)*G_RADIAL(B)))/pie
    END FUNCTION G_ANGLE
        
    RECURSIVE FUNCTION G_RADIAL(input) RESULT(res)
        IMPLICIT NONE
        REAL(KIND = 8),INTENT(IN) :: input(3)
        REAL(KIND = 8)            :: res
        res = dsqrt(input(1)*input(1)+input(2)*input(2)+input(3)*input(3))
    END FUNCTION G_RADIAL


    SUBROUTINE WRITE_OUTPUT_FILE(num_states, Kramers, CFP, input_file, energy, cf_energies, g, g_extr_locs, ang, composition)
        IMPLICIT NONE
        INTEGER, INTENT(IN)             :: num_states, g_extr_locs(:,:)
        INTEGER                         :: row, col, i, k, p, q
        INTEGER, ALLOCATABLE            :: mJ_heads(:)
        LOGICAL, INTENT(IN)             :: Kramers
        REAL(KIND = 8), INTENT(IN)      :: CFP(:,:), energy(:), cf_energies(:), g(:,:), ang(:), composition(:,:)
        REAL(KIND = 8)                  :: J
        REAL(KIND = 8), ALLOCATABLE     :: state_labels(:)
        CHARACTER(LEN = *)              :: input_file
        CHARACTER(LEN = 3)              :: cartesian(3)
        CHARACTER(LEN = 300)            :: states_char, FMT, FMTT

        !Calculate J 
        J = (REAL(num_states, 8) - 1.0_8) / 2.0_8
        allocate(mJ_heads(num_states), state_labels(num_states))

        mJ_heads(1) = J*2.0_8

        DO i = 1, num_states-1
            mJ_heads(i + 1) = J*2.0_8 - 2*real(i,8)
        END DO

        !Open output file
        OPEN(66,FILE = TRIM(input_file) //"_extracted", STATUS = 'UNKNOWN')
    
            !Write header for file
            WRITE(66,'(A, A)') 'Extracted information from ', TRIM(input_file)
            WRITE(66, '(A)') ''
            IF(Kramers .EQV. .TRUE.) WRITE(66, '(A)') '!!!Kramers ion -- all states doubly degenerate!!!'
            WRITE(66, '(A)') ''
            WRITE(66, '(A)') ''
    
            !If Kramers - write molcas energies. crystal field energies, principal g values, and gz angles.
                IF(Kramers .EQV. .TRUE.) THEN 
                    WRITE(66,'(A)') '     Molcas       Crystal Field               Principal G values'
                    WRITE(66,'(A)') '  Energy (cm-1)   Energy (cm-1)         Gx             Gy             Gz           Angle'
                    DO i = 1,num_states/2
                        IF(i /= 1) THEN
                            WRITE(66,'(6F15.7)') energy(i), cf_energies(i), g(i,g_extr_locs(i,1)), g(i,g_extr_locs(i,3)), g(i,g_extr_locs(i,2)), ang(i)
                        ELSE
                            WRITE(66,'(5F15.7,A)') energy(i), cf_energies(i), g(i,g_extr_locs(i,1)), g(i,g_extr_locs(i,3)), g(i,g_extr_locs(i,2)),'        --     '
                        END IF
                    END DO
        
                    cartesian = ['x  ', 'y  ', 'z  ']
        
                    !Write g tensors for each state if Kramers
                    WRITE(66,'(A)') ''
                    WRITE(66,'(A)') 'G tensors:'
                    DO i = 1,num_states/2
                        WRITE(66, '(A, I0, A)')  'State ',i, ' :'
                        WRITE(66, '(A)') '            Gx              Gy             Gz'
                        IF(i /= 1) THEN
                            WRITE(66,'(A, 3F15.7)') (cartesian(col), g_vec(i,g_extr_locs(i,1),col),g_vec(i,g_extr_locs(i,3),col),g_vec(i,g_extr_locs(i,2),col), col = 1, 3)
                        ELSE
                            WRITE(66,'(A, 3F15.7)') (cartesian(col), g_vec(i,g_extr_locs(i,1),col),g_vec(i,g_extr_locs(i,3),col),g_vec(i,g_extr_locs(i,2),col), col = 1, 3)
                        END IF
                        WRITE(66,'(A)')
                    END DO
                    WRITE(66,'(A)')
        
                    !Write molcas contributions from each state
                    WRITE(66,'(A)') ''
                    WRITE(66,'(A)') 'SINGLE_ANISO: % contributions from spin-orbit states to molcas eigenstates:'
                    WRITE(66,'(A)') '‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾'
            
                    WRITE(FMTT,*) '(I3,A',(',I3,A',i = 2, num_states),')'
                    WRITE(states_char,TRIM(FMTT)) (INT(mj_heads(i)),'/2     ',i = 1, num_states)
                    WRITE(66,'(A,A)') ' mJ =       ', TRIM(states_char)
            
                    WRITE(FMT, '(A, I0, A)')'(',num_states+1,'A)'
                    WRITE(66,FMT) '‾‾‾‾‾‾‾‾‾‾',('‾‾‾‾‾‾‾‾‾‾',col = 1,num_states)
            
                    WRITE(FMT, '(A, I0, A)') '(A, I0, ', num_states,'F10.3)'
                    DO i = 1, num_states/2
                        WRITE(66,FMT) 'State ', i, (100.0_8*composition(row, 2*i-1), row = 1, num_states)
                    END DO
            
                    !Write crystal field contributions from each state
                    WRITE(66,'(A)') ''
                    WRITE(66,'(A)') 'Crystal Field: % contributions from mJ states to crystal field eigenstates:'
                    WRITE(66,'(A)') '‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾'
            
                    WRITE(66,'(A,A)') ' mJ =       ', TRIM(states_char)
            
                    WRITE(FMT, '(A, I0, A)')'(',num_states+1,'A)'
                    WRITE(66,FMT) '‾‾‾‾‾‾‾‾‾‾',('‾‾‾‾‾‾‾‾‾‾',col = 1,num_states)
            
                    WRITE(FMT, '(A, I0, A)') '(A, I0, ', num_states,'F10.3)'
                    DO i = 1, num_states/2
                        WRITE(66,FMT) 'State ', i, (100.0_8*cf_percents(i,col),col = 1, num_states)
                    END DO
        
                !If non-Kramers write molcas energies, cf energies, and wavefunction compositions
                ELSE 
                    WRITE(66,'(A)') '     Molcas       Crystal Field'
                    WRITE(66,'(A)') '  Energy (cm-1)   Energy (cm-1)'
                    DO i = 1,num_states
                        IF(i /= 1) THEN
                            WRITE(66,'(2F15.7)') energy(i), cf_energies(i)
                        ELSE
                            WRITE(66,'(2F15.7,A)') energy(i), cf_energies(i)
                        END IF
                    END DO
                    WRITE(66,'(A)')
        
                    !Write molcas contributions from each state
                    WRITE(66,'(A)') ''
                    WRITE(66,'(A)') 'Molcas: % contributions from spin-orbit states to molcas eigenstates:'
                    WRITE(66,'(A)') '‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾'
            
                    WRITE(FMTT,*) '(F5.1,A',(',F5.1,A',i = 2, num_states),')'
                    WRITE(states_char,TRIM(FMTT)) (state_labels(i),'     ',i = 1, num_states)
                    WRITE(66,'(A,A)') ' mJ =     ', TRIM(states_char)
            
                    WRITE(FMT, '(A, I0, A)')'(',num_states+1,'A)'
                    WRITE(6,FMT) '‾‾‾‾‾‾‾‾‾‾',('‾‾‾‾‾‾‾‾‾‾',col = 1,num_states)
            
                    WRITE(FMT, '(A, I0, A)') '(A, I2, ', num_states,'F10.3)'
                    DO i = 1, num_states
                        WRITE(66,FMT) 'State ', i, (100.0_8*composition(row, i), row = 1, num_states)
                    END DO
            
                    !Write crystal field contributions from each state
                    WRITE(66,'(A)') ''
                    WRITE(66,'(A)') 'Crystal Field: % contributions from mJ states to crystal field eigenstates:'
                    WRITE(66,'(A)') '‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾'
            
                    WRITE(66,'(A,A)') ' mJ =     ', TRIM(states_char)
            
                    WRITE(FMT, '(A, I0, A)')'(',num_states+1,'A)'
                    WRITE(66,FMT) '‾‾‾‾‾‾‾‾‾‾',('‾‾‾‾‾‾‾‾‾‾',col = 1,num_states)
            
                    WRITE(FMT, '(A, I0, A)') '(A, I2, ', num_states,'F10.3)'
                    DO i = 1, num_states
                        WRITE(66,FMT) 'State ', i, (100.0_8*cf_percents(i,col),col = 1, num_states)
                    END DO
                END IF
        
                !Write crystal field parameters in PHI format
                WRITE(66,'(A)')
                WRITE(66,'(A)') 'Crystal field parameters in PHI format - these include OEFs :'
                WRITE(66,'(A)') '‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾'
                DO k = 1,3
                    p = -1
                    DO q = 1,4*k+1
                        p = p + 1
                        WRITE(66,'(3I3,E22.14E2)') 1,2*k,-2*k+p,CFP(k,q)
                    END DO
                END DO

                !Write crystal field parameters
                WRITE(66,'(A)')
                WRITE(66,'(A)') 'Crystal field parameters in Stevens form - these include OEFs :'
                WRITE(66,'(A)') '‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾'
                WRITE(66,'(A)') '  k     q     B_k^q'
                DO k = 1,3
                    p = -1
                    DO q = 1,4*k+1
                        p = p + 1
                        WRITE(66,'(I3, A, I3, A ,E22.14E2)') 2*k, '   ', -2*k+p, '   ', CFP(k,q)
                    END DO
                END DO
        CLOSE(66)

    END SUBROUTINE WRITE_OUTPUT_FILE

END program extractor
