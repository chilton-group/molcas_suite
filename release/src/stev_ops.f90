MODULE STEV_OPS 
    USE, INTRINSIC :: iso_fortran_env
    USE matrix_tools
    IMPLICIT NONE
    
    INTERFACE GET_STEVENS_OPERATORS
    !Creates Stevens operators and returns Stevens operators with
    ! with or without the angular momentum matrices
        MODULE PROCEDURE STEV_DP
        MODULE PROCEDURE STEV_QP
        MODULE PROCEDURE STEV_WITH_ANG_OPS_DP
        MODULE PROCEDURE STEV_WITH_ANG_OPS_QP
    END INTERFACE GET_STEVENS_OPERATORS

    INTERFACE STEVENS2
        !Creates 2nd rank Stevens operators
        MODULE PROCEDURE STEVENS2_DP
        MODULE PROCEDURE STEVENS2_QP
    END INTERFACE STEVENS2

    INTERFACE STEVENS4
        !Creates 4th rank Stevens operators
        MODULE PROCEDURE STEVENS4_DP
        MODULE PROCEDURE STEVENS4_QP
    END INTERFACE STEVENS4

    INTERFACE STEVENS6
        !Creates 6th rank Stevens operators
        MODULE PROCEDURE STEVENS6_DP
        MODULE PROCEDURE STEVENS6_QP
    END INTERFACE STEVENS6

CONTAINS

    SUBROUTINE STEV_QP(twoJ, StevensOperators)
        !Calculate and return stevens operators at quadruple precision
        IMPLICIT NONE
        INTEGER, INTENT(IN)                            :: twoJ
        INTEGER                                        :: i, k, q, j, col
        REAL(KIND = 16)                                :: mJ
        COMPLEX(KIND = 16), ALLOCATABLE, INTENT(OUT)   :: StevensOperators(:,:,:,:)
        COMPLEX(KIND = 16), ALLOCATABLE                :: JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        ALLOCATE(JP(twoJ+1,twoJ+1),JM(twoJ+1,twoJ+1),J2(twoJ+1,twoJ+1),JZ(twoJ+1,twoJ+1),StevensOperators(3, 13,twoJ+1,twoJ+1))
    
        JP               = (0.0_16,0.0_16)
        JM               = (0.0_16,0.0_16)
        J2               = (0.0_16,0.0_16)
        JZ               = (0.0_16,0.0_16)
        StevensOperators = (0.0_16,0.0_16)
    
        DO i = 1,twoJ+1
            JZ(i,i) = -0.5_16*twoJ + (i-1)*1.0_16
            J2(i,i) = 0.5_16*twoJ*(0.5_16*twoJ+1)
            DO k = 1,twoJ+1
                mJ = -0.5_16*twoJ + (k-1)*1.0_16
                IF (i == k+1) JP(i,k) = SQRT(0.5_16*twoJ*(0.5_16*twoJ+1)-mJ*(mJ+1.0_16))
                IF (i == k-1) JM(i,k) = SQRT(0.5_16*twoJ*(0.5_16*twoJ+1)-mJ*(mJ-1.0_16))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,StevensOperators(1, 1,:,:),StevensOperators(1, 2,:,:),StevensOperators(1, 3,:,:),StevensOperators(1, 4,:,:),StevensOperators(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,StevensOperators(2, 1,:,:),StevensOperators(2, 2,:,:),StevensOperators(2, 3,:,:),StevensOperators(2, 4,:,:),StevensOperators(2, 5,:,:),StevensOperators(2, 6,:,:),StevensOperators(2, 7,:,:),StevensOperators(2, 8,:,:),StevensOperators(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,StevensOperators(3, 1,:,:),StevensOperators(3, 2,:,:),StevensOperators(3, 3,:,:),StevensOperators(3, 4,:,:),StevensOperators(3, 5,:,:),StevensOperators(3, 6,:,:),StevensOperators(3, 7,:,:),StevensOperators(3, 8,:,:),StevensOperators(3, 9,:,:),StevensOperators(3, 10,:,:),StevensOperators(3, 11, :,:),StevensOperators(3, 12,:,:),StevensOperators(3, 13,:,:))
        
    END SUBROUTINE STEV_QP

    SUBROUTINE STEV_DP(twoJ, StevensOperators)
        !Calculate and return stevens operators at double precision
        IMPLICIT NONE
        INTEGER, INTENT(IN)                           :: twoJ
        INTEGER                                       :: i, k, q, j, col
        REAL(KIND = 8)                                :: mJ
        COMPLEX(KIND = 8), ALLOCATABLE, INTENT(OUT)   :: StevensOperators(:,:,:,:)
        COMPLEX(KIND = 8), ALLOCATABLE                :: JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        !Allocate arrays
        ALLOCATE(JP(twoJ+1,twoJ+1),JM(twoJ+1,twoJ+1),J2(twoJ+1,twoJ+1),JZ(twoJ+1,twoJ+1),StevensOperators(3, 13,twoJ+1,twoJ+1))
    
        JP               = (0.0_8,0.0_8)
        JM               = (0.0_8,0.0_8)
        J2               = (0.0_8,0.0_8)
        JZ               = (0.0_8,0.0_8)
        StevensOperators = (0.0_8,0.0_8)
    
        DO i = 1,twoJ+1
            JZ(i,i) = -0.5_8*twoJ + (i-1)*1.0_8
            J2(i,i) = 0.5_8*twoJ*(0.5_8*twoJ+1)
            DO k = 1,twoJ+1
                mJ = -0.5_8*twoJ + (k-1)*1.0_8
                IF (i == k+1) JP(i,k) = SQRT(0.5_8*twoJ*(0.5_8*twoJ+1)-mJ*(mJ+1.0_8))
                IF (i == k-1) JM(i,k) = SQRT(0.5_8*twoJ*(0.5_8*twoJ+1)-mJ*(mJ-1.0_8))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,StevensOperators(1, 1,:,:),StevensOperators(1, 2,:,:),StevensOperators(1, 3,:,:),StevensOperators(1, 4,:,:),StevensOperators(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,StevensOperators(2, 1,:,:),StevensOperators(2, 2,:,:),StevensOperators(2, 3,:,:),StevensOperators(2, 4,:,:),StevensOperators(2, 5,:,:),StevensOperators(2, 6,:,:),StevensOperators(2, 7,:,:),StevensOperators(2, 8,:,:),StevensOperators(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,StevensOperators(3, 1,:,:),StevensOperators(3, 2,:,:),StevensOperators(3, 3,:,:),StevensOperators(3, 4,:,:),StevensOperators(3, 5,:,:),StevensOperators(3, 6,:,:),StevensOperators(3, 7,:,:),StevensOperators(3, 8,:,:),StevensOperators(3, 9,:,:),StevensOperators(3, 10,:,:),StevensOperators(3, 11, :,:),StevensOperators(3, 12,:,:),StevensOperators(3, 13,:,:))
        
    END SUBROUTINE STEV_DP

    SUBROUTINE STEV_WITH_ANG_OPS_QP(twoJ, StevensOperators, JP, JM, J2, JZ)
        !Calculate and return stevens operators at quadruple precision
        !Also returns angular momentum operators
        IMPLICIT NONE
        INTEGER, INTENT(IN)                            :: twoJ
        INTEGER                                        :: i, k, q, j, col
        REAL(KIND = 16)                                :: mJ
        COMPLEX(KIND = 16), ALLOCATABLE, INTENT(OUT)   :: StevensOperators(:,:,:,:), JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        ALLOCATE(JP(twoJ+1,twoJ+1),JM(twoJ+1,twoJ+1),J2(twoJ+1,twoJ+1),JZ(twoJ+1,twoJ+1),StevensOperators(3, 13,twoJ+1,twoJ+1))
    
        JP               = (0.0_16,0.0_16)
        JM               = (0.0_16,0.0_16)
        J2               = (0.0_16,0.0_16)
        JZ               = (0.0_16,0.0_16)
        StevensOperators = (0.0_16,0.0_16)
    
        DO i = 1,twoJ+1
            JZ(i,i) = -0.5_16*twoJ + (i-1)*1.0_16
            J2(i,i) = 0.5_16*twoJ*(0.5_16*twoJ+1)
            DO k = 1,twoJ+1
                mJ = -0.5_16*twoJ + (k-1)*1.0_16
                IF (i == k+1) JP(i,k) = SQRT(0.5_16*twoJ*(0.5_16*twoJ+1)-mJ*(mJ+1.0_16))
                IF (i == k-1) JM(i,k) = SQRT(0.5_16*twoJ*(0.5_16*twoJ+1)-mJ*(mJ-1.0_16))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,StevensOperators(1, 1,:,:),StevensOperators(1, 2,:,:),StevensOperators(1, 3,:,:),StevensOperators(1, 4,:,:),StevensOperators(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,StevensOperators(2, 1,:,:),StevensOperators(2, 2,:,:),StevensOperators(2, 3,:,:),StevensOperators(2, 4,:,:),StevensOperators(2, 5,:,:),StevensOperators(2, 6,:,:),StevensOperators(2, 7,:,:),StevensOperators(2, 8,:,:),StevensOperators(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,StevensOperators(3, 1,:,:),StevensOperators(3, 2,:,:),StevensOperators(3, 3,:,:),StevensOperators(3, 4,:,:),StevensOperators(3, 5,:,:),StevensOperators(3, 6,:,:),StevensOperators(3, 7,:,:),StevensOperators(3, 8,:,:),StevensOperators(3, 9,:,:),StevensOperators(3, 10,:,:),StevensOperators(3, 11, :,:),StevensOperators(3, 12,:,:),StevensOperators(3, 13,:,:))
        
    END SUBROUTINE STEV_WITH_ANG_OPS_QP

    SUBROUTINE STEV_WITH_ANG_OPS_DP(twoJ, StevensOperators, JP, JM, J2, JZ)
        !Calculate and return stevens operators at double precision
        !Also returns angular momentum operators
        IMPLICIT NONE
        INTEGER, INTENT(IN)                            :: twoJ
        INTEGER                                        :: i, k, q, j, col
        REAL(KIND = 8)                                 :: mJ
        COMPLEX(KIND = 8), ALLOCATABLE, INTENT(OUT)    :: StevensOperators(:,:,:,:), JP(:,:), JM(:,:), J2(:,:), JZ(:,:)

        !Allocate arrays
        ALLOCATE(JP(twoJ+1,twoJ+1),JM(twoJ+1,twoJ+1),J2(twoJ+1,twoJ+1),JZ(twoJ+1,twoJ+1),StevensOperators(3, 13,twoJ+1,twoJ+1))
    
        JP               = (0.0_8,0.0_8)
        JM               = (0.0_8,0.0_8)
        J2               = (0.0_8,0.0_8)
        JZ               = (0.0_8,0.0_8)
        StevensOperators = (0.0_8,0.0_8)
    
        DO i = 1,twoJ+1
            JZ(i,i) = -0.5_8*twoJ + (i-1)*1.0_8
            J2(i,i) = 0.5_8*twoJ*(0.5_8*twoJ+1)
            DO k = 1,twoJ+1
                mJ = -0.5_8*twoJ + (k-1)*1.0_8
                IF (i == k+1) JP(i,k) = SQRT(0.5_8*twoJ*(0.5_8*twoJ+1)-mJ*(mJ+1.0_8))
                IF (i == k-1) JM(i,k) = SQRT(0.5_8*twoJ*(0.5_8*twoJ+1)-mJ*(mJ-1.0_8))
            END DO
        END DO
    
        CALL STEVENS2(JP,JM,J2,JZ,StevensOperators(1, 1,:,:),StevensOperators(1, 2,:,:),StevensOperators(1, 3,:,:),StevensOperators(1, 4,:,:),StevensOperators(1, 5,:,:))
        CALL STEVENS4(JP,JM,J2,JZ,StevensOperators(2, 1,:,:),StevensOperators(2, 2,:,:),StevensOperators(2, 3,:,:),StevensOperators(2, 4,:,:),StevensOperators(2, 5,:,:),StevensOperators(2, 6,:,:),StevensOperators(2, 7,:,:),StevensOperators(2, 8,:,:),StevensOperators(2, 9,:,:))
        CALL STEVENS6(JP,JM,J2,JZ,StevensOperators(3, 1,:,:),StevensOperators(3, 2,:,:),StevensOperators(3, 3,:,:),StevensOperators(3, 4,:,:),StevensOperators(3, 5,:,:),StevensOperators(3, 6,:,:),StevensOperators(3, 7,:,:),StevensOperators(3, 8,:,:),StevensOperators(3, 9,:,:),StevensOperators(3, 10,:,:),StevensOperators(3, 11, :,:),StevensOperators(3, 12,:,:),StevensOperators(3, 13,:,:))
        
    END SUBROUTINE STEV_WITH_ANG_OPS_DP

    SUBROUTINE STEVENS2_QP(JP,JM,J2,JZ,O2M2,O2M1,O20,O21,O22)
        !Creates 2nd rank Stevens operators at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)   :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 16), INTENT(OUT)  :: O2M2(:,:),O2M1(:,:),O20(:,:),O21(:,:),O22(:,:)
        COMPLEX(KIND = 16)               :: ii
        
        ii = (0.0_16, 1.0_16)

        O2m2 = ii*0.5_16*(ALT_MATMUL(JP,JP) - ALT_MATMUL(JM,JM))
        O2m1 = ii*0.25_16*(ALT_MATMUL(JZ,(JP-JM)) + ALT_MATMUL((JP-JM),JZ))
        O20 = 3.0_16*ALT_MATMUL(JZ,JZ) - J2
        O21 = 0.25_16*(ALT_MATMUL(JZ,(JP+JM)) + ALT_MATMUL((JP+JM),JZ))
        O22 = 0.5_16*(ALT_MATMUL(JP,JP) + ALT_MATMUL(JM,JM))

    END SUBROUTINE STEVENS2_QP
   
    SUBROUTINE STEVENS2_DP(JP,JM,J2,JZ,O2M2,O2M1,O20,O21,O22)
        !Creates 2nd rank Stevens operators at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)   :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 8), INTENT(OUT)  :: O2M2(:,:),O2M1(:,:),O20(:,:),O21(:,:),O22(:,:)
        COMPLEX(KIND = 8)               :: ii
        
        ii = (0.0_8, 1.0_8)

        O2m2 = ii*0.5_8*(ALT_MATMUL(JP,JP) - ALT_MATMUL(JM,JM))
        O2m1 = ii*0.25_8*(ALT_MATMUL(JZ,(JP-JM)) + ALT_MATMUL((JP-JM),JZ))
        O20 = 3.0_8*ALT_MATMUL(JZ,JZ) - J2
        O21 = 0.25_8*(ALT_MATMUL(JZ,(JP+JM)) + ALT_MATMUL((JP+JM),JZ))
        O22 = 0.5_8*(ALT_MATMUL(JP,JP) + ALT_MATMUL(JM,JM))

    END SUBROUTINE STEVENS2_DP
    
    SUBROUTINE STEVENS4_QP(JP,JM,J2,JZ,O4M4,O4M3,O4M2,O4M1,O40,O41,O42,O43,O44)
        !Creates 4th rank Stevens operators at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)   :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 16), INTENT(INOUT) :: O4M4(:,:),O4M3(:,:),O4M2(:,:),O4M1(:,:),O40(:,:),O41(:,:),O42(:,:),O43(:,:),O44(:,:)
        COMPLEX(KIND = 16)                :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                         :: J
        COMPLEX(KIND = 16)               :: ii
        
        ii = (0.0_16, 1.0_16)

        IDENT = (0.0_16,0.0_16)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_16,0.0_16)
        END DO
    
        O4m4 = 0.5_16*ii*(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)) - ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)))
        O4m3 = 0.25_16*ii*(ALT_MATMUL((ALT_MATMUL(ALT_MATMUL(JP,JP),JP)-(ALT_MATMUL(ALT_MATMUL(JM,JM),JM))),JZ)+ALT_MATMUL(JZ,((ALT_MATMUL(ALT_MATMUL(JP,JP),JP)-(ALT_MATMUL(ALT_MATMUL(JM,JM),JM))))))
        O4m2 = 0.25_16*ii*(ALT_MATMUL((7.0_16*ALT_MATMUL(JZ,JZ)-J2-5.0_16*IDENT),(ALT_MATMUL(JP,JP)-ALT_MATMUL(JM,JM)))+ALT_MATMUL((ALT_MATMUL(JP,JP)-ALT_MATMUL(JM,JM)),(7.0_16*ALT_MATMUL(JZ,JZ)-J2-5.0_16*IDENT)))
        O4m1 = 0.25_16*ii*(ALT_MATMUL((7.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ) - 3.0_16*ALT_MATMUL(J2,JZ)-JZ),(JP-JM))+ALT_MATMUL((JP-JM),(7.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ) - 3.0_16*ALT_MATMUL(J2,JZ)-JZ)))
        O40  = 35.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)) - 30.0_16*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ))+ 25.0_16*ALT_MATMUL(JZ,JZ) + 3.0_16*ALT_MATMUL(J2,J2) - 6.0_16*J2
        O41  = 0.25_16*(ALT_MATMUL((JP+JM),(7.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)-3.0_16*ALT_MATMUL(J2,JZ)-JZ))+ALT_MATMUL((7.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)-3.0_16*ALT_MATMUL(J2,JZ)-JZ),(JP+JM)))
        O42  = 0.25_16*(ALT_MATMUL((7.0_16*ALT_MATMUL(JZ,JZ)-J2-5.0_16*IDENT),(ALT_MATMUL(JP,JP)+ALT_MATMUL(JM,JM)))+ALT_MATMUL((ALT_MATMUL(JP,JP)+ALT_MATMUL(JM,JM)),(7.0_16*ALT_MATMUL(JZ,JZ)-J2-5.0_16*IDENT)))
        O43  = 0.25_16*(ALT_MATMUL((ALT_MATMUL(ALT_MATMUL(JP,JP),JP)+(ALT_MATMUL(ALT_MATMUL(JM,JM),JM))),JZ)+ALT_MATMUL(JZ,((ALT_MATMUL(ALT_MATMUL(JP,JP),JP)+(ALT_MATMUL(ALT_MATMUL(JM,JM),JM))))))
        O44  = 0.5_16*(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)) + ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)))
    
    END SUBROUTINE STEVENS4_QP
    
    SUBROUTINE STEVENS4_DP(JP,JM,J2,JZ,O4M4,O4M3,O4M2,O4M1,O40,O41,O42,O43,O44)
        !Creates 4th rank Stevens operators at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)    :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 8), INTENT(INOUT) :: O4M4(:,:),O4M3(:,:),O4M2(:,:),O4M1(:,:),O40(:,:),O41(:,:),O42(:,:),O43(:,:),O44(:,:)
        COMPLEX(KIND = 8)                :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                          :: J
        COMPLEX(KIND = 8)                :: ii
        
        ii = (0.0_8, 1.0_8)

        IDENT = (0.0_8,0.0_8)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_8,0.0_8)
        END DO
    
        O4m4 = 0.5_8*ii*(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)) - ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)))
        O4m3 = 0.25_8*ii*(ALT_MATMUL((ALT_MATMUL(ALT_MATMUL(JP,JP),JP)-(ALT_MATMUL(ALT_MATMUL(JM,JM),JM))),JZ)+ALT_MATMUL(JZ,((ALT_MATMUL(ALT_MATMUL(JP,JP),JP)-(ALT_MATMUL(ALT_MATMUL(JM,JM),JM))))))
        O4m2 = 0.25_8*ii*(ALT_MATMUL((7.0_8*ALT_MATMUL(JZ,JZ)-J2-5.0_8*IDENT),(ALT_MATMUL(JP,JP)-ALT_MATMUL(JM,JM)))+ALT_MATMUL((ALT_MATMUL(JP,JP)-ALT_MATMUL(JM,JM)),(7.0_8*ALT_MATMUL(JZ,JZ)-J2-5.0_8*IDENT)))
        O4m1 = 0.25_8*ii*(ALT_MATMUL((7.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ) - 3.0_8*ALT_MATMUL(J2,JZ)-JZ),(JP-JM))+ALT_MATMUL((JP-JM),(7.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ) - 3.0_8*ALT_MATMUL(J2,JZ)-JZ)))
        O40  = 35.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)) - 30.0_8*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ))+ 25.0_8*ALT_MATMUL(JZ,JZ) + 3.0_8*ALT_MATMUL(J2,J2) - 6.0_8*J2
        O41  = 0.25_8*(ALT_MATMUL((JP+JM),(7.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)-3.0_8*ALT_MATMUL(J2,JZ)-JZ))+ALT_MATMUL((7.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)-3.0_8*ALT_MATMUL(J2,JZ)-JZ),(JP+JM)))
        O42  = 0.25_8*(ALT_MATMUL((7.0_8*ALT_MATMUL(JZ,JZ)-J2-5.0_8*IDENT),(ALT_MATMUL(JP,JP)+ALT_MATMUL(JM,JM)))+ALT_MATMUL((ALT_MATMUL(JP,JP)+ALT_MATMUL(JM,JM)),(7.0_8*ALT_MATMUL(JZ,JZ)-J2-5.0_8*IDENT)))
        O43  = 0.25_8*(ALT_MATMUL((ALT_MATMUL(ALT_MATMUL(JP,JP),JP)+(ALT_MATMUL(ALT_MATMUL(JM,JM),JM))),JZ)+ALT_MATMUL(JZ,((ALT_MATMUL(ALT_MATMUL(JP,JP),JP)+(ALT_MATMUL(ALT_MATMUL(JM,JM),JM))))))
        O44  = 0.5_8*(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)) + ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)))
    
    END SUBROUTINE STEVENS4_DP

    SUBROUTINE STEVENS6_QP(JP,JM,J2,JZ,O6M6,O6M5,O6M4,O6M3,O6M2,O6M1,O60,O61,O62,O63,O64,O65,O66)
        !Creates 6th rank Stevens operators at quadruple precision
        IMPLICIT NONE
        COMPLEX(KIND = 16), INTENT(IN)  :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 16), INTENT(OUT) :: O6M6(:,:),O6M5(:,:),O6M4(:,:),O6M3(:,:),O6M2(:,:),O6M1(:,:),O60(:,:),O61(:,:),O62(:,:),O63(:,:),O64(:,:),O65(:,:),O66(:,:)
        COMPLEX(KIND = 16)              :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                       :: J
        COMPLEX(KIND = 16)               :: ii
        
        ii = (0.0_16, 1.0_16)
    
        IDENT = (0.0_16,0.0_16)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_16,0.0_16)
        END DO
    
        O6M6 = 0.5_16*ii*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),ALT_MATMUL(JP,JP)) - ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),ALT_MATMUL(JM,JM))) 
        O6M5 = 0.25_16*ii*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),JP) - ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),JM),JZ)+ALT_MATMUL(JZ,(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),JP) - ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),JM))))
        O6M4 = 0.25_16*ii*(ALT_MATMUL((ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP))-ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM))),(11.0_16*ALT_MATMUL(JZ,JZ)-J2-38.0_16*IDENT))+ALT_MATMUL((11.0_16*ALT_MATMUL(JZ,JZ)-J2-38.0_16*IDENT),(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP))-ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)))))
        O6M3 = 0.25_16*ii*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),JP)-ALT_MATMUL(ALT_MATMUL(JM,JM),JM),(11.0_16*ALT_MATMUL(JZ,ALT_MATMUL(JZ,JZ))-3.0_16*ALT_MATMUL(J2,JZ)-59.0_16*JZ))+ALT_MATMUL((11.0_16*ALT_MATMUL(JZ,ALT_MATMUL(JZ,JZ))-3.0_16*J2*JZ-59.0_16*JZ),(ALT_MATMUL(ALT_MATMUL(JP,JP),JP)-ALT_MATMUL(ALT_MATMUL(JM,JM),JM))))
        O6M2 = 0.25_16*ii*(ALT_MATMUL(ALT_MATMUL(JP,JP)-ALT_MATMUL(JM,JM),33.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ))-18.0_16*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ))-123.0_16*ALT_MATMUL(JZ,JZ)+ALT_MATMUL(J2,J2)+10.0_16*J2+102.0_16*IDENT)+ALT_MATMUL(33.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ))-18.0_16*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ))-123.0_16*ALT_MATMUL(JZ,JZ)+ALT_MATMUL(J2,J2)+10.0_16*J2+102.0_16*IDENT,ALT_MATMUL(JP,JP)-ALT_MATMUL(JM,JM)))
        O6M1 = 0.25_16*ii*(ALT_MATMUL((JP-JM),(33.0_16*ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)),JZ)-30.0_16*ALT_MATMUL(J2,ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ))+15.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)+5.0_16*ALT_MATMUL(ALT_MATMUL(J2,J2),JZ)-10.0_16*ALT_MATMUL(J2,JZ)+12.0_16*JZ))+ALT_MATMUL((33.0_16*ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)),JZ)-30.0_16*ALT_MATMUL(J2,ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ))+15.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)+5.0_16*ALT_MATMUL(ALT_MATMUL(J2,J2),JZ)-10.0_16*ALT_MATMUL(J2,JZ)+12.0_16*JZ),(JP-JM)))
        O60 = 231.0_16*ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)),ALT_MATMUL(JZ,JZ)) - 315.0_16*J2*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)) + 735.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)) + 105.0_16*ALT_MATMUL(ALT_MATMUL(J2,J2),ALT_MATMUL(JZ,JZ)) - 525.0_16*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ)) + 294.0_16*ALT_MATMUL(JZ,JZ) - 5.0_16*ALT_MATMUL(ALT_MATMUL(J2,J2),J2) + 40.0_16*ALT_MATMUL(J2,J2) - 60.0_16*J2
        O61 = 0.25_16*(ALT_MATMUL((JP+JM),(33.0_16*ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)),JZ)-30.0_16*ALT_MATMUL(J2,ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ))+15.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)+5.0_16*ALT_MATMUL(ALT_MATMUL(J2,J2),JZ)-10.0_16*ALT_MATMUL(J2,JZ)+12.0_16*JZ))+ALT_MATMUL((33.0_16*ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)),JZ)-30.0_16*ALT_MATMUL(J2,ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ))+15.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)+5.0_16*ALT_MATMUL(ALT_MATMUL(J2,J2),JZ)-10.0_16*ALT_MATMUL(J2,JZ)+12.0_16*JZ),(JP+JM)))
        O62 = 0.25_16*(ALT_MATMUL(ALT_MATMUL(JP,JP)+ALT_MATMUL(JM,JM),33.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ))-18.0_16*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ))-123.0_16*ALT_MATMUL(JZ,JZ)+ALT_MATMUL(J2,J2)+10.0_16*J2+102.0_16*IDENT)+ALT_MATMUL(33.0_16*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ))-18.0_16*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ))-123.0_16*ALT_MATMUL(JZ,JZ)+ALT_MATMUL(J2,J2)+10.0_16*J2+102.0_16*IDENT,ALT_MATMUL(JP,JP)+ALT_MATMUL(JM,JM)))
        O63 = 0.25_16*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),JP)+ALT_MATMUL(ALT_MATMUL(JM,JM),JM),(11.0_16*ALT_MATMUL(JZ,ALT_MATMUL(JZ,JZ))-3.0_16*ALT_MATMUL(J2,JZ)-59.0_16*JZ))+ALT_MATMUL((11.0_16*ALT_MATMUL(JZ,ALT_MATMUL(JZ,JZ))-3.0_16*J2*JZ-59.0_16*JZ),(ALT_MATMUL(ALT_MATMUL(JP,JP),JP)+ALT_MATMUL(ALT_MATMUL(JM,JM),JM))))
        O64 = 0.25_16*(ALT_MATMUL((ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP))+ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM))),(11.0_16*ALT_MATMUL(JZ,JZ)-J2-38.0_16*IDENT))+ALT_MATMUL((11.0_16*ALT_MATMUL(JZ,JZ)-J2-38.0_16*IDENT),(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP))+ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)))))
        O65 = 0.25_16*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),JP) + ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),JM),JZ)+ALT_MATMUL(JZ,(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),JP) + ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),JM))))
        O66 = 0.5_16*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),ALT_MATMUL(JP,JP)) + ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),ALT_MATMUL(JM,JM))) 
    
    
    END SUBROUTINE STEVENS6_QP

    SUBROUTINE STEVENS6_DP(JP,JM,J2,JZ,O6M6,O6M5,O6M4,O6M3,O6M2,O6M1,O60,O61,O62,O63,O64,O65,O66)
        !Creates 6th rank Stevens operators at double precision
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)  :: JP(:,:),JM(:,:),J2(:,:),JZ(:,:)
        COMPLEX(KIND = 8), INTENT(OUT) :: O6M6(:,:),O6M5(:,:),O6M4(:,:),O6M3(:,:),O6M2(:,:),O6M1(:,:),O60(:,:),O61(:,:),O62(:,:),O63(:,:),O64(:,:),O65(:,:),O66(:,:)
        COMPLEX(KIND = 8)              :: IDENT(SIZE(JP,1),SIZE(JP,1))
        INTEGER                       :: J
        COMPLEX(KIND = 8)               :: ii
        
        ii = (0.0_8, 1.0_8)
    
        IDENT = (0.0_8,0.0_8)
    
        DO J=1,SIZE(JP,1)
            IDENT(J,J) = (1.0_8,0.0_8)
        END DO
    
        O6M6 = 0.5_8*ii*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),ALT_MATMUL(JP,JP)) - ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),ALT_MATMUL(JM,JM))) 
        O6M5 = 0.25_8*ii*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),JP) - ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),JM),JZ)+ALT_MATMUL(JZ,(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),JP) - ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),JM))))
        O6M4 = 0.25_8*ii*(ALT_MATMUL((ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP))-ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM))),(11.0_8*ALT_MATMUL(JZ,JZ)-J2-38.0_8*IDENT))+ALT_MATMUL((11.0_8*ALT_MATMUL(JZ,JZ)-J2-38.0_8*IDENT),(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP))-ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)))))
        O6M3 = 0.25_8*ii*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),JP)-ALT_MATMUL(ALT_MATMUL(JM,JM),JM),(11.0_8*ALT_MATMUL(JZ,ALT_MATMUL(JZ,JZ))-3.0_8*ALT_MATMUL(J2,JZ)-59.0_8*JZ))+ALT_MATMUL((11.0_8*ALT_MATMUL(JZ,ALT_MATMUL(JZ,JZ))-3.0_8*J2*JZ-59.0_8*JZ),(ALT_MATMUL(ALT_MATMUL(JP,JP),JP)-ALT_MATMUL(ALT_MATMUL(JM,JM),JM))))
        O6M2 = 0.25_8*ii*(ALT_MATMUL(ALT_MATMUL(JP,JP)-ALT_MATMUL(JM,JM),33.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ))-18.0_8*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ))-123.0_8*ALT_MATMUL(JZ,JZ)+ALT_MATMUL(J2,J2)+10.0_8*J2+102.0_8*IDENT)+ALT_MATMUL(33.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ))-18.0_8*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ))-123.0_8*ALT_MATMUL(JZ,JZ)+ALT_MATMUL(J2,J2)+10.0_8*J2+102.0_8*IDENT,ALT_MATMUL(JP,JP)-ALT_MATMUL(JM,JM)))
        O6M1 = 0.25_8*ii*(ALT_MATMUL((JP-JM),(33.0_8*ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)),JZ)-30.0_8*ALT_MATMUL(J2,ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ))+15.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)+5.0_8*ALT_MATMUL(ALT_MATMUL(J2,J2),JZ)-10.0_8*ALT_MATMUL(J2,JZ)+12.0_8*JZ))+ALT_MATMUL((33.0_8*ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)),JZ)-30.0_8*ALT_MATMUL(J2,ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ))+15.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)+5.0_8*ALT_MATMUL(ALT_MATMUL(J2,J2),JZ)-10.0_8*ALT_MATMUL(J2,JZ)+12.0_8*JZ),(JP-JM)))
        O60 = 231.0_8*ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)),ALT_MATMUL(JZ,JZ)) - 315.0_8*J2*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)) + 735.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)) + 105.0_8*ALT_MATMUL(ALT_MATMUL(J2,J2),ALT_MATMUL(JZ,JZ)) - 525.0_8*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ)) + 294.0_8*ALT_MATMUL(JZ,JZ) - 5.0_8*ALT_MATMUL(ALT_MATMUL(J2,J2),J2) + 40.0_8*ALT_MATMUL(J2,J2) - 60.0_8*J2
        O61 = 0.25_8*(ALT_MATMUL((JP+JM),(33.0_8*ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)),JZ)-30.0_8*ALT_MATMUL(J2,ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ))+15.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)+5.0_8*ALT_MATMUL(ALT_MATMUL(J2,J2),JZ)-10.0_8*ALT_MATMUL(J2,JZ)+12.0_8*JZ))+ALT_MATMUL((33.0_8*ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ)),JZ)-30.0_8*ALT_MATMUL(J2,ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ))+15.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),JZ)+5.0_8*ALT_MATMUL(ALT_MATMUL(J2,J2),JZ)-10.0_8*ALT_MATMUL(J2,JZ)+12.0_8*JZ),(JP+JM)))
        O62 = 0.25_8*(ALT_MATMUL(ALT_MATMUL(JP,JP)+ALT_MATMUL(JM,JM),33.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ))-18.0_8*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ))-123.0_8*ALT_MATMUL(JZ,JZ)+ALT_MATMUL(J2,J2)+10.0_8*J2+102.0_8*IDENT)+ALT_MATMUL(33.0_8*ALT_MATMUL(ALT_MATMUL(JZ,JZ),ALT_MATMUL(JZ,JZ))-18.0_8*ALT_MATMUL(J2,ALT_MATMUL(JZ,JZ))-123.0_8*ALT_MATMUL(JZ,JZ)+ALT_MATMUL(J2,J2)+10.0_8*J2+102.0_8*IDENT,ALT_MATMUL(JP,JP)+ALT_MATMUL(JM,JM)))
        O63 = 0.25_8*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),JP)+ALT_MATMUL(ALT_MATMUL(JM,JM),JM),(11.0_8*ALT_MATMUL(JZ,ALT_MATMUL(JZ,JZ))-3.0_8*ALT_MATMUL(J2,JZ)-59.0_8*JZ))+ALT_MATMUL((11.0_8*ALT_MATMUL(JZ,ALT_MATMUL(JZ,JZ))-3.0_8*J2*JZ-59.0_8*JZ),(ALT_MATMUL(ALT_MATMUL(JP,JP),JP)+ALT_MATMUL(ALT_MATMUL(JM,JM),JM))))
        O64 = 0.25_8*(ALT_MATMUL((ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP))+ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM))),(11.0_8*ALT_MATMUL(JZ,JZ)-J2-38.0_8*IDENT))+ALT_MATMUL((11.0_8*ALT_MATMUL(JZ,JZ)-J2-38.0_8*IDENT),(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP))+ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)))))
        O65 = 0.25_8*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),JP) + ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),JM),JZ)+ALT_MATMUL(JZ,(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),JP) + ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),JM))))
        O66 = 0.5_8*(ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JP,JP),ALT_MATMUL(JP,JP)),ALT_MATMUL(JP,JP)) + ALT_MATMUL(ALT_MATMUL(ALT_MATMUL(JM,JM),ALT_MATMUL(JM,JM)),ALT_MATMUL(JM,JM))) 
    
    
    END SUBROUTINE STEVENS6_DP

END MODULE STEV_OPS
